window.onscroll = function() {myFunction()};

var navbar = document.getElementById("mainNav");
var sticky = navbar.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("fixed-top");
    document.getElementById("about").style.marginTop = "56px";
  } else {
    navbar.classList.remove("fixed-top");
    document.getElementById("about").style.marginTop = "0px";
  }
}
