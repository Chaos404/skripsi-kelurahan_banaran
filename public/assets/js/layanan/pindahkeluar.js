$(document).ready(function() {
    var buttonAdd = $("#add-button");
    var buttonRemove = $("#remove-button");
    var className = ".dynamic-field";
    var count = 0;
    var field = "";
    var maxFields = 7;

function totalFields() {
    return $(className).length;
}

function addNewField() {
    count = totalFields() + 1;
    field = $("#dynamic-field-1").clone();
    field.attr("id", "dynamic-field-" + count);
    field.children("label").text("Field " + count);
    field.find("input").val("");
    $(className + ":last").after($(field));
}

function removeLastField() {
    if (totalFields() > 1) {
    $(className + ":last").remove();
    }
}

function enableButtonRemove() {
    if (totalFields() === 2) {
    buttonRemove.removeAttr("disabled");
    buttonRemove.addClass("shadow-sm");
    }
}

function disableButtonRemove() {
    if (totalFields() === 1) {
    buttonRemove.attr("disabled", "disabled");
    buttonRemove.removeClass("shadow-sm");
    }
}

function disableButtonAdd() {
    if (totalFields() === maxFields) {
    buttonAdd.attr("disabled", "disabled");
    buttonAdd.removeClass("shadow-sm");
    }
}

function enableButtonAdd() {
    if (totalFields() === (maxFields - 1)) {
    buttonAdd.removeAttr("disabled");
    buttonAdd.addClass("shadow-sm");
    }
}

buttonAdd.click(function() {
    addNewField();
    enableButtonRemove();
    disableButtonAdd();
});

buttonRemove.click(function() {
    removeLastField();
    disableButtonRemove();
    enableButtonAdd();
    });
});

$("#seeAnotherField").change(function () {
    if ($(this).val() == "Lainnya") {
        $('#otherFieldDiv').show();
        $('#otherField').attr('required', '');
        $('#otherField').attr('data-error', 'This field is required.');
    } else {
        $('#otherFieldDiv').hide();
        $('#otherField').removeAttr('required');
        $('#otherField').removeAttr('data-error');
    }
});
$("#seeAnotherField").trigger("change");

// Datepicker
$('#datepicker').datepicker({
    uiLibrary: 'bootstrap4',
    format: 'yyyy-mm-dd',
});

$('#datepicker1').datepicker({
    uiLibrary: 'bootstrap4',
    format: 'yyyy-mm-dd',
});

$('#datepicker2').datepicker({
    uiLibrary: 'bootstrap4',
    format: 'yyyy-mm-dd',
});

$('#datepicker3').datepicker({
    uiLibrary: 'bootstrap4',
    format: 'yyyy-mm-dd',
});

$('#datepicker4').datepicker({
    uiLibrary: 'bootstrap4',
    format: 'yyyy-mm-dd',
});

$('#datepicker5').datepicker({
    uiLibrary: 'bootstrap4',
    format: 'yyyy-mm-dd',
});

$('#datepicker6').datepicker({
    uiLibrary: 'bootstrap4',
    format: 'yyyy-mm-dd',
});

// Timepicker
$('#timepicker').timepicker({
    uiLibrary: 'bootstrap'
});

// Preview Image
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readURL1(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah1')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readURL2(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah2')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readURL3(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah3')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readURL4(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah4')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readURL5(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah5')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readURL6(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah6')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readURL7(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah7')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function () {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

// ASAL
$(document).ready(function () {
    $('select[name="dukuh_asal"]').on('change', function () {
        $('select[name="carirtAsal"]').on('change', function () {
            var nomorRt = $("#rtAsal option:selected").attr("namartAsal");
            $("#rtnyaAsal").val(nomorRt);
        });

        let idRt = $(this).val();
        if (idRt) {
            jQuery.ajax({
                url: '/pelayanan/akta-kelahiran/get_RT/' + idRt,
                type: "GET",
                dataType: "json",
                success: function (response) {
                    $('select[name="carirtAsal"]').empty();
                    $('select[name="carirtAsal"]').append(
                        '<option value="">-- Pilih RT --</option>');
                    $.each(response, function (key, value) {
                        $('select[name="carirtAsal"]').append(
                            '<option value="' + key + '"namartAsal="' +
                            value.rt + '">' + value.rt + '</option>');
                    });
                },
            });
        } else {
            $('select[name="carirtAsal"]').empty().append(
                '<option value="">-- Pilih RT --</option>');
        }
    });
});

{/* Reset */}
$(document).ready(function () {
    $('#dukuhnya_asal').click(function () {
        $('#rtAsal').prop('selectedIndex', "");
        $('#rtnyaAsal').prop('selectedIndex', 0).empty();
        $('#rtnyaAsal').val("");
    })
});
