$('#datepicker').datepicker({
    uiLibrary: 'bootstrap4',
    format: 'yyyy-mm-dd',
});

$('#datepicker1').datepicker({
    uiLibrary: 'bootstrap4',
    format: 'yyyy-mm-dd',
});

$('#datepicker2').datepicker({
    uiLibrary: 'bootstrap4',
    format: 'yyyy-mm-dd',
});

$('#datepicker3').datepicker({
    uiLibrary: 'bootstrap4',
    format: 'yyyy-mm-dd',
});

$('#datepicker4').datepicker({
    uiLibrary: 'bootstrap4',
    format: 'yyyy-mm-dd',
});

$('#datepicker5').datepicker({
    uiLibrary: 'bootstrap4',
    format: 'yyyy-mm-dd',
});

$('#datepicker6').datepicker({
    uiLibrary: 'bootstrap4',
    format: 'yyyy-mm-dd',
});

$('#timepicker').timepicker({
    uiLibrary: 'bootstrap'
});

// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readURL1(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah1')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readURL2(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah2')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readURL3(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah3')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readURL4(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah4')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readURL5(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah5')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readURL6(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah6')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readURL7(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah7')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

$(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

$(document).ready(function () {
    $('select[name="dukuh_orangtua"]').on('change', function () {
        $('select[name="carirt"]').on('change', function () {
            var nomorRt = $("#rt option:selected").attr("namaRt");
            $("#rtnya").val(nomorRt);
        });

        let idRt = $(this).val();
        if (idRt) {
            jQuery.ajax({
                url: '/pelayanan/akta-kelahiran/get_RT/' + idRt,
                type: "GET",
                dataType: "json",
                success: function (response) {
                    $('select[name="carirt"]').empty();
                    $('select[name="carirt"]').append('<option value="">-- Pilih RT --</option>');
                    $.each(response, function (key, value) {
                        $('select[name="carirt"]').append('<option value="' + key + '"namaRt="' + value.rt + '">' + value.rt + '</option>');
                    });
                },
            });
        } else {
            $('select[name="carirt"]').empty().append('<option value="">-- Pilih RT --</option>');
        }
    });
});


$(document).ready(function () {
    $('select[name="dukuh_saksi1"]').on('change', function () {
        $('select[name="carirtSaksi1"]').on('change', function () {
            var nomorRt = $("#rtSaksi1 option:selected").attr("namartSaksi1");
            $("#rtnyaSaksi1").val(nomorRt);
        });

        let idRt = $(this).val();
        if (idRt) {
            jQuery.ajax({
                url: '/pelayanan/akta-kelahiran/get_RT/' + idRt,
                type: "GET",
                dataType: "json",
                success: function (response) {
                    $('select[name="carirtSaksi1"]').empty();
                    $('select[name="carirtSaksi1"]').append('<option value="">-- Pilih RT --</option>');
                    $.each(response, function (key, value) {
                        $('select[name="carirtSaksi1"]').append('<option value="' + key + '"namartSaksi1="' + value.rt + '">' + value.rt + '</option>');
                    });
                },
            });
        } else {
            $('select[name="carirtSaksi1"]').empty().append('<option value="">-- Pilih RT --</option>');
        }
    });
});



$(document).ready(function () {
    $('select[name="dukuh_saksi2"]').on('change', function () {
        $('select[name="carirtSaksi2"]').on('change', function () {
            var nomorRt = $("#rtSaksi2 option:selected").attr("namartSaksi2");
            $("#rtnyaSaksi2").val(nomorRt);
        });

        let idRt = $(this).val();
        if (idRt) {
            jQuery.ajax({
                url: '/pelayanan/akta-kelahiran/get_RT/' + idRt,
                type: "GET",
                dataType: "json",
                success: function (response) {
                    $('select[name="carirtSaksi2"]').empty();
                    $('select[name="carirtSaksi2"]').append('<option value="">-- Pilih RT --</option>');
                    $.each(response, function (key, value) {
                        $('select[name="carirtSaksi2"]').append('<option value="' + key + '"namartSaksi2="' + value.rt + '">' + value.rt + '</option>');
                    });
                },
            });
        } else {
            $('select[name="carirtSaksi2"]').empty().append('<option value="">-- Pilih RT --</option>');
        }
    });
});

$(document).ready(function () {
    $('select[name="dukuh_jenazah"]').on('change', function () {
        $('select[name="carirtJenazah"]').on('change', function () {
            var nomorRt = $("#rtJenazah option:selected").attr("namartJenazah");
            $("#rtnyaJenazah").val(nomorRt);
        });

        let idRt = $(this).val();
        if (idRt) {
            jQuery.ajax({
                url: '/pelayanan/akta-kelahiran/get_RT/' + idRt,
                type: "GET",
                dataType: "json",
                success: function (response) {
                    $('select[name="carirtJenazah"]').empty();
                    $('select[name="carirtJenazah"]').append('<option value="">-- Pilih RT --</option>');
                    $.each(response, function (key, value) {
                        $('select[name="carirtJenazah"]').append('<option value="' + key + '"namartJenazah="' + value.rt + '">' + value.rt + '</option>');
                    });
                },
            });
        } else {
            $('select[name="carirtJenazah"]').empty().append('<option value="">-- Pilih RT --</option>');
        }
    });
});

$(document).ready(function(){
    $('#dukuhnya').click(function(){
        $('#rt').prop('selectedIndex',"");
        $('#rtnya').prop('selectedIndex',0).empty();
        $('#rtnya').val("");
    })
});

$(document).ready(function(){
    $('#dukuhnya_saksi1').click(function(){
        $('#rtSaksi1').prop('selectedIndex',"");
        $('#rtnyaSaksi1').prop('selectedIndex',0).empty();
        $('#rtnyaSaksi1').val("");
    })
});

$(document).ready(function(){
    $('#dukuhnya_saksi2').click(function(){
        $('#rtSaksi2').prop('selectedIndex',"");
        $('#rtnyaSaksi2').prop('selectedIndex',0).empty();
        $('#rtnyaSaksi2').val("");
    })
});

$(document).ready(function(){
    $('#dukuhnya_jenazah').click(function(){
        $('#rtJenazah').prop('selectedIndex',"");
        $('#rtnyaJenazah').prop('selectedIndex',0).empty();
        $('#rtnyaJenazah').val("");
    })
});

// PINDAH KELUAR
$(document).ready(function() {
    var buttonAdd = $("#add-button");
    var buttonRemove = $("#remove-button");
    var className = ".dynamic-field";
    var count = 0;
    var field = "";
    var maxFields = 7;

function totalFields() {
    return $(className).length;
}

function addNewField() {
    count = totalFields() + 1;
    field = $("#dynamic-field-1").clone();
    field.attr("id", "dynamic-field-" + count);
    field.children("label").text("Field " + count);
    field.find("input").val("");
    $(className + ":last").after($(field));
}

function removeLastField() {
    if (totalFields() > 1) {
    $(className + ":last").remove();
    }
}

function enableButtonRemove() {
    if (totalFields() === 2) {
    buttonRemove.removeAttr("disabled");
    buttonRemove.addClass("shadow-sm");
    }
}

function disableButtonRemove() {
    if (totalFields() === 1) {
    buttonRemove.attr("disabled", "disabled");
    buttonRemove.removeClass("shadow-sm");
    }
}

function disableButtonAdd() {
    if (totalFields() === maxFields) {
    buttonAdd.attr("disabled", "disabled");
    buttonAdd.removeClass("shadow-sm");
    }
}

function enableButtonAdd() {
    if (totalFields() === (maxFields - 1)) {
    buttonAdd.removeAttr("disabled");
    buttonAdd.addClass("shadow-sm");
    }
}

buttonAdd.click(function() {
    addNewField();
    enableButtonRemove();
    disableButtonAdd();
});

buttonRemove.click(function() {
    removeLastField();
    disableButtonRemove();
    enableButtonAdd();
    });
});

$("#seeAnotherField").change(function () {
    if ($(this).val() == "Lainnya") {
        $('#otherFieldDiv').show();
        $('#otherField').attr('required', '');
        $('#otherField').attr('data-error', 'This field is required.');
    } else {
        $('#otherFieldDiv').hide();
        $('#otherField').removeAttr('required');
        $('#otherField').removeAttr('data-error');
    }
});
$("#seeAnotherField").trigger("change");

// ASAL
$(document).ready(function () {
    $('select[name="dukuh_asal"]').on('change', function () {
        $('select[name="carirtAsal"]').on('change', function () {
            var nomorRt = $("#rtAsal option:selected").attr("namartAsal");
            $("#rtnyaAsal").val(nomorRt);
        });

        let idRt = $(this).val();
        if (idRt) {
            jQuery.ajax({
                url: '/pelayanan/akta-kelahiran/get_RT/' + idRt,
                type: "GET",
                dataType: "json",
                success: function (response) {
                    $('select[name="carirtAsal"]').empty();
                    $('select[name="carirtAsal"]').append(
                        '<option value="">-- Pilih RT --</option>');
                    $.each(response, function (key, value) {
                        $('select[name="carirtAsal"]').append(
                            '<option value="' + key + '"namartAsal="' +
                            value.rt + '">' + value.rt + '</option>');
                    });
                },
            });
        } else {
            $('select[name="carirtAsal"]').empty().append(
                '<option value="">-- Pilih RT --</option>');
        }
    });
});

$(document).ready(function () {
    $('#dukuhnya_asal').click(function () {
        $('#rtAsal').prop('selectedIndex', "");
        $('#rtnyaAsal').prop('selectedIndex', 0).empty();
        $('#rtnyaAsal').val("");
    })
});

// PINDAH DATANG
$(document).ready(function() {
    var buttonAdd = $("#add-buttondatang");
    var buttonRemove = $("#remove-buttondatang");
    var className = ".dynamic-field";
    var count = 0;
    var field = "";
    var maxFields = 7;

function totalFields() {
    return $(className).length;
}

function addNewField() {
    count = totalFields() + 1;
    field = $("#dynamic-field-1").clone();
    field.attr("id", "dynamic-field-" + count);
    field.children("label").text("Field " + count);
    field.find("input").val("");
    $(className + ":last").after($(field));
}

function removeLastField() {
    if (totalFields() > 1) {
    $(className + ":last").remove();
    }
}

function enableButtonRemove() {
    if (totalFields() === 2) {
    buttonRemove.removeAttr("disabled");
    buttonRemove.addClass("shadow-sm");
    }
}

function disableButtonRemove() {
    if (totalFields() === 1) {
    buttonRemove.attr("disabled", "disabled");
    buttonRemove.removeClass("shadow-sm");
    }
}

function disableButtonAdd() {
    if (totalFields() === maxFields) {
    buttonAdd.attr("disabled", "disabled");
    buttonAdd.removeClass("shadow-sm");
    }
}

function enableButtonAdd() {
    if (totalFields() === (maxFields - 1)) {
    buttonAdd.removeAttr("disabled");
    buttonAdd.addClass("shadow-sm");
    }
}

buttonAdd.click(function() {
    addNewField();
    enableButtonRemove();
    disableButtonAdd();
});

buttonRemove.click(function() {
    removeLastField();
    disableButtonRemove();
    enableButtonAdd();
    });
});

$('#otherFieldDiv2').hide();
$("#statuskk").change(function () {
    if ($(this).val() == "Membuat KK Baru") {
        $('#otherFieldDiv1').hide();
        $('#otherFieldDiv2').show();
    } else {
        $('#otherFieldDiv2').hide();
        $('#otherFieldDiv1').show();
    }
});

$(document).ready(function () {
    $('select[name="dukuh_asal"]').on('change', function () {
        $('select[name="carirtAsal"]').on('change', function () {
            var nomorRt = $("#rtAsal option:selected").attr("namartAsal");
            $("#rtnyaAsal").val(nomorRt);
        });

        let idRt = $(this).val();
        if (idRt) {
            jQuery.ajax({
                url: '/pelayanan/akta-kelahiran/get_RT/' + idRt,
                type: "GET",
                dataType: "json",
                success: function (response) {
                    $('select[name="carirtAsal"]').empty();
                    $('select[name="carirtAsal"]').append(
                        '<option value="">-- Pilih RT --</option>');
                    $.each(response, function (key, value) {
                        $('select[name="carirtAsal"]').append(
                            '<option value="' + key + '"namartAsal="' +
                            value.rt + '">' + value.rt + '</option>');
                    });
                },
            });
        } else {
            $('select[name="carirtAsal"]').empty().append(
                '<option value="">-- Pilih RT --</option>');
        }
    });
});

$(document).ready(function () {
    $('#dukuhnya_asal').click(function () {
        $('#rtAsal').prop('selectedIndex', "");
        $('#rtnyaAsal').prop('selectedIndex', 0).empty();
        $('#rtnyaAsal').val("");
    })
});

