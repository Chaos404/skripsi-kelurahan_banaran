
$('#datepicker').datepicker({
    uiLibrary: 'bootstrap4',
    format: 'yyyy-mm-dd',
});

$('#datepicker2').datepicker({
    uiLibrary: 'bootstrap4',
    format: 'yyyy-mm-dd',
});

$('#datepicker3').datepicker({
    uiLibrary: 'bootstrap4',
    format: 'yyyy-mm-dd',
});

$('#datepicker4').datepicker({
    uiLibrary: 'bootstrap4',
    format: 'yyyy-mm-dd',
});

$('#datepicker5').datepicker({
    uiLibrary: 'bootstrap4',
    format: 'yyyy-mm-dd',
});

$('#datepicker6').datepicker({
    uiLibrary: 'bootstrap4',
    format: 'yyyy-mm-dd',
});

$('#timepicker').timepicker({
    uiLibrary: 'bootstrap'
});

// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readURL1(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah1')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readURL2(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah2')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readURL3(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah3')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readURL4(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah4')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readURL5(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah5')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readURL6(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah6')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readURL7(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah7')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readURL8(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah8')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

// ORTU
$(document).ready(function () {
    $('select[name="dukuh_orangtua"]').on('change', function () {
        $('select[name="carirt"]').on('change', function () {
            var nomorRt = $("#rt option:selected").attr("namaRt");
            $("#rtnya").val(nomorRt);
        });

        let idRt = $(this).val();
        if (idRt) {
            jQuery.ajax({
                url: '/pelayanan/akta-kelahiran/get_RT/' + idRt,
                type: "GET",
                dataType: "json",
                success: function (response) {
                    $('select[name="carirt"]').empty();
                    $('select[name="carirt"]').append('<option value="">-- Pilih RT --</option>');
                    $.each(response, function (key, value) {
                        $('select[name="carirt"]').append('<option value="' + key + '"namaRt="' + value.rt + '">' + value.rt + '</option>');
                    });
                },
            });
        } else {
            $('select[name="carirt"]').empty().append('<option value="">-- Pilih RT --</option>');
        }
    });
});


// SAKSI I
$(document).ready(function () {
    $('select[name="dukuh_saksi1"]').on('change', function () {
        $('select[name="carirtSaksi1"]').on('change', function () {
            var nomorRt = $("#rtSaksi1 option:selected").attr("namartSaksi1");
            $("#rtnyaSaksi1").val(nomorRt);
        });

        let idRt = $(this).val();
        if (idRt) {
            jQuery.ajax({
                url: '/pelayanan/akta-kelahiran/get_RT/' + idRt,
                type: "GET",
                dataType: "json",
                success: function (response) {
                    $('select[name="carirtSaksi1"]').empty();
                    $('select[name="carirtSaksi1"]').append('<option value="">-- Pilih RT --</option>');
                    $.each(response, function (key, value) {
                        $('select[name="carirtSaksi1"]').append('<option value="' + key + '"namartSaksi1="' + value.rt + '">' + value.rt + '</option>');
                    });
                },
            });
        } else {
            $('select[name="carirtSaksi1"]').empty().append('<option value="">-- Pilih RT --</option>');
        }
    });
});


// SAKSI II
$(document).ready(function () {
    $('select[name="dukuh_saksi2"]').on('change', function () {
        $('select[name="carirtSaksi2"]').on('change', function () {
            var nomorRt = $("#rtSaksi2 option:selected").attr("namartSaksi2");
            $("#rtnyaSaksi2").val(nomorRt);
        });

        let idRt = $(this).val();
        if (idRt) {
            jQuery.ajax({
                url: '/pelayanan/akta-kelahiran/get_RT/' + idRt,
                type: "GET",
                dataType: "json",
                success: function (response) {
                    $('select[name="carirtSaksi2"]').empty();
                    $('select[name="carirtSaksi2"]').append('<option value="">-- Pilih RT --</option>');
                    $.each(response, function (key, value) {
                        $('select[name="carirtSaksi2"]').append('<option value="' + key + '"namartSaksi2="' + value.rt + '">' + value.rt + '</option>');
                    });
                },
            });
        } else {
            $('select[name="carirtSaksi2"]').empty().append('<option value="">-- Pilih RT --</option>');
        }
    });
});

$(document).ready(function(){
    $('#dukuhnya').click(function(){
        $('#rt').prop('selectedIndex',"");
        $('#rtnya').prop('selectedIndex',0).empty();
        $('#rtnya').val("");
    })
});

$(document).ready(function(){
    $('#dukuhnya_saksi1').click(function(){
        $('#rtSaksi1').prop('selectedIndex',"");
        $('#rtnyaSaksi1').prop('selectedIndex',0).empty();
        $('#rtnyaSaksi1').val("");
    })
});

$(document).ready(function(){
    $('#dukuhnya_saksi2').click(function(){
        $('#rtSaksi2').prop('selectedIndex',"");
        $('#rtnyaSaksi2').prop('selectedIndex',0).empty();
        $('#rtnyaSaksi2').val("");
    })
});
