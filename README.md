<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

## Sistem Informasi Pelayanan Administrasi Kependudukan Berbasis Website Kelurahan Banaran

## Laravel 6.*
Laravel adalah kerangka aplikasi web dengan sintaks yang ekspresif dan elegan. Kami percaya bahwa pengembangan harus menjadi pengalaman kreatif yang menyenangkan agar benar-benar memuaskan. Laravel mencoba menghilangkan kesulitan dalam pengembangan dengan mengurangi tugas-tugas umum yang digunakan di sebagian besar proyek web, seperti otentikasi, perutean, sesi, antrian, dan caching. Laravel dapat diakses, namun kuat, menyediakan alat yang dibutuhkan untuk aplikasi yang besar dan kuat. Pembalikan wadah kontrol yang luar biasa, sistem migrasi ekspresif, dan dukungan pengujian unit yang terintegrasi erat memberi Anda alat yang Anda butuhkan untuk membangun aplikasi apa pun yang Anda tugaskan.

# Persyaratan
## Persyaratan Laravel
Sebelum mencoba menerapkan aplikasi Laravel di environment lokal, shared hosting atau vps, Pastikan bahwa layanan yang akan digunakan menyediakan [persyaratan yang sesuai untuk Laravel] (https://laravel.com/docs/6.x#server-requirements). Pada nyata, item berikut diperlukan untuk Laravel 6.*:

* PHP >= 7.2.5
* BCMath PHP Extension
* Ctype PHP Extension
* Fileinfo PHP Extension
* JSON PHP Extension
* Mbstring PHP Extension
* OpenSSL PHP Extension
* PDO PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension

# Pengembangan pada Lokal

## Step 1

### Download ZIP
- Download di https://gitlab.com/Chaos404/skripsi-kelurahan_banaran/-/tree/master/
- Extract file ZIP
- Buka terminal
- Arahkan kedirektori project (Hasil extract)

### Menggunakan GIT
Clone git repository

Menggunakan Git SSH
```
git clone git@gitlab.com:Chaos404/skripsi-kelurahan_banaran.git
```

Atau dengan HTTPS
```
git clone https://gitlab.com/Chaos404/skripsi-kelurahan_banaran.git
```

Masuk ke folder projek laravel dengan perintah
```
cd laravel
```

## Step 2
Lalu lakukan install laravel kedalam project  
```
composer install
```

Lalu lakukan update composer  
```
composer update
```

## Step 2
Salin file ```.env.example``` pada direktori yang sama dengan nama ```.env```

Buka file ```.env.example```
```
copy seluruh isinya
```

Buat file dengan nama ```.env```
```
paste semua kedalam file .env kemudian Save
```

## Step 4 
Buka fitur **MySQL® Databases** yang ada pada fitur phpmyadmin XAMPP.

Import database dengan file yang berada **Folder laravel/export_DB** .

Sekarang buka file ".env`` perbarui *field-field* berikut dengan informasi database baru; 
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=NAMA_DATABASE
DB_USERNAME=root
DB_PASSWORD=
```

Lakukan perintah dibawah untuk menggenerate key unik laravel

```
php artisan key:generate
```

## Step 3
Untuk menjalankan project Laravel

Ketikan perintah berikut
```
php artisan serv
```

