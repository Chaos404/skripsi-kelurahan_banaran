<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Dashboard Admin
Route::group(['middleware' => 'role:admin'], function () {  
    Route::get('/', function () {
        return view('landingpage.home');
    })->name('home');

    Route::prefix('/dashboard-admin')->group(function () {
        Route::get('/', 'DashboardAdmin\DashboardController@index')->name('dashboardAdmin');

        // Manage User
        Route::get('/manage-user', 'DashboardAdmin\Manageuser\RegistrasiController@index');
        Route::post('/manage-user/update/{id}', 'DashboardAdmin\Manageuser\RegistrasiController@update');
        Route::post('/manage-user/create', 'DashboardAdmin\Manageuser\RegistrasiController@create');
        Route::get('/manage-user/delete/{id}', 'DashboardAdmin\Manageuser\RegistrasiController@destroy');


        // Homepage Banner Utama
        Route::get('/homepage/banner-utama', 'DashboardAdmin\Homepage\BannerutamaController@index');
        Route::post('/homepage/banner-utama/update/{id}', 'DashboardAdmin\Homepage\BannerutamaController@update');

        // Homepage Tagline
        Route::get('/homepage/tagline', 'DashboardAdmin\Homepage\TaglineController@index');
        Route::post('/homepage/tagline/update/{id}', 'DashboardAdmin\Homepage\TaglineController@update');

        // Homepage Sambutan
        Route::get('/homepage/sambutan', 'DashboardAdmin\Homepage\SambutanController@index');
        Route::post('/homepage/sambutan/update/{id}', 'DashboardAdmin\Homepage\SambutanController@update');
        Route::post('/homepage/sambutan/create', 'DashboardAdmin\Homepage\SambutanController@create');
        Route::get('/homepage/sambutan/delete/{id}', 'DashboardAdmin\Homepage\SambutanController@destroy');

        // Profil Visi dan Misi
        Route::get('/profil/visi-misi', 'DashboardAdmin\Profil\VisimisiController@index');
        Route::post('/profil/visi-misi/update/{id}', 'DashboardAdmin\Profil\VisimisiController@update');
        Route::post('/profil/visi-misi/create', 'DashboardAdmin\Profil\VisimisiController@create');
        Route::get('/profil/visi-misi/delete/{id}', 'DashboardAdmin\Profil\VisimisiController@destroy');

        // Profil Tupoksi
        Route::get('/profil/tupoksi', 'DashboardAdmin\Profil\TupoksiController@index');
        Route::post('/profil/tupoksi/update/{id}', 'DashboardAdmin\Profil\TupoksiController@update');
        Route::post('/profil/tupoksi/create', 'DashboardAdmin\Profil\TupoksiController@create');
        Route::get('/profil/tupoksi/delete/{id}', 'DashboardAdmin\Profil\TupoksiController@destroy');

        // Profil Susunan Organisasi
        Route::get('/profil/susunanorganisasi', 'DashboardAdmin\Profil\SusunanorganisasiController@index');
        Route::post('/profil/susunanorganisasi/update/{id}', 'DashboardAdmin\Profil\SusunanorganisasiController@update');

        // Profil Struktur Pemerintahan
        Route::get('/profil/strukturpemerintahan', 'DashboardAdmin\Profil\StrukturpemerintahanController@index');
        Route::post('/profil/strukturpemerintahan/update/{id}', 'DashboardAdmin\Profil\StrukturpemerintahanController@update');
        Route::post('/profil/strukturpemerintahan/create', 'DashboardAdmin\Profil\StrukturpemerintahanController@create');
        Route::delete('/profil/strukturpemerintahan/delete/{id}', 'DashboardAdmin\Profil\StrukturpemerintahanController@destroy');

        // Profil Kampung KB
        Route::get('/profil/kampungkb', 'DashboardAdmin\Profil\KampungkbController@index');
        Route::post('/profil/kampungkb/update/{id}', 'DashboardAdmin\Profil\KampungkbController@update');

        // Profil Kependudukan
        Route::get('/profil/kependudukan', 'DashboardAdmin\Profil\KependudukanController@index');
        Route::post('/profil/kependudukan/update/{id}', 'DashboardAdmin\Profil\KependudukanController@update');

        // Berita & Informasi
        Route::get('/berita', 'DashboardAdmin\Berita\BeritaController@index');
        Route::get('/berita/edit/{id}', 'DashboardAdmin\Berita\BeritaController@edit');
        Route::post('/berita/update/{id}', 'DashboardAdmin\Berita\BeritaController@update');
        Route::get('/berita/create', 'DashboardAdmin\Berita\BeritaController@create');
        Route::post('/berita/store', 'DashboardAdmin\Berita\BeritaController@store');
        Route::delete('/berita/delete/{id}', 'DashboardAdmin\Berita\BeritaController@destroy');

        // Alamat
        Route::get('/alamat', 'DashboardAdmin\Alamat\KelurahanController@index');
        Route::post('/alamat/update/{id}', 'DashboardAdmin\Alamat\KelurahanController@update');
        Route::post('/alamat/create', 'DashboardAdmin\Alamat\KelurahanController@create');
        Route::delete('/alamat/delete/{id}', 'DashboardAdmin\Alamat\KelurahanController@destroy');
    });
});

// Dashboard Petugas
Route::group(['middleware' => 'role:petugas'], function () {
    Route::get('/', function () {
        return view('landingpage.home');
    })->name('home');

    Route::prefix('/dashboard-petugas')->group(function () {
        Route::get('/', 'DashboardPetugas\DashboardController@index')->name('dashboardPetugas');

        // Manage User Masuk
        Route::get('/manage-users', 'DashboardPetugas\Manageuser\RegistrasiController@index');
        Route::post('/manage-users/update/{id}', 'DashboardPetugas\Manageuser\RegistrasiController@update');
        Route::get('/manage-users/delete/{id}', 'DashboardPetugas\Manageuser\RegistrasiController@destroy');

        // Manage User Verifikasi
        Route::get('/manage-users/verified', 'DashboardPetugas\Manageuser\VerifiedController@index');
        Route::post('/manage-users/verified/update/{id}', 'DashboardPetugas\Manageuser\VerifiedController@update');
        Route::get('/manage-users/verified/delete/{id}', 'DashboardPetugas\Manageuser\VerifiedController@destroy');

        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        
        // Layanan Surat Kelahiran
        Route::get('/layanan/masuk', 'DashboardPetugas\LayananSurat\PengajuanmasukController@index');
        Route::get('/layanan/masuk/edit-kelahiran/{id}', 'Landingpage\Pelayanan\KelahiranController@edit');
        Route::post('/layanan/masuk/update-kelahiran/{id}', 'Landingpage\Pelayanan\KelahiranController@update');
        Route::get('/layanan/masuk/delete-kelahiran/{id}', 'Landingpage\Pelayanan\KelahiranController@destroy');



        // Layanan Surat Kematian
        Route::get('/layanan/masuk', 'DashboardPetugas\LayananSurat\PengajuanmasukController@index');
        Route::get('/layanan/masuk/edit-kematian/{id}', 'Landingpage\Pelayanan\KematianController@edit');
        Route::post('/layanan/masuk/update-kematian/{id}', 'Landingpage\Pelayanan\KematianController@update');
        Route::get('/layanan/masuk/delete-kematian/{id}', 'Landingpage\Pelayanan\KematianController@destroy');


        // Layanan Surat Pindah Keluar
        Route::get('/layanan/masuk', 'DashboardPetugas\LayananSurat\PengajuanmasukController@index');
        Route::get('/layanan/masuk/edit-pindahkeluar/{id}', 'Landingpage\Pelayanan\PindahKeluarController@edit');
        Route::post('/layanan/masuk/update-pindahkeluar/{id}', 'Landingpage\Pelayanan\PindahKeluarController@update');
        Route::get('/layanan/masuk/delete-pindahkeluar/{id}', 'Landingpage\Pelayanan\PindahKeluarController@destroy');


        // Layanan Surat Pindah Masuk
        Route::get('/layanan/masuk', 'DashboardPetugas\LayananSurat\PengajuanmasukController@index');
        Route::get('/layanan/masuk/edit-pindahmasuk/{id}', 'Landingpage\Pelayanan\PindahMasukController@edit');
        Route::post('/layanan/masuk/update-pindahmasuk/{id}', 'Landingpage\Pelayanan\PindahMasukController@update');
        Route::get('/layanan/masuk/delete-pindahmasuk/{id}', 'Landingpage\Pelayanan\PindahMasukController@destroy');

        // Layanan KTP Baru
        Route::get('/layanan/masuk', 'DashboardPetugas\LayananSurat\PengajuanmasukController@index');
        Route::get('/layanan/masuk/edit-ktpbaru/{id}', 'Landingpage\Pelayanan\KtpbaruController@edit');
        Route::post('/layanan/masuk/update-ktpbaru/{id}', 'Landingpage\Pelayanan\KtpbaruController@update');
        Route::get('/layanan/masuk/delete-ktpbaru/{id}', 'Landingpage\Pelayanan\KtpbaruController@destroy');

        // Layanan Mengurus Kehilangan
        Route::get('/layanan/masuk', 'DashboardPetugas\LayananSurat\PengajuanmasukController@index');
        Route::get('/layanan/masuk/edit-menguruskehilangan/{id}', 'Landingpage\Pelayanan\KehilanganController@edit');
        Route::post('/layanan/masuk/update-menguruskehilangan/{id}', 'Landingpage\Pelayanan\KehilanganController@update');
        Route::get('/layanan/masuk/delete-menguruskehilangan/{id}', 'Landingpage\Pelayanan\KehilanganController@destroy');


        // Layanan Mengurus Kehilangan
        Route::get('/layanan/masuk', 'DashboardPetugas\LayananSurat\PengajuanmasukController@index');
        Route::get('/layanan/masuk/edit-keterangandomisili/{id}', 'Landingpage\Pelayanan\DomisiliController@edit');
        Route::post('/layanan/masuk/update-keterangandomisili/{id}', 'Landingpage\Pelayanan\DomisiliController@update');
        Route::get('/layanan/masuk/delete-keterangandomisili/{id}', 'Landingpage\Pelayanan\DomisiliController@destroy');      



        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        // Layanan Surat Verify
        Route::get('/layanan/verify', 'DashboardPetugas\LayananSurat\PengajuandiverifikasiController@index');

        Route::get('/layanan/verify/lihat-kelahiran/{id}', 'Landingpage\Pelayanan\KelahiranController@lihat');
        Route::get('/layanan/verify/lihat-kematian/{id}', 'Landingpage\Pelayanan\KematianController@lihat');
        Route::get('/layanan/verify/lihat-pindahkeluar/{id}', 'Landingpage\Pelayanan\PindahKeluarController@lihat');
        Route::get('/layanan/verify/lihat-pindahmasuk/{id}', 'Landingpage\Pelayanan\PindahMasukController@lihat');
        Route::get('/layanan/verify/lihat-ktpbaru/{id}', 'Landingpage\Pelayanan\KtpbaruController@lihat');
        Route::get('/layanan/verify/lihat-menguruskehilangan/{id}', 'Landingpage\Pelayanan\KehilanganController@lihat');
        Route::get('/layanan/verify/lihat-keterangandomisili/{id}', 'Landingpage\Pelayanan\DomisiliController@lihat');

        // Pendaftaran UMKM
        Route::get('/pendaftaran/masuk', 'DashboardPetugas\PendaftaranUmkm\PengajuanmasukController@index');
        Route::post('/pendaftaran/masuk/update/{id}', 'DashboardPetugas\PendaftaranUmkm\PengajuanmasukController@update');
        Route::get('/pendaftaran/masuk/delete/{id}', 'DashboardPetugas\PendaftaranUmkm\PengajuanmasukController@destroy');

        // Pendaftaran UMKM Verify
        Route::get('/pendaftaran/verify', 'DashboardPetugas\PendaftaranUmkm\PengajuandiverifikasiController@index');

        // Laporan Layanan Surat
        Route::get('/laporan/layanan-surat', 'DashboardPetugas\Laporan\LayanansuratController@report')->name('report');
        Route::get('/laporan/layanan-surat/{daterange}', 'DashboardPetugas\Laporan\LayanansuratController@reportPdf')->name('report_pdf');
        Route::get('/laporan/allsurat/', 'DashboardPetugas\Laporan\LayanansuratController@reportAllPdf')->name('reportumkm_allpdf');

         // Laporan Layanan Surat
         Route::get('/laporan/umkm', 'DashboardPetugas\Laporan\UmkmController@report')->name('reportumkm');
         Route::get('/laporan/umkm/{daterange}', 'DashboardPetugas\Laporan\UmkmController@reportPdf')->name('reportumkm_pdf');
         Route::get('/laporan/allumkm/', 'DashboardPetugas\Laporan\UmkmController@reportAllPdf')->name('reportumkm_allpdf');
    });
});

// Dashboard Masyarakat
Route::group(['middleware' => 'role:masyarakat'], function () {
    Route::get('/', function () {
        return view('landingpage.home');
    })->name('home');

    Route::prefix('/dashboard/{iduser}')->group(function () {
        Route::get('/', 'DashboardMasyarakat\DashboardController@dashboard')->name('dashboardMasyarakat');

        Route::get('/profile', 'DashboardMasyarakat\ProfileController@index');
        Route::get('/profile/rt/{id}', 'DashboardMasyarakat\ProfileController@rt');
        Route::post('/profile/update/', 'DashboardMasyarakat\ProfileController@update');

        // Pengajuan Layanan Surat
        Route::get('/pengajuan-surat', 'DashboardMasyarakat\Surat\PengajuanController@index');
        // Verifikasi Layanan Surat
        Route::get('/verify-surat', 'DashboardMasyarakat\Surat\VerifyController@index');

        // Pengajuan UMKM
        Route::get('/pengajuan-umkm', 'DashboardMasyarakat\UMKM\PengajuanController@index');
        // Verifikasi UMKM
        Route::get('/verify-umkm', 'DashboardMasyarakat\UMKM\VerifyController@index');
    });
});

// ================================================= Landingpage Kelurahan Banaran =================================================

// Homepage
Route::get('/', 'Landingpage\HomeController@index');

// Register
Route::get('/get_RT/{id}', 'Auth\RegisterController@getRt');
// Route::get('/get_RT/{id}', 'Auth\RegisterController@getBiaya');

// Profil page
Route::prefix('/profil')->group(function () {
    Route::get('/visimisi', 'Landingpage\VisimisiController@index');
    Route::get('/tupoksi', 'Landingpage\TupoksiController@index');
    Route::get('/strukturpemerintahan', 'Landingpage\StrukturpemerintahanController@index');
    Route::get('/geografisdankependudukan', 'Landingpage\GeografisdankependudukanController@index');
});

// Berita Page
Route::get('/berita', 'Landingpage\BeritaController@index')->name('berita');
Route::get('/berita/search', 'Landingpage\BeritaController@search')->name('search');

Route::get('/berita/{slug}', 'Landingpage\DetailberitaController@index');

// UMKM Page
Route::get('/umkm', 'Landingpage\UmkmController@index');
Route::get('/umkm/{slug}', 'Landingpage\DetailumkmController@index');

// Pelayanan Page
Route::get('/pelayanan', 'Landingpage\PelayananController@index');
Route::post('/pelayanan/create', 'Landingpage\PelayananController@create')->name('pelayanan');

// Mendaftar UMKM
Route::get('/mendaftar/umkm', 'Landingpage\UmkmdaftarController@index');
Route::post('/mendaftar/umkm/create', 'Landingpage\UmkmdaftarController@create')->name('daftarumkm');


// ------- Layanan ------- 
Route::prefix('/pelayanan')->group(function () {
    // Buat Akta Kelahiran
    Route::get('/akta-kelahiran', 'Landingpage\Pelayanan\KelahiranController@index');
    Route::get('/akta-kelahiran/get_RT/{id}', 'Landingpage\Pelayanan\KelahiranController@getRt');
    Route::post('/akta-kelahiran/create', 'Landingpage\Pelayanan\KelahiranController@create')->name('createAktaKelahiran');


    // Buat Akta Kematian
    Route::get('/akta-kematian', 'Landingpage\Pelayanan\KematianController@index');
    Route::get('/akta-kematian/get_RT/{id}', 'Landingpage\Pelayanan\KematianController@getRt');
    Route::post('/akta-kematian/create', 'Landingpage\Pelayanan\KematianController@create')->name('createAktaKematian');

    // Pindah Keluar
    Route::get('/pindah-keluar', 'Landingpage\Pelayanan\PindahKeluarController@index');
    Route::get('/pindah-keluar/get_RT/{id}', 'Landingpage\Pelayanan\PindahKeluarController@getRt');
    Route::post('/pindah-keluar/create', 'Landingpage\Pelayanan\PindahKeluarController@create')->name('createPindahKeluar');

    // Pindah Masuk
    Route::get('/pindah-masuk', 'Landingpage\Pelayanan\PindahMasukController@index');
    Route::get('/pindah-masuk/get_RT/{id}', 'Landingpage\Pelayanan\PindahMasukController@getRt');
    Route::post('/pindah-masuk/create', 'Landingpage\Pelayanan\PindahMasukController@create')->name('createPindahMasuk');

    Route::get('/cities/{province_id}', 'Landingpage\Pelayanan\PindahMasukController@getCities');

    // KTP Baru
    Route::get('/ktp-baru', 'Landingpage\Pelayanan\KtpbaruController@index');
    Route::get('/ktp-baru/get_RT/{id}', 'Landingpage\Pelayanan\KtpbaruController@getRt');
    Route::post('/ktp-baru/create', 'Landingpage\Pelayanan\KtpbaruController@create')->name('createKtpBaru');

    // Mengurus Kehilangan
    Route::get('/mengurus-kehilangan', 'Landingpage\Pelayanan\KehilanganController@index');
    Route::get('/mengurus-kehilangan/get_RT/{id}', 'Landingpage\Pelayanan\KehilanganController@getRt');
    Route::post('/mengurus-kehilangan/create', 'Landingpage\Pelayanan\KehilanganController@create')->name('createMengurusKehilangan');

    // Keterangan Domisili
    Route::get('/keterangan-domisili', 'Landingpage\Pelayanan\DomisiliController@index');
    Route::get('/keterangan-domisili/get_RT/{id}', 'Landingpage\Pelayanan\DomisiliController@getRt');
    Route::post('/keterangan-domisili/create', 'Landingpage\Pelayanan\DomisiliController@create')->name('createKeteranganDomisili');


    
});






















































































// Dashboard Admin
// Route::group(['middleware' => 'admin'], function () {  
//     Route::get('/', function () {
//         return view('home');
//     })->name('home');

//     Route::prefix('/dashboard-admin')->group(function () {
//         Route::get('/', 'DashboardAdmin\DashboardController@index')->name('dashboardAdmin');
//     });
// });

// // Dashboard Petugas
// Route::group(['middleware' => 'petugas'], function () {
//     Route::get('/', function () {
//         return view('home');
//     })->name('home');

//     Route::prefix('/dashboard-petugas')->group(function () {
//         Route::get('/', 'DashboardPetugas\DashboardController@index')->name('dashboardPetugas');
//     });
// });

// Route::get('/home', 'HomeController@index')->name('home');