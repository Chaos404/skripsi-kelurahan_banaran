@extends('layouts.dashboard2')
@section('title', 'Dashboard | Surat Terverifikasi')

<!-- TagPage -->
@section('tagPage')
<div class="page-header" style="color: white">
    <h4 class="page-title" style="color: white">Surat Terverifikasi</h4>
    <ul class="breadcrumbs">
        <li class="nav-home">
            <a href="#">
                <i class="flaticon-home" style="color: white"></i>
            </a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="{{ '/dashboard-petugas' }}" style="color: white">Dashboard</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="" style="color: white">Layanan Surat</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="" style="color: white">Surat Terverifikasi</a>
        </li>
    </ul>
</div>
@endsection
<!-- End TagPage -->

@section('descPage','Halaman ini digunakan untuk melihat pengajuan Surat dari masyarakat yang berhasil terverifikasi')

@section('content')
<div class="page-inner mt--5">
    <div class="row mt--2">
        <div class="col-md-12">
            <div class="card full-height">
                <div class="card-body">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-check"></i> <strong>Berhasil! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Danger! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('warning'))
                    <div class="alert alert-warning">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-exclamation"></i> <strong>Warning! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('info'))
                    <div class="alert alert-info">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-info"></i> <strong>Info! &nbsp;&nbsp;</strong> 
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Bahaya! &nbsp;&nbsp;</strong>
                        Silakan periksa formulir di bawah ini untuk kesalahan <br>
                        <strong>
                            <ul>
                                @foreach ($errors->all() as $message)
                                    <li>{{$message}}</li>
                                @endforeach
                            </ul>
                        </strong>
                    </div>
                    @endif

                    <div class="table-responsive" style="padding-top: 10px">
                        <table id="basic-datatables" class="display table table-striped table-hover" >
                            <thead>
                                <tr>
                                    <th class="text-center" width="3%">#</th>
                                    <th class="text-center" width="20%">ID Pengajuan</th>
                                    <th class="text-center" width="15%">NIK</th>
                                    <th class="text-center" width="20%">Nama</th>
                                    <th class="text-center" width="20%">Keperluan</th>
                                    <th class="text-center" width="15%">File</th>
                                    <th class="text-center" width="20%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                            $no = 1;
                            @endphp
                            @foreach ($surat as $row)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td class="text-center">{{ $row->id_pengajuan }}</td>
                                    <td>{{ $row->User['nik'] }}</td>
                                    <td class="text-center">{{ $row->User['name'] }}</td>   
                                    <td class="text-center">{{ $row->Keperluan['keperluan'] }}</td>    
                                    <td class="text-center">
                                        @if ($row->t_keperluan_id == 6)
                                            <a href="{{ URL::to('public') }}/assets/1197268/{{ $row['id_pengajuan'] }}-Suket.pdf" target="_blank"><i class="far fa-file-pdf fa-2x" style="color: rgb(7, 179, 231)"></i></a>
                                        @elseif ($row->t_keperluan_id == 7)
                                            <a href="{{ URL::to('public') }}/assets/1197268/{{ $row['id_pengajuan'] }}-Suket.pdf" target="_blank"><i class="far fa-file-pdf fa-2x" style="color: rgb(7, 179, 231)"></i></a>
                                        @else
                                            <a href="{{ URL::to('public') }}/assets/1197268/{{ $row['id_pengajuan'] }}-Suket.pdf" target="_blank"><i class="far fa-file-pdf fa-2x" style="color: rgb(7, 179, 231)"></i></a>
                                            &nbsp;
                                            <a href="{{ URL::to('public') }}/assets/1197824/{{ $row['id_pengajuan'] }}-Lampiran.pdf" target="_blank"><i class="far fa-file-pdf fa-2x" style="color: rgb(14, 224, 25)"></i></a> 
                                        @endif 
                                    </td>  
                                    <td class="text-center">
                                        @if ($row->t_keperluan_id == 1)
                                            <a href="{{ url('dashboard-petugas/layanan/verify/lihat-kelahiran', [$row->id]) }}" class="btn btn-sm btn-info"><i class="fas fa-eye" style="color: white;" ></i></a>
                                        @elseif ($row->t_keperluan_id == 2)
                                            <a href="{{ url('dashboard-petugas/layanan/verify/lihat-kematian', [$row->id]) }}" class="btn btn-sm btn-info"><i class="fas fa-eye" style="color: white;" ></i></a>
                                        @elseif ($row->t_keperluan_id == 3)
                                            <a href="{{ url('dashboard-petugas/layanan/verify/lihat-pindahkeluar', [$row->id]) }}" class="btn btn-sm btn-info"><i class="fas fa-eye" style="color: white;" ></i></a>
                                        @elseif ($row->t_keperluan_id == 4)
                                            <a href="{{ url('dashboard-petugas/layanan/verify/lihat-pindahmasuk', [$row->id]) }}" class="btn btn-sm btn-info"><i class="fas fa-eye" style="color: white;" ></i></a>
                                        @elseif ($row->t_keperluan_id == 5)
                                            <a href="{{ url('dashboard-petugas/layanan/verify/lihat-ktpbaru', [$row->id]) }}" class="btn btn-sm btn-info"><i class="fas fa-eye" style="color: white;" ></i></a>
                                        @elseif ($row->t_keperluan_id == 6)
                                            <a href="{{ url('dashboard-petugas/layanan/verify/lihat-menguruskehilangan', [$row->id]) }}" class="btn btn-sm btn-info"><i class="fas fa-eye" style="color: white;" ></i></a>
                                        @else
                                            <a href="{{ url('dashboard-petugas/layanan/verify/lihat-keterangandomisili', [$row->id]) }}" class="btn btn-sm btn-info"><i class="fas fa-eye" style="color: white;" ></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach   
                            </tbody>         
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@foreach ($surat as $row)
<div class="modal fade bd-example-modal-lg" id="dataEdit{{ $row->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <form action="{{url('dashboard-petugas/layanan/masuk/update', [$row->id])}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="page-title" id="myModalLabel">Pengajuan Surat Terverifikasi</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="status" value="terverifikasi">
                    <input type="hidden" name="id_pengajuan" value="{{ $row->id_pengajuan }}">
                    <input type="hidden" name="id_name" value="{{ $row->User['name'] }}">
                    <input type="hidden" name="nik" value="{{ $row->User['nik'] }}">
                    <input type="hidden" name="keperluannya" value="{{ $row->Keperluan['keperluan'] }}">
                    <input type="hidden" name="keperluan" value="{{ $row->Keperluan['id'] }}">
                    <input type="hidden" name="email" value="{{ $row->User['email'] }}">
                    <div class="form-group">
                        <label >Nomor Surat</label>
                        <input type="text" class="form-control" name="no_surat" id="inputName4" placeholder="Nomor Surat" required autocomplete="off" value="{{ $row->no_surat }}" disabled>
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                        <textarea name="keterangan" class="form-control" id="exampleFormControlTextarea1" rows="3" disabled>{{ $row->keterangan }}</textarea>
                    </div>
                    {{-- <div class="form-group">
                        <label >Lampiran Tanda Tangan Lurah</label>
                        <input type='file' class="form-control" name="tanda_tangan_kepala_desa">
                    </div> --}}
                    <hr>
                    <div class="form-group">
                        <label >Nama Lengkap</label>
                        <input type="text" class="form-control" name="nama_lengkap" id="inputName4" placeholder="Nama Lengkap" value="{{ $row->User['name'] }}" disabled>
                    </div>
                    <div class="row" style="padding-left: 13px;">
                        <div class="form-group col-md-6">
                            <label >Dukuh</label>
                            <select name="dukuh" id="" class="form-control" disabled>
                                @foreach ($dukuh as $row1)
                                    <?php
                                    $selected = ' ';
                                    if($row->User['t_dukuh_id'] == $row1['id']){
                                        $selected = 'selected';
                                    }
                                    ?>
                                    <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['dukuh']; ?> </option>
                                    ?>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label >RT</label>
                            <select name="rt" id="" class="form-control" disabled>
                                <option selected value="{{ $row->User['rt'] }}"> <?php echo $row->User['rt']; ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="row" style="padding-left: 13px;">
                        <div class="form-group col-md-6">
                            <label >NIK</label>
                            <input type="text" class="form-control" id="inputName4" name="nik" placeholder="NIK" value="{{ $row->User['nik'] }}" disabled>
                        </div>
                        <div class="form-group col-md-6">
                            <label >Nomor KK</label>
                            <input type="text" class="form-control" id="inputName4" name="no_kk" placeholder="Nomor KK" value="{{ $row->User['no_kk'] }}" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label >Keperluan</label>
                        <select name="" id="" class="form-control" disabled>
                            @foreach ($keperluan as $row1)
                                <?php
                                $selected = ' ';
                                if($row['t_keperluan_id'] == $row1['id']){
                                    $selected = 'selected';
                                }
                                ?>
                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['keperluan']; ?> </option>
                                ?>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label >Lampiran Surat Pengantar RT</label><br>
                        <center><a href="{{ URL::to('/') }}/assets/1134798/{{ $row->lamp_pengantar }}" target="_blank"><img src="{{ URL::to('/') }}/assets/1134798/{{ $row->lamp_pengantar }}" class="img-thumbnail" height="100px" width="150px"></a></center>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endforeach
@endsection