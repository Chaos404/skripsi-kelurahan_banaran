@extends('layouts.dashboard2')
@section('title', 'Dashboard | Pengajuan Masuk')

<!-- TagPage -->
@section('tagPage')
<div class="page-header" style="color: white">
    <h4 class="page-title" style="color: white">Pengajuan Masuk</h4>
    <ul class="breadcrumbs">
        <li class="nav-home">
            <a href="#">
                <i class="flaticon-home" style="color: white"></i>
            </a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="{{ '/dashboard-petugas' }}" style="color: white">Dashboard</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="" style="color: white">Layanan Surat</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="" style="color: white">Pengajuan Masuk</a>
        </li>
    </ul>
</div>
@endsection
<!-- End TagPage -->

@section('descPage','Halaman ini digunakan untuk mengelola pengajuan Surat yang masuk dari masyarakat')

@section('content')
<div class="page-inner mt--5">
    <div class="row mt--2">
        <div class="col-md-12">
            <div class="card full-height">
                <div class="card-body">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-check"></i> <strong>Berhasil! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Danger! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('warning'))
                    <div class="alert alert-warning">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-exclamation"></i> <strong>Warning! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('info'))
                    <div class="alert alert-info">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-info"></i> <strong>Info! &nbsp;&nbsp;</strong> 
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Bahaya! &nbsp;&nbsp;</strong>
                        Silakan periksa formulir di bawah ini untuk kesalahan <br>
                        <strong>
                            <ul>
                                @foreach ($errors->all() as $message)
                                    <li>{{$message}}</li>
                                @endforeach
                            </ul>
                        </strong>
                    </div>
                    @endif

                    <div class="table-responsive" style="padding-top: 10px">
                        <table id="basic-datatables" class="display table table-striped table-hover" >
                            <thead>
                                <tr>
                                    <th class="text-center" width="3%">#</th>
                                    <th class="text-center" width="20%">ID Pengajuan</th>
                                    <th class="text-center" width="15%">NIK</th>
                                    <th class="text-center" width="20%">Nama</th>
                                    <th class="text-center" width="20%">Keperluan</th>
                                    <th class="text-center" width="5%">Status</th>
                                    <th class="text-center" width="20%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                            $no = 1;
                            @endphp
                            @foreach ($surat as $row)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td class="text-center">{{ $row->id_pengajuan }}</td>
                                    <td>{{ $row->User['nik'] }}</td>
                                    <td class="text-center">{{ $row->User['name'] }}</td>   
                                    <td class="text-center">{{ $row->Keperluan['keperluan'] }}</td>    
                                    <td class="text-center">
                                        @if ($row->status == "proses")
                                        <span class="badge badge-danger">Baru</span>
                                        @else 
                                        <span class="badge badge-success">Terverifikasi</span>
                                        @endif
                                    </td>  
                                    <td class="text-center">
                                        @if ($row->t_keperluan_id == 1)
                                            <a href="{{ url('dashboard-petugas/layanan/masuk/edit-kelahiran', [$row->id]) }}" class="btn btn-sm btn-primary"><i class="fas fa-check-circle" style="color: white;" ></i></a>
                                            &nbsp;&nbsp;
                                            <a href="{{url('dashboard-petugas/layanan/masuk/delete-kelahiran', [$row->id])}}" onclick="return confirm('Anda yakin menghapus?');" class="btn btn-sm btn-danger" height:100px><i class="far fa-trash-alt"></i></a>
                                        @elseif ($row->t_keperluan_id == 2)
                                            <a href="{{ url('dashboard-petugas/layanan/masuk/edit-kematian', [$row->id]) }}" class="btn btn-sm btn-primary"><i class="fas fa-check-circle" style="color: white;" ></i></a>
                                            &nbsp;&nbsp;
                                            <a href="{{url('dashboard-petugas/layanan/masuk/delete-kematian', [$row->id])}}" onclick="return confirm('Anda yakin menghapus?');" class="btn btn-sm btn-danger" height:100px><i class="far fa-trash-alt"></i></a>
                                        @elseif ($row->t_keperluan_id == 3)
                                            <a href="{{ url('dashboard-petugas/layanan/masuk/edit-pindahkeluar', [$row->id]) }}" class="btn btn-sm btn-primary"><i class="fas fa-check-circle" style="color: white;" ></i></a>
                                            &nbsp;&nbsp;
                                            <a href="{{url('dashboard-petugas/layanan/masuk/delete-pindahkeluar', [$row->id])}}" onclick="return confirm('Anda yakin menghapus?');" class="btn btn-sm btn-danger" height:100px><i class="far fa-trash-alt"></i></a>
                                        @elseif ($row->t_keperluan_id == 4)
                                            <a href="{{ url('dashboard-petugas/layanan/masuk/edit-pindahmasuk', [$row->id]) }}" class="btn btn-sm btn-primary"><i class="fas fa-check-circle" style="color: white;" ></i></a>
                                            &nbsp;&nbsp;
                                            <a href="{{url('dashboard-petugas/layanan/masuk/delete-pindahmasuk', [$row->id])}}" onclick="return confirm('Anda yakin menghapus?');" class="btn btn-sm btn-danger" height:100px><i class="far fa-trash-alt"></i></a>
                                        @elseif ($row->t_keperluan_id == 5)
                                            <a href="{{ url('dashboard-petugas/layanan/masuk/edit-ktpbaru', [$row->id]) }}" class="btn btn-sm btn-primary"><i class="fas fa-check-circle" style="color: white;" ></i></a>
                                            &nbsp;&nbsp;
                                            <a href="{{url('dashboard-petugas/layanan/masuk/delete-ktpbaru', [$row->id])}}" onclick="return confirm('Anda yakin menghapus?');" class="btn btn-sm btn-danger" height:100px><i class="far fa-trash-alt"></i></a>
                                        @elseif ($row->t_keperluan_id == 6)
                                            <a href="{{ url('dashboard-petugas/layanan/masuk/edit-menguruskehilangan', [$row->id]) }}" class="btn btn-sm btn-primary"><i class="fas fa-check-circle" style="color: white;" ></i></a>
                                            &nbsp;&nbsp;
                                            <a href="{{url('dashboard-petugas/layanan/masuk/delete-menguruskehilangan', [$row->id])}}" onclick="return confirm('Anda yakin menghapus?');" class="btn btn-sm btn-danger" height:100px><i class="far fa-trash-alt"></i></a>
                                        @else
                                            <a href="{{ url('dashboard-petugas/layanan/masuk/edit-keterangandomisili', [$row->id]) }}" class="btn btn-sm btn-primary"><i class="fas fa-check-circle" style="color: white;" ></i></a>
                                            &nbsp;&nbsp;
                                            <a href="{{url('dashboard-petugas/layanan/masuk/delete-keterangandomisili', [$row->id])}}" onclick="return confirm('Anda yakin menghapus?');" class="btn btn-sm btn-danger" height:100px><i class="far fa-trash-alt"></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach   
                            </tbody>         
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
