@extends('layouts.dashboard2')
@section('title', 'Dashboard | Lihat Verifikasi Akta Kematian')

<!-- TagPage -->
@section('tagPage')
<div class="page-header" style="color: white">
    <h4 class="page-title" style="color: white">Lihat Verifikasi Akta Kematian</h4>
    <ul class="breadcrumbs">
        <li class="nav-home">
            <a href="#">
                <i class="flaticon-home" style="color: white"></i>
            </a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="{{ '/dashboard-petugas' }}" style="color: white">Dashboard</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="" style="color: white">Layanan Surat</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="{{ '/dashboard-petugas/layanan/masuk' }}" style="color: white">Pengajuan Masuk</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="" style="color: white">Lihat Verifikasi Akta Kematian</a>
        </li>
    </ul>
</div>
@endsection
<!-- End TagPage -->

@section('descPage','Halaman ini digunakan untuk melihat detail pengajuan Akta Kematian')

@section('content')
<div class="page-inner mt--5">
    <div class="row mt--2">
        <div class="col-md-12">
            <div class="card full-height">
                <div class="card-body">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-check"></i> <strong>Berhasil! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Danger! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('warning'))
                    <div class="alert alert-warning">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-exclamation"></i> <strong>Warning! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('info'))
                    <div class="alert alert-info">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-info"></i> <strong>Info! &nbsp;&nbsp;</strong> 
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Bahaya! &nbsp;&nbsp;</strong>
                        Silakan periksa formulir di bawah ini untuk kesalahan <br>
                        <strong>
                            <ul>
                                @foreach ($errors->all() as $message)
                                    <li>{{$message}}</li>
                                @endforeach
                            </ul>
                        </strong>
                    </div>
                    @endif
                        @csrf
                        @foreach ($data as $row)
                        <h3><b>DATA PEMOHON</b></h3>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label >Nama Lengkap</label>
                                <input type="text" class="form-control" name="nama_lengkap" id="inputName4" placeholder="Nama Lengkap" value="{{ $row->User['name'] }}" disabled>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Tempat Lahir</label>
                                <input type="text" class="form-control" name="tempat_lahir" id="inputName4" placeholder="Tempat Lahir" value="{{ $row->User['name'] }}" disabled>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Tanggal Lahir</label>
                                {{-- <input id="datepicker" name="tanggal_lahir"  placeholder="Tanggal Lahir" value="{{ uth::user()->tgl_lahir }}"> --}}
                                <div class="input-group">
                                    <input type="text" class="form-control" name="datepicker" value="{{ $row->User['tgl_lahir'] }}" disabled>
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Jenis Kelamin</label>
                                <select name="jenis_kelamin" id="" class="form-control" disabled>
                                    @if ($row->User['jeniskelamin'] ==  "Laki - laki")
                                    <option <?php 'selected' ?> value= "Laki - laki"> Laki - Laki </option>
                                    @else
                                    <option <?php 'selected' ?> value="Perempuan"> Perempuan </option>
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Agama</label>
                                <select name="agama" id="" class="form-control" disabled>
                                    @foreach ($agama as $row1)
                                        <?php
                                        $selected = ' ';
                                        if($row->User['t_agama_id'] == $row1['id']){
                                            $selected = 'selected';
                                        }
                                        ?>
                                        <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['agama']; ?> </option>
                                        ?>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Pekerjaan</label>
                                <select name="pekerjaan" id="" class="form-control" disabled>
                                    @foreach ($pekerjaan as $row1)
                                        <?php
                                        $selected = ' ';
                                        if($row->User['t_pekerjaan_id'] == $row1['id']){
                                            $selected = 'selected';
                                        }
                                        ?>
                                        <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['pekerjaan']; ?> </option>
                                        ?>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Status Perkawinan</label>
                                <select name="status_perkawinan" id="" class="form-control" disabled>
                                    @if ($row->User['statusperkawinan'] == "Belum Kawin")
                                    <option <?php 'selected' ?> value="Belum Kawin"> Belum Kawin </option>
                                    @elseif ($row->User['statusperkawinan'] == "Kawin")
                                    <option <?php 'selected' ?> value="Kawin"> Kawin </option>
                                    @else 
                                    <option <?php 'selected' ?> value="Pernah Kawin"> Pernah Kawin </option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Dukuh</label>
                                <select name="dukuh" id="" class="form-control" disabled>
                                    @foreach ($dukuh as $row1)
                                        <?php
                                        $selected = ' ';
                                        if($row->User['t_dukuh_id'] == $row1['id']){
                                            $selected = 'selected';
                                        }
                                        ?>
                                        <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['dukuh']; ?> </option>
                                        ?>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label >RT</label>
                                <select name="rt" id="" class="form-control" disabled>
                                    <option value="{{ $row->User['rt'] }}">{{ $row->User['rt'] }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >NIK</label>
                                <input type="text" class="form-control" id="inputName4" name="no_nik" placeholder="NIK" value="{{ $row->User['nik'] }}" disabled>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Nomor KK</label>
                                <input type="text" class="form-control" id="inputName4" name="no_kk" placeholder="Nomor KK" value="{{ $row->User['no_kk'] }}" disabled>
                            </div>
                        </div>
                        <br>
                        @foreach ($kematian as $kematians)
                        <h3><b>DATA JENAZAH</b></h3>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label >NIK</label>
                                <input type="text" name="nik_jenazah" class="form-control" placeholder="NIK" value="{{ $kematians['nik'] }}" readonly>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label >Nama Lengkap</label>
                                <input type="text" name="nama_jenazah" class="form-control" placeholder="Nama Lengkap" value="{{ $kematians['nama'] }}" readonly>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Jenis Kelamin</label>
                                <select name="jeniskelamin" id="" class="form-control" disabled>
                                    @if ($kematians['jeniskelamin'] == "Laki - laki")
                                        <option <?php echo $selected = 'selected'; ?> value="Laki - laki">Laki - laki</option>
                                    @elseif ($kematians['jeniskelamin'] == "Perempuan")
                                        <option <?php echo $selected = 'selected'; ?> value="Perempuan">Perempuan</option>
                                    @else
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Agama</label>
                                <select name="agama_jenazah" id="" class="form-control @error('agama') is-invalid @enderror" disabled>
                                    @foreach ($agama as $row1)
                                        <?php
                                        $selected = ' ';
                                        if($kematians['t_agama_id'] == $row1['id']){
                                            $selected = 'selected';
                                        }
                                        ?>
                                        <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['agama']; ?> </option>
                                        ?>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Tempat Lahir</label>
                                <input type="text" name="tempat_lahir_jenazah" class="form-control" placeholder="Tempat Lahir" value="{{ $kematians['tempat_lahir'] }}" readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Tanggal Lahir</label>
                                <input placeholder="Tanggal Lahir" class="form-control" name="tanggal_lahir_jenazah" id="datepicker" value="{{ Carbon\Carbon::parse($kematians['tgl_lahir'])->translatedFormat('d F Y') }}" readonly>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Pekerjaan</label>
                                <select name="pekerjaan_jenazah" id="" class="form-control" disabled>
                                    @foreach ($pekerjaan as $row1)
                                        <?php
                                        $selected = ' ';
                                        if($kematians['t_pekerjaan_id'] == $row1['id']){
                                            $selected = 'selected';
                                        }
                                        ?>
                                        <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['pekerjaan']; ?> </option>
                                        ?>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <label >Dukuh</label>
                                <select name="dukuh_jenazah" id="dukuhnya_jenazah" class="form-control @error('dukuh') is-invalid @enderror" onclick="Reset()" disabled>
                                    @foreach ($dukuh as $row1)
                                        <?php
                                        $selected = ' ';
                                        if($kematians['t_dukuh_id'] == $row1['id']){
                                            $selected = 'selected';
                                        }
                                        ?>
                                        <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['dukuh']; ?> </option>
                                        ?>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <label >RT</label>
                                <select name="carirtJenazah" id="rtJenazah" class="form-control @error('rt') is-invalid @enderror" disabled>
                                    <option value="{{ $kematians['rt'] }}">{{ $kematians['rt'] }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Tanggal Kematian</label>
                                <input placeholder="Tanggal Kematian" class="form-control" name="tanggal_kematian" id="datepicker1" value="{{ Carbon\Carbon::parse($kematians['tgl_mati'])->translatedFormat('d F Y') }}" readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Pukul Kematian</label>
                                <input id="timepicker" name="pukul_kematian" class="form-control" placeholder="Pukul Kematian" value="{{ Carbon\Carbon::parse($kematians['pukul'])->format('H:i') }}" readonly/>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Sebab Kematian</label>
                                <select name="sebab_kematian" id="" class="form-control" disabled>
                                    @if ($kematians['sebab'] == "Sakit Biasa / Tua")
                                        <option <?php echo $selected = 'selected'; ?> value="Sakit Biasa / Tua">Sakit Biasa / Tua</option>
                                    @elseif ($kematians['sebab'] == "Wabah Penyakit")
                                        <option <?php echo $selected = 'selected'; ?> value="Wabah Penyakit">Wabah Penyakit</option>
                                    @elseif ($kematians['sebab'] == "Kecelakaan")
                                        <option <?php echo $selected = 'selected'; ?> value="Kecelakaan">Kecelakaan</option>
                                    @elseif ($kematians['sebab'] == "Kriminalitas")
                                        <option <?php echo $selected = 'selected'; ?> value="Kriminalitas">Kriminalitas</option>
                                    @elseif ($kematians['sebab'] == "Bunuh Diri")
                                        <option <?php echo $selected = 'selected'; ?> value="Bunuh Diri">Bunuh Diri</option>
                                    @else
                                        <option <?php echo $selected = 'selected'; ?> value="Lainnya">Lainnya</option>
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Tempat Kematian</label>
                                <input type="input" name="tempat_kematian" class="form-control" placeholder="Tempat Kematian" value="{{ $kematians['tempat_kematian'] }}" readonly>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label >Yang Menerangkan</label>
                                <select name="menerangkan" id="" class="form-control" disabled>
                                    @if ($kematians['menerangkan'] == "Dokter")
                                        <option <?php echo $selected = 'selected'; ?> value="Dokter">Dokter</option>
                                    @elseif ($kematians['menerangkan'] == "Dokter")
                                        <option <?php echo $selected = 'selected'; ?> value="Tenaga Kesehatan">Tenaga Kesehatan</option>
                                    @elseif ($kematians['menerangkan'] == "Dokter")
                                        <option <?php echo $selected = 'selected'; ?> value="Kepolisian">Kepolisian</option>
                                    @else
                                        <option <?php echo $selected = 'selected'; ?> value="Lainnya">Lainnya</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-6">
                                <!-- ----------- Data IBU ----------- -->
                                <h3><b>DATA IBU</b></h3>
                                @foreach ($ibu as $ibus)
                                @if ($ibus->form_kematians_id == $kematians['id'])
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label >NIK</label>
                                        <input type="text" name="nik_ibu" class="form-control" placeholder="NIK Ibu" value="{{ $ibus['nik'] }}" readonly>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label >Nama Lengkap</label>
                                        <input type="text" name="nama_ibu" class="form-control" placeholder="Nama Lengkap Ibu" value="{{ $ibus['nama'] }}" readonly>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label >Tanggal Lahir</label>
                                        <input placeholder="Tanggal Lahir Ibu" class="form-control" name="tanggal_lahir_ibu" id="datepicker2" value="{{ Carbon\Carbon::parse($ibus['tgl_lahir'])->translatedFormat('d F Y') }}" readonly>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label >Pekerjaan</label>
                                        <select id="" name="pekerjaan_ibu" class="form-control" disabled>
                                            {{-- <option value="">-- Pilih Pekerjaan --</option> --}}
                                            @foreach ($pekerjaan as $row1)
                                                <?php
                                                $selected = ' ';
                                                if($ibus['t_pekerjaan_id'] == $row1['id']){
                                                    $selected = 'selected';
                                                }
                                                ?>
                                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['pekerjaan']; ?> </option>
                                                ?>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label >Alamat</label>
                                        <input class="form-control" placeholder="Dukuh" name="dukuh_ibu"  value="{{ $ibus['dukuh'] }} Rt. {{ $ibus['rt'] }} {{ $ibus['kelurahan'] }}, {{ $ibus['kecamatan'] }}, {{ $ibus['kabupaten'] }}" readonly>
                                    </div>
                                </div>
                                @endif
                                @endforeach
                            </div>
                            <div class="col-lg-6">
                                <!-- ----------- Data AYAH ----------- -->
                                <h3><b>DATA AYAH</b></h3>
                                @foreach ($ayah as $ayahs)
                                @if ($ayahs->form_kematians_id == $kematians['id'])
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label >NIK</label>
                                        <input type="text" name="nik_ibu" class="form-control" placeholder="NIK Ibu" value="{{ $ayahs['nik'] }}" readonly>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label >Nama Lengkap</label>
                                        <input type="text" name="nama_ibu" class="form-control" placeholder="Nama Lengkap Ibu" value="{{ $ayahs['nama'] }}" readonly>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label >Tanggal Lahir</label>
                                        <input placeholder="Tanggal Lahir Ibu" class="form-control" name="tanggal_lahir_ibu" id="datepicker2" value="{{ Carbon\Carbon::parse($ayahs['tgl_lahir'])->translatedFormat('d F Y') }}" readonly>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label >Pekerjaan</label>
                                        <select id="" name="pekerjaan_ibu" class="form-control" disabled>
                                            {{-- <option value="">-- Pilih Pekerjaan --</option> --}}
                                            @foreach ($pekerjaan as $row1)
                                                <?php
                                                $selected = ' ';
                                                if($ayahs['t_pekerjaan_id'] == $row1['id']){
                                                    $selected = 'selected';
                                                }
                                                ?>
                                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['pekerjaan']; ?> </option>
                                                ?>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label >Alamat</label>
                                        <input class="form-control" placeholder="Dukuh" name="dukuh_ibu"  value="{{ $ayahs['dukuh'] }} Rt. {{ $ayahs['rt'] }} {{ $ayahs['kelurahan'] }}, {{ $ayahs['kecamatan'] }}, {{ $ayahs['kabupaten'] }}" readonly>
                                    </div>
                                </div>
                                @endif
                                @endforeach
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-6">
                                <!-- ----------- Data SAKSI I ----------- -->
                                <h3><b>DATA SAKSI I</b></h3>
                                @foreach ($saksi1 as $saksi1s)
                                @if ($saksi1s->form_kematians_id == $kematians['id'])
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label >NIK</label>
                                        <input type="text" name="nik_saksi1" class="form-control" placeholder="NIK Saksi I" value="{{ $saksi1s['nik'] }}" readonly>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label >Nama Lengkap</label>
                                        <input type="text" name="nama_saksi1" class="form-control" placeholder="Nama Lengkap Saksi I" value="{{ $saksi1s['nama'] }}" readonly>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label >Tanggal Lahir</label>
                                        <input placeholder="Tanggal Lahir Saksi I" class="form-control" name="tanggal_lahir_saksi1" id="datepicker5" value="{{ Carbon\Carbon::parse($saksi1s['tgl_lahir'])->translatedFormat('d F Y') }}" readonly>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label >Jenis Kelamin</label>
                                        <select name="jeniskelamin" id="" class="form-control" disabled>
                                            @if ($saksi1s['jeniskelamin'] == "Laki - laki")
                                                <option <?php echo $selected = 'selected'; ?> value="Laki - laki">Laki - laki</option>
                                            @elseif ($saksi1s['jeniskelamin'] == "Perempuan")
                                                <option <?php echo $selected = 'selected'; ?> value="Perempuan">Perempuan</option>
                                            @else
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label >Pekerjaan</label>
                                        <select id="" name="pekerjaan_ibu" class="form-control" disabled>
                                            {{-- <option value="">-- Pilih Pekerjaan --</option> --}}
                                            @foreach ($pekerjaan as $row1)
                                                <?php
                                                $selected = ' ';
                                                if($saksi1s['t_pekerjaan_id'] == $row1['id']){
                                                    $selected = 'selected';
                                                }
                                                ?>
                                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['pekerjaan']; ?> </option>
                                                ?>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label >Dukuh</label>
                                        <select name="dukuh_saksi1" id="dukuhnya_saksi1" class="form-control @error('dukuh') is-invalid @enderror" onclick="Reset()" disabled>
                                            @foreach ($dukuh as $row2)
                                                <?php
                                                $selected = ' ';
                                                if($saksi1s['t_dukuh_id'] == $row2['id']){
                                                    $selected = 'selected';
                                                }
                                                ?>
                                                <option <?php echo $selected; ?> value="{{ $row2->id }}"> <?php echo $row2['dukuh']; ?> </option>
                                                ?>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label >RT</label>
                                        <select name="carirtSaksi1" id="rtSaksi1" class="form-control @error('rt') is-invalid @enderror" disabled>
                                            <option value="{{ $saksi1s['rt'] }}">{{ $saksi1s['rt'] }}</option>
                                        </select>
                                        <input type="hidden" class="form-control @error('rt') is-invalid @enderror" id="rtnyaSaksi1" name="rt_saksi1">
                                    </div>
                                </div>
                                @endif
                                @endforeach
                            </div>
                            <div class="col-lg-6">
                                <!-- ----------- Data SAKSI II ----------- -->
                                <h3><b>DATA SAKSI II</b></h3>
                                @foreach ($saksi2 as $saksi2s)
                                @if ($saksi2s->form_kematians_id == $kematians['id'])
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label >NIK</label>
                                        <input type="text" name="nik_saksi1" class="form-control" placeholder="NIK Saksi I" value="{{ $saksi2s['nik'] }}" readonly>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label >Nama Lengkap</label>
                                        <input type="text" name="nama_saksi1" class="form-control" placeholder="Nama Lengkap Saksi I" value="{{ $saksi2s['nama'] }}" readonly>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label >Tanggal Lahir</label>
                                        <input placeholder="Tanggal Lahir Saksi I" class="form-control" name="tanggal_lahir_saksi1" id="datepicker5" value="{{ Carbon\Carbon::parse($saksi2s['tgl_lahir'])->translatedFormat('d F Y') }}" readonly>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label >Jenis Kelamin</label>
                                        <select name="jeniskelamin" id="" class="form-control" disabled>
                                            @if ($saksi2s['jeniskelamin'] == "Laki - laki")
                                                <option <?php echo $selected = 'selected'; ?> value="Laki - laki">Laki - laki</option>
                                            @elseif ($saksi2s['jeniskelamin'] == "Perempuan")
                                                <option <?php echo $selected = 'selected'; ?> value="Perempuan">Perempuan</option>
                                            @else
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label >Pekerjaan</label>
                                        <select id="" name="pekerjaan_ibu" class="form-control" disabled>
                                            {{-- <option value="">-- Pilih Pekerjaan --</option> --}}
                                            @foreach ($pekerjaan as $row1)
                                                <?php
                                                $selected = ' ';
                                                if($saksi2s['t_pekerjaan_id'] == $row1['id']){
                                                    $selected = 'selected';
                                                }
                                                ?>
                                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['pekerjaan']; ?> </option>
                                                ?>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label >Dukuh</label>
                                        <select name="dukuh_saksi1" id="dukuhnya_saksi1" class="form-control @error('dukuh') is-invalid @enderror" onclick="Reset()" disabled>
                                            @foreach ($dukuh as $row2)
                                                <?php
                                                $selected = ' ';
                                                if($saksi2s['t_dukuh_id'] == $row2['id']){
                                                    $selected = 'selected';
                                                }
                                                ?>
                                                <option <?php echo $selected; ?> value="{{ $row2->id }}"> <?php echo $row2['dukuh']; ?> </option>
                                                ?>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label >RT</label>
                                        <select name="carirtSaksi1" id="rtSaksi1" class="form-control @error('rt') is-invalid @enderror" disabled>
                                            <option value="{{ $saksi2s['rt'] }}">{{ $saksi2s['rt'] }}</option>
                                        </select>
                                        <input type="hidden" class="form-control @error('rt') is-invalid @enderror" id="rtnyaSaksi1" name="rt_saksi1">
                                    </div>
                                </div>
                                @endif
                                @endforeach
                            </div>
                        </div>
        
                        <!-- ----------- PELAPOR ----------- -->
                        <input type="hidden" class="form-control" name="id_user" placeholder="Nama Lengkap" value="{{ Auth::user()->id }}">
                        <input type="hidden" name="keperluan" id="" value="Akta Kelahiran">
                        <input type="hidden" name="status" id="" value="proses">
        
                        <!-- ----------- UPLOAD PERSYARATAN ----------- -->
                        <h3><b>PERSYARATAN</b></h3>
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label >KTP Pelapor</label>
                                <center padding-bottom: 100px;><a href="{{ URL::to('public') }}/assets/lampiran/ktp_pelapor/{{ $row->ktp_pelapor }}" target="_blank"><img src="{{ URL::to('public') }}/assets/lampiran/ktp_pelapor/{{ $row->ktp_pelapor }}" height="150px" width="200px"></a></center><br>
                            </div> 

                            @foreach ($ibu as $ibus)
                            @if ($ibus->form_kematians_id == $kematians['id'])
                            <div class="form-group col-md-3">
                                <label >KTP Ibu</label>
                                <center padding-bottom: 100px;><a href="{{ URL::to('public') }}/assets/lampiran/buku_nikah/{{ $ibus['lamp_ktp'] }}" target="_blank"><img src="{{ URL::to('public') }}/assets/lampiran/ktp_ibu/{{ $ibus['lamp_ktp'] }}" height="150px" width="200px"></a></center><br>
                            </div>
                            @endif
                            @endforeach

                            @foreach ($ayah as $ayahs)
                            @if ($ayahs->form_kematians_id == $kematians['id'])
                            <div class="form-group col-md-3">
                                <label >KTP Ayah</label>
                                <center padding-bottom: 100px;><a href="{{ URL::to('public') }}/assets/lampiran/ktp_ayah/{{ $ayahs['lamp_ktp'] }}" target="_blank"><img src="{{ URL::to('public') }}/assets/lampiran/ktp_ayah/{{ $ayahs['lamp_ktp'] }}" height="150px" width="200px"></a></center><br>
                            </div> 
                            @endif
                            @endforeach

                            <div class="form-group col-md-3">
                                <label >KTP Jenazah</label>
                                <center padding-bottom: 100px;><a href="{{ URL::to('public') }}/assets/lampiran/ktp_jenazah/{{ $kematians['lamp_ktp'] }}" target="_blank"><img src="{{ URL::to('public') }}/assets/lampiran/ktp_jenazah/{{ $kematians['lamp_ktp'] }}" height="150px" width="200px"></a></center><br>
                            </div> 

                        </div>
        
                        <div class="form-row">
                            @foreach ($ayah as $ayahs)
                            @if ($ayahs->form_kematians_id == $kematians['id'])
                            <div class="form-group col-md-3">
                                <label >Kartu Keluarga</label>
                                <center padding-bottom: 100px;><a href="{{ URL::to('public') }}/assets/lampiran/kartu_keluarga/{{ $ayahs['lamp_kk'] }}" target="_blank"><img src="{{ URL::to('public') }}/assets/lampiran/kartu_keluarga/{{ $ayahs['lamp_kk'] }}" height="150px" width="200px"></a></center><br>
                            </div> 
                            @endif
                            @endforeach

                            @foreach ($saksi1 as $saksi1s)
                            @if ($saksi1s->form_kematians_id == $kematians['id'])
                            <div class="form-group col-md-3">
                                <label >KTP Saksi I</label>
                                <center padding-bottom: 100px;><a href="{{ URL::to('public') }}/assets/lampiran/saksi_I/{{ $saksi1s['lamp_ktp'] }}" target="_blank"><img src="{{ URL::to('public') }}/assets/lampiran/saksi_I/{{ $saksi1s['lamp_ktp'] }}" height="150px" width="200px"></a></center><br>
                            </div>
                            @endif
                            @endforeach

                            @foreach ($saksi2 as $saksi2s)
                            @if ($saksi2s->form_kematians_id == $kematians['id'])
                            <div class="form-group col-md-3">
                                <label >KTP Saksi II</label>
                                <center padding-bottom: 100px;><a href="{{ URL::to('public') }}/assets/lampiran/saksi_II/{{ $saksi2s['lamp_ktp'] }}" target="_blank"><img src="{{ URL::to('public') }}/assets/lampiran/saksi_II/{{ $saksi2s['lamp_ktp'] }}" height="150px" width="200px"></a></center><br>
                            </div> 
                            @endif
                            @endforeach

                            @if ($kematians['lamp_keterangankematian'] != null)
                            <div class="form-group col-md-3">
                                <label >Surat Keterangan dari RS</label>
                                <center padding-bottom: 100px;><a href="{{ URL::to('public') }}/assets/lampiran/keterangan_kematian_RS/{{ $kematians['lamp_keterangankematian'] }}" target="_blank"><img src="{{ URL::to('public') }}/assets/lampiran/keterangan_kematian_RS/{{ $kematians['lamp_keterangankematian'] }}" height="150px" width="200px"></a></center><br>
                            </div>
                            @else
                                <div class="form-group col-md-3">
                                    <label >Surat Keterangan dari RS</label><br>
                                    <center padding-bottom: 100px;><a href="{{ URL::to('public') }}/assets/lampiran/keterangan_kematian_RS/tidak_ada.png" target="_blank"><img src="{{ URL::to('public') }}/assets/lampiran/keterangan_kematian_RS/tidak_ada.png" height="150px" width="200px"></a></center><br>
                                </div>
                            @endif 
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label >Surat Pengantar RT</label>
                                <center padding-bottom: 100px;><a href="{{ URL::to('public') }}/assets/lampiran/pengantar_RT/{{ $row->lamp_pengantarRT }}" target="_blank"><img src="{{ URL::to('public') }}/assets/lampiran/pengantar_RT/{{ $row->lamp_pengantarRT }}" height="150px" width="200px"></a></center><br>
                            </div> 
                        </div>
                        <br>
                        <h3><b>KETERANGAN DAN LAIN - LAIN</b></h3>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <textarea name="keterangan" class="form-control" id="exampleFormControlTextarea1" rows="3" readonly>{{ $row->keterangan }}</textarea>
                            </div>
                        </div>
                        @endforeach
                        @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
