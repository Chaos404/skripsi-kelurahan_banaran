@extends('layouts.dashboard2')
@section('title', 'Dashboard | Lihat Verifikasi Pindah Masuk')

<!-- TagPage -->
@section('tagPage')
<div class="page-header" style="color: white">
    <h4 class="page-title" style="color: white">Lihat Verifikasi Pindah Masuk</h4>
    <ul class="breadcrumbs">
        <li class="nav-home">
            <a href="#">
                <i class="flaticon-home" style="color: white"></i>
            </a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="{{ '/dashboard-petugas' }}" style="color: white">Dashboard</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="" style="color: white">Layanan Surat</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="{{ '/dashboard-petugas/layanan/masuk' }}" style="color: white">Pengajuan Masuk</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="" style="color: white">Lihat Verifikasi Pindah Masuk</a>
        </li>
    </ul>
</div>
@endsection
<!-- End TagPage -->

@section('descPage','Halaman ini digunakan untuk melihat detail pengajuan Pindah Masuk (Kedatangan)')

@section('content')
<div class="page-inner mt--5">
    <div class="row mt--2">
        <div class="col-md-12">
            <div class="card full-height">
                <div class="card-body">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-check"></i> <strong>Berhasil! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Danger! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('warning'))
                    <div class="alert alert-warning">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-exclamation"></i> <strong>Warning! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('info'))
                    <div class="alert alert-info">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-info"></i> <strong>Info! &nbsp;&nbsp;</strong> 
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Bahaya! &nbsp;&nbsp;</strong>
                        Silakan periksa formulir di bawah ini untuk kesalahan <br>
                        <strong>
                            <ul>
                                @foreach ($errors->all() as $message)
                                    <li>{{$message}}</li>
                                @endforeach
                            </ul>
                        </strong>
                    </div>
                    @endif

                    @foreach ($data as $row)
                    <h3><b>DATA PEMOHON</b></h3>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label >Nama Lengkap</label>
                            <input type="text" class="form-control" name="nama_lengkap" id="inputName4" placeholder="Nama Lengkap" value="{{ $row->User['name'] }}" disabled>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label >Tempat Lahir</label>
                            <input type="text" class="form-control" name="tempat_lahir" id="inputName4" placeholder="Tempat Lahir" value="{{ $row->User['name'] }}" disabled>
                        </div>
                        <div class="form-group col-md-6">
                            <label >Tanggal Lahir</label>
                            {{-- <input id="datepicker" name="tanggal_lahir"  placeholder="Tanggal Lahir" value="{{ uth::user()->tgl_lahir }}"> --}}
                            <div class="input-group">
                                <input type="text" class="form-control" name="datepicker" value="{{ $row->User['tgl_lahir'] }}" disabled>
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label >Jenis Kelamin</label>
                            <select name="jenis_kelamin" id="" class="form-control" disabled>
                                @if ($row->User['jeniskelamin'] ==  "Laki - laki")
                                <option <?php 'selected' ?> value= "Laki - laki"> Laki - Laki </option>
                                @else
                                <option <?php 'selected' ?> value="Perempuan"> Perempuan </option>
                                @endif
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Agama</label>
                            <select name="agama" id="" class="form-control" disabled>
                                @foreach ($agama as $row1)
                                    <?php
                                    $selected = ' ';
                                    if($row->User['t_agama_id'] == $row1['id']){
                                        $selected = 'selected';
                                    }
                                    ?>
                                    <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['agama']; ?> </option>
                                    ?>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label >Pekerjaan</label>
                            <select name="pekerjaan" id="" class="form-control" disabled>
                                @foreach ($pekerjaan as $row1)
                                    <?php
                                    $selected = ' ';
                                    if($row->User['t_pekerjaan_id'] == $row1['id']){
                                        $selected = 'selected';
                                    }
                                    ?>
                                    <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['pekerjaan']; ?> </option>
                                    ?>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label >Status Perkawinan</label>
                            <select name="status_perkawinan" id="" class="form-control" disabled>
                                @if ($row->User['statusperkawinan'] == "Belum Kawin")
                                <option <?php 'selected' ?> value="Belum Kawin"> Belum Kawin </option>
                                @elseif ($row->User['statusperkawinan'] == "Kawin")
                                <option <?php 'selected' ?> value="Kawin"> Kawin </option>
                                @else 
                                <option <?php 'selected' ?> value="Pernah Kawin"> Pernah Kawin </option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label >Dukuh</label>
                            <select name="dukuh" id="" class="form-control" disabled>
                                @foreach ($dukuh as $row1)
                                    <?php
                                    $selected = ' ';
                                    if($row->User['t_dukuh_id'] == $row1['id']){
                                        $selected = 'selected';
                                    }
                                    ?>
                                    <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['dukuh']; ?> </option>
                                    ?>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label >RT</label>
                            <select name="rt" id="" class="form-control" disabled>
                                <option value="{{ $row->User['rt'] }}">{{ $row->User['rt'] }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label >NIK</label>
                            <input type="text" class="form-control" id="inputName4" name="no_nik" placeholder="NIK" value="{{ $row->User['nik'] }}" disabled>
                        </div>
                        <div class="form-group col-md-6">
                            <label >Nomor KK</label>
                            <input type="text" class="form-control" id="inputName4" name="no_kk" placeholder="Nomor KK" value="{{ $row->User['no_kk'] }}" disabled>
                        </div>
                    </div>
                    <br>
                    @foreach ($pindahdatang as $pindahdatangs)
                        @csrf
                        <h3><b>DATA DAERAH ASAL</b></h3>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label >Kepindahan</label>
                                <select name="kepindahan" id="" class="form-control @error('agama') is-invalid @enderror" disabled>
                                    @foreach ($jeniskepindahan as $row1)
                                        <?php
                                        $selected = ' ';
                                        if($pindahdatangs['t_jeniskepindahans_id'] == $row1['id']){
                                            $selected = 'selected';
                                        }
                                        ?>
                                        <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['kepindahan']; ?> </option>
                                        ?>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Nama Kepala Keluarga</label>
                                <input type="text" name="nama_kepala_keluarga" class="form-control" placeholder="Nama Kepala Keluarga" value="{{ $pindahdatangs['kepala_keluarga'] }}" readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label>NIK Kepala Keluarga</label>
                                <input type="text" name="nama_kepala_keluarga" class="form-control" placeholder="Nama Kepala Keluarga" value="{{ $pindahdatangs['nik_kepalakeluarga'] }}" readonly>
                            </div>
                        </div>
                        @foreach ($tinggal as $tinggals)
                        @if ($tinggals->form_perpindahans_id == $pindahdatangs['id'])
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Alamat</label>
                                <input type="text" class="form-control" value="{{ $tinggals->dukuh }} Rt. {{ $tinggals->rt }} Rw. {{ $tinggals->rw }} {{ $tinggals->kelurahan }}, {{ $tinggals->kecamatan }}, {{ $tinggals->kabupaten }}, {{ $tinggals->provinsi }}" readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Kode Pos</label>
                                <input type="text" name="kode_pos_kepindahan" class="form-control" placeholder="Kode Pos" value="{{ $tinggals->kodepos }}" readonly>
                            </div>
                        </div>
                        {{-- <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Kabupaten</label>
                                <select name="kota_atau_kabupaten_kepindahan" id="" class="form-control" disabled>
                                    <option value="{{ $tinggals['kabupaten'] }}">{{ $tinggals['kabupaten'] }}</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Kecamatan</label>
                                <input type="text" name="kecamatan_kepindahan" class="form-control" placeholder="Kecamatan Tujuan" value="{{ $tinggals['kecamatan'] }}" readonly>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Kelurahan</label>
                                <input type="text" name="kelurahan_kepindahan" class="form-control" placeholder="Kelurahan Tujuan" value="{{ $tinggals['kelurahan'] }}" readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Dukuh</label>
                                <input type="text" name="dukuh_kepindahan" class="form-control" placeholder="Dukuh Tujuan" value="{{ $tinggals['dukuh'] }}" readonly>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label>RT</label>
                                <input type="text" name="rt_kepindahan" class="form-control" placeholder="RT Tujuan" value="{{ $tinggals['rt'] }}" readonly>
                            </div>
                            <div class="form-group col-md-3">
                                <label>RW</label>
                                <input type="text" name="rw_kepindahan" class="form-control" placeholder="RW Tujuan" value="{{ $tinggals['rw'] }}" readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Kode Pos</label>
                                <input type="text" name="kode_pos_kepindahan" class="form-control" placeholder="Kode Pos" value="{{ $tinggals['kodepos'] }}" readonly>
                            </div>
                        </div> --}}
                        @endif
                        @endforeach
                        <br>
                        <!-- DATA KEPINDAHAN -->
                        <h3><b>DATA DAERAH TUJUAN</b></h3>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Status Nomor KK Bagi Yang Pindah</label>
                                <select name="status_KK_keluarga_pindah" class="form-control" disabled>
                                    @if ($pindahdatangs['statuskk_pindah'] == "Numpang KK")
                                        <option <?php echo $selected = 'selected'; ?> value="Numpang KK">Numpang KK</option>
                                    @elseif ($pindahdatangs['statuskk_pindah'] == "Membuat KK Baru")
                                        <option <?php echo $selected = 'selected'; ?> value="Membuat KK Baru">Membuat KK Baru</option>
                                    @else
                                        <option <?php echo $selected = 'selected'; ?> value="Nomor KK Tetap">Nomor KK Tetap</option>
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Tanggal Kedatangan</label>
                                <input placeholder="Tanggal Lahir Ibu" class="form-control" name="tanggal_lahir_ibu" id="datepicker2" value="{{ Carbon\Carbon::parse($pindahdatangs['tanggal_kedatangan'])->translatedFormat('d F Y') }}" readonly>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Nomor Kartu Keluarga</label>
                                <input type="text" name="nomor_kk" class="inputnih form-control" placeholder="Nomor Kepala Keluarga" value="{{ $pindahdatangs->nomor_kk }}" readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label>NIK Kepala Keluarga</label>
                                <input type="text" name="nik_kepala_keluarga" class="form-control" placeholder="NIK Kepala Keluarga" value="{{ $pindahdatangs->nik_kepalakeluarga }}" readonly>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Dukuh</label>
                                <select name="dukuh_asal" id="dukuhnya_asal"
                                    class="form-control @error('dukuh') is-invalid @enderror" onclick="Reset()" disabled>
                                    <option value="">-- Pilih Dukuh --</option>
                                    @foreach ($dukuh as $row1)
                                    <?php
                                            $selected = ' ';
                                            if($pindahdatangs['t_dukuh_id'] == $row1['id']){
                                                $selected = 'selected';
                                            }
                                            ?>
                                    <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['dukuh']; ?>
                                    </option>
                                    ?>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label>RT</label>
                                <select name="carirtAsal" id="rtAsal" class="form-control @error('rt') is-invalid @enderror" disabled>
                                    <option value="{{ $pindahdatangs['rt'] }}">{{ $pindahdatangs['rt'] }}</option>
                                </select>
                                <input type="hidden" class="form-control @error('rt') is-invalid @enderror" id="rtnyaAsal"
                                    name="rt_asal">
                            </div>
                        </div>

                        <h3><b>Keluarga Yang Datang</b></h3>
                        <div class="table-responsive" style="padding-top: 10px">
                            <table class="display table table-hover" >
                                <thead>
                                    <tr>
                                        <th class="text-center" style="width:5%;"><b>No.</b></th>
                                        <th class="text-center" style="width:25%;"><b>NIK</b></th>
                                        <th class="text-center" style="width:50%;"><b>NAMA</b></th>
                                        <th class="text-center" style="width:20%;"><b>SHDK</b></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php
                                $no = 1;
                                @endphp
                                @foreach ($keluarga as $keluargas)
                                    @if ($keluargas->form_perpindahans_id == $pindahdatangs['id'])
                                        <tr>
                                            <td class="text-center">{{ $no++ }}</td>
                                            <td class="text-center">{{ $keluargas->nik }}</td>
                                            <td class="text-center">{{ $keluargas->nama }}</td>
                                            <td class="text-center">{{ $keluargas->shdk }}</td>
                                        </tr>
                                    @endif
                                @endforeach   
                                </tbody>         
                            </table>
                        </div>
                        
                        <!-- ----------- PELAPOR ----------- -->
                        <input type="hidden" class="form-control" name="id_user" placeholder="Nama Lengkap"
                            value="{{ Auth::user()->id }}">
                        <input type="hidden" name="keperluan" id="" value="Akta Kelahiran">
                        <input type="hidden" name="status" id="" value="proses">

                        <br>
                        <!-- ----------- UPLOAD PERSYARATAN ----------- -->
                        <h3><b>PERSYARATAN</b></h3>
                        <div class="form-row">
                            {{-- <div class="form-group col-md-4">
                                    <label >KTP Pelapor</label>
                                    <center padding-bottom: 100px;><img id="blah" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="auto" name="lampiran_ktp"/></center><br>
                                    <div class="custom-file">
                                        <input type="file" name="lampiran_KTP_pelapor" class="custom-file-input" id="customFile" onchange="readURL(this);">
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                                </div> --}}
                            {{-- <div class="form-group col-md-6">
                                    <label >Kartu Keluarga</label>
                                    <center padding-bottom: 100px;><img id="blah1" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="auto" name="lampiran_ktp"/></center><br>
                                    <div class="custom-file">
                                        <input type="file" name="lampiran_KK" class="custom-file-input" id="customFile" onchange="readURL1(this);">
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                                </div>  --}}
                            {{-- <div class="form-group col-md-6">
                                    <label >Akta Kelahiran Pelapor</label>
                                    <center padding-bottom: 100px;><img id="blah2" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="auto" name="lampiran_ktp"/></center><br>
                                    <div class="custom-file">
                                        <input type="file" name="lampiran_akta_kelahiran_pelapor" class="custom-file-input" id="customFile" onchange="readURL2(this);">
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                                </div>  --}}
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label >KTP Pelapor</label>
                                <center padding-bottom: 100px;><a href="{{ URL::to('public') }}/assets/lampiran/ktp_pelapor/{{ $row->ktp_pelapor }}" target="_blank"><img src="{{ URL::to('public') }}/assets/lampiran/ktp_pelapor/{{ $row->ktp_pelapor }}" height="150px" width="200px"></a></center><br>
                            </div> 

                            <div class="form-group col-md-3">
                                <label>Akta Kelahiran Pelapor</label>
                                <center padding-bottom: 100px;><a href="{{ URL::to('public') }}/assets/lampiran/akta_kelahiran/{{ $pindahdatangs['lamp_aktakelahiran'] }}" target="_blank"><img src="{{ URL::to('public') }}/assets/lampiran/akta_kelahiran/{{ $pindahdatangs['lamp_aktakelahiran'] }}" height="150px" width="200px"></a></center><br>
                            </div>
                            <div class="form-group col-md-3">
                                <label>Ijazah Terakhir Pelapor</label>
                                <center padding-bottom: 100px;><a href="{{ URL::to('public') }}/assets/lampiran/ijazah_terakhir/{{ $pindahdatangs['lamp_ijazahterakhir'] }}" target="_blank"><img src="{{ URL::to('public') }}/assets/lampiran/ijazah_terakhir/{{ $pindahdatangs['lamp_ijazahterakhir'] }}" height="150px" width="200px"></a></center><br>
                            </div>
                            @if ($pindahdatangs['lamp_aktanikah'] != null)
                            <div class="form-group col-md-3">
                                <label >Akta Nikah</label>
                                <center padding-bottom: 100px;><a href="{{ URL::to('public') }}/assets/lampiran/buku_nikah/{{ $pindahdatangs['lamp_aktanikah'] }}" target="_blank"><img src="{{ URL::to('public') }}/assets/lampiran/buku_nikah/{{ $pindahdatangs['lamp_aktanikah'] }}" height="150px" width="200px"></a></center><br>
                            </div>
                            @else
                                <div class="form-group col-md-3">
                                    <label >Akta Nikah</label><br>
                                    <center padding-bottom: 100px;><a href="{{ URL::to('public') }}/assets/lampiran/keterangan_kematian_RS/tidak_ada.png" target="_blank"><img src="{{ URL::to('public') }}/assets/lampiran/keterangan_kematian_RS/tidak_ada.png" height="150px" width="200px"></a></center><br>
                                </div>
                            @endif 
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label >Surat Pindah Dari Tempat Asal</label>
                                <center padding-bottom: 100px;><a href="{{ URL::to('public') }}/assets/lampiran/surat_pindah/{{ $pindahdatangs['lamp_suratpindah'] }}" target="_blank"><img src="{{ URL::to('public') }}/assets/lampiran/surat_pindah/{{ $pindahdatangs['lamp_suratpindah'] }}" height="150px" width="200px"></a></center><br>
                            </div> 
                        </div>
                        <br>
                        <h3><b>KETERANGAN DAN LAIN - LAIN</b></h3>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <textarea name="keterangan" class="form-control" id="exampleFormControlTextarea1" rows="3" readonly>{{ $row->keterangan }}</textarea>
                            </div>
                        </div>                        
                    @endforeach
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
