@extends('layouts.dashboard2')
@section('title', 'Dashboard | Lihat Verifikasi Keterangan Domisili')

<!-- TagPage -->
@section('tagPage')
<div class="page-header" style="color: white">
    <h4 class="page-title" style="color: white">Lihat Verifikasi Keterangan Domisili</h4>
    <ul class="breadcrumbs">
        <li class="nav-home">
            <a href="#">
                <i class="flaticon-home" style="color: white"></i>
            </a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="{{ '/dashboard-petugas' }}" style="color: white">Dashboard</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="" style="color: white">Layanan Surat</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="{{ '/dashboard-petugas/layanan/masuk' }}" style="color: white">Pengajuan Masuk</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="" style="color: white">Lihat Verifikasi Keterangan Domisili</a>
        </li>
    </ul>
</div>
@endsection
<!-- End TagPage -->

@section('descPage','Halaman ini digunakan untuk melihat detail pengajuan Keterangan Domisili')

@section('content')
<div class="page-inner mt--5">
    <div class="row mt--2">
        <div class="col-md-12">
            <div class="card full-height">
                <div class="card-body">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-check"></i> <strong>Berhasil! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Danger! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('warning'))
                    <div class="alert alert-warning">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-exclamation"></i> <strong>Warning! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('info'))
                    <div class="alert alert-info">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-info"></i> <strong>Info! &nbsp;&nbsp;</strong> 
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Bahaya! &nbsp;&nbsp;</strong>
                        Silakan periksa formulir di bawah ini untuk kesalahan <br>
                        <strong>
                            <ul>
                                @foreach ($errors->all() as $message)
                                    <li>{{$message}}</li>
                                @endforeach
                            </ul>
                        </strong>
                    </div>
                    @endif
                    @csrf
                    @foreach ($data as $row)
                    <h3><b>DATA PEMOHON</b></h3>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label >Nama Lengkap</label>
                            <input type="text" class="form-control" name="nama_lengkap" id="inputName4" placeholder="Nama Lengkap" value="{{ $row->User['name'] }}" disabled>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label >Tempat Lahir</label>
                            <input type="text" class="form-control" name="tempat_lahir" id="inputName4" placeholder="Tempat Lahir" value="{{ $row->User['name'] }}" disabled>
                        </div>
                        <div class="form-group col-md-6">
                            <label >Tanggal Lahir</label>
                            {{-- <input id="datepicker" name="tanggal_lahir"  placeholder="Tanggal Lahir" value="{{ uth::user()->tgl_lahir }}"> --}}
                            <div class="input-group">
                                <input type="text" class="form-control" name="datepicker" value="{{ $row->User['tgl_lahir'] }}" disabled>
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label >Jenis Kelamin</label>
                            <select name="jenis_kelamin" id="" class="form-control" disabled>
                                @if ($row->User['jeniskelamin'] ==  "Laki - laki")
                                <option <?php 'selected' ?> value= "Laki - laki"> Laki - Laki </option>
                                @else
                                <option <?php 'selected' ?> value="Perempuan"> Perempuan </option>
                                @endif
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Agama</label>
                            <select name="agama" id="" class="form-control" disabled>
                                @foreach ($agama as $row1)
                                    <?php
                                    $selected = ' ';
                                    if($row->User['t_agama_id'] == $row1['id']){
                                        $selected = 'selected';
                                    }
                                    ?>
                                    <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['agama']; ?> </option>
                                    ?>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label >Pekerjaan</label>
                            <select name="pekerjaan" id="" class="form-control" disabled>
                                @foreach ($pekerjaan as $row1)
                                    <?php
                                    $selected = ' ';
                                    if($row->User['t_pekerjaan_id'] == $row1['id']){
                                        $selected = 'selected';
                                    }
                                    ?>
                                    <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['pekerjaan']; ?> </option>
                                    ?>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label >Status Perkawinan</label>
                            <select name="status_perkawinan" id="" class="form-control" disabled>
                                @if ($row->User['statusperkawinan'] == "Belum Kawin")
                                <option <?php 'selected' ?> value="Belum Kawin"> Belum Kawin </option>
                                @elseif ($row->User['statusperkawinan'] == "Kawin")
                                <option <?php 'selected' ?> value="Kawin"> Kawin </option>
                                @else 
                                <option <?php 'selected' ?> value="Pernah Kawin"> Pernah Kawin </option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label >Dukuh</label>
                            <select name="dukuh" id="" class="form-control" disabled>
                                @foreach ($dukuh as $row1)
                                    <?php
                                    $selected = ' ';
                                    if($row->User['t_dukuh_id'] == $row1['id']){
                                        $selected = 'selected';
                                    }
                                    ?>
                                    <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['dukuh']; ?> </option>
                                    ?>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label >RT</label>
                            <select name="rt" id="" class="form-control" disabled>
                                <option value="{{ $row->User['rt'] }}">{{ $row->User['rt'] }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label >NIK</label>
                            <input type="text" class="form-control" id="inputName4" name="no_nik" placeholder="NIK" value="{{ $row->User['nik'] }}" disabled>
                        </div>
                        <div class="form-group col-md-6">
                            <label >Nomor KK</label>
                            <input type="text" class="form-control" id="inputName4" name="no_kk" placeholder="Nomor KK" value="{{ $row->User['no_kk'] }}" disabled>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label >KTP</label>
                            <center padding-bottom: 100px;><a href="{{ URL::to('public') }}/assets/lampiran/ktp_pelapor/{{ $row->ktp_pelapor }}" target="_blank"><img src="{{ URL::to('public') }}/assets/lampiran/ktp_pelapor/{{ $row->ktp_pelapor }}" height="250px" width="300px"></a></center><br>
                        </div> 
                        <div class="form-group col-md-6">
                            <label >Lampiran Surat Pengantar RT</label>
                            <center padding-bottom: 100px;><a href="{{ URL::to('public') }}/assets/lampiran/pengantar_RT/{{ $row->lamp_pengantarRT }}" target="_blank"><img src="{{ URL::to('public') }}/assets/lampiran/pengantar_RT/{{ $row->lamp_pengantarRT }}" height="250px" width="300px"></a></center><br>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>Keterangan</label>
                            <textarea name="keterangan" class="form-control" id="exampleFormControlTextarea1" rows="5" disabled>{{ $row->keterangan }}</textarea>
                        </div>
                    </div>                    
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
