@extends('layouts.dashboard2')
@section('title', 'Dashboard | Verifikasi Akta Kelahiran')

<!-- TagPage -->
@section('tagPage')
<div class="page-header" style="color: white">
    <h4 class="page-title" style="color: white">Verifikasi Akta Kelahiran</h4>
    <ul class="breadcrumbs">
        <li class="nav-home">
            <a href="#">
                <i class="flaticon-home" style="color: white"></i>
            </a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="{{ '/dashboard-petugas' }}" style="color: white">Dashboard</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="" style="color: white">Layanan Surat</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="{{ '/dashboard-petugas/layanan/masuk' }}" style="color: white">Pengajuan Masuk</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="" style="color: white">Verifikasi Akta Kelahiran</a>
        </li>
    </ul>
</div>
@endsection
<!-- End TagPage -->

@section('descPage','Halaman ini digunakan untuk mengelola pengajuan dan verifikasi Akta Kelahiran')

@section('content')
<div class="page-inner mt--5">
    <div class="row mt--2">
        <div class="col-md-12">
            <div class="card full-height">
                <div class="card-body">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-check"></i> <strong>Berhasil! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Danger! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('warning'))
                    <div class="alert alert-warning">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-exclamation"></i> <strong>Warning! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('info'))
                    <div class="alert alert-info">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-info"></i> <strong>Info! &nbsp;&nbsp;</strong> 
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Bahaya! &nbsp;&nbsp;</strong>
                        Silakan periksa formulir di bawah ini untuk kesalahan <br>
                        <strong>
                            <ul>
                                @foreach ($errors->all() as $message)
                                    <li>{{$message}}</li>
                                @endforeach
                            </ul>
                        </strong>
                    </div>
                    @endif

                    {{-- <form class="padding-top-10px margin-top-10px border-top-1 border-grey-1" method="post" enctype="multipart/form-data"> --}}
                        @csrf
                        @foreach ($data as $row)
                        <h3><b>DATA PEMOHON</b></h3>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label >Nama Lengkap</label>
                                <input type="text" class="form-control" name="nama_lengkap" id="inputName4" placeholder="Nama Lengkap" value="{{ $row->User['name'] }}" disabled>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Tempat Lahir</label>
                                <input type="text" class="form-control" name="tempat_lahir" id="inputName4" placeholder="Tempat Lahir" value="{{ $row->User['name'] }}" disabled>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Tanggal Lahir</label>
                                {{-- <input id="datepicker" name="tanggal_lahir"  placeholder="Tanggal Lahir" value="{{ uth::user()->tgl_lahir }}"> --}}
                                <div class="input-group">
                                    <input type="text" class="form-control" name="datepicker" value="{{ $row->User['tgl_lahir'] }}" disabled>
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Jenis Kelamin</label>
                                <select name="jenis_kelamin" id="" class="form-control" disabled>
                                    @if ($row->User['jeniskelamin'] ==  "Laki - laki")
                                    <option <?php 'selected' ?> value= "Laki - laki"> Laki - Laki </option>
                                    @else
                                    <option <?php 'selected' ?> value="Perempuan"> Perempuan </option>
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Agama</label>
                                <select name="agama" id="" class="form-control" disabled>
                                    @foreach ($agama as $row1)
                                        <?php
                                        $selected = ' ';
                                        if($row->User['t_agama_id'] == $row1['id']){
                                            $selected = 'selected';
                                        }
                                        ?>
                                        <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['agama']; ?> </option>
                                        ?>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Pekerjaan</label>
                                <select name="pekerjaan" id="" class="form-control" disabled>
                                    @foreach ($pekerjaan as $row1)
                                        <?php
                                        $selected = ' ';
                                        if($row->User['t_pekerjaan_id'] == $row1['id']){
                                            $selected = 'selected';
                                        }
                                        ?>
                                        <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['pekerjaan']; ?> </option>
                                        ?>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Status Perkawinan</label>
                                <select name="status_perkawinan" id="" class="form-control" disabled>
                                    @if ($row->User['statusperkawinan'] == "Belum Kawin")
                                    <option <?php 'selected' ?> value="Belum Kawin"> Belum Kawin </option>
                                    @elseif ($row->User['statusperkawinan'] == "Kawin")
                                    <option <?php 'selected' ?> value="Kawin"> Kawin </option>
                                    @else 
                                    <option <?php 'selected' ?> value="Pernah Kawin"> Pernah Kawin </option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Dukuh</label>
                                <select name="dukuh" id="" class="form-control" disabled>
                                    @foreach ($dukuh as $row1)
                                        <?php
                                        $selected = ' ';
                                        if($row->User['t_dukuh_id'] == $row1['id']){
                                            $selected = 'selected';
                                        }
                                        ?>
                                        <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['dukuh']; ?> </option>
                                        ?>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label >RT</label>
                                <select name="rt" id="" class="form-control" disabled>
                                    <option value="{{ $row->User['rt'] }}">{{ $row->User['rt'] }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >NIK</label>
                                <input type="text" class="form-control" id="inputName4" name="no_nik" placeholder="NIK" value="{{ $row->User['nik'] }}" disabled>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Nomor KK</label>
                                <input type="text" class="form-control" id="inputName4" name="no_kk" placeholder="Nomor KK" value="{{ $row->User['no_kk'] }}" disabled>
                            </div>
                        </div>
                        <br>
                        @foreach ($kelahiran as $kelahirans)
                        <h3><b>DATA BAYI / ANAK</b></h3>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label >Nama Lengkap</label>
                                <input type="text" name="nama_anak" class="form-control" placeholder="Nama Lengkap" value="{{ $kelahirans['nama'] }}" readonly>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Jenis Kelamin</label>
                                <select name="jeniskelamin" id="" class="form-control" disabled>
                                    @if ($kelahirans['jeniskelamin'] == "Laki - laki")
                                        <option <?php echo $selected = 'selected'; ?> value="Laki - laki">Laki - laki</option>
                                    @elseif ($kelahirans['jeniskelamin'] == "Perempuan")
                                        <option <?php echo $selected = 'selected'; ?> value="Perempuan">Perempuan</option>
                                    @else
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Tempat Dilahirkan</label>
                                <select name="tempat_lahir" id="" class="form-control" disabled>
                                    @if ($kelahirans['tempat_lahir'] == "Rumah Sakit / Rumah Bersalin")
                                        <option <?php echo $selected = 'selected'; ?> value="Rumah Sakit / Rumah Bersalin">Rumah Sakit / Rumah Bersalin</option>
                                    @elseif ($kelahirans['tempat_lahir'] == "Puskesmas")
                                        <option <?php echo $selected = 'selected'; ?> value="Puskesmas">Puskesmas</option>
                                    @elseif ($kelahirans['tempat_lahir'] == "Polides")
                                        <option <?php echo $selected = 'selected'; ?> value="Polides">Polides</option>
                                    @elseif ($kelahirans['tempat_lahir'] == "Rumah")
                                        <option <?php echo $selected = 'selected'; ?> value="Rumah">Rumah</option>
                                    @else
                                        <option <?php echo $selected = 'selected'; ?> value="Lainnya">Lainnya</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Tanggal Lahir</label>
                                <input placeholder="Tanggal Lahir" class="form-control" name="tanggal_lahir" id="datepicker" value="{{ Carbon\Carbon::parse($kelahirans['tgl_lahir'])->translatedFormat('d F Y') }}" readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Pukul</label>
                                <input id="timepicker" class="form-control" name="pukul_kelahiran" placeholder="Pukul Kelahiran" value="{{ Carbon\Carbon::parse($kelahirans['pukul'])->format('H:i') }}" readonly/>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Nomor / Tempat Kelahiran</label>
                                <input type="text" name="no_atau_tempat_kelahiran" class="form-control" placeholder="Nomor / Tempat Kelahiran" value="{{ $kelahirans['nomor_tempat_kelahiran'] }}" readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Jenis Kelahiran</label>
                                <select name="jenis_kelahiran" id="" class="form-control" disabled>
                                    @if ($kelahirans['jeniskelahiran'] == "Tunggal")
                                        <option <?php echo $selected = 'selected'; ?> value="Tunggal">Tunggal</option>
                                    @elseif ($kelahirans['jeniskelahiran'] == "Kembar")
                                        <option <?php echo $selected = 'selected'; ?> value="Kembar">Kembar</option>
                                    @elseif ($kelahirans['jeniskelahiran'] == "Kembar 3")
                                        <option <?php echo $selected = 'selected'; ?> value="Kembar 3">Kembar 3</option>
                                    @elseif ($kelahirans['jeniskelahiran'] == "Kembar 4")
                                        <option <?php echo $selected = 'selected'; ?> value="Kembar 4">Kembar 4</option>
                                    @else
                                        <option <?php echo $selected = 'selected'; ?> value="Lainnya">Lainnya</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Kelahiran Ke</label>
                                <input type="number" name="kelahiran_ke" min="1" max="10000.00" step="1" class="form-control" placeholder="Kelahiran Ke" value="{{ $kelahirans['kelahiran_ke'] }}" readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Penolong Kelahiran</label>
                                <select name="penolong_kelahiran" id="" class="form-control" disabled>
                                    @if ($kelahirans['penolong_kelahiran'] == "Dokter")
                                        <option <?php echo $selected = 'selected'; ?> value="Dokter">Dokter</option>
                                    @elseif ($kelahirans['penolong_kelahiran'] == "Bidan/Perawat")
                                        <option <?php echo $selected = 'selected'; ?> value="Bidan/Perawat">Bidan/Perawat</option>
                                    @elseif ($kelahirans['penolong_kelahiran'] == "Dukun")
                                        <option <?php echo $selected = 'selected'; ?> value="Dukun">Dukun</option>
                                    @else
                                        <option <?php echo $selected = 'selected'; ?> value="Lainnya">Lainnya</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Berat Lahir (Kg)</label>
                                <input type="text" name="berat_bayi" class="form-control" placeholder="Berat Lahir Contoh: 2.5" value="{{ $kelahirans['berat'] }}" readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Panjang Lahir (Cm)</label>
                                <input type="number" name="tinggi_bayi" min="1" max="10000.00" step="1" class="form-control" placeholder="Panjang Lahir" value="{{ $kelahirans['tinggi'] }}" readonly>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-6">
                                <!-- ----------- Data IBU ----------- -->
                                <h3><b>DATA IBU</b></h3>
                                @foreach ($ibu as $ibus)
                                @if ($ibus->form_kelahirans_id == $kelahirans['id'])
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label >NIK</label>
                                        <input type="text" name="nik_ibu" class="form-control" placeholder="NIK Ibu" value="{{ $ibus['nik'] }}" readonly>
                                    </div>
                                </div>
                               <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label >Nama Lengkap</label>
                                        <input type="text" name="nama_ibu" class="form-control" placeholder="Nama Lengkap Ibu" value="{{ $ibus['nama'] }}" readonly>
                                    </div>
                               </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label >Tanggal Lahir</label>
                                        <input placeholder="Tanggal Lahir Ibu" class="form-control" name="tanggal_lahir_ibu" id="datepicker2" value="{{ Carbon\Carbon::parse($ibus['tgl_lahir'])->translatedFormat('d F Y') }}" readonly>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label >Pekerjaan</label>
                                        <select id="" name="pekerjaan_ibu" class="form-control" disabled>
                                            {{-- <option value="">-- Pilih Pekerjaan --</option> --}}
                                            @foreach ($pekerjaan as $row1)
                                                <?php
                                                $selected = ' ';
                                                if($ibus['t_pekerjaan_id'] == $row1['id']){
                                                    $selected = 'selected';
                                                }
                                                ?>
                                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['pekerjaan']; ?> </option>
                                                ?>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label >Alamat</label>
                                        <input class="form-control" placeholder="Dukuh" name="dukuh_ibu"  value="{{ $ibus['dukuh'] }} Rt. {{ $ibus['rt'] }} {{ $ibus['kelurahan'] }}, {{ $ibus['kecamatan'] }}, {{ $ibus['kabupaten'] }}" readonly>
                                    </div>
                                </div>
                                @endif
                                @endforeach
                            </div>
                            <div class="col-lg-6">
                                <!-- ----------- Data AYAH ----------- -->
                                <h3><b>DATA AYAH</b></h3>
                                @foreach ($ayah as $ayahs)
                                @if ($ayahs->form_kelahirans_id == $kelahirans['id'])
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label >NIK</label>
                                        <input type="text" name="nik_ibu" class="form-control" placeholder="NIK Ibu" value="{{ $ayahs['nik'] }}" readonly>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label >Nama Lengkap</label>
                                        <input type="text" name="nama_ibu" class="form-control" placeholder="Nama Lengkap Ibu" value="{{ $ayahs['nama'] }}" readonly>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label >Tanggal Lahir</label>
                                        <input placeholder="Tanggal Lahir Ibu" class="form-control" name="tanggal_lahir_ibu" id="datepicker2" value="{{ Carbon\Carbon::parse($ayahs['tgl_lahir'])->translatedFormat('d F Y') }}" readonly>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label >Pekerjaan</label>
                                        <select id="" name="pekerjaan_ibu" class="form-control" disabled>
                                            {{-- <option value="">-- Pilih Pekerjaan --</option> --}}
                                            @foreach ($pekerjaan as $row1)
                                                <?php
                                                $selected = ' ';
                                                if($ayahs['t_pekerjaan_id'] == $row1['id']){
                                                    $selected = 'selected';
                                                }
                                                ?>
                                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['pekerjaan']; ?> </option>
                                                ?>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label >Alamat</label>
                                        <input class="form-control" placeholder="Dukuh" name="dukuh_ibu"  value="{{ $ayahs['dukuh'] }} Rt. {{ $ayahs['rt'] }} {{ $ayahs['kelurahan'] }}, {{ $ayahs['kecamatan'] }}, {{ $ayahs['kabupaten'] }}" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label >Tanggal Pencatatan Nikah</label>
                                <input placeholder="Tanggal Pencatatan Nikah" class="form-control" name="tanggal_pencatatan_nikah" id="datepicker4" value="{{ Carbon\Carbon::parse($ayahs['tgl_pencatatankawin'])->translatedFormat('d F Y') }}" readonly>
                            </div>
                        </div>
                        @endif
                        @endforeach
                        <br>
                        <div class="row">
                            <div class="col-lg-6">
                                <!-- ----------- Data SAKSI I ----------- -->
                                <h3><b>DATA SAKSI I</b></h3>
                                @foreach ($saksi1 as $saksi1s)
                                @if ($saksi1s->form_kelahirans_id == $kelahirans['id'])
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label >NIK</label>
                                        <input type="text" name="nik_saksi1" class="form-control" placeholder="NIK Saksi I" value="{{ $saksi1s['nik'] }}" readonly>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label >Nama Lengkap</label>
                                        <input type="text" name="nama_saksi1" class="form-control" placeholder="Nama Lengkap Saksi I" value="{{ $saksi1s['nama'] }}" readonly>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label >Tanggal Lahir</label>
                                        <input placeholder="Tanggal Lahir Saksi I" class="form-control" name="tanggal_lahir_saksi1" id="datepicker5" value="{{ Carbon\Carbon::parse($saksi1s['tgl_lahir'])->translatedFormat('d F Y') }}" readonly>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label >Jenis Kelamin</label>
                                        <select name="jeniskelamin" id="" class="form-control" disabled>
                                            @if ($saksi1s['jeniskelamin'] == "Laki - laki")
                                                <option <?php echo $selected = 'selected'; ?> value="Laki - laki">Laki - laki</option>
                                            @elseif ($saksi1s['jeniskelamin'] == "Perempuan")
                                                <option <?php echo $selected = 'selected'; ?> value="Perempuan">Perempuan</option>
                                            @else
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label >Pekerjaan</label>
                                        <select id="" name="pekerjaan_ibu" class="form-control" disabled>
                                            {{-- <option value="">-- Pilih Pekerjaan --</option> --}}
                                            @foreach ($pekerjaan as $row1)
                                                <?php
                                                $selected = ' ';
                                                if($saksi1s['t_pekerjaan_id'] == $row1['id']){
                                                    $selected = 'selected';
                                                }
                                                ?>
                                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['pekerjaan']; ?> </option>
                                                ?>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label >Dukuh</label>
                                        <select name="dukuh_saksi1" id="dukuhnya_saksi1" class="form-control @error('dukuh') is-invalid @enderror" onclick="Reset()" disabled>
                                            @foreach ($dukuh as $row2)
                                                <?php
                                                $selected = ' ';
                                                if($saksi1s['t_dukuh_id'] == $row2['id']){
                                                    $selected = 'selected';
                                                }
                                                ?>
                                                <option <?php echo $selected; ?> value="{{ $row2->id }}"> <?php echo $row2['dukuh']; ?> </option>
                                                ?>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label >RT</label>
                                        <select name="carirtSaksi1" id="rtSaksi1" class="form-control @error('rt') is-invalid @enderror" disabled>
                                            <option value="{{ $saksi1s['rt'] }}">{{ $saksi1s['rt'] }}</option>
                                        </select>
                                        <input type="hidden" class="form-control @error('rt') is-invalid @enderror" id="rtnyaSaksi1" name="rt_saksi1">
                                    </div>
                                </div>
                                @endif
                                @endforeach
                            </div>
                            <div class="col-lg-6">
                                <!-- ----------- Data SAKSI II ----------- -->
                                <h3><b>DATA SAKSI II</b></h3>
                                @foreach ($saksi2 as $saksi2s)
                                @if ($saksi2s->form_kelahirans_id == $kelahirans['id'])
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label >NIK</label>
                                        <input type="text" name="nik_saksi1" class="form-control" placeholder="NIK Saksi I" value="{{ $saksi2s['nik'] }}" readonly>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label >Nama Lengkap</label>
                                        <input type="text" name="nama_saksi1" class="form-control" placeholder="Nama Lengkap Saksi I" value="{{ $saksi2s['nama'] }}" readonly>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label >Tanggal Lahir</label>
                                        <input placeholder="Tanggal Lahir Saksi I" class="form-control" name="tanggal_lahir_saksi1" id="datepicker5" value="{{ Carbon\Carbon::parse($saksi2s['tgl_lahir'])->translatedFormat('d F Y') }}" readonly>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label >Jenis Kelamin</label>
                                        <select name="jeniskelamin" id="" class="form-control" disabled>
                                            @if ($saksi2s['jeniskelamin'] == "Laki - laki")
                                                <option <?php echo $selected = 'selected'; ?> value="Laki - laki">Laki - laki</option>
                                            @elseif ($saksi2s['jeniskelamin'] == "Perempuan")
                                                <option <?php echo $selected = 'selected'; ?> value="Perempuan">Perempuan</option>
                                            @else
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label >Pekerjaan</label>
                                        <select id="" name="pekerjaan_ibu" class="form-control" disabled>
                                            {{-- <option value="">-- Pilih Pekerjaan --</option> --}}
                                            @foreach ($pekerjaan as $row1)
                                                <?php
                                                $selected = ' ';
                                                if($saksi2s['t_pekerjaan_id'] == $row1['id']){
                                                    $selected = 'selected';
                                                }
                                                ?>
                                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['pekerjaan']; ?> </option>
                                                ?>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label >Dukuh</label>
                                        <select name="dukuh_saksi1" id="dukuhnya_saksi1" class="form-control @error('dukuh') is-invalid @enderror" onclick="Reset()" disabled>
                                            @foreach ($dukuh as $row2)
                                                <?php
                                                $selected = ' ';
                                                if($saksi2s['t_dukuh_id'] == $row2['id']){
                                                    $selected = 'selected';
                                                }
                                                ?>
                                                <option <?php echo $selected; ?> value="{{ $row2->id }}"> <?php echo $row2['dukuh']; ?> </option>
                                                ?>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label >RT</label>
                                        <select name="carirtSaksi1" id="rtSaksi1" class="form-control @error('rt') is-invalid @enderror" disabled>
                                            <option value="{{ $saksi2s['rt'] }}">{{ $saksi2s['rt'] }}</option>
                                        </select>
                                        <input type="hidden" class="form-control @error('rt') is-invalid @enderror" id="rtnyaSaksi1" name="rt_saksi1">
                                    </div>
                                </div>
                                @endif
                                @endforeach
                            </div>
                        </div>

                        <!-- ----------- PELAPOR ----------- -->
                        <input type="hidden" class="form-control" name="id_user" placeholder="Nama Lengkap" value="{{ Auth::user()->id }}">
                        <input type="hidden" name="keperluan" id="" value="Akta Kelahiran">
                        <input type="hidden" name="status" id="" value="proses">
                        <br>
                        <!-- ----------- UPLOAD PERSYARATAN ----------- -->
                        <h3><b>PERSYARATAN</b></h3>
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label >KTP Pelapor</label>
                                <center padding-bottom: 100px;><a href="{{ URL::to('public') }}/assets/lampiran/ktp_pelapor/{{ $row->ktp_pelapor }}" target="_blank"><img src="{{ URL::to('public') }}/assets/lampiran/ktp_pelapor/{{ $row->ktp_pelapor }}" height="150px" width="200px"></a></center><br>
                            </div> 
                            @foreach ($ibu as $ibus)
                            @if ($ibus->form_kelahirans_id == $kelahirans['id'])
                            <div class="form-group col-md-3">
                                <label >KTP Ibu</label>
                                <center padding-bottom: 100px;><a href="{{ URL::to('public') }}/assets/lampiran/ktp_ibu/{{ $ibus['lamp_ktp'] }}" target="_blank"><img src="{{ URL::to('public') }}/assets/lampiran/ktp_ibu/{{ $ibus['lamp_ktp'] }}" height="150px" width="200px"></a></center><br>
                            </div>
                            @endif
                            @endforeach

                            @foreach ($ayah as $ayahs)
                            @if ($ayahs->form_kelahirans_id == $kelahirans['id'])
                            <div class="form-group col-md-3">
                                <label >KTP Ayah</label>
                                <center padding-bottom: 100px;><a href="{{ URL::to('public') }}/assets/lampiran/ktp_ayah/{{ $ayahs['lamp_ktp'] }}" target="_blank"><img src="{{ URL::to('public') }}/assets/lampiran/ktp_ayah/{{ $ayahs['lamp_ktp'] }}" height="150px" width="200px"></a></center><br>
                            </div> 
                            <div class="form-group col-md-3">
                                <label >Buku Nikah</label>
                                <center padding-bottom: 100px;><a href="{{ URL::to('public') }}/assets/lampiran/buku_nikah/{{ $ayahs['lamp_bukunikah'] }}" target="_blank"><img src="{{ URL::to('public') }}/assets/lampiran/buku_nikah/{{ $ayahs['lamp_bukunikah'] }}" height="150px" width="200px"></a></center><br>
                            </div> 
                        </div>
        
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label >Kartu Keluarga</label>
                                <center padding-bottom: 100px;><a href="{{ URL::to('public') }}/assets/lampiran/kartu_keluarga/{{ $ayahs['lamp_kk'] }}" target="_blank"><img src="{{ URL::to('public') }}/assets/lampiran/kartu_keluarga/{{ $ayahs['lamp_kk'] }}" height="150px" width="200px"></a></center><br>
                            </div> 
                            @endif
                            @endforeach

                            @foreach ($saksi1 as $saksi1s)
                            @if ($saksi1s->form_kelahirans_id == $kelahirans['id'])
                            <div class="form-group col-md-3">
                                <label >KTP Saksi I</label>
                                <center padding-bottom: 100px;><a href="{{ URL::to('public') }}/assets/lampiran/saksi_I/{{ $saksi1s['lamp_ktp'] }}" target="_blank"><img src="{{ URL::to('public') }}/assets/lampiran/saksi_I/{{ $saksi1s['lamp_ktp'] }}" height="150px" width="200px"></a></center><br>
                            </div>
                            @endif
                            @endforeach

                            @foreach ($saksi2 as $saksi2s)
                            @if ($saksi2s->form_kelahirans_id == $kelahirans['id'])
                            <div class="form-group col-md-3">
                                <label >KTP Saksi II</label>
                                <center padding-bottom: 100px;><a href="{{ URL::to('public') }}/assets/lampiran/saksi_II/{{ $saksi2s['lamp_ktp'] }}" target="_blank"><img src="{{ URL::to('public') }}/assets/lampiran/saksi_II/{{ $saksi2s['lamp_ktp'] }}" height="150px" width="200px"></a></center><br>
                            </div> 
                            @endif
                            @endforeach

                            <div class="form-group col-md-3">
                                <label >Keterangan Lahir dari RS</label>
                                <center padding-bottom: 100px;><a href="{{ URL::to('public') }}/assets/lampiran/keterangan_RS/{{ $kelahirans['lamp_KeteranganRS'] }}" target="_blank"><img src="{{ URL::to('public') }}/assets/lampiran/keterangan_RS/{{ $kelahirans['lamp_KeteranganRS'] }}" height="150px" width="200px"></a></center><br>
                            </div> 
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label >Surat Pengantar RT</label>
                                <center padding-bottom: 100px;><a href="{{ URL::to('public') }}/assets/lampiran/pengantar_RT/{{ $row->lamp_pengantarRT }}" target="_blank"><img src="{{ URL::to('public') }}/assets/lampiran/pengantar_RT/{{ $row->lamp_pengantarRT }}" height="150px" width="200px"></a></center><br>
                            </div> 
                        </div>
                        <br>
                        <h3><b>KETERANGAN DAN LAIN - LAIN</b></h3>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <textarea name="keterangan" class="form-control" id="exampleFormControlTextarea1" rows="3" readonly>{{ $row->keterangan }}</textarea>
                            </div>
                        </div>
                        <button data-toggle="modal" data-target="#aktakelahiran{{ $row->id }}" class="btn btn-primary btn-lg btn-block background-main-color text-white text-center font-weight-bold text-uppercase rounded-0 padding-15px">VERIFIKASI PENGAJUAN</button>
                        @endforeach
                        @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

@foreach ($data as $rowEdit)
<div class="modal fade bd-example-modal-lg" id="aktakelahiran{{ $rowEdit->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <form action="{{url('dashboard-petugas/layanan/masuk/update-kelahiran', [$rowEdit->id])}}" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id_pengajuan" value="{{ $rowEdit->id_pengajuan }}">
        <input type="hidden" name="email" value="{{ $rowEdit->User['email'] }}">
        <div class="modal-dialog modal-lg" style="; ">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="page-title" id="myModalLabel">Verifikasi Surat Keterangan Akta Kelahiran</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-warning">
                        <i class="fa fa-exclamation"></i> <strong>Perhatian! &nbsp;&nbsp;</strong>
                        <strong>Cek pengajuan kembali, pastikan data dan persyaratan sudah lengkap dan sesuai !</strong>
                    </div><br>
                    <div class="form-group">
                        <label >Nomor Surat</label>
                        <input type="text" style="border-color:#06a5d6;" class="form-control" name="no_surat" id="inputName4" placeholder="Nomor Surat" required autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                        <textarea name="keterangan"  style="border-color:#06a5d6" class="form-control" id="exampleFormControlTextarea1" rows="3" required>{{ $rowEdit->keterangan }}</textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success"><i class="fas fa-check-circle"></i>&nbsp; Verifikasi Pengajuan</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endforeach

@endsection
