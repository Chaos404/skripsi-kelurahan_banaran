@extends('layouts.dashboard2')
@section('title', 'Kelurahan Banaran | Dashboard Petugas')

@section('style')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/echarts/4.1.0/echarts.min.js"></script>
@endsection

<!-- Tagpage -->
@section('tagPage')
<div class="page-header" style="color: white">
    <h4 class="page-title" style="color: white">Dashboard Petugas</h4>
</div>
@endsection
<!-- // Tagpage -->

@section('content')
<div class="page-inner mt--5">
    <div class="row mt--2">
        <div class="col-md-12">
            <div class="row">
                <div class="col-sm-6 col-lg-4">
                    <a href="">
                        <div class="card p-4">
                            <div class="d-flex align-items-center">
                                <span class="stamp stamp-md bg-secondary mr-3">
                                    <i class="fas fa-user-cog"></i>
                                </span>
                                <div>
                                    <h5 class="mb-1"><b><a href="#">{{ $masyarakat->count() }} <small>Masyarakat</small></a></b></h5>
                                    {{-- <small class="text-muted">12 waiting payments</small> --}}
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="card p-4">
                        <div class="d-flex align-items-center">
                            <span class="stamp stamp-md bg-success mr-3">
                                <i class="far fa-newspaper"></i>
                            </span>
                            <div>
                                <h5 class="mb-1"><b><a href="#">{{ $pengajuan_surat->count() }} <small>Surat Terverifikasi</small></a></b></h5>
                                {{-- <small class="text-muted">32 shipped</small> --}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="card p-4">
                        <div class="d-flex align-items-center">
                            <span class="stamp stamp-md bg-danger mr-3">
                                <i class="fa fa-tag"></i>
                            </span>
                            <div>
                                <h5 class="mb-1"><b><a href="#">{{ $pengajuan_umkm->count() }} <small>UMKM Terverifikasi</small></a></b></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card p-3">
        <div class="row mt--2">
            <div class="col-md-6 col-md-offset-1">            
                <div class="panel panel-default">                
                    <h6 class="page-title" style="color: #146ee1">Pengguna Terdaftar {{ Carbon\Carbon::now()->year }}</h6>                    
                    <div class="panel-body">
                        <div id="chartpengguna"></div>
                    </div>
                </div>            
            </div>   
            <script src="https://code.highcharts.com/highcharts.js"></script>
            <script type="text/javascript">
                var masyarakatChart =  {{ $masyarakatChart }};
            
                Highcharts.chart('chartpengguna', {
                    title: {
                        text: 'Pengguna Terverifikasi'
                    },
                    xAxis: {
                        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                    },
                    yAxis: {
                        title: {
                            text: 'Jumlah Masyarakat Terverifikasi',
                        },
                        allowDecimals: false
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },
                    plotOptions: {
                        series: {
                            allowPointSelect: true
                        }
                    },
                    series: [{
                        name: 'Pengguna Baru',
                        data: masyarakatChart
                    }],
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 200
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                    }
                });
            </script>

            <div class="col-md-6 col-md-offset-1">            
                <div class="panel panel-default">                
                    <h6 class="page-title" style="color: #146ee1">UMKM Terdaftar {{ Carbon\Carbon::now()->year }}</h6>                   
                    <div class="panel-body">
                        <div id="chartumkm"></div>
                    </div>
                </div>            
            </div>   
            <script type="text/javascript">
                var umkm =  {{ $umkm }};
            
                Highcharts.chart('chartumkm', {
                    title: {
                        text: 'UMKM Terverifikasi'
                    },
                    xAxis: {
                        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                    },
                    yAxis: {
                        title: {
                            text: 'Jumlah UMKM Terverifikasi'
                        },
                        allowDecimals: false
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },
                    plotOptions: {
                        series: {
                            allowPointSelect: true
                        }
                    },
                    series: [{
                        name: 'UMKM Baru',
                        data: umkm
                    }],
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 200
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                    }
                });
            </script>
        </div>
    </div>

    <div class="card p-3">    
        <div class="row  mt--2">        
            <div class="col-md-8 col-md-offset-1">            
                <div class="panel panel-default">                
                    <h6 class="page-title" style="color: #146ee1">Pengajuan Surat</h6>                    
                    <div class="panel-body">
                        <canvas id="canvas"></canvas>
                    </div>
                </div>            
            </div> 
            <div class="col-md-4 my-auto col-md-offset-1">            
                <div class="panel panel-default">                                 
                    <div class="panel-body">
                        <div id="chartPie" style="height: 350px;"></div>
                    </div>
                </div>            
            </div>        
        </div>
    </div>
    
    <!-- CHART DIAGRAM PER BULAN -->
    <script>
        var month = {{ $month }};
        var kelahiran = {{ $surat1 }};
        var kematian = {{ $surat2 }};
        var ktp = {{ $surat5 }};
        var kehilangan = {{ $surat6 }};
        var domisili = {{ $surat7 }};
        var keluar = {{ $surat3 }};
        var datang = {{ $surat4 }};

        var barChartData = {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            datasets: [{
                label: 'Kelahiran',
                backgroundColor: "#e5cf0d",
                data: kelahiran
            }, {
                label: 'Kematian',
                backgroundColor: "#b6a2de",
                data: kematian
            }, {
                label: 'KTP',
                backgroundColor: "#5ab1ef",
                data: ktp
            }, {
                label: 'Kehilangan',
                backgroundColor: "#ffb980",
                data: kehilangan
            }, {
                label: 'Domisili',
                backgroundColor: "#d87a80",
                data: domisili
            }, {
                label: 'Keluar',
                backgroundColor: "#8d98b3",
                data: keluar
            }, {
                label: 'Datang',
                backgroundColor: "#e5cf0d",
                data: datang
            }]
        };


        window.onload = function() {
            var ctx = document.getElementById("canvas").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: barChartData,
                options: {
                    elements: {
                        rectangle: {
                            borderWidth: 2,
                            borderColor: 'rgba(73,164,250,0.5)',
                            borderSkipped: 'bottom'
                        }
                    },
                    responsive: true,
                    title: {
                        display: true,
                        text: 'Pengajuan Surat Terverifikasi'
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                userCallback: function(label, index, labels) {
                                    // when the floored value is the same as the value we have a whole number
                                    if (Math.floor(label) === label) {
                                        return label;
                                    }

                                },
                            }
                        }],
                    }
                }
            });


        };
    </script>

    <!-- PRESENTASE PIE DIAGRAM -->
    <script>
        var pie_basic_element = document.getElementById('chartPie');
        if (pie_basic_element) {
            var pie_basic = echarts.init(pie_basic_element);
            pie_basic.setOption({
                color: [
                    '#2ec7c9','#b6a2de','#5ab1ef','#ffb980','#d87a80',
                    '#8d98b3','#e5cf0d','#97b552','#95706d','#dc69aa',
                    '#07a2a4','#9a7fd1','#588dd5','#f5994e','#c05050',
                    '#59678c','#c9ab00','#7eb00a','#6f5553','#c14089'
                ],          
                
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                title: {
                    text: 'Presentase',
                    left: 'center',
                    textStyle: {
                        fontSize: 16,
                        fontWeight: 500
                    },
                    subtextStyle: {
                        fontSize: 12
                    }
                },

                tooltip: {
                    trigger: 'item',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 12,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    formatter: "{a} <br/>{b}: {c} ({d}%)"
                },

                series: [{
                    name: 'Pengajuan Surat',
                    type: 'pie',
                    radius: '65%',
                    center: ['50%', '50%'],
                    itemStyle: {
                        normal: {
                            borderWidth: 1,
                            borderColor: '#fff'
                        }
                    },
                    data: [
                        {value: {{$all1}}, name: 'Kelahiran'},
                        {value: {{$all2}}, name: 'Kematian'},
                        {value: {{$all5}}, name: 'KTP'},
                        {value: {{$all6}}, name: 'Kehilangan'},
                        {value: {{$all7}}, name: 'Domisili'},
                        {value: {{$all3}}, name: 'Keluar'},
                        {value: {{$all4}}, name: 'Datang'}
                    ]
                }]
            });
        }
    </script>
    
</div>
@endsection


