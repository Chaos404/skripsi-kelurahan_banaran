@extends('layouts.dashboard2')
@section('title', 'Dashboard | UMKM Terverifikasi')

<!-- TagPage -->
@section('tagPage')
<div class="page-header" style="color: white">
    <h4 class="page-title" style="color: white">UMKM Terverifikasi</h4>
    <ul class="breadcrumbs">
        <li class="nav-home">
            <a href="#">
                <i class="flaticon-home" style="color: white"></i>
            </a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="{{ '/dashboard-petugas' }}" style="color: white">Dashboard</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="" style="color: white">UMKM Terverifikasi</a>
        </li>
    </ul>
</div>
@endsection
<!-- End TagPage -->

@section('descPage','Halaman ini digunakan untuk melihat pendaftaran UMKM dari masyarakat yang berhasil terverifikasi')

@section('content')
<div class="page-inner mt--5">
    <div class="row mt--2">
        <div class="col-md-12">
            <div class="card full-height">
                <div class="card-body">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-check"></i> <strong>Berhasil! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Danger! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('warning'))
                    <div class="alert alert-warning">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-exclamation"></i> <strong>Warning! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('info'))
                    <div class="alert alert-info">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-info"></i> <strong>Info! &nbsp;&nbsp;</strong> 
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Bahaya! &nbsp;&nbsp;</strong>
                        Silakan periksa formulir di bawah ini untuk kesalahan <br>
                        <strong>
                            <ul>
                                @foreach ($errors->all() as $message)
                                    <li>{{$message}}</li>
                                @endforeach
                            </ul>
                        </strong>
                    </div>
                    @endif

                    <div class="table-responsive" style="padding-top: 10px">
                        <table id="basic-datatables" class="display table table-striped table-hover" >
                            <thead>
                                <tr>
                                    <th class="text-center" width="3%">#</th>
                                    <th class="text-center" width="15%">Merk Usaha</th>
                                    <th class="text-center" width="20%">Nama Pemilik</th>
                                    <th class="text-center" width="10%">Nomor SIUP</th>
                                    <th class="text-center" width="10%">Nomor NPWP</th>
                                    <th class="text-center" width="10%">Status</th>
                                    <th class="text-center" width="20%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                            $no = 1;
                            @endphp
                            @foreach ($umkm as $row)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td class="text-center">{{ $row->merk_usaha }}</td>
                                    <td class="text-center">{{ $row->User['name'] }}</td>   
                                    <td class="text-center">{{ $row->no_siup }}</td>    
                                    <td class="text-center">{{ $row->no_npwp }}</td>    
                                    <td class="text-center">
                                        @if ($row->status == "proses")
                                        <span class="badge badge-danger">Baru</span>
                                        @else 
                                        <span class="badge badge-success">Terverifikasi</span>
                                        @endif
                                    </td>  
                                    <td class="text-center">
                                        <a data="{{$row->id}}" id="edit" data-toggle="modal" data-target="#dataEdit{{ $row->id }}" class="btn btn-sm btn-info"><i class="fas fa-eye" style="color: white;" ></i></a>
                                        {{-- &nbsp;&nbsp;
                                        <a href="{{url('dashboard-petugas/layanan/masuk/delete', [$row->id])}}" onclick="return confirm('Are you sure to delete this?')" class="btn btn-sm btn-danger" height:100px><i class="far fa-trash-alt"></i></a> --}}
                                    </td>
                                </tr>
                            @endforeach   
                            </tbody>         
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Edit Modal -->
@foreach ($umkm as $row)
<div class="modal fade bd-example-modal-lg" id="dataEdit{{ $row->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <form action="{{url('dashboard-petugas/pendaftaran/masuk/update', [$row->id])}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="page-title" id="myModalLabel">Pendaftaran UMKM Terverifikasi</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="status" value="terverifikasi">
                    <div class="form-group">
                        <label >Nama Permilik</label>
                        <input type="text" class="form-control" name="nama_lengkap" id="inputName4" placeholder="Nama Lengkap" value="{{ $row->User['name'] }}" disabled>
                    </div>
                    <div class="row" style="padding-left: 13px;">
                        <div class="form-group col-md-6">
                            <label >Dukuh</label>
                            <select name="dukuh" id="" class="form-control" disabled>
                                @foreach ($dukuh as $row1)
                                    <?php
                                    $selected = ' ';
                                    if($row->User['t_dukuh_id'] == $row1['id']){
                                        $selected = 'selected';
                                    }
                                    ?>
                                    <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['dukuh']; ?> </option>
                                    ?>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label >RT</label>
                            <select name="rt" id="" class="form-control" disabled>
                                <option selected value="{{ $row->User['rt'] }}"> <?php echo $row->User['rt']; ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label >Merk Usaha</label>
                        <input type="text" class="form-control" id="inputName4" name="merk_usaha" placeholder="Merk Usaha" value="{{ $row->merk_usaha }}" disabled>
                    </div>
                    <div class="form-group">
                        <label >Nomor SIUP</label>
                        <input type="text" class="form-control" id="inputName4" name="nomor_siup" placeholder="Nomor SIUP" value="{{ $row->no_siup }}" disabled>
                    </div>
                    <div class="form-group">
                        <label >Nomor NPWP</label>
                        <input type="text" class="form-control" id="inputName4" name="nomor_npwp" placeholder="Nomor SIUP" value="{{ $row->no_npwp }}" disabled>
                    </div>
                    <div class="form-group">
                        <label >Nomor Whatsapp</label>
                        <input type="text" class="form-control" id="inputName4" name="nomor_whatsapp" placeholder="Nomor Whatsapp (Gunakan kode negara +62)" value="{{ $row->no_whatsapp }}" disabled>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label >Foto Produk</label>
                            <center><a href="{{ URL::to('public') }}/assets/1119126/{{ $row->image }}" target="_blank"><img src="{{ URL::to('public') }}/assets/1119126/{{ $row->image }}" class="img-thumbnail" height="100px" width="150px"></a></center>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label >Lampiran SIUP</label>
                            <center><a href="{{ URL::to('public') }}/assets/1119126/{{ $row->lamp_siup }}" target="_blank"><img src="{{ URL::to('public') }}/assets/1119126/{{ $row->lamp_siup }}" class="img-thumbnail" height="100px" width="150px"></a></center>
                        </div>
                        <div class="form-group col-md-6">
                            <label >Lampiran NPWP</label>
                            <center><a href="{{ URL::to('public') }}/assets/1119126/{{ $row->lamp_npwp }}" target="_blank"><img src="{{ URL::to('public') }}/assets/1119126/{{ $row->lamp_npwp }}" class="img-thumbnail" height="100px" width="150px"></a></center>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                        <textarea id="basic-example{{ $row->id }}" class="form-control" data-validation="required" data-validation="custom" data-validation-regexp="^[a-zA-Z ]{2,30}$"  name="keterangan">{{ $row->keterangan }}</textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endforeach
<!-- // Edit Modal -->
@endsection

@section('script')
@foreach ($umkm as $row)
    <script>
        tinymce.init({
            selector: '#basic-example{{ $row->id }}',
            height: 300,
            menubar: false,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code help wordcount'
            ],
            toolbar: 'undo redo | formatselect | ' +
            'bold italic backcolor | alignleft aligncenter ' +
            'alignright alignjustify | bullist numlist outdent indent | ' +
            'removeformat | help',
            content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
        });
    </script>
@endforeach
@endsection

