<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laporan UMKM</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <center>
        <h6>DAFTAR UMKM TERDAFTAR DALAM WEBSITE</h6>
    </center>
    <table style="font-size: 10px;font-weight:bold">
        <tr>
            <td>KECAMATAN &nbsp;</td>
            <td>KALIJAMBE</td>
        </tr>
        <tr>
            <td>KELURAHAN &nbsp;</td>
            <td>BANARAN</td>
        </tr>
        <tr>
            <td>PERIODE &nbsp;</td>
            <td>{{ Carbon\Carbon::parse($date[0])->isoFormat('D MMMM Y') }} s/d {{ Carbon\Carbon::parse($date[1])->isoFormat('D MMMM Y') }}</td>
        </tr>
    </table>
    <hr>
    <div class="table-responsive" style="padding-top: 0px">
        <center>
            <table id="basic-datatables" border="1" style="font-size: 12px">
                <thead>
                    <tr>
                        <th class="text-center" width="2%">No.</th>
                        <th class="text-center" width="10%">Terverifikasi</th>
                        <th class="text-center" width="20%">Merk</th>
                        <th class="text-center" width="20%">Pemilik</th>
                        <th class="text-center" width="10%">Nomor Whatsapp</th>
                        <th class="text-center" width="10%">SIUP</th>
                        <th class="text-center" width="10%">NPWP</th>
                        <th class="text-center" width="20%">Alamat</th>
                    </tr>
                </thead>
                <tbody>
                @php
                $no = 1;
                @endphp
                @foreach ($umkm as $row)
                <tr>
                    <td  class="text-center">{{ $no++ }}</td>
                    <td  class="text-center">{{ Carbon\Carbon::parse($row['updated_at'])->format('d/m/Y') }}</td>
                    <td  class="text-center">{{ $row->merk_usaha }}</td>
                    <td  class="text-center">{{ $row->User['name'] }}</td>
                    <td  class="text-center">{{ $row->no_whatsapp }}</td>
                    <td  class="text-center">{{ $row->no_siup }}</td>
                    <td  class="text-center">{{ $row->no_npwp }}</td>
                    <td  class="text-center">{{ $row->User->Dukuh['dukuh'] }}</td>
                </tr>
                @endforeach
                <tr  style="background: #06a5d6">
                    <td colspan="7" class="text-center"><b>JUMLAH</b></td>
                    <td class="text-center">{{ $umkm->count() }}</td>               
                </tr>
                
                </tbody>         
            </table>
        </center>
    </div>
</body>
</html>