@extends('layouts.dashboard2')
@section('title', 'Dashboard | Laporan Layanan Surat')

@section('style')
    <!--Daterangepicker -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <!--Jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!--Boostrap -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <!--DataTables -->
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
@endsection

<!-- TagPage -->
@section('tagPage')
<div class="page-header" style="color: white">
    <h4 class="page-title" style="color: white">Laporan Layanan UMKM</h4>
    <ul class="breadcrumbs">
        <li class="nav-home">
            <a href="#">
                <i class="flaticon-home" style="color: white"></i>
            </a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="{{ '/dashboard-petugas' }}" style="color: white">Dashboard</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="" style="color: white">Laporan</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="" style="color: white">Laporan Layanan UMKM</a>
        </li>
    </ul>
</div>
@endsection
<!-- End TagPage -->

@section('descPage','Halaman ini digunakan untuk melihat laporan dari layanan UMKM yang telah terverifikasi')

@section('content')
<div class="page-inner mt--5">
    <div class="row mt--2">
        <div class="col-md-12">
            <div class="card full-height">
                <div class="card-body">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-check"></i> <strong>Berhasil! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Danger! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('warning'))
                    <div class="alert alert-warning">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-exclamation"></i> <strong>Warning! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('info'))
                    <div class="alert alert-info">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-info"></i> <strong>Info! &nbsp;&nbsp;</strong> 
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Bahaya! &nbsp;&nbsp;</strong>
                        Silakan periksa formulir di bawah ini untuk kesalahan <br>
                        <strong>
                            <ul>
                                @foreach ($errors->all() as $message)
                                    <li>{{$message}}</li>
                                @endforeach
                            </ul>
                        </strong>
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-9"></div>
                        <div class="col-md-3 d-flex justify-content-between">
                            <!-- FORM UNTUK FILTER BERDASARKAN DATE RANGE -->
                            <form action="{{ route('reportumkm') }}" method="get">
                                <div class="text-right" data-toggle="modal" data-target="#create" style="margin-bottom: rem">
                                    <a target="_blank" id="exportpdf" class="btn btn-sm btn-info btn-round pull-right">
                                        <i class="far fa-file"></i>&nbsp; Export Filter PDF
                                    </a>
                                </div>
                            </form>

                            <div class="text-left" style="margin-bottom: rem">
                                <a href="{{ url('/dashboard-petugas/laporan/allumkm/') }}" target="_blank" id="exportpdf" class="btn btn-sm btn-info btn-round pull-right">
                                    <i class="far fa-file"></i>&nbsp; Export All PDF
                                </a>
                            </div>
                        </div>
                    </div>
                    

                    <div class="table-responsive mt-5">
                        <table id="basic-datatabless" class="display table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center" width="2%">#</th>
                                    <th class="text-center" width="10%">Terverifikasi</th>
                                    <th class="text-center" width="20%">Merk</th>
                                    <th class="text-center" width="10%">Foto Produk</th>
                                    <th class="text-center" width="10%">Pemilik</th>
                                    <th class="text-center" width="10%">Nomor Whatsapp</th>
                                    <th class="text-center" width="5%">SIUP</th>
                                    <th class="text-center" width="5%">NPWP</th>
                                    <th class="text-center" width="10%">Alamat</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $no = 1;
                                @endphp
                                @foreach ($umkm as $row)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ Carbon\Carbon::parse($row['updated_at'])->format('d/m/Y') }}</td>
                                    <td>{{ $row->merk_usaha }}</td>
                                    <td><a href="{{ URL::to('public') }}/assets/1119126/{{ $row->image }}" target="_blank"><img src="{{ URL::to('public') }}/assets/1119126/{{ $row->image }}" alt="" height="75px" width="75px"></a></td>
                                    <td>{{ $row->User['name'] }}</td>
                                    <td>{{ $row->no_whatsapp }}</td>
                                    <td>{{ $row->no_siup }}</td>
                                    <td>{{ $row->no_npwp }}</td>
                                    <td>{{ $row->User->Dukuh['dukuh'] }}</td>
                                </tr>
                                @endforeach
                            </tbody>         
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<!--DateRangePicker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script type="text/javascript"> 
 //fungsi untuk filtering data berdasarkan tanggal 
var start_date;
var end_date;
var DateFilterFunction = (function (oSettings, aData, iDataIndex) {
var dateStart = parseDateValue(start_date);
var dateEnd = parseDateValue(end_date);
var evalDate= parseDateValue(aData[1]);
    if ( ( isNaN( dateStart ) && isNaN( dateEnd ) ) ||
        ( isNaN( dateStart ) && evalDate <= dateEnd ) ||
        ( dateStart <= evalDate && isNaN( dateEnd ) ) ||
        ( dateStart <= evalDate && evalDate <= dateEnd ) )
    {
        return true;
    }
    return false;
});

function parseDateValue(rawDate) {
    var dateArray= rawDate.split("/");
    var parsedDate= new Date(dateArray[1], parseInt(dateArray[1])-1, dateArray[0]); 
    return parsedDate;
}    

$( document ).ready(function() {
 var $dTable = $('#basic-datatabless').DataTable({
  "dom": "<'row'<'col-sm-4'l><'col-sm-5' <'datesearchbox'>><'col-sm-3'f>>" +
    "<'row'<'col-sm-12'tr>>" +
    "<'row'<'col-sm-5'i><'col-sm-7'p>>"
});


$("div.datesearchbox").html('<div class="input-group"> <div class="input-group-addon"></div><div class="input-group-prepend" style="background:blue;"><span class="input-group-text"><i class="fas fa-list-alt"></i></span></div><input style="border-color:#06a5d6" type="text" class="form-control pull-right" id="datesearch" placeholder="Cari dengan range tanggal"> </div>');

document.getElementsByClassName("datesearchbox")[0].style.textAlign = "right";

$('#datesearch').daterangepicker({
    autoUpdateInput: false
} , function(first, last) {
    $('#exportpdf').attr('href', '/dashboard-petugas/laporan/umkm/' + first.format('DD-MM-YYYY') + '+' + last.format('DD-MM-YYYY'))
});

$('#datesearch').on('apply.daterangepicker', function(ev, picker) {
    $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
    start_date=picker.startDate.format('DD/MM/YYYY');
    end_date=picker.endDate.format('DD/MM/YYYY');
    $.fn.dataTableExt.afnFiltering.push(DateFilterFunction);
    $dTable.draw();
});

$('#datesearch').on('cancel.daterangepicker', function(ev, picker) {
    $(this).val('');
    start_date='';
    end_date='';
    $.fn.dataTable.ext.search.splice($.fn.dataTable.ext.search.indexOf(DateFilterFunction, 1));
    $dTable.draw();
  });
});

</script>
@endsection
