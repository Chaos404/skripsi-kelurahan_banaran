<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laporan Layanan Surat</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <center>
        <h6>DAFTAR PERMOHONAN SURAT KETERANGAN BANARAN</h6>
    </center>
    <table style="font-size: 10px;font-weight:bold">
        <tr>
            <td>KECAMATAN &nbsp;</td>
            <td>KALIJAMBE</td>
        </tr>
        <tr>
            <td>KELURAHAN &nbsp;</td>
            <td>BANARAN</td>
        </tr>
        <tr>
            <td>PERIODE &nbsp;</td>
            <td>{{ Carbon\Carbon::parse($awal['updated_at'])->isoFormat('D MMMM Y') }} s/d {{ Carbon\Carbon::parse($akhir['updated_at'])->isoFormat('D MMMM Y') }}</td>
        </tr>
    </table>
    <hr>
    <div class="table-responsive">
        <center>
            <table id="basic-datatables" border="1" style="font-size: 10px">
                <thead>
                    <tr>
                        <th class="text-center" width="2%">No.</th>
                        <th class="text-center" width="10%">Tanggal</th>
                        <th class="text-center" width="10%">NIK</th>
                        <th class="text-center" width="10%">Nama Lengkap</th>
                        <th class="text-center" width="10%">Jenis Kelamin</th>
                        <th class="text-center" width="10%">Alamat</th>
                        <th class="text-center" width="10%">RT/RW</th>
                        <th class="text-center" width="20%">Keperluan</th>
                        <th class="text-center" width="10%">Keterangan</th>
                    </tr>
                </thead>
                <tbody>
                @php
                $no = 1;
                @endphp
                @foreach ($surat as $row)
                <tr>
                    <td class="text-center">{{ $no++ }}</td>
                    <td class="text-center">{{ Carbon\Carbon::parse($row['updated_at'])->format('d/m/Y') }}</td>
                    <td class="text-center">{{ $row->User['nik'] }}</td>
                    <td class="text-center">{{ $row->User['name'] }}</td>
                    <td class="text-center">{{ $row->User['jeniskelamin'] }}</td>
                    <td class="text-center">{{ $row->User->Dukuh['dukuh'] }}</td>
                    <td class="text-center">{{ $row->User['rt'] }} / -</td>
                    <td>{{ $row->keterangan }}</td>
                    <td class="text-center">DESA/KELURAHAN</td>
                </tr>
                @endforeach
                <tr  style="background: #06a5d6">
                    <td colspan="8" class="text-center"><b>JUMLAH</b></td>
                    <td class="text-center">{{ $surat->count() }}</td>               
                </tr>
                
                </tbody>         
            </table>
        </center>
    </div>
</body>
</html>