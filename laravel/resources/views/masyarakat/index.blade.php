@extends('layouts.dashboard3')
@section('title', 'Kelurahan Banaran | Dashboard Masyarakat')

<!-- Tagpage -->
@section('tagPage')
<div class="page-header" style="color: white">
    <h4 class="page-title" style="color: white">Dashboard Masyarakat</h4>
</div>
@endsection
<!-- // Tagpage -->

@section('content')
<div class="page-inner mt--5">
    <div class="row mt--2">
        <div class="col-md-12">
            <div class="card full-height">
                <div class="card-body">
                    <div class="card-title">Statistik Pengajuan</div>
                    <div class="card-category">Memberikan informasi mengenai pengajuan yang anda lakukan pada sistem ini.</div>
                    <div class="d-flex flex-wrap justify-content-around pb-2 pt-4">
                        <div class="px-2 pb-2 pb-md-0 text-center">
                            <div id="circles-1"></div>
                            <h6 class="fw-bold mt-3 mb-0"><a href="{{ url('dashboard/'. Auth::user()->id.'/verify-surat') }}" style="text-decoration:none;">Surat</a></h6>
                        </div>
                        <div class="px-2 pb-2 pb-md-0 text-center">
                            <div id="circles-2"></div>
                            <h6 class="fw-bold mt-3 mb-0"><a href="{{ url('dashboard/'. Auth::user()->id.'/verify-umkm') }}" style="text-decoration:none;">UMKM</a></h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    Circles.create({
        id:'circles-1',
        radius:100,
        value:{{ $surat }}/1 ,
        maxValue:100,
        width:7,
        text: {{ $surat }},
        colors:['#f1f1f1', '#FF9E27'],
        duration:400,
        wrpClass:'circles-wrp',
        textClass:'circles-text',
        styleWrapper:true,
        styleText:true
    })

    Circles.create({
        id:'circles-2',
        radius:100,
        value:{{ $umkm }}/1,
        maxValue:100,
        width:7,
        text: {{ $umkm }},
        colors:['#f1f1f1', '#2BB930'],
        duration:400,
        wrpClass:'circles-wrp',
        textClass:'circles-text',
        styleWrapper:true,
        styleText:true
    })
</script>
@endsection
    