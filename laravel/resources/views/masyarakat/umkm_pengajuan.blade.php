@extends('layouts.dashboard3')
@section('title', 'Dashboard | Pengajuan UMKM')

<!-- TagPage -->
@section('tagPage')
<div class="page-header" style="color: white">
    <h4 class="page-title" style="color: white">Pengajuan UMKM</h4>
    <ul class="breadcrumbs">
        <li class="nav-home">
            <a href="#">
                <i class="flaticon-home" style="color: white"></i>
            </a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="{{ '/dashboard' }}" style="color: white">Dashboard</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="" style="color: white">Pengajuan UMKM</a>
        </li>
    </ul>
</div>
@endsection
<!-- End TagPage -->

@section('descPage','Halaman ini digunakan untuk mengetahui pengajuan UMKM dalam proses')

@section('content')
<div class="page-inner mt--5">
    <div class="row mt--2">
        <div class="col-md-12">
            <div class="card full-height">
                <div class="card-body">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-check"></i> <strong>Success! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Danger! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('warning'))
                    <div class="alert alert-warning">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-exclamation"></i> <strong>Warning! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('info'))
                    <div class="alert alert-info">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-info"></i> <strong>Info! &nbsp;&nbsp;</strong> 
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Error! &nbsp;&nbsp;</strong>
                        Please check the form below for errors <br>
                        <strong>
                            <ul>
                                @foreach ($errors->all() as $message)
                                    <li>{{$message}}</li>
                                @endforeach
                            </ul>
                        </strong>
                    </div>
                    @endif

                    <div class="text-right" data-toggle="modal" data-target="#create" style="margin-bottom: -2rem">
                        <a class="btn btn-info btn-round pull-right" href="{{ url('/mendaftar/umkm') }}">
                          <i class="fas fa-plus-circle"></i>&nbsp; Pengajuan
                        </a>
                    </div>
                    <div class="table-responsive" style="padding-top: 10px">
                        <table id="basic-datatables" class="display table table-striped table-hover" >
                            <thead>
                                <tr>
                                    <th class="text-center" width="2%">#</th>
                                    <th class="text-center" width="20%">Merk</th>
                                    <th class="text-center" width="10%">Foto Produk</th>
                                    <th class="text-center" width="10%">Nomor Whatsapp</th>
                                    <th class="text-center" width="5%">SIUP</th>
                                    <th class="text-center" width="5%">NPWP</th>
                                    <th class="text-center" width="5%">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                            $no = 1;
                            @endphp
                            @foreach ($umkm as $row)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td class="text-center">{{ $row->merk_usaha }}</td>  
                                    <td class="text-center"><a href="{{ URL::to('public') }}/assets/1119126/{{ $row->image }}" target="_blank"><img src="{{ URL::to('public') }}/assets/1119126/{{ $row->image }}" alt="" height="75px" width="75px"></a></td>  
                                    <td class="text-center">{{ $row->no_whatsapp }}</td>
                                    <td class="text-center">{{ $row->no_siup }}</td>  
                                    <td class="text-center">{{ $row->no_npwp }}</td>
                                    <td class="text-center">
                                        @if ($row->status == "proses")
                                        <span class="badge badge-danger">Baru</span>
                                        @else 
                                        <span class="badge badge-success">Terverifikasi</span>
                                        @endif
                                    </td>  
                                </tr>
                            @endforeach   
                            </tbody>         
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection