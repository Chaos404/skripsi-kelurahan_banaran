@extends('layouts.dashboard3')
@section('title', 'Dashboard | Profil')

<!-- TagPage -->
@section('tagPage')
<div class="page-header" style="color: white">
    <h4 class="page-title" style="color: white">Profil</h4>
    <ul class="breadcrumbs">
        <li class="nav-home">
            <a href="#">
                <i class="flaticon-home" style="color: white"></i>
            </a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="{{ '/dashboard' }}" style="color: white">Dashboard</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="" style="color: white">Profil</a>
        </li>
    </ul>
</div>
@endsection
<!-- End TagPage -->

@section('descPage','Halaman ini digunakan untuk melihat profil pengguna')

@section('content')
<div class="page-inner mt--5">
    <div class="row mt--2">
        <div class="col-md-12">
            <div class="card full-height">
                <div class="card-body">
                    <div class="container">
                        <div class="page-inner">
                            <h4 class="page-title">Profil Pengguna</h4>
                            @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <i class="fa fa-check"></i> <strong>Success! &nbsp;&nbsp;</strong>
                                <strong>{{ $message }}</strong>
                            </div>
                            @endif

                            @if ($message = Session::get('error'))
                            <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <i class="fa fa-times"></i> <strong>Danger! &nbsp;&nbsp;</strong>
                                <strong>{{ $message }}</strong>
                            </div>
                            @endif

                            @if ($message = Session::get('warning'))
                            <div class="alert alert-warning">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <i class="fa fa-exclamation"></i> <strong>Warning! &nbsp;&nbsp;</strong>
                                <strong>{{ $message }}</strong>
                            </div>
                            @endif

                            @if ($message = Session::get('info'))
                            <div class="alert alert-info">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <i class="fa fa-info"></i> <strong>Info! &nbsp;&nbsp;</strong> 
                                <strong>{{ $message }}</strong>
                            </div>
                            @endif

                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <i class="fa fa-times"></i> <strong>Bahaya! &nbsp;&nbsp;</strong>
                                Silakan periksa formulir di bawah ini untuk kesalahan <br>
                                <strong>
                                    <ul>
                                        @foreach ($errors->all() as $message)
                                            <li>{{$message}}</li>
                                        @endforeach
                                    </ul>
                                </strong>
                            </div>
                            @endif
                            <div class="alert alert-warning">
                                <a href="#" data-dismiss="alert"></a>
                                <i class="fa fa-exclamation"></i> <strong>Perhatian ! &nbsp;&nbsp;</strong>
                                <strong>Apabila anda memperbaharui data diri anda, maka membutuhkan verifikasi dari petugas kembali. Karena anda melakukan perubahan data diri</strong>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="card card-with-nav">
                                        @foreach ($user as $row)
                                        <div class="card-body">
                                            <form action="{{url('dashboard/'.Auth::user()->id.'/profile/update')}}" method="post" enctype="multipart/form-data">
                                                @csrf
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <label for="fullname" class="placeholder"><b>Nama Lengkap</b></label>
                                                        <input type="text" class="form-control @error('nama_lengkap') is-invalid @enderror" name="nama_lengkap" value="{{ $row->name }}" placeholder="Nama Lengkap" >
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label >Tempat Lahir</label>
                                                        <input type="text" class="form-control @error('tempat_lahir') is-invalid @enderror" name="tempat_lahir" value="{{ $row->tempat_lahir }}" placeholder="Tempat Lahir">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label >Tanggal Lahir</label>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control @error('tanggal_lahir') is-invalid @enderror" name="tanggal_lahir" data-toggle="datepicker" placeholder="Tanggal Lahir" value="{{ $row->tgl_lahir }}">
                                                            {{-- <input type="text" class="form-control @error('tanggal_lahir') is-invalid @enderror" id="datepicker" placeholder="Tanggal Lahir" name="tanggal_lahir"> --}}
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <i class="fa fa-calendar-check"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label >Jenis Kelamin</label>
                                                        <select name="jenis_kelamin" id="" class="form-control">
                                                            @if ($row['jeniskelamin'] == "Laki - laki")
                                                                <option <?php echo $selected = 'selected'; ?> value="Laki - laki">Laki - laki</option>
                                                                <option  value="Perempuan">Perempuan</option>
                                                            @elseif ($row['jeniskelamin'] == "Perempuan")
                                                                <option <?php echo $selected = 'selected'; ?> value="Perempuan">Perempuan</option>
                                                                <option  value="Laki - laki">Laki - laki</option>
                                                            @else
                                                            @endif
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label >Agama</label>
                                                        <select name="agama" id="" class="form-control @error('agama') is-invalid @enderror">
                                                            @foreach ($agama as $row1)
                                                                <?php
                                                                $selected = ' ';
                                                                if($row['t_agama_id'] == $row1['id']){
                                                                    $selected = 'selected';
                                                                }
                                                                ?>
                                                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['agama']; ?> </option>
                                                                ?>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <label for="email" class="placeholder"><b>Email</b></label>
                                                        <input  id="email" name="email" type="email" class="form-control @error('email') is-invalid @enderror"  placeholder="Email" value="{{ $row->email }}">
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label >Pekerjaan</label>
                                                        <select name="pekerjaan" id="" class="form-control @error('pekerjaan') is-invalid @enderror">
                                                            @foreach ($pekerjaan as $row1)
                                                                <?php
                                                                $selected = ' ';
                                                                if($row['t_pekerjaan_id'] == $row1['id']){
                                                                    $selected = 'selected';
                                                                }
                                                                ?>
                                                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['pekerjaan']; ?> </option>
                                                                ?>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label >Status Perkawinan</label>
                                                        <select name="status_perkawinan" id="" class="form-control @error('status_perkawinan') is-invalid @enderror">
                                                            @if ($row['statusperkawinan'] == "Belum Kawin")
                                                                <option <?php echo $selected = 'selected'; ?> value="Belum Kawin">Belum Kawin</option>
                                                                <option value="Kawin">Kawin</option>
                                                                <option value="Pernah Kawin">Pernah Kawin</option>
                                                            @elseif ($row['statusperkawinan'] == "Kawin")
                                                                <option value="Belum Kawin">Belum Kawin</option>
                                                                <option <?php echo $selected = 'selected'; ?> value="Kawin">Kawin</option>
                                                                <option value="Pernah Kawin">Pernah Kawin</option>
                                                            @else
                                                                <option value="Belum Kawin">Belum Kawin</option>
                                                                <option value="Kawin">Kawin</option>
                                                                <option <?php echo $selected = 'selected'; ?> value="Pernah Kawin">Pernah Kawin</option>
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label >Dukuh</label>
                                                        <select name="dukuh" id="dukuhnya" class="form-control @error('dukuh') is-invalid @enderror">
                                                            @foreach ($dukuh as $row1)
                                                                <?php
                                                                $selected = ' ';
                                                                if($row['t_dukuh_id'] == $row1['id']){
                                                                    $selected = 'selected';
                                                                }
                                                                ?>
                                                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['dukuh']; ?> </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label >RT</label>
                                                        <select name="carirt" id="rt" class="form-control @error('rt') is-invalid @enderror">
                                                            <option value="">{{ $row->rt }}</option>
                                                        </select>
                                                        <input type="hidden" class="form-control @error('rt') is-invalid @enderror" id="rtnya" name="rt" value="{{ $row->rt }}">
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <label >NIK</label>
                                                        <input type="text" class="form-control @error('nik') is-invalid @enderror" value="{{ $row->nik }}" name="nik" placeholder="NIK (16 Digit)">
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <label >Nomor KK</label>
                                                        <input type="text" class="form-control @error('no_kk') is-invalid @enderror" value="{{ $row->no_kk }}" name="no_kk" placeholder="Nomor KK (16 Digit)">
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    {{-- <div class="form-group col-md-6">
                                                        <label >Lampiran KTP</label>
                                                        <center padding-bottom: 100px;><img id="blah" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="150px" name="lampiran_ktp"/></center><br>
                                                        <input type='file' class="form-control @error('lampiran_ktp') is-invalid @enderror" name="lampiran_ktp" onchange="readURL(this);" />
                                                    </div> --}}
                                                    <div class="form-group col-md-12">
                                                        <label >Lampiran KK</label>
                                                        {{-- <input type="file" class="form-control" value="{{ $row->name }}" name="lampiran_kk" placeholder="NIK"> --}}
                                                        <center padding-bottom: 100px;><img id="blah" src="{{ asset('assets/1117688/'. $row->lamp_kk) }}" alt="jpg, jpeg, png." height="300px" width="350px" name="lampiran_kk"/></center><br>
                                                        <input type='file' class="form-control @error('lampiran_kk') is-invalid @enderror" name="lampiran_kk" onchange="readURL(this);" />
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-primary"><i class="far fa-save"></i>&nbsp; Update Data Diri</button>
                                                </div>
                                            </form>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card card-profile">
                                        <div class="card-header" style="background-image: url('../assets/img/blogpost.jpg')">
                                            <div class="profile-picture">
                                                <div class="avatar avatar-xl">
                                                    <img src="{{ asset('assets/assets/img/profile3.png')}}" alt="..." class="avatar-img rounded-circle">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            @foreach ($user as $row)
                                            <div class="user-profile text-center">
                                                <div class="name">{{ $row->name }}</div>
                                                <div class="job">{{ $row->Pekerjaan['pekerjaan'] }}</div>
                                                <div class="desc">{{ $row->email }}</div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(function() {
        $('[data-toggle="datepicker"]').datepicker({
            autoHide: true,
            zIndex: 2048,
            format: 'yyyy-mm-dd'
        });
    });
</script>
<script>
    $('#datepicker').datetimepicker({
        format: 'YYYY-MM-DD',
    });

</script>
<script>
    $(document).ready(function () {
        $('#dukuhnya').click(function () {
            $("input[name=rt]").val("");
        });
    });

    $(document).ready(function () {
        $('select[name="dukuh"]').on('change', function () {
            $('select[name="carirt"]').on('change', function () {
                var nomorRt = $("#rt option:selected").attr("namaRt");
                $("#rtnya").val(nomorRt);
            });

            let idRt = $(this).val();
            console.log(idRt);
            if (idRt) {
                jQuery.ajax({
                    url: '/dashboard/{{ Auth::user()->id }}/profile/rt/' + idRt,
                    type: "GET",
                    dataType: "json",
                    success: function (response) {
                        $('select[name="carirt"]').empty();
                        $('select[name="carirt"]').append(
                            '<option value="">-- Pilih RT --</option>');
                        $.each(response, function (key, value) {
                            $('select[name="carirt"]').append(
                                '<option value="' + key + '"namaRt="' +
                                value.rt + '">' + value.rt + '</option>');
                        });
                    },
                });
            } else {
                // $('select[name="dukuh"]').append('<option value="">-- pilih kota asal --</option>');
            }
        });
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection