@extends('layouts.landingpage')

@section('title')
<title>Kelurahan Banaran | Berita</title>    
@endsection

@section('pagetitle')
<section class="background-grey-1 padding-tb-25px text-grey-4">
    <div id="hapus" class="container">
        <h6 class="float-md-left font-2 mt-3">Berita dan Informasi</h6>
        <ol class="breadcrumb float-md-right">
            <li><a href="#" class="text-grey-4">Home</a></li><i class="fa fa-angle-right"> </i>
            <li class="active">Berita dan Informasi</li>
        </ol>
        <div class="clearfix"></div>
    </div>
</section>
    
@endsection

@section('content')
    <!-- Blog -->
    <section class="padding-tb-50px">
        <div class="container pad-top pad-bott">
            <div class="text-center margin-bottom-35px wow fadeInUp" style="padding-bottom: 50px;">
                <h1 class="font-weight-300 text-title-large font-3 text-main-color wow fadeInUp" data-wow-delay="0.2s">BERITA DAN INFORMASI</h1>
                <span class="opacity-7">Berita dan Informasi Kelurahan Banaran</span>
            </div>
            
            <div class="row">
                <div class="col-lg-8 ">
                    <div class="container">
                        @if (count($berita) > 0) 
                        <h2 class="font-1 text-main-color mt-3">Hasil Pencarian :</h2>
                        <div class="row">
                            @foreach ($berita as $row)
                            <div class="col-lg-6" style="margin-bottom:35px">
                                <div class="card">
                                    <div class="card-img-wrapper">
                                        <img class="card-img-top" src="{{ URL::to('public') }}/assets/1117942/{{ $row->image }}" alt="Card image cap">
                                    </div>
                                    <div class="card-body" style="text-align: justify">
                                        <a href="#" class="font-weight-bold d-block text-dark text-uppercase text-medium margin-bottom-10px font-weight-700">{{ \Illuminate\Support\Str::limit($row->judul, $limit = 50, $end = ' ...') }}</a>    
                                        <span class="text-extra-small"><a href="#" class="text-main-color">{{ Carbon\Carbon::parse($row['created_at'])->isoFormat('D MMMM Y') }}</a></span>
                                        <p class="card-text">{!! \Illuminate\Support\Str::limit($row->deskripsi, $limit = 150, $end = ' ......') !!}</p>
                                        <a href="{{ url('/berita', [$row->slug]) }}" class="btn btn-primary">Read More</a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        @else
                        <h2 class="font-1 text-main-color mt-3">Hasil Pencarian :</h2>
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <h6 class="font-2 mt-3">Hasil Pencarian tidak ditemukan. <span style="color:#0093dd;"> <a style="cursor: pointer;text-decoration:none" href="{{ URL::previous() }}">{{ __('Kembali . .') }}</a></h6>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>

                <div class="col-lg-4 col-md-4">
                    <!-- widget Latest -->
					<div class="widget">
                        <h4 class="widget-title clearfix"><span><i class="fa fa-search" aria-hidden="true"></i>&nbsp;<b>Search</b></span></h4>
                        <form action="{{ route('search') }}" method="GET" autocomplete="off">
                            <div class="input-group my-2">
                                <input type="text" class="form-control" placeholder="Search" aria-label="Search" name="search" required  style="border-color:#0093dd">
                                <div class="input-group-append">
                                    <button class="btn" type="submit" style="background-color:#FFFFFF; border-color:#0093dd">
                                        <i class="fa fa-search" style="color:#0093dd"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                        <br>
						<h4 class="widget-title clearfix"><span><i class="fa fa-file" aria-hidden="true"></i>&nbsp;<b>Terkini</b></span></h4>
						<ul class="last-posts">
							@foreach ($terkinis as $terkini)
                            <li>
								<a href="{{ url('/berita', [$terkini->slug]) }}" class="float-left margin-right-widget width-widget"><img src="{{ URL::to('public') }}/assets/1117942/{{ $terkini->image }}" alt=""></a>
								<a href="{{ url('/berita', [$terkini->slug]) }}" class="font-weight-bold d-block  text-uppercase text-medium text-dark font-weight-700">{{ \Illuminate\Support\Str::limit($terkini->judul, $limit = 20, $end = ' ...') }}</a>
								<span class="text-extra-small">Date :  <a href="{{ url('/berita', [$terkini->slug]) }}" class="text-main-color">{{ Carbon\Carbon::parse($terkini['created_at'])->isoFormat('D MMMM Y') }}</a></span>
							</li>
                            @endforeach
						</ul>

					</div>
					<!-- //  widget Latest -->

                    <!-- Informasi -->
                    <div class="widget widget_categories">
                        <h4 class="widget-title clearfix"><span><i class="fa fa-bullhorn"
                            aria-hidden="true"></i>&nbsp;<b>Informasi</b></span></h4>
                        <div class="row">
                            @foreach ($informasis as $informasi)
                            <a href="{{ url('/berita', [$informasi->slug]) }}"><div class="col-lg-6 col-md-6 mb-3 sm-mb-30px"><a href="{{ url('/berita', [$informasi->slug]) }}"><img src="{{ URL::to('public') }}/assets/1117942/{{ $informasi->image }}" alt=""></a></div></a>
                            @endforeach
                        </div>
                    </div>
                    <!-- //  Informasi -->
                </div>

            </div>
        </div>
    </section>
    <!-- // Blog -->
@endsection