@extends('layouts.landingpage')

@section('title')
<title>Kelurahan Banaran | Tugas Pokok dan Fungsi</title>    
@endsection

@section('pagetitle')
<section class="background-grey-1 padding-tb-25px text-grey-4">
    <div id="hapus" class="container">
        <h6 class="float-md-left font-2 mt-3">Tugas Pokok dan Fungsi</h6>
        <ol class="breadcrumb float-md-right">
            <li><a href="#" class="text-grey-4">Home</a></li><i class="fa fa-angle-right"> </i>
            <li><a href="#" class="text-grey-4">Profil </a></li><i class="fa fa-angle-right"></i>
            <li class="active">Tugas Pokok dan Fungsi</li>
        </ol>
        <div class="clearfix"></div>
    </div>
</section>
    
@endsection

@section('content')
    <!-- Tugas Pokok dan Fungsi -->
    <section class="padding-tb-50px">
        <div class="container pad-top pad-bott">
            <div class="text-center margin-bottom-35px wow fadeInUp" style="padding-bottom: 50px;">
                <h1 class="font-weight-300 text-title-large font-3 text-main-color wow fadeInUp" data-wow-delay="0.2s">TUPOKSI</h1>
                <span class="opacity-7">Tugas Pokok dan Fungsi Unit Kerja Kelurahan Banaran</span>
            </div>
            <style type="text/css">
                .col-centered {
                    display: inline-block;
                    float: none;
                    text-align: center;
                    /* margin-right: -4px; */
                }
            </style>
            <div class="row">
                <div class="col-lg-12 my-auto justify-content-center">
                    <ol>
                        @foreach ($tupoksi as $row)
                        {!! $row->tupoksi !!}
                        @endforeach
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- End Tugas Pokok dan Fungsi -->
@endsection