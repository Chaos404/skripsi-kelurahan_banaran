@extends('layouts.landingpage')

@section('title')
<title>Banaran | Geografis dan Kependududkan</title>    
@endsection

@section('pagetitle')
<section class="background-grey-1 padding-tb-25px text-grey-4">
    <div id="hapus" class="container">
        <h6 class="float-md-left font-2 mt-3">Geografis dan Kependududkan</h6>
        <ol class="breadcrumb float-md-right">
            <li><a href="#" class="text-grey-4">Home</a></li><i class="fa fa-angle-right"> </i>
            <li><a href="#" class="text-grey-4">Profil </a></li><i class="fa fa-angle-right"></i>
            <li class="active">Geografis dan Kependududkan</li>
        </ol>
        <div class="clearfix"></div>
    </div>
</section>
@endsection

@section('content')
    <!-- Visi dan Misi -->
    <section class="padding-tb-50px">
        <div class="container pad-top pad-bott">
            <div class="text-center margin-bottom-35px wow fadeInUp" style="padding-bottom: 50px;">
                <h1 class="font-weight-300 text-title-large font-3 text-main-color wow fadeInUp" data-wow-delay="0.2s">GEOGRAFIS DAN KEPENDUDUKAN</h1>
                <span class="opacity-7">Geografis dan Kependududkan Banaran</span>
            </div>
            <div class="row">
                <div class="col-lg-8 my-auto justify-content-center">
                    <h3 class="text-main-color wow fadeInDown">WILAYAH</h3>
                    <div class="iframe-rwd">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d10364.555178631572!2d110.81316189794735!3d-7.4373799284320405!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a0e05111e61c5%3A0x5027a76e355ab50!2sBanaran%2C%20Kec.%20Kalijambe%2C%20Kabupaten%20Sragen%2C%20Jawa%20Tengah!5e1!3m2!1sid!2sid!4v1602599019475!5m2!1sid!2sid" width="700" height="550" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    </div>
                </div>
                <div class="col-lg-4  justify-content-center">
                    <h3 class="text-main-color mb-3 wow fadeInDown">KONDISI GEOGRAFIS</h3>
                    <p class="mb-3">Luas Wilayah <b>± 855,838 Ha</b></p>
                    <p>Batas - batas Wilayah :</p>
                    <table class="mb-5">
                        <tr>
                            <td>1.</td>
                            <td>Sebelah Barat</td>
                            <td>: &nbsp; Sambirembe</td>
                        </tr>
                        <tr>
                            <td>2.</td>
                            <td>Sebelah Utara</td>
                            <td>: &nbsp; Karangjati</td>
                        </tr>
                        <tr>
                            <td>3.</td>
                            <td>Sebelah Timur</td>
                            <td>: &nbsp; Ngebung</td>
                        </tr>
                        <tr>
                            <td>4.</td>
                            <td>Sebelah Selatan</td>
                            <td>: &nbsp; Jetis Karangpung</td>
                        </tr>
                    </table>
                    <h3 class="text-main-color mb-3 wow fadeInDown">KONDISI DEMOGRAFI</h3>
                    <table>
                        @foreach ($kelurahan as $row)
                        <tr>
                            <td>1.</td>
                            <td>Jumlah Penduduk : <b>{{ $row->jumlah_penduduk }}</b> jiwa</td>
                        </tr>
                        <tr>
                            <td>2.</td>
                            <td>Jumlah penduduk laki-laki : <b>{{ $row->penduduk_laki }}</b> jiwa</td>
                        </tr>
                        <tr>
                            <td>3.</td>
                            <td>Jumlah penduduk perempuan : <b>{{ $row->penduduk_perempuan }}</b> jiwa</td>
                        </tr>
                        <tr>
                            <td>4.</td>
                            <td>Jumlah Dukuh : <b>13</b> dukuh</td>
                        </tr>
                        <tr>
                            <td>5.</td>
                            <td>Jumlah RT : <b>23</b> RT</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!-- End Visi dan Misi -->
@endsection