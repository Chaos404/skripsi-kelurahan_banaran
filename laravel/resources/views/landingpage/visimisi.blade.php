@extends('layouts.landingpage')

@section('title')
<title>Kelurahan Banaran | Visi & Misi</title>    
@endsection

@section('pagetitle')
<section class="background-grey-1 padding-tb-25px text-grey-4">
    <div id="hapus" class="container">
        <h6 class="float-md-left font-2 mt-3">Visi & Misi</h6>
        <ol class="breadcrumb float-md-right">
            <li><a href="#" class="text-grey-4">Home</a></li><i class="fa fa-angle-right"> </i>
            <li><a href="#" class="text-grey-4">Profil </a></li><i class="fa fa-angle-right"></i>
            <li class="active">Visi & Misi</li>
        </ol>
        <div class="clearfix"></div>
    </div>
</section>
    
@endsection

@section('content')
    <!-- Visi dan Misi -->
    <section class="padding-tb-50px">
        <div class="container pad-top pad-bott">
            <div class="text-center margin-bottom-35px wow fadeInUp" style="padding-bottom: 50px;">
                <h1 class="font-weight-300 text-title-large font-3 text-main-color wow fadeInUp" data-wow-delay="0.2s">VISI & MISI</h1>
                <span class="opacity-7">Berikut Visi dan Misi Kelurahan Banaran</span>
            </div>
            <div class="row">
                <div class="col-lg-6 justify-content-center">
                    <h3 class="text-main-color wow fadeInDown">VISI</h3>
                    <ol>
                        @foreach ($visi as $row)
                            <li>{{ $row->deskripsi }}</li>
                        @endforeach
                    </ol>
                </div>
                <div class="col-lg-6 justify-content-center">
                    <h3 class="text-main-color wow fadeInDown">MISI</h3>
                    <ol>
                        @foreach ($misi as $row)
                            <li>{{ $row->deskripsi }}</li>
                        @endforeach
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- End Visi dan Misi -->
@endsection