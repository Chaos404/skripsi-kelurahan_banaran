@extends('layouts.landingpage')

@section('title')
<title>Kelurahan Banaran | Daftar UMKM</title>    
@endsection

@section('style')
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('pagetitle')
<section class="background-grey-1 padding-tb-25px text-grey-4">
    <div id="hapus" class="container">
        <h6 class="float-md-left font-2 mt-3">Daftar UMKM</h6>
        <ol class="breadcrumb float-md-right">
            <li><a href="#" class="text-grey-4">Home</a></li><i class="fa fa-angle-right"> </i>
            <li class="active">Daftar UMKM</li>
        </ol>
        <div class="clearfix"></div>
    </div>
</section>
    
@endsection

@section('content')
    <!-- Daftar UMKM -->
    <section class="padding-tb-50px">
        <div class="container pad-top pad-bott">
            <div class="text-center margin-bottom-35px wow fadeInUp" style="padding-bottom: 50px;">
                <h1 class="font-weight-300 text-title-large font-3 text-main-color wow fadeInUp" data-wow-delay="0.2s">FORM PENDAFTARAN UMKM</h1>
            </div>
            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="fa fa-check"></i> <strong>Berhasil! &nbsp;&nbsp;</strong>
                <strong>{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('error'))
            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="fa fa-times"></i> <strong>Danger! &nbsp;&nbsp;</strong>
                <strong>{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('warning'))
            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="fa fa-exclamation"></i> <strong>Warning! &nbsp;&nbsp;</strong>
                <strong>{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('info'))
            <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="fa fa-info"></i> <strong>Info! &nbsp;&nbsp;</strong> 
                <strong>{{ $message }}</strong>
            </div>
            @endif

            @if ($errors->any())
            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="fa fa-times"></i> <strong>Bahaya! &nbsp;&nbsp;</strong>
                Silakan periksa formulir di bawah ini untuk kesalahan <br>
                <strong>
                    <ul>
                        @foreach ($errors->all() as $message)
                            <li>{{$message}}</li>
                        @endforeach
                    </ul>
                </strong>
            </div>
            @endif
            
            @guest
            @elseif (Auth::user()->role == "unverified")
            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                <i class="fa fa-exclamation"></i> <strong>Perhatian! &nbsp;&nbsp;</strong>
                <strong>Akun anda belum terverifikasi.</strong>
            </div>
            <form class="padding-top-10px margin-top-10px border-top-1 border-grey-1" action="{{ route('daftarumkm')}}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="role" value="{{ Auth::user()->role }}">
                <input type="hidden" name="id_user" value="{{ Auth::user()->id }}">
                <div class="form-group">
                    <label >Nama Permilik</label>
                    <input type="text" class="form-control" name="nama_lengkap" id="inputName4" required placeholder="Nama Lengkap" value="{{ Auth::user()->name }}" disabled>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label >Dukuh</label>
                        <select name="dukuh" id="" class="form-control" disabled>
                            @foreach ($dukuh as $row1)
                                <?php
                                $selected = ' ';
                                if(Auth::user()->t_dukuh_id == $row1['id']){
                                    $selected = 'selected';
                                }
                                ?>
                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['dukuh']; ?> </option>
                                ?>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label >RT</label>
                        <select name="rt" id="" class="form-control" disabled>
                            <option value="{{ Auth::user()->rt }}">{{ Auth::user()->rt }}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label >Merk Usaha</label>
                    <input type="text" class="form-control @error('merk_usaha') is-invalid @enderror" name="merk_usaha" required placeholder="Merk Usaha">
                </div>
                <div class="form-group">
                    <label >Nomor SIUP</label>
                    <input type="text" class="form-control @error('nomor_siup') is-invalid @enderror" name="nomor_siup" required placeholder="Nomor SIUP">
                </div>
                <div class="form-group">
                    <label >Nomor NPWP</label>
                    <input type="text" class="form-control @error('nomor_npwp') is-invalid @enderror" name="nomor_npwp" required placeholder="Nomor SIUP">
                </div>
                <div class="form-group">
                    <label >Nomor Whatsapp</label>
                    {{-- <input type="text" class="form-control @error('nomor_whatsapp') is-invalid @enderror" name="nomor_whatsapp" required placeholder="Nomor Whatsapp (Gunakan kode negara +62)"> --}}
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">+62</span>
                        </div>
                        <input type="text" class="form-control @error('nomor_whatsapp') is-invalid @enderror" required placeholder="Nomor Whatsapp" name="nomor_whatsapp" aria-describedby="basic-addon1">
                    </div>
                </div>
                <div class="form-group">
                    <label >Foto Produk</label>
                    <center padding-bottom: 100px;><img id="blah3" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="300px" width="auto" name="foto_produk"/></center><br>
                    <input type='file' required class="form-control @error('foto_produk') is-invalid @enderror" name="foto_produk" onchange="readURL3(this);" />
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label >Lampiran SIUP</label>
                        <center padding-bottom: 100px;><img id="blah" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="150px" name="lampiran_siup"/></center><br>
                        <input type='file' required class="form-control @error('lampiran_siup') is-invalid @enderror" name="lampiran_siup" onchange="readURL(this);" />
                    </div>
                    <div class="form-group col-md-6">
                        <label >Lampiran NPWP</label>
                        <center padding-bottom: 100px;><img id="blah2" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="150px" name="lampiran_npwp"/></center><br>
                        <input type='file' required class="form-control @error('lampiran_npwp') is-invalid @enderror" name="lampiran_npwp" onchange="readURL2(this);" />
                    </div>
                </div>
                <div class="form-group">
                    <label>Keterangan</label>
                    <textarea id="basic-example" name="keterangan" class="form-control @error('keterangan') is-invalid @enderror">
                    </textarea>
                </div>
                <button type="submit" class="btn btn-primary btn btn-lg btn-block background-main-color text-white text-center font-weight-bold text-uppercase rounded-0 padding-15px">PROSES DAFTAR</button>
            </form>
            @else
            <form class="padding-top-10px margin-top-10px border-top-1 border-grey-1" action="{{ route('daftarumkm')}}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="role" value="{{ Auth::user()->role }}">
                <input type="hidden" name="id_user" value="{{ Auth::user()->id }}">
                <div class="form-group">
                    <label >Nama Permilik</label>
                    <input type="text" class="form-control" name="nama_lengkap" id="inputName4" required placeholder="Nama Lengkap" value="{{ Auth::user()->name }}" disabled>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label >Dukuh</label>
                        <select name="dukuh" id="" class="form-control" disabled>
                            @foreach ($dukuh as $row1)
                                <?php
                                $selected = ' ';
                                if(Auth::user()->t_dukuh_id == $row1['id']){
                                    $selected = 'selected';
                                }
                                ?>
                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['dukuh']; ?> </option>
                                ?>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label >RT</label>
                        <select name="rt" id="" class="form-control" disabled>
                            <option value="{{ Auth::user()->rt }}">{{ Auth::user()->rt }}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label >Merk Usaha</label>
                    <input type="text" class="form-control @error('merk_usaha') is-invalid @enderror" name="merk_usaha" required placeholder="Merk Usaha">
                </div>
                <div class="form-group">
                    <label >Nomor SIUP</label>
                    <input type="text" class="form-control @error('nomor_siup') is-invalid @enderror" name="nomor_siup" required placeholder="Nomor SIUP">
                </div>
                <div class="form-group">
                    <label >Nomor NPWP</label>
                    <input type="text" class="form-control @error('nomor_npwp') is-invalid @enderror" name="nomor_npwp" required placeholder="Nomor SIUP">
                </div>
                <div class="form-group">
                    <label >Nomor Whatsapp</label>
                    {{-- <input type="text" class="form-control @error('nomor_whatsapp') is-invalid @enderror" name="nomor_whatsapp" required placeholder="Nomor Whatsapp (Gunakan kode negara +62)"> --}}
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">+62</span>
                        </div>
                        <input type="text" class="form-control @error('nomor_whatsapp') is-invalid @enderror" required placeholder="Nomor Whatsapp" name="nomor_whatsapp" aria-describedby="basic-addon1">
                    </div>
                </div>
                <div class="form-group">
                    <label >Foto Produk</label>
                    <center padding-bottom: 100px;><img id="blah3" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="300px" width="auto" name="foto_produk"/></center><br>
                    <input type='file' required class="form-control @error('foto_produk') is-invalid @enderror" name="foto_produk" onchange="readURL3(this);" />
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label >Lampiran SIUP</label>
                        <center padding-bottom: 100px;><img id="blah" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="150px" name="lampiran_siup"/></center><br>
                        <input type='file' required class="form-control @error('lampiran_siup') is-invalid @enderror" name="lampiran_siup" onchange="readURL(this);" />
                    </div>
                    <div class="form-group col-md-6">
                        <label >Lampiran NPWP</label>
                        <center padding-bottom: 100px;><img id="blah2" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="150px" name="lampiran_npwp"/></center><br>
                        <input type='file' required class="form-control @error('lampiran_npwp') is-invalid @enderror" name="lampiran_npwp" onchange="readURL2(this);" />
                    </div>
                </div>
                <div class="form-group">
                    <label>Keterangan</label>
                    <textarea id="basic-example" name="keterangan" class="form-control @error('keterangan') is-invalid @enderror" placeholder="Minimal 200 karakter"></textarea>
                </div>
                <button type="submit" class="btn btn-primary btn btn-lg btn-block background-main-color text-white text-center font-weight-bold text-uppercase rounded-0 padding-15px">PROSES DAFTAR</button>
            </form>
            @endguest

        </div>
    </section>
    <!-- // Pelayanan -->    
@endsection

@section('script')
    <script>
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4',
            format: 'yyyy-mm-dd',
        });

        // Dropdown Hover
        const $dropdown = $(".dropdown");
            const $dropdownToggle = $(".dropdown-toggle");
            const $dropdownMenu = $(".dropdown-menu");
            const showClass = "show";

            $(window).on("load resize", function() {
            if (this.matchMedia("(min-width: 768px)").matches) {
                $dropdown.hover(
                function() {
                    const $this = $(this);
                    $this.addClass(showClass);
                    $this.find($dropdownToggle).attr("aria-expanded", "true");
                    $this.find($dropdownMenu).addClass(showClass);
                },
                function() {
                    const $this = $(this);
                    $this.removeClass(showClass);
                    $this.find($dropdownToggle).attr("aria-expanded", "false");
                    $this.find($dropdownMenu).removeClass(showClass);
                }
                );
            } else {
                $dropdown.off("mouseenter mouseleave");
            }
        });
    </script>

    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        function readURL2(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah2')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        function readURL3(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah3')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <script>
        tinymce.init({
            selector: 'textarea#basic-example',
            height: 300,
            menubar: false,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code help wordcount'
            ],
            toolbar: 'undo redo | formatselect | ' +
            'bold italic backcolor | alignleft aligncenter ' +
            'alignright alignjustify | bullist numlist outdent indent | ' +
            'removeformat | help',
            content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
        });
    </script>
@endsection