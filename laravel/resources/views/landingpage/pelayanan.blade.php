@extends('layouts.landingpage')

@section('title')
<title>Banaran | Pelayanan</title>    
@endsection

@section('pagetitle')
<section class="background-grey-1 padding-tb-25px text-grey-4">
    <div id="hapus" class="container">
        <h6 class="float-md-left font-2 mt-3">Pelayanan</h6>
        <ol class="breadcrumb float-md-right">
            <li><a href="#" class="text-grey-4">Home</a></li><i class="fa fa-angle-right"> </i>
            <li class="active">Pelayanan</li>
        </ol>
        <div class="clearfix"></div>
    </div>
</section>
@endsection

@section('content')
    <!-- Pelayanan -->
    <section class="padding-tb-50px">
        <div class="container pad-top pad-bott">
            <div class="text-center margin-bottom-35px wow fadeInUp" style="padding-bottom: 50px;">
                <h1 class="font-weight-300 text-title-large font-3 text-main-color wow fadeInUp" data-wow-delay="0.2s">PELAYANAN ADMINISTRASI KEPENDUDUKAN</h1>
                <span class="opacity-7">Layanan Yang Tersedia di Kelurahan Banaran</span>
            </div>
            <div class="row justify-content-md-center wow fadeInUp">
                <div class="col-12 mb-3 col-sm-6 col-md-4 col-lg-3">
                    <a href="" data-toggle="modal" data-target="#aktakelahiran">
                        <div class="card-box widget-box-two"
                            style="background-color: #008dde; border-radius: 8px; padding: 15px">
                            <div class="media">
                                <div class="media-left " style="padding-right: 5px">
                                    <img class="media-object img-circle hidden-xs " alt=""
                                        src="{{ URL::to('public') }}/assets/img/widget/undraw_baby_ja7a.svg"
                                        data-src="{{ URL::to('public') }}/assets/img/widget/undraw_baby_ja7a.svg"
                                        style="width: 128px; height: 128px;">
                                </div>
                                <div class="my-auto media-body" style="text-align: center; vertical-align: middle; ">
                                    <h2 class="media-heading hidden-xs" style="color: white">Akta Kelahiran</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-12 mb-3 col-sm-6 col-md-4 col-lg-3 wow fadeInUp" data-wow-delay="0.2s">
                    <a href="" data-toggle="modal" data-target="#aktakematian">
                        <div class="card-box widget-box-two"
                            style="background-color: #008dde; border-radius: 8px; padding: 15px">
                            <div class="media">
                                <div class="media-left " style="padding-right: 5px">
                                    <img class="media-object img-circle hidden-xs " alt=""
                                        src="{{ URL::to('public') }}/assets/img/widget/undraw_rising_8svm.svg"
                                        data-src="{{ URL::to('public') }}/assets/img/widget/undraw_rising_8svm.svg"
                                        style="width: 128px; height: 128px;">
                                </div>
                                <div class="my-auto media-body" style="text-align: center; vertical-align: middle; ">
                                    <h2 class="media-heading hidden-xs" style="color: white">Akta Kematian</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-12 mb-3 col-sm-6 col-md-4 col-lg-3 wow fadeInUp" data-wow-delay="0.4s">
                    <a href="" data-toggle="modal" data-target="#ktp">
                        <div class="card-box widget-box-two"
                            style="background-color: #008dde; border-radius: 8px; padding: 15px">
                            <div class="media">
                                <div class="media-left " style="padding-right: 5px">
                                    <img class="media-object img-circle hidden-xs " alt=""
                                        src="{{ URL::to('public') }}/assets/img/widget/undraw_Credit_card_re_blml.svg"
                                        data-src="{{ URL::to('public') }}/assets/img/widget/undraw_Credit_card_re_blml.svg"
                                        style="width: 128px; height: 128px;">
                                </div>
                                <div class="my-auto media-body" style="text-align: center; vertical-align: middle; ">
                                    <h2 class="media-heading hidden-xs" style="color: white">KTP Baru</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-12 mb-3 col-sm-6 col-md-4 col-lg-3 wow fadeInUp" data-wow-delay="0.6s">
                    <a href="" data-toggle="modal" data-target="#kehilangan">
                        <div class="card-box widget-box-two"
                            style="background-color: #008dde; border-radius: 8px; padding: 15px">
                            <div class="media">
                                <div class="media-left " style="padding-right: 5px">
                                    <img class="media-object img-circle hidden-xs " alt=""
                                        src="{{ URL::to('public') }}/assets/img/widget/undraw_blog_anyj.svg"
                                        data-src="{{ URL::to('public') }}/assets/img/widget/undraw_blog_anyj.svg"
                                        style="width: 128px; height: 128px;">
                                </div>
                                <div class="my-auto media-body" style="text-align: center; vertical-align: middle; ">
                                    <h2 class="media-heading hidden-xs" style="color: white">Mengurus Kehilangan</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-12 mb-3 col-sm-6 col-md-4 col-lg-3 wow fadeInUp" data-wow-delay="0.8s">
                    <a href="" data-toggle="modal" data-target="#domisili">
                        <div class="card-box widget-box-two"
                            style="background-color: #008dde; border-radius: 8px; padding: 15px">
                            <div class="media">
                                <div class="media-left " style="padding-right: 5px">
                                    <img class="media-object img-circle hidden-xs " alt=""
                                        src="{{ URL::to('public') }}/assets/img/widget/undraw_map_1r69.svg"
                                        data-src="{{ URL::to('public') }}/assets/img/widget/undraw_map_1r69.svg"
                                        style="width: 128px; height: 128px;">
                                </div>
                                <div class="my-auto media-body" style="text-align: center; vertical-align: middle; ">
                                    <h2 class="media-heading hidden-xs" style="color: white">Keterangan Domisili</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-12 mb-3 col-sm-6 col-md-4 col-lg-3 wow fadeInUp" data-wow-delay="1s">
                    <a href="" data-toggle="modal" data-target="#pindahkeluar">
                        <div class="card-box widget-box-two"
                            style="background-color: #008dde; border-radius: 8px; padding: 15px">
                            <div class="media">
                                <div class="media-left " style="padding-right: 5px">
                                    <img class="media-object img-circle hidden-xs " alt=""
                                        src="{{ URL::to('public') }}/assets/img/widget/undraw_traveling_t8y2.svg"
                                        data-src="{{ URL::to('public') }}/assets/img/widget/undraw_traveling_t8y2.svg"
                                        style="width: 128px; height: 128px;">
                                </div>
                                <div class="my-auto media-body" style="text-align: center; vertical-align: middle; ">
                                    <h2 class="media-heading hidden-xs" style="color: white">Perpindahan Keluar</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-12 mb-3 col-sm-6 col-md-4 col-lg-3 wow fadeInUp" data-wow-delay="1.2s">
                    <a href="" data-toggle="modal" data-target="#pindahdatang">
                        <div class="card-box widget-box-two"
                            style="background-color: #008dde; border-radius: 8px; padding: 15px">
                            <div class="media">
                                <div class="media-left " style="padding-right: 5px">
                                    <img class="media-object img-circle hidden-xs " alt=""
                                        src="{{ URL::to('public') }}/assets/img/widget/undraw_travelers_qlt1.svg"
                                        data-src="{{ URL::to('public') }}/assets/img/widget/undraw_travelers_qlt1.svg"
                                        style="width: 128px; height: 128px;">
                                </div>
                                <div class="my-auto media-body" style="text-align: center; vertical-align: middle; ">
                                    <h2 class="media-heading hidden-xs" style="color: white">Kedatangan</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- // Pelayanan -->

<!-- Modal Akta Kelahiran -->
<div class="modal fade bd-example-modal-lg" id="aktakelahiran" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" style="; ">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="page-title" id="myModalLabel">Pengajuan Surat Keterangan Akta Kelahiran</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Persyaratan :</p>
                <ol class="font-2 text-justify">
                    <li>KTP Kedua Orang Tua</li>
                    <li>Buku Nikah</li>
                    <li>Kartu Keluarga</li>
                    <li>Surat Keterangan Lahir dari Rumah Sakit</li>
                    <li>KTP 2 Orang Saksi</li>
                    <li>Surat Pengantar RT</li>
                </ol>
                <p>Tata Cara :</p>
                <ol class="font-2 text-justify">
                    <li>Login dengan menggunakan akun yang sudah anda registrasi sebelumnya.</li>
                    <li>Isikan semua form yang tersedia dengan data yang benar.</li>
                    <li>Upload file sesuai dengan format yang diizinkan.</li>
                    <li>Setelah data semua diupload baru di klik Kirim.</li>
                    <li>Proses pengajuan dapat kami proses selambat - lambatnya 1 x 24 jam.</li>
                    <li>Jika pengajuan <span style="color: green"><b>terverifikasi</b></span>, pemohon mendapatkan notifikasi beserta berkas yang telah dibutuhkan melalui email.</li>
                    <li>Download dan cetak berkas yang telah dikirimkan melalui email dengan <b>kertas berukuran Legal.</b></li>
                    <li>Pengajuan <span style="color: red"><b>tidak akan diproses</b></span> apabila data yang anda masukkan tidak sesuai dengan kependudukan Desa Banaran dan tidak valid.</li>
                </ol>
            </div>
            <div class="modal-footer">
                @guest
                <a href="{{url('login')}}"><button class="btn btn-primary"><i class="fa fa-arrow-circle-o-right"></i>&nbsp; Lanjutkan</button></a>
                @else
                <a href="{{url('pelayanan/akta-kelahiran')}}"><button class="btn btn-primary"><i class="fa fa-arrow-circle-o-right"></i>&nbsp; Lanjutkan</button></a>
                @endguest
            </div>
        </div>
    </div>
</div>
<!-- // Modal Akta Kelahiran -->

<!-- Modal Akta Kematian -->
<div class="modal fade bd-example-modal-lg" id="aktakematian" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" style="; ">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="page-title" id="myModalLabel">Pengajuan Surat Keterangan Akta Kematian</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Persyaratan :</p>
                <ol class="font-2 text-justify">
                    <li>KTP Asli Orang yang Akan Dibuatkan Akta Kematian</li>
                    <li>Kartu Keluarga</li>
                    <li>Surat Keterangan Kematian Dari Rumah Sakit *</li>
                    <li>KTP Pelapor</li>
                    <li>KTP 2 Orang Saksi</li>
                    <li>Surat Pengantar RT</li>
                </ol>
                <p>Tata Cara :</p>
                <ol class="font-2 text-justify">
                    <li>Login dengan menggunakan akun yang sudah anda registrasi sebelumnya.</li>
                    <li>Isikan semua form yang tersedia dengan data yang benar.</li>
                    <li>Upload file sesuai dengan format yang diizinkan.</li>
                    <li>Setelah data semua diupload baru di klik Kirim.</li>
                    <li>Proses pengajuan dapat kami proses selambat - lambatnya 1 x 24 jam.</li>
                    <li>Jika pengajuan <span style="color: green"><b>terverifikasi</b></span>, pemohon mendapatkan notifikasi beserta berkas yang telah dibutuhkan melalui email.</li>
                    <li>Download dan cetak berkas yang telah dikirimkan melalui email dengan <b>kertas berukuran Legal.</b></li>
                    <li>Pengajuan <span style="color: red"><b>tidak akan diproses</b></span> apabila data yang anda masukkan tidak sesuai dengan kependudukan Desa Banaran dan tidak valid.</li>
                </ol>
                <span style="font-size: 10px;color:red"><b>*) Catatan : Jika dokumen tersebut ada.</b></span>
            </div>
            <div class="modal-footer">
                @guest
                <a href="{{url('login')}}"><button class="btn btn-primary"><i class="fa fa-arrow-circle-o-right"></i>&nbsp; Lanjutkan</button></a>
                @else
                <a href="{{url('pelayanan/akta-kematian')}}"><button class="btn btn-primary"><i class="fa fa-arrow-circle-o-right"></i>&nbsp; Lanjutkan</button></a>
                @endguest
            </div>
        </div>
    </div>
</div>
<!-- // Modal Akta Kematian -->

<!-- Modal KTP -->
<div class="modal fade bd-example-modal-lg" id="ktp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" style="; ">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="page-title" id="myModalLabel">Pengajuan Surat Keterangan KTP Baru</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Persyaratan :</p>
                <ol class="font-2 text-justify">
                    <li>Kartu Keluarga</li>
                    <li>Surat Pengantar RT</li>
                </ol>
                <p>Tata Cara :</p>
                <ol class="font-2 text-justify">
                    <li>Login dengan menggunakan akun yang sudah anda registrasi sebelumnya.</li>
                    <li>Isikan semua form yang tersedia dengan data yang benar.</li>
                    <li>Upload file sesuai dengan format yang diizinkan.</li>
                    <li>Setelah data semua diupload baru di klik Kirim.</li>
                    <li>Proses pengajuan dapat kami proses selambat - lambatnya 1 x 24 jam.</li>
                    <li>Jika pengajuan <span style="color: green"><b>terverifikasi</b></span>, pemohon mendapatkan notifikasi beserta berkas yang telah dibutuhkan melalui email.</li>
                    <li>Download dan cetak berkas yang telah dikirimkan melalui email dengan <b>kertas berukuran Legal.</b></li>
                    <li>Pengajuan <span style="color: red"><b>tidak akan diproses</b></span> apabila data yang anda masukkan tidak sesuai dengan kependudukan Desa Banaran dan tidak valid.</li>
                </ol>
            </div>
            <div class="modal-footer">
                @guest
                <a href="{{url('login')}}"><button class="btn btn-primary"><i class="fa fa-arrow-circle-o-right"></i>&nbsp; Lanjutkan</button></a>
                @else
                <a href="{{url('pelayanan/ktp-baru')}}"><button class="btn btn-primary"><i class="fa fa-arrow-circle-o-right"></i>&nbsp; Lanjutkan</button></a>
                @endguest
            </div>
        </div>
    </div>
</div>
<!-- // Modal KTP -->

<!-- Modal Kehilangan -->
<div class="modal fade bd-example-modal-lg" id="kehilangan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" style="; ">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="page-title" id="myModalLabel">Pengajuan Surat Keterangan Kehilangan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Persyaratan :</p>
                <ol class="font-2 text-justify">
                    <li>KTP</li>
                    <li>Kartu Keluarga</li>
                    <li>Surat Pengantar RT</li>
                </ol>
                <p>Tata Cara :</p>
                <ol class="font-2 text-justify">
                    <li>Login dengan menggunakan akun yang sudah anda registrasi sebelumnya.</li>
                    <li>Isikan semua form yang tersedia dengan data yang benar.</li>
                    <li>Upload file sesuai dengan format yang diizinkan.</li>
                    <li>Setelah data semua diupload baru di klik Kirim.</li>
                    <li>Proses pengajuan dapat kami proses selambat - lambatnya 1 x 24 jam.</li>
                    <li>Jika pengajuan <span style="color: green"><b>terverifikasi</b></span>, pemohon mendapatkan notifikasi beserta berkas yang telah dibutuhkan melalui email.</li>
                    <li>Download dan cetak berkas yang telah dikirimkan melalui email dengan <b>kertas berukuran Legal.</b></li>
                    <li>Pengajuan <span style="color: red"><b>tidak akan diproses</b></span> apabila data yang anda masukkan tidak sesuai dengan kependudukan Desa Banaran dan tidak valid.</li>
                </ol>
            </div>
            <div class="modal-footer">
                @guest
                <a href="{{url('login')}}"><button class="btn btn-primary"><i class="fa fa-arrow-circle-o-right"></i>&nbsp; Lanjutkan</button></a>
                @else
                <a href="{{url('pelayanan/mengurus-kehilangan')}}"><button class="btn btn-primary"><i class="fa fa-arrow-circle-o-right"></i>&nbsp; Lanjutkan</button></a>
                @endguest
            </div>
        </div>
    </div>
</div>
<!-- // Modal Kehilangan -->

<!-- Modal Domisili -->
<div class="modal fade bd-example-modal-lg" id="domisili" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" style="; ">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="page-title" id="myModalLabel">Pengajuan Surat Keterangan Domisili</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Persyaratan :</p>
                <ol class="font-2 text-justify">
                    <li>KTP</li>
                    <li>Kartu Keluarga</li>
                    <li>Surat Pengantar RT</li>
                </ol>
                <p>Tata Cara :</p>
                <ol class="font-2 text-justify">
                    <li>Login dengan menggunakan akun yang sudah anda registrasi sebelumnya.</li>
                    <li>Isikan semua form yang tersedia dengan data yang benar.</li>
                    <li>Upload file sesuai dengan format yang diizinkan.</li>
                    <li>Setelah data semua diupload baru di klik Kirim.</li>
                    <li>Proses pengajuan dapat kami proses selambat - lambatnya 1 x 24 jam.</li>
                    <li>Jika pengajuan <span style="color: green"><b>terverifikasi</b></span>, pemohon mendapatkan notifikasi beserta berkas yang telah dibutuhkan melalui email.</li>
                    <li>Download dan cetak berkas yang telah dikirimkan melalui email dengan <b>kertas berukuran Legal.</b></li>
                    <li>Pengajuan <span style="color: red"><b>tidak akan diproses</b></span> apabila data yang anda masukkan tidak sesuai dengan kependudukan Desa Banaran dan tidak valid.</li>
                </ol>
            </div>
            <div class="modal-footer">
                @guest
                <a href="{{url('login')}}"><button class="btn btn-primary"><i class="fa fa-arrow-circle-o-right"></i>&nbsp; Lanjutkan</button></a>
                @else
                <a href="{{url('pelayanan/keterangan-domisili')}}"><button class="btn btn-primary"><i class="fa fa-arrow-circle-o-right"></i>&nbsp; Lanjutkan</button></a>
                @endguest
            </div>
        </div>
    </div>
</div>
<!-- // Modal Domisili -->

<!-- Modal Pindah Keluar -->
<div class="modal fade bd-example-modal-lg" id="pindahkeluar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" style="; ">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="page-title" id="myModalLabel">Pengajuan Surat Keterangan Pindah Keluar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Persyaratan :</p>
                <ol class="font-2 text-justify">
                    <li>KTP</li>
                    <li>Kartu Keluarga</li>
                    <li>Akta Kelahiran</li>
                    <li>Ijazah Terakhir</li>
                    <li>Akta Nikah *</li>
                    <li>Surat Pengantar RT</li>
                </ol>
                <p>Tata Cara :</p>
                <ol class="font-2 text-justify">
                    <li>Login dengan menggunakan akun yang sudah anda registrasi sebelumnya.</li>
                    <li>Isikan semua form yang tersedia dengan data yang benar.</li>
                    <li>Upload file sesuai dengan format yang diizinkan.</li>
                    <li>Setelah data semua diupload baru di klik Kirim.</li>
                    <li>Proses pengajuan dapat kami proses selambat - lambatnya 1 x 24 jam.</li>
                    <li>Jika pengajuan <span style="color: green"><b>terverifikasi</b></span>, pemohon mendapatkan notifikasi beserta berkas yang telah dibutuhkan melalui email.</li>
                    <li>Download dan cetak berkas yang telah dikirimkan melalui email dengan <b>kertas berukuran Legal.</b></li>
                    <li>Pengajuan <span style="color: red"><b>tidak akan diproses</b></span> apabila data yang anda masukkan tidak sesuai dengan kependudukan Desa Banaran dan tidak valid.</li>
                </ol>
                <span style="font-size: 10px;color:red"><b>*) Catatan : Bagi yang sudah menikah.</b></span>
            </div>
            <div class="modal-footer">
                @guest
                <a href="{{url('login')}}"><button class="btn btn-primary"><i class="fa fa-arrow-circle-o-right"></i>&nbsp; Lanjutkan</button></a>
                @else
                <a href="{{url('pelayanan/pindah-keluar')}}"><button class="btn btn-primary"><i class="fa fa-arrow-circle-o-right"></i>&nbsp; Lanjutkan</button></a>
                @endguest
            </div>
        </div>
    </div>
</div>
<!-- // Modal Pindah Keluar -->

<!-- Modal Pindah Datang -->
<div class="modal fade bd-example-modal-lg" id="pindahdatang" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" style="; ">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="page-title" id="myModalLabel">Pengajuan Surat Keterangan Pindah Datang</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Persyaratan :</p>
                <ol class="font-2 text-justify">
                    <li>KTP</li>
                    <li>Kartu Keluarga</li>
                    <li>Akta Kelahiran</li>
                    <li>Ijazah Terakhir</li>
                    <li>Akta Nikah *</li>
                    <li>Surat Pindah Dari Tempat Asal</li>
                </ol>
                <p>Tata Cara :</p>
                <ol class="font-2 text-justify">
                    <li>Login dengan menggunakan akun yang sudah anda registrasi sebelumnya.</li>
                    <li>Isikan semua form yang tersedia dengan data yang benar.</li>
                    <li>Upload file sesuai dengan format yang diizinkan.</li>
                    <li>Setelah data semua diupload baru di klik Kirim.</li>
                    <li>Proses pengajuan dapat kami proses selambat - lambatnya 1 x 24 jam.</li>
                    <li>Jika pengajuan <span style="color: green"><b>terverifikasi</b></span>, pemohon mendapatkan notifikasi beserta berkas yang telah dibutuhkan melalui email.</li>
                    <li>Download dan cetak berkas yang telah dikirimkan melalui email dengan <b>kertas berukuran Legal.</b></li>
                    <li>Pengajuan <span style="color: red"><b>tidak akan diproses</b></span> apabila data yang anda masukkan tidak benar.</li>
                </ol>
                <span style="font-size: 10px;color:red"><b>*) Catatan : Bagi yang sudah menikah.</b></span>
            </div>
            <div class="modal-footer">
                @guest
                <a href="{{url('login')}}"><button class="btn btn-primary"><i class="fa fa-arrow-circle-o-right"></i>&nbsp; Lanjutkan</button></a>
                @else
                <a href="{{url('pelayanan/pindah-masuk')}}"><button class="btn btn-primary"><i class="fa fa-arrow-circle-o-right"></i>&nbsp; Lanjutkan</button></a>
                @endguest
            </div>
        </div>
    </div>
</div>
<!-- // Modal Pindah Datang -->
@endsection