@extends('layouts.landingpage')

@section('title')
<title>Kelurahan Banaran | Pelayanan</title>    
@endsection

@section('style')
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('pagetitle')
<section class="background-grey-1 padding-tb-25px text-grey-4">
    <div id="hapus" class="container">
        <h6 class="float-md-left font-2 mt-3">Pelayanan</h6>
        <ol class="breadcrumb float-md-right">
            <li><a href="#" class="text-grey-4">Home</a></li><i class="fa fa-angle-right"> </i>
            <li class="active">Pelayanan</li>
        </ol>
        <div class="clearfix"></div>
    </div>
</section>
    
@endsection

@section('content')
    <!-- Pelayanan -->
    <section>
        <div class="container" style="padding-bottom: 100px;">
            <div class="row justify-content-md-center">
                <div class="col-lg-6 col-md-4">
                    <div class="row justify-content-md-center">
                        <div class="my-auto col-md-3">
                            <i class="fa fa-check-square fa-4x" aria-hidden="true"></i>
                        </div>
                        <div class="col-lg-9 mb-md-0 mb-0">
                            <h4><b>Prosedur Pengajuan</b></h4>
                            <p>Pehatikan mengenai prosedur pengajuan surat</p>
                        </div>
                        
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row justify-content-md-center">
                        <div class="my-auto col-md-3">
                            <i class="fa fa-list fa-4x" aria-hidden="true"></i>
                        </div>
                        <div class="col-lg-9 mb-md-0 mb-0">
                            <h4><b>Form Pelayanan</b></h4>
                            <p>Lengkapi Formulir yang tersedia dengan data yang benar</p>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-check"></i> <strong>Berhasil! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Danger! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('warning'))
                    <div class="alert alert-warning">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-exclamation"></i> <strong>Warning! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('info'))
                    <div class="alert alert-info">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-info"></i> <strong>Info! &nbsp;&nbsp;</strong> 
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Bahaya! &nbsp;&nbsp;</strong>
                        Silakan periksa formulir di bawah ini untuk kesalahan <br>
                        <strong>
                            <ul>
                                @foreach ($errors->all() as $message)
                                    <li>{{$message}}</li>
                                @endforeach
                            </ul>
                        </strong>
                    </div>
                    @endif
                    <!-- ===================================== PENGUNJUNG ========================================= !-->
                    @guest
                    <form class="padding-top-10px margin-top-10px border-top-1 border-grey-1">
                        <div class="form-group">
                            <label >Nama Lengkap</label>
                            <input type="text" class="form-control" id="inputName4" placeholder="Nama Lengkap">
                        </div>
						<div class="form-row">
							<div class="form-group col-md-6">
								<label >Tempat Lahir</label>
								<input type="text" class="form-control" id="inputName4" placeholder="Tempat Lahir">
							</div>
							<div class="form-group col-md-6">
								<label >Tanggal Lahir</label>
								{{-- <input type="email" class="form-control" id="inputEmail4" placeholder="Tanggal Lahir"> --}}
                                <input id="datepicker"/>
							</div>
                        </div>
                        <div class="form-row">
							<div class="form-group col-md-6">
                                <label >Jenis Kelamin</label>
                                <select name="" id="" class="form-control">
                                    <option value="">Laki - Laki</option>
                                    <option value="">Perempuan</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Agama</label>
                                <select name="" id="" class="form-control">
                                    <option value="">Islam</option>
                                    <option value="">Kristen</option>
                                    <option value="">Katholik</option>
                                    <option value="">Hindu</option>
                                    <option value="">Budha</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
							<div class="form-group col-md-6">
                                <label >Pekerjaan</label>
                                <select name="" id="" class="form-control">
                                    <option value="">Belum/Tidak Bekerja</option>
                                    <option value="">Mengurus Rumah Tangga</option>
                                    <option value="">Pelajar/Mahasiswa</option>
                                    <option value="">Pensiunan</option>
                                    <option value="">Pegawai Negeri Sipil</option>
                                    <option value="">Karyawan Swasta</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Status Perkawinan</label>
                                <select name="" id="" class="form-control">
                                    <option value="">Belum Kawin</option>
                                    <option value="">Kawin</option>
                                    <option value="">Pernah Kawin</option>
                                </select>
							</div>
                        </div>
                        <div class="form-group">
                            <label >Alamat Lengkap</label>
                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="2"></textarea>
                        </div>
                        <div class="form-group">
                            <label >NIK</label>
                            <input type="text" class="form-control" id="inputName4" placeholder="NIK">
                        </div>
                        <div class="form-group">
                            <label >Nomor KK</label>
                            <input type="text" class="form-control" id="inputName4" placeholder="Nomor KK">
                        </div>
                        <div class="form-group">
                            <label >Lampiran Surat Pengantar RT</label>
                            {{-- <input type="file" class="form-control" id="inputName4" name="lampiran_kk" placeholder="NIK"> --}}
                            <center padding-bottom: 100px;><img id="blah" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="300px" width="auto" name="lampiran_ktp"/></center><br>
                            <input type='file' class="form-control" name="lampiran_surat_pengantar_RT" onchange="readURL(this);" />
                        </div>
                        <div class="form-group">
                            <label >Keperluan</label>
                            <select name="" id="" class="form-control">
                                <option value="">Belum Kawin</option>
                                <option value="">Kawin</option>
                                <option value="">Pernah Kawin</option>
                            </select>
                        </div>
						<div class="form-group">
							<label>Keterangan</label>
							<textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
						</div>
						<a href="{{ url('/login') }}" class="btn btn-primary btn-block background-main-color text-white text-center font-weight-bold text-uppercase rounded-0 padding-15px">PROSES PENGAJUAN</a>
                    </form>
                    
                    <!-- ================================================ AUTHENTHIFIKASI =========================================== !-->
                    @else
                        {{-- @foreach ($user as $row) --}}
                        <form class="padding-top-10px margin-top-10px border-top-1 border-grey-1" action="{{ route('pelayanan')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="role" value="{{ Auth::user()->role }}">
                            <input type="hidden" class="form-control" name="id_user" id="inputName4" placeholder="Nama Lengkap" value="{{ Auth::user()->id }}">
                            <div class="form-group">
                                <label >Nama Lengkap</label>
                                <input type="text" class="form-control" name="nama_lengkap" id="inputName4" placeholder="Nama Lengkap" value="{{ Auth::user()->name }}" disabled>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label >Tempat Lahir</label>
                                    <input type="text" class="form-control" name="tempat_lahir" id="inputName4" placeholder="Tempat Lahir" value="{{ Auth::user()->tempat_lahir }}" disabled>
                                </div>
                                <div class="form-group col-md-6">
                                    <label >Tanggal Lahir</label>
                                    {{-- <input id="datepicker" name="tanggal_lahir"  placeholder="Tanggal Lahir" value="{{ uth::user()->tgl_lahir }}"> --}}
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="datepicker" value="{{ Auth::user()->tgl_lahir }}" disabled>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label >Jenis Kelamin</label>
                                    <select name="jenis_kelamin" id="" class="form-control" disabled>
                                        @if (Auth::user()->jeniskelamin == "L")
                                        <option <?php 'selected' ?> value="L"> Laki - Laki </option>
                                        @else
                                        <option <?php 'selected' ?> value="P"> Perempuan </option>
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Agama</label>
                                    <select name="agama" id="" class="form-control" disabled>
                                        @foreach ($agama as $row1)
                                            <?php
                                            $selected = ' ';
                                            if(Auth::user()->t_agama_id == $row1['id']){
                                                $selected = 'selected';
                                            }
                                            ?>
                                            <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['agama']; ?> </option>
                                            ?>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label >Pekerjaan</label>
                                    <select name="pekerjaan" id="" class="form-control" disabled>
                                        @foreach ($pekerjaan as $row1)
                                            <?php
                                            $selected = ' ';
                                            if(Auth::user()->t_pekerjaan_id == $row1['id']){
                                                $selected = 'selected';
                                            }
                                            ?>
                                            <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['pekerjaan']; ?> </option>
                                            ?>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label >Status Perkawinan</label>
                                    <select name="status_perkawinan" id="" class="form-control" disabled>
                                        @if (Auth::user()->statusperkawinan == "Belum Kawin")
                                        <option <?php 'selected' ?> value="Belum Kawin"> Belum Kawin </option>
                                        @elseif (Auth::user()->statusperkawinan == "Kawin")
                                        <option <?php 'selected' ?> value="Kawin"> Kawin </option>
                                        @else 
                                        <option <?php 'selected' ?> value="Pernah Kawin"> Pernah Kawin </option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label >Dukuh</label>
                                    <select name="dukuh" id="" class="form-control" disabled>
                                        @foreach ($dukuh as $row1)
                                            <?php
                                            $selected = ' ';
                                            if(Auth::user()->t_dukuh_id == $row1['id']){
                                                $selected = 'selected';
                                            }
                                            ?>
                                            <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['dukuh']; ?> </option>
                                            ?>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label >RT</label>
                                    <select name="rt" id="" class="form-control" disabled>
                                        @foreach ($rt as $row1)
                                        <?php
                                        $selected = ' ';
                                        if(Auth::user()->t_rt_id == $row1['id']){
                                            $selected = 'selected';
                                        }
                                        ?>
                                        <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['rt']; ?> </option>
                                        ?>
                                            {{-- <option value="{{ $row1->id }}">{{ $row1->tag }}</option> --}}
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label >NIK</label>
                                <input type="text" class="form-control" id="inputName4" name="no_nik" placeholder="NIK" value="{{ Auth::user()->nik }}" disabled>
                            </div>
                            <div class="form-group">
                                <label >Nomor KK</label>
                                <input type="text" class="form-control" id="inputName4" name="no_kk" placeholder="Nomor KK" value="{{ Auth::user()->no_kk }}" disabled>
                            </div>
                            <div class="form-group">
                                <label >Lampiran Surat Pengantar RT</label>
                                {{-- <input type="file" class="form-control" id="inputName4" name="lampiran_kk" placeholder="NIK"> --}}
                                <center padding-bottom: 100px;><img id="blah" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="300px" width="auto" name="lampiran_ktp"/></center><br>
                                <input  style="border-color:#0093dd" type='file' class="form-control" name="lampiran_surat_pengantar_RT" onchange="readURL(this);" />
							</div>
                            <div class="form-group">
                                <label >Keperluan</label>
                                <select style="border-color:#0093dd" name="keperluan" id="" class="form-control">
                                    <option value="">-- Pilih Pelayanan --</option>
                                    @foreach ($keperluan as $row1)
                                        <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['keperluan']; ?> </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Keterangan</label>
                                <textarea name="keterangan"  style="border-color:#0093dd" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary btn-sm btn-lg btn-block background-main-color text-white text-center font-weight-bold text-uppercase rounded-0 padding-15px">PROSES PENGAJUAN</button>
                        </form>
                        {{-- @endforeach --}}
                    @endguest
                </div>

            </div>
        </div>
    </section>
    <!-- // Pelayanan -->    
@endsection

@section('script')
    <script>
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4',
            format: 'yyyy-mm-dd',
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).ready(function () {
            var url = window.location;
            $('ul.navbar-nav li a[href="' + url + '"]').parent().addClass('active');
            $('ul.navbar-nav li a').filter(function () {
                return this.href == url;
            }).parent().addClass('active');
        });
        
        // Dropdown Hover
        const $dropdown = $(".dropdown");
            const $dropdownToggle = $(".dropdown-toggle");
            const $dropdownMenu = $(".dropdown-menu");
            const showClass = "show";

            $(window).on("load resize", function() {
            if (this.matchMedia("(min-width: 768px)").matches) {
                $dropdown.hover(
                function() {
                    const $this = $(this);
                    $this.addClass(showClass);
                    $this.find($dropdownToggle).attr("aria-expanded", "true");
                    $this.find($dropdownMenu).addClass(showClass);
                },
                function() {
                    const $this = $(this);
                    $this.removeClass(showClass);
                    $this.find($dropdownToggle).attr("aria-expanded", "false");
                    $this.find($dropdownMenu).removeClass(showClass);
                }
                );
            } else {
                $dropdown.off("mouseenter mouseleave");
            }
        });
    </script>
@endsection