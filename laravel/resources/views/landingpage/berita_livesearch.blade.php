@if ($beritas ->count() > 0)
@foreach ($posts as $row)
<div class="col-lg-6" style="margin-bottom:35px">
    <div class="card">
        <div class="card-img-wrapper">
            <img class="card-img-top" src="{{ URL::to('public') }}/assets/1117942/{{ $row->image }}" alt="Card image cap">
        </div>
        <div class="card-body" style="text-align: justify">
            <a href="#" class="font-weight-bold d-block text-dark text-uppercase text-medium margin-bottom-10px font-weight-700">{{ \Illuminate\Support\Str::limit($row->judul, $limit = 50, $end = ' ...') }}</a>    
            <span class="text-extra-small"><a href="#" class="text-main-color">{{ Carbon\Carbon::parse($row['created_at'])->isoFormat('D MMMM Y') }}</a></span>
            <p class="card-text">{!! \Illuminate\Support\Str::limit($row->deskripsi, $limit = 150, $end = ' ......') !!}</p>
            <a href="{{ url('/berita', [$row->slug]) }}" class="btn btn-primary">Read More</a>
        </div>
    </div>
</div>
@endforeach
@else    
<p>Data Not Found</p>
@endif