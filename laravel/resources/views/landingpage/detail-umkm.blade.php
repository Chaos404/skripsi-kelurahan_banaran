@extends('layouts.landingpage')

@section('title')
<title>Kelurahan Banaran | Detail UMKM</title>    
@endsection

@section('pagetitle')
<section class="background-grey-1 padding-tb-25px text-grey-4">
    <div id="hapus" class="container">
        <h6 class="float-md-left font-2 mt-3">Detail UMKM</h6>
        <ol class="breadcrumb float-md-right">
            <li><a href="#" class="text-grey-4">Home</a></li><i class="fa fa-angle-right"> </i>
            <li class="active">Detail UMKM</li>
        </ol>
        <div class="clearfix"></div>
    </div>
</section>
    
@endsection

@section('content')
    <!-- Detail UMKM -->
    <section class="padding-tb-50px">
        <div class="container pad-top pad-bott">
            <div class="row">
                <div class="col-lg-8 white-bg">
                    <!-- post -->
					<div class="blog-entry background-white border-1 border-grey-1 margin-bottom-35px">
						<div class="padding-30px">
							<div class="meta">
                                <span class="margin-right-20px text-extra-small">Pemilik : <a href="#" class="text-main-color">{{ $umkm->User['name'] }}</a></span>
								<span class="text-extra-small" style="float: right"><a style="float: right" target="_blank" href="https://api.whatsapp.com/send?phone={{ $umkm->no_whatsapp }}&text=Saya berminat dan bermaksud untuk order :)" class="btn btn-sm btn-success"><i class="fa fa-whatsapp"></i> Hubungi Pejual</a></span>
							</div>
							<hr>
							<h1 class="d-block  text-capitalize text-large text-dark font-weight-700 margin-bottom-10px" href="#">{{ $umkm->merk_usaha }}</h1>
                            <hr>
                            
                            <center><img src="{{ URL::to('public') }}/assets/1119126/{{ $umkm->image }}" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></center>
                            
                            <br>
							<div class="post-entry">
                                {!! $umkm->keterangan !!}
                            </div>
                            <!-- // post -->
                        </div>
                        <div id="disqus_thread"></div>
                            <script type="text/javascript">
                                /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
                                var disqus_shortname = 'alvicky31'; // required: replace example with your forum shortname
                            
                                /* * * DON'T EDIT BELOW THIS LINE * * */
                                (function() {
                                    var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                                    dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                                    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                                })();
                            </script>
                            <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a>
                            </noscript>
                            <a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
					</div>
                </div>

                <div class="col-lg-4 col-md-4">
                    <!-- widget Latest -->
					<div class="widget">
                        <h4 class="widget-title clearfix"><span><i class="fa fa-search" aria-hidden="true"></i>&nbsp;<b>Search</b></span></h4>
                        <form action="{{ route('search') }}" method="GET" autocomplete="off">
                            <div class="input-group my-2">
                                <input type="text" class="form-control" placeholder="Search" aria-label="Search" name="search" required  style="border-color:#0093dd">
                                <div class="input-group-append">
                                    <button class="btn" type="submit" style="background-color:#FFFFFF; border-color:#0093dd">
                                        <i class="fa fa-search" style="color:#0093dd"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                        <br>
						<h4 class="widget-title clearfix"><span><i class="fa fa-file" aria-hidden="true"></i>&nbsp;<b>Terkini</b></span></h4>
						<ul class="last-posts">
							@foreach ($terkinis as $terkini)
                            <li>
								<a href="{{ url('/berita', [$terkini->slug]) }}" class="float-left margin-right-widget width-widget"><img src="{{ URL::to('public') }}/assets/1117942/{{ $terkini->image }}" alt=""></a>
								<a href="{{ url('/berita', [$terkini->slug]) }}" class="font-weight-bold d-block  text-uppercase text-medium text-dark font-weight-700">{{ \Illuminate\Support\Str::limit($terkini->judul, $limit = 20, $end = ' ...') }}</a>
								<span class="text-extra-small">Date :  <a href="{{ url('/berita', [$terkini->slug]) }}" class="text-main-color">{{ Carbon\Carbon::parse($terkini['created_at'])->isoFormat('D MMMM Y') }}</a></span>
							</li>
                            @endforeach
						</ul>

					</div>
					<!-- //  widget Latest -->

                    <!-- Informasi -->
                    <div class="widget widget_categories">
                        <h4 class="widget-title clearfix"><span><i class="fa fa-bullhorn"
                            aria-hidden="true"></i>&nbsp;<b>Informasi</b></span></h4>
                        <div class="row">
                            @foreach ($informasis as $informasi)
                            <a href="{{ url('/berita', [$informasi->slug]) }}"><div class="col-lg-6 col-md-6 mb-3 sm-mb-30px"><a href="{{ url('/berita', [$informasi->slug]) }}"><img src="{{ URL::to('public') }}/assets/1117942/{{ $informasi->image }}" alt=""></a></div></a>
                            @endforeach
                        </div>
                    </div>
                    <!-- //  Informasi -->
                </div>
            </div>
        </div>
    </section>
    <!-- End Detail UMKM -->
@endsection