@extends('layouts.landingpage')

@section('title')
<title>Banaran | Pindah Keluar</title>    
@endsection

@section('style')
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{ asset('assets/css/steps.css')}}">
@endsection

@section('pagetitle')
<section class="background-grey-1 padding-tb-25px text-grey-4">
    <div id="hapus" class="container">
        <h6 class="float-md-left font-2 mt-3">Pindah Keluar</h6>
        <ol class="breadcrumb float-md-right">
            <li><a href="#" class="text-grey-4">Home</a></li><i class="fa fa-angle-right"> </i>
            <li><a href="#" class="text-grey-4">Pelayanan</a></li><i class="fa fa-angle-right"> </i>
            <li class="active">Pindah Keluar</li>
        </ol>
        <div class="clearfix"></div>
    </div>
</section>
@endsection

@section('content')
    <!-- Pindah Keluar -->
    <section class="padding-tb-50px">
        <div class="container pad-top pad-bott">
            <div class="text-center margin-bottom-35px wow fadeInUp" style="padding-bottom: 50px;">
                <h1 class="font-weight-300 text-title-large font-3 text-main-color wow fadeInUp" data-wow-delay="0.2s">FORM PERMOHONAN PINDAH KELUAR</h1>
            </div>
            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="fa fa-check"></i> <strong>Berhasil! &nbsp;&nbsp;</strong>
                <strong>{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('error'))
            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="fa fa-times"></i> <strong>Bahaya! &nbsp;&nbsp;</strong>
                <strong>{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('warning'))
            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="fa fa-exclamation"></i> <strong>Perhatian! &nbsp;&nbsp;</strong>
                <strong>{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('info'))
            <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="fa fa-info"></i> <strong>Info! &nbsp;&nbsp;</strong> 
                <strong>{{ $message }}</strong>
            </div>
            @endif

            @if ($errors->any())
            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="fa fa-times"></i> <strong>Bahaya! &nbsp;&nbsp;</strong>
                Silakan periksa formulir di bawah ini untuk kesalahan <br>
                <strong>
                    <ul>
                        @foreach ($errors->all() as $message)
                            <li>{{$message}}</li>
                        @endforeach
                    </ul>
                </strong>
            </div>
            @endif
            
            @guest
            @elseif (Auth::user()->role == "unverified")
            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                <i class="fa fa-exclamation"></i> <strong>Perhatian! &nbsp;&nbsp;</strong>
                <strong>Akun anda belum terverifikasi.</strong>
            </div>
            <form id="msform" class="padding-top-10px margin-top-10px border-top-1 border-grey-1" action="{{ route('createPindahKeluar')}}" method="post" enctype="multipart/form-data">
                @csrf
                <!-- ----------- PELAPOR ----------- -->
                <input type="hidden" name="role" value="{{ Auth::user()->role }}">
                <input type="hidden" class="form-control" name="id_user" placeholder="Nama Lengkap" value="{{ Auth::user()->id }}">
                <input type="hidden" name="keperluan" id="" value="Akta Kelahiran">
                <input type="hidden" name="status" id="" value="proses">
                <!-- progressbar2 -->
                <ul id="progressbar2">
                    <li class="active" id="home"><strong>Data Daerah Asal</strong></li>
                    <li id="move"><strong>Data Kepidahan</strong></li>
                    <li id="persyaratan"><strong>Pesyaratan</strong></li>
                    <li id="confirm"><strong>Finish</strong></li>
                </ul>
                <div class="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar2" aria-valuemin="0" aria-valuemax="100"></div>
                </div> <br> <!-- fieldsets -->
                <fieldset>
                    <div class="form-card">
                        <div class="row">
                            <div class="col-7">
                                <h2 class="fs-title">Data Daerah Asal :</h2>
                            </div>
                            <div class="col-5">
                                <h2 class="steps">Langkah 1 - 4 &nbsp;<span style="float:right"><button type="button" class="btn btn-info" data-toggle="modal" data-target="#pindahkeluar">Info</button></span></h2>
                            </div>
                        </div> 
                        <div class="form-group">
                        <label >Kepindahan</label>
                        <select name="kepindahan" id="" class="form-control @error('kepindahan') is-invalid @enderror">
                            <option value="">-- Pilih Kepindahan --</option>
                            @foreach ($jeniskepindahan as $row1)
                                <?php
                                $selected = ' ';
                                if($row1['t_jeniskepindahans_id'] == $row1['id']){
                                    $selected = 'selected';
                                }
                                ?>
                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['kepindahan']; ?> </option>
                                ?>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Nama Kepala Keluarga</label>
                        <input type="text" name="nama_kepala_keluarga" class="form-control @error('nama_kepala_keluarga') is-invalid @enderror" placeholder="Nama Kepala Keluarga">
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Dukuh</label>
                            <select name="dukuh_asal" id="dukuhnya_asal" class="form-control @error('dukuh_asal') is-invalid @enderror" onclick="Reset()">
                                <option value="">-- Pilih Dukuh --</option>
                                @foreach ($dukuh as $row1)
                                <?php
                                        $selected = ' ';
                                        if($row1['t_dukuh_id'] == $row1['id']){
                                            $selected = 'selected';
                                        }
                                        ?>
                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['dukuh']; ?>
                                </option>
                                ?>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>RT</label>
                            <select name="carirtAsal" id="rtAsal" class="form-control @error('rt_asal') is-invalid @enderror">
                                <option value="">-- Pilih RT --</option>
                            </select>
                            <input type="hidden" class="form-control @error('rt_asal') is-invalid @enderror" id="rtnyaAsal"
                                name="rt_asal">
                        </div>
                    </div>
                    </div> <input type="button" name="next" class="next action-button" value="Lanjut" />
                </fieldset>
                <fieldset>
                    <div class="form-card">
                        <div class="row">
                            <div class="col-7">
                                <h2 class="fs-title">Data Kepidahan :</h2>
                            </div>
                            <div class="col-5">
                                <h2 class="steps">Langkah 2 - 4 &nbsp;<span style="float:right"><button type="button" class="btn btn-info" data-toggle="modal" data-target="#pindahkeluar">Info</button></span></h2>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Alasan Pindah</label>
                            <select id="seeAnotherField" name="alasan_pindah" class="form-control @error('alasan_pindah') is-invalid @enderror">
                                <option value="">-- Pilih Alasan Pindah --</option>
                                <option value="Pekerjaan">Pekerjaan</option>
                                <option value="Pendidikan">Pendidikan</option>
                                <option value="Keamanan">Keamanan</option>
                                <option value="Kesehatan">Kesehatan</option>
                                <option value="Perumahan">Perumahan</option>
                                <option value="Keluarga">Keluarga</option>
                                <option value="Lainnya">Lainnya</option>
                            </select>
                        </div>
                        <div class="form-group" id="otherFieldDiv">
                            <label for="otherField1">Alasan Pindah Lainnya</label>
                            <input type="text" name="alasan_pindah_lainnya" class="form-control @error('alasan_pindah_lainnya') is-invalid @enderror w-100" id="otherField1">
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Provinsi</label>
                                <input type="text" name="provinsi_kepindahan" class="form-control @error('provinsi_kepindahan') is-invalid @enderror" placeholder="Provinsi Tujuan">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Kabupaten/Kota</label>
                                <input type="text" name="kota_atau_kabupaten_kepindahan" class="form-control @error('kota_atau_kabupaten_kepindahan') is-invalid @enderror" placeholder="Kabupaten/Kota Tujuan">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Kecamatan</label>
                                <input type="text" name="kecamatan_kepindahan" class="form-control @error('kecamatan_kepindahan') is-invalid @enderror" placeholder="Kecamatan Tujuan">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Kelurahan</label>
                                <input type="text" name="kelurahan_kepindahan" class="form-control @error('kelurahan_kepindahan') is-invalid @enderror" placeholder="Kelurahan Tujuan">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label>Dukuh</label>
                                <input type="text" name="dukuh_kepindahan" class="form-control @error('dukuh_kepindahan') is-invalid @enderror" placeholder="Dukuh Tujuan">
                            </div>
                            <div class="form-group col-md-3">
                                <label>RT</label>
                                <input type="text" name="rt_kepindahan" class="form-control @error('rt_kepindahan') is-invalid @enderror" placeholder="RT Tujuan">
                            </div>
                            <div class="form-group col-md-3">
                                <label>RW</label>
                                <input type="text" name="rw_kepindahan" class="form-control @error('rw_kepindahan') is-invalid @enderror" placeholder="RW Tujuan">
                            </div>
                            <div class="form-group col-md-3">
                                <label>Kode Pos</label>
                                <input type="text" name="kode_pos_kepindahan" class="form-control @error('kode_pos_kepindahan') is-invalid @enderror" placeholder="Kode Pos">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Jenis Kepindahan</label>
                            <select id="seeAnotherField" name="jenis_kepindahan" class="form-control @error('jenis_kepindahan') is-invalid @enderror">
                                <option value="">-- Pilih Jenis Kepindahan --</option>
                                <option value="Kepala Keluarga">Kepala Keluarga</option>
                                <option value="Kepala Keluarga dan Seluruh Anggota Keluarga">Kepala Keluarga dan Seluruh Anggota
                                    Keluarga</option>
                                <option value="Kepala Keluarga dan Sebagian Anggota Keluarga">Kepala Keluarga dan Sebagian Anggota
                                    Keluarga</option>
                                <option value="Anggota Keluarga">Anggota Keluarga</option>
                            </select>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Status KK Bagi Yang Tidak Pindah</label>
                                <select name="status_kk_keluarga_tidak_pindah" class="form-control @error('status_kk_keluarga_tidak_pindah') is-invalid @enderror">
                                    <option value="">-- Pilih Status KK --</option>
                                    <option value="Numpang KK">Numpang KK</option>
                                    <option value="Membuat KK Baru">Membuat KK Baru</option>
                                    <option value="Nomor KK Tetap">Nomor KK Tetap</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Status KK Bagi Yang Pindah</label>
                                <select name="status_kk_keluarga_pindah" class="form-control @error('status_kk_keluarga_pindah') is-invalid @enderror">
                                    <option value="">-- Pilih Status KK --</option>
                                    <option value="Numpang KK">Numpang KK</option>
                                    <option value="Membuat KK Baru">Membuat KK Baru</option>
                                    <option value="Nomor KK Tetap">Nomor KK Tetap</option>
                                </select>
                            </div>
                        </div>

                        <label>Keluarga Yang Pindah</label>
                        <div class="form-row dynamic-field" id="dynamic-field-1">
                            <div class="form-group col-md-6 ">
                                <input type="text" id="field" class="form-control @error('nik_anggota_keluarga_pindah.*') is-invalid @enderror" placeholder="NIK" name="nik_anggota_keluarga_pindah[]" />
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" id="field" class="form-control @error('nama_anggota_keluarga_pindah.*') is-invalid @enderror" placeholder="Nama Lengkap" name="nama_anggota_keluarga_pindah[]" />
                            </div>
                        </div>
                        <div class="clearfix">
                            <button type="button" id="add-button" class="btn btn-primary float-left text-uppercase shadow-sm"><i class="fas fa-plus fa-fw"></i> Tambah</button>
                            <button type="button" id="remove-button" class="btn btn-danger float-left text-uppercase ml-1" disabled="disabled"><i class="fas fa-minus fa-fw"></i> Hapus</button>
                        </div>
                    </div> 
                    <input type="button" name="next" class="next action-button" value="Lanjut" /> <input type="button" name="previous" class="previous action-button-previous" value="Sebelumnya" />
                </fieldset>
                <fieldset>
                    <div class="form-card">
                        <div class="row">
                            <div class="col-7">
                                <h2 class="fs-title">Persyaratan :</h2>
                            </div>
                            <div class="col-5">
                                <h2 class="steps">Langkah 3 - 4 &nbsp;<span style="float:right"><button type="button" class="btn btn-info" data-toggle="modal" data-target="#pindahkeluar">Info</button></span></h2>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label >Lampiran KTP Pelapor</label>
                                <center padding-bottom: 100px;><img id="blah" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="auto" name="lampiran_ktp"/></center><br>
                                <div class="custom-file">
                                    <input type="file" name="lampiran_ktp_pelapor" class="custom-file-input @error('lampiran_ktp_pelapor') is-invalid @enderror" id="customFile" onchange="readURL(this);">
                                    <label class="custom-file-label" for="customFile">Pilih Foto</label>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <label>Akta Kelahiran Pelapor</label>
                                <center padding-bottom: 100px;><img id="blah2" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="auto" name="lampiran_ktp" /></center><br>
                                <div class="custom-file">
                                    <input type="file" name="lampiran_akta_kelahiran_pelapor" class="custom-file-input @error('lampiran_akta_kelahiran_pelapor') is-invalid @enderror" id="customFile" onchange="readURL2(this);">
                                    <label class="custom-file-label" for="customFile">Pilih Foto</label>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <label>Ijazah Terakhir Pelapor</label>
                                <center padding-bottom: 100px;><img id="blah4" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="auto" name="lampiran_ktp" /></center><br>
                                <div class="custom-file">
                                    <input type="file" name="lampiran_ijazah_terakhir_pelapor" class="custom-file-input @error('lampiran_ijazah_terakhir_pelapor') is-invalid @enderror" id="customFile" onchange="readURL4(this);">
                                    <label class="custom-file-label" for="customFile">Pilih Foto</label>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <label>Akta Nikah *</label>
                                <center padding-bottom: 100px;><img id="blah5" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="auto" name="lampiran_ktp" /></center><br>
                                <div class="custom-file">
                                    <input type="file" name="lampiran_akta_nikah" class="custom-file-input" id="customFile" onchange="readURL5(this);">
                                    <label class="custom-file-label" for="customFile">Pilih Foto</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label>Surat Pengantar RT</label>
                                <center padding-bottom: 100px;><img id="blah6" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="auto" name="lampiran_ktp" /></center><br>
                                <div class="custom-file">
                                    <input type="file" name="lampiran_surat_pengantar_rt" class="custom-file-input @error('lampiran_surat_pengantar_rt') is-invalid @enderror" id="customFile" onchange="readURL6(this);">
                                    <label class="custom-file-label" for="customFile">Pilih Foto</label>
                                </div>
                            </div>
                        </div>
                        <br>
                        <h3><b>KETERANGAN DAN LAIN - LAIN</b></h3>
                        <div class="form-group">
                            <textarea name="keterangan" class="form-control @error('keterangan') is-invalid @enderror" id="exampleFormControlTextarea1" rows="3"></textarea>
                        </div>
                    </div>
                    <input type="button" name="next" class="next action-button" value="Lanjut" /> <input type="button" name="previous" class="previous action-button-previous" value="Sebelumnya" />
                </fieldset>
                <fieldset>
                    <div class="form-card">
                        <div class="row">
                            <div class="col-7">
                                <h2 class="fs-title">Selesai :</h2>
                            </div>
                            <div class="col-5">
                                <h2 class="steps">Langkah 4 - 4 &nbsp;<span style="float:right"><button type="button" class="btn btn-info" data-toggle="modal" data-target="#pindahkeluar">Info</button></span></h2>
                            </div>
                        </div> <br><br>
                        <h2 class="purple-text text-center"><strong>PERHATIAN !</strong></h2> <br>
                        <div class="row justify-content-center">
                            <div class="col-3"> <img src="{{ asset('assets/img/logo.png')}}" class="fit-image"> </div>
                        </div> <br><br>
                        <div class="row justify-content-center">
                            <div class="col-7 text-center">
                                <h5 class="purple-text text-center">Apakah anda yakin dengan data yang anda masukkan?</h5>
                            </div>
                        </div>
                    </div>
                    <input type="submit" name="next" class="next action-button" value="Submit" /> <input type="button" name="previous" class="previous action-button-previous" value="Sebelumnya" />
                </fieldset>
            </form>
            @else
            <form id="msform" class="padding-top-10px margin-top-10px border-top-1 border-grey-1" action="{{ route('createPindahKeluar')}}" method="post" enctype="multipart/form-data">
                @csrf
                <!-- ----------- PELAPOR ----------- -->
                <input type="hidden" name="role" value="{{ Auth::user()->role }}">
                <input type="hidden" class="form-control" name="id_user" placeholder="Nama Lengkap" value="{{ Auth::user()->id }}">
                <input type="hidden" name="keperluan" id="" value="Akta Kelahiran">
                <input type="hidden" name="status" id="" value="proses">
                <!-- progressbar2 -->
                <ul id="progressbar2">
                    <li class="active" id="home"><strong>Data Daerah Asal</strong></li>
                    <li id="move"><strong>Data Kepidahan</strong></li>
                    <li id="persyaratan"><strong>Pesyaratan</strong></li>
                    <li id="confirm"><strong>Finish</strong></li>
                </ul>
                <div class="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar2" aria-valuemin="0" aria-valuemax="100"></div>
                </div> <br> <!-- fieldsets -->
                <fieldset>
                    <div class="form-card">
                        <div class="row">
                            <div class="col-7">
                                <h2 class="fs-title">Data Daerah Asal :</h2>
                            </div>
                            <div class="col-5">
                                <h2 class="steps">Langkah 1 - 4 &nbsp;<span style="float:right"><button type="button" class="btn btn-info" data-toggle="modal" data-target="#pindahkeluar">Info</button></span></h2>
                            </div>
                        </div> 
                        <div class="form-group">
                        <label >Kepindahan</label>
                        <select name="kepindahan" id="" class="form-control @error('kepindahan') is-invalid @enderror">
                            <option value="">-- Pilih Kepindahan --</option>
                            @foreach ($jeniskepindahan as $row1)
                                <?php
                                $selected = ' ';
                                if($row1['t_jeniskepindahans_id'] == $row1['id']){
                                    $selected = 'selected';
                                }
                                ?>
                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['kepindahan']; ?> </option>
                                ?>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Nama Kepala Keluarga</label>
                        <input type="text" name="nama_kepala_keluarga" class="form-control @error('nama_kepala_keluarga') is-invalid @enderror" placeholder="Nama Kepala Keluarga">
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Dukuh</label>
                            <select name="dukuh_asal" id="dukuhnya_asal" class="form-control @error('dukuh_asal') is-invalid @enderror" onclick="Reset()">
                                <option value="">-- Pilih Dukuh --</option>
                                @foreach ($dukuh as $row1)
                                <?php
                                        $selected = ' ';
                                        if($row1['t_dukuh_id'] == $row1['id']){
                                            $selected = 'selected';
                                        }
                                        ?>
                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['dukuh']; ?>
                                </option>
                                ?>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>RT</label>
                            <select name="carirtAsal" id="rtAsal" class="form-control @error('rt_asal') is-invalid @enderror">
                                <option value="">-- Pilih RT --</option>
                            </select>
                            <input type="hidden" class="form-control @error('rt_asal') is-invalid @enderror" id="rtnyaAsal"
                                name="rt_asal">
                        </div>
                    </div>
                    </div> <input type="button" name="next" class="next action-button" value="Lanjut" />
                </fieldset>
                <fieldset>
                    <div class="form-card">
                        <div class="row">
                            <div class="col-7">
                                <h2 class="fs-title">Data Kepidahan :</h2>
                            </div>
                            <div class="col-5">
                                <h2 class="steps">Langkah 2 - 4 &nbsp;<span style="float:right"><button type="button" class="btn btn-info" data-toggle="modal" data-target="#pindahkeluar">Info</button></span></h2>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Alasan Pindah</label>
                            <select id="seeAnotherField" name="alasan_pindah" class="form-control @error('alasan_pindah') is-invalid @enderror">
                                <option value="">-- Pilih Alasan Pindah --</option>
                                <option value="Pekerjaan">Pekerjaan</option>
                                <option value="Pendidikan">Pendidikan</option>
                                <option value="Keamanan">Keamanan</option>
                                <option value="Kesehatan">Kesehatan</option>
                                <option value="Perumahan">Perumahan</option>
                                <option value="Keluarga">Keluarga</option>
                                <option value="Lainnya">Lainnya</option>
                            </select>
                        </div>
                        <div class="form-group" id="otherFieldDiv">
                            <label for="otherField1">Alasan Pindah Lainnya</label>
                            <input type="text" name="alasan_pindah_lainnya" class="form-control @error('alasan_pindah_lainnya') is-invalid @enderror w-100" id="otherField1">
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Provinsi</label>
                                <input type="text" name="provinsi_kepindahan" class="form-control @error('provinsi_kepindahan') is-invalid @enderror" placeholder="Provinsi Tujuan">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Kabupaten/Kota</label>
                                <input type="text" name="kota_atau_kabupaten_kepindahan" class="form-control @error('kota_atau_kabupaten_kepindahan') is-invalid @enderror" placeholder="Kabupaten/Kota Tujuan">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Kecamatan</label>
                                <input type="text" name="kecamatan_kepindahan" class="form-control @error('kecamatan_kepindahan') is-invalid @enderror" placeholder="Kecamatan Tujuan">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Kelurahan</label>
                                <input type="text" name="kelurahan_kepindahan" class="form-control @error('kelurahan_kepindahan') is-invalid @enderror" placeholder="Kelurahan Tujuan">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label>Dukuh</label>
                                <input type="text" name="dukuh_kepindahan" class="form-control @error('dukuh_kepindahan') is-invalid @enderror" placeholder="Dukuh Tujuan">
                            </div>
                            <div class="form-group col-md-3">
                                <label>RT</label>
                                <input type="text" name="rt_kepindahan" class="form-control @error('rt_kepindahan') is-invalid @enderror" placeholder="RT Tujuan">
                            </div>
                            <div class="form-group col-md-3">
                                <label>RW</label>
                                <input type="text" name="rw_kepindahan" class="form-control @error('rw_kepindahan') is-invalid @enderror" placeholder="RW Tujuan">
                            </div>
                            <div class="form-group col-md-3">
                                <label>Kode Pos</label>
                                <input type="text" name="kode_pos_kepindahan" class="form-control @error('kode_pos_kepindahan') is-invalid @enderror" placeholder="Kode Pos">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Jenis Kepindahan</label>
                            <select id="seeAnotherField" name="jenis_kepindahan" class="form-control @error('jenis_kepindahan') is-invalid @enderror">
                                <option value="">-- Pilih Jenis Kepindahan --</option>
                                <option value="Kepala Keluarga">Kepala Keluarga</option>
                                <option value="Kepala Keluarga dan Seluruh Anggota Keluarga">Kepala Keluarga dan Seluruh Anggota
                                    Keluarga</option>
                                <option value="Kepala Keluarga dan Sebagian Anggota Keluarga">Kepala Keluarga dan Sebagian Anggota
                                    Keluarga</option>
                                <option value="Anggota Keluarga">Anggota Keluarga</option>
                            </select>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Status KK Bagi Yang Tidak Pindah</label>
                                <select name="status_kk_keluarga_tidak_pindah" class="form-control @error('status_kk_keluarga_tidak_pindah') is-invalid @enderror">
                                    <option value="">-- Pilih Status KK --</option>
                                    <option value="Numpang KK">Numpang KK</option>
                                    <option value="Membuat KK Baru">Membuat KK Baru</option>
                                    <option value="Nomor KK Tetap">Nomor KK Tetap</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Status KK Bagi Yang Pindah</label>
                                <select name="status_kk_keluarga_pindah" class="form-control @error('status_kk_keluarga_pindah') is-invalid @enderror">
                                    <option value="">-- Pilih Status KK --</option>
                                    <option value="Numpang KK">Numpang KK</option>
                                    <option value="Membuat KK Baru">Membuat KK Baru</option>
                                    <option value="Nomor KK Tetap">Nomor KK Tetap</option>
                                </select>
                            </div>
                        </div>

                        <label>Keluarga Yang Pindah</label>
                        <div class="form-row dynamic-field" id="dynamic-field-1">
                            <div class="form-group col-md-6 ">
                                <input type="text" id="field" class="form-control @error('nik_anggota_keluarga_pindah.*') is-invalid @enderror" placeholder="NIK" name="nik_anggota_keluarga_pindah[]" />
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" id="field" class="form-control @error('nama_anggota_keluarga_pindah.*') is-invalid @enderror" placeholder="Nama Lengkap" name="nama_anggota_keluarga_pindah[]" />
                            </div>
                        </div>
                        <div class="clearfix">
                            <button type="button" id="add-button" class="btn btn-primary float-left text-uppercase shadow-sm"><i class="fas fa-plus fa-fw"></i> Tambah</button>
                            <button type="button" id="remove-button" class="btn btn-danger float-left text-uppercase ml-1" disabled="disabled"><i class="fas fa-minus fa-fw"></i> Hapus</button>
                        </div>
                    </div> 
                    <input type="button" name="next" class="next action-button" value="Lanjut" /> <input type="button" name="previous" class="previous action-button-previous" value="Sebelumnya" />
                </fieldset>
                <fieldset>
                    <div class="form-card">
                        <div class="row">
                            <div class="col-7">
                                <h2 class="fs-title">Persyaratan :</h2>
                            </div>
                            <div class="col-5">
                                <h2 class="steps">Langkah 3 - 4 &nbsp;<span style="float:right"><button type="button" class="btn btn-info" data-toggle="modal" data-target="#pindahkeluar">Info</button></span></h2>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label >Lampiran KTP Pelapor</label>
                                <center padding-bottom: 100px;><img id="blah" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="auto" name="lampiran_ktp"/></center><br>
                                <div class="custom-file">
                                    <input type="file" name="lampiran_ktp_pelapor" class="custom-file-input @error('lampiran_ktp_pelapor') is-invalid @enderror" id="customFile" onchange="readURL(this);">
                                    <label class="custom-file-label" for="customFile">Pilih Foto</label>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <label>Akta Kelahiran Pelapor</label>
                                <center padding-bottom: 100px;><img id="blah2" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="auto" name="lampiran_ktp" /></center><br>
                                <div class="custom-file">
                                    <input type="file" name="lampiran_akta_kelahiran_pelapor" class="custom-file-input @error('lampiran_akta_kelahiran_pelapor') is-invalid @enderror" id="customFile" onchange="readURL2(this);">
                                    <label class="custom-file-label" for="customFile">Pilih Foto</label>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <label>Ijazah Terakhir Pelapor</label>
                                <center padding-bottom: 100px;><img id="blah4" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="auto" name="lampiran_ktp" /></center><br>
                                <div class="custom-file">
                                    <input type="file" name="lampiran_ijazah_terakhir_pelapor" class="custom-file-input @error('lampiran_ijazah_terakhir_pelapor') is-invalid @enderror" id="customFile" onchange="readURL4(this);">
                                    <label class="custom-file-label" for="customFile">Pilih Foto</label>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <label>Akta Nikah *</label>
                                <center padding-bottom: 100px;><img id="blah5" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="auto" name="lampiran_ktp" /></center><br>
                                <div class="custom-file">
                                    <input type="file" name="lampiran_akta_nikah" class="custom-file-input" id="customFile" onchange="readURL5(this);">
                                    <label class="custom-file-label" for="customFile">Pilih Foto</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label>Surat Pengantar RT</label>
                                <center padding-bottom: 100px;><img id="blah6" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="auto" name="lampiran_ktp" /></center><br>
                                <div class="custom-file">
                                    <input type="file" name="lampiran_surat_pengantar_rt" class="custom-file-input @error('lampiran_surat_pengantar_rt') is-invalid @enderror" id="customFile" onchange="readURL6(this);">
                                    <label class="custom-file-label" for="customFile">Pilih Foto</label>
                                </div>
                            </div>
                        </div>
                        <br>
                        <h3><b>KETERANGAN DAN LAIN - LAIN</b></h3>
                        <div class="form-group">
                            <textarea name="keterangan" class="form-control @error('keterangan') is-invalid @enderror" id="exampleFormControlTextarea1" rows="3"></textarea>
                        </div>
                    </div>
                    <input type="button" name="next" class="next action-button" value="Lanjut" /> <input type="button" name="previous" class="previous action-button-previous" value="Sebelumnya" />
                </fieldset>
                <fieldset>
                    <div class="form-card">
                        <div class="row">
                            <div class="col-7">
                                <h2 class="fs-title">Selesai :</h2>
                            </div>
                            <div class="col-5">
                                <h2 class="steps">Langkah 4 - 4 &nbsp;<span style="float:right"><button type="button" class="btn btn-info" data-toggle="modal" data-target="#pindahkeluar">Info</button></span></h2>
                            </div>
                        </div> <br><br>
                        <h2 class="purple-text text-center"><strong>PERHATIAN !</strong></h2> <br>
                        <div class="row justify-content-center">
                            <div class="col-3"> <img src="{{ asset('assets/img/logo.png')}}" class="fit-image"> </div>
                        </div> <br><br>
                        <div class="row justify-content-center">
                            <div class="col-7 text-center">
                                <h5 class="purple-text text-center">Apakah anda yakin dengan data yang anda masukkan?</h5>
                            </div>
                        </div>
                    </div>
                    <input type="submit" name="next" class="next action-button" value="Kirim" /> <input type="button" name="previous" class="previous action-button-previous" value="Sebelumnya" />
                </fieldset>
            </form>
            @endguest
        </div>
    </section>
    <!-- // Pindah Keluar -->

    {{-- Modal Info --}}
    <div class="modal fade bd-example-modal-lg" id="pindahkeluar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg" style="; ">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="page-title" id="myModalLabel">Perhatikan Isian Data Kepindahan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p style="color: black"><b>Status KK Bagi Yang Tidak Pindah :</b></p>
                    <ul style="padding-left: 20px">
                        <li>Elemen data ini terkait dengan pengisian pada elemen data No. 3 (Jenis Kepindahan).</li>
                        <li>Jika yang pindah <span style="color: red"><i>"Kepala Keluarga"</i></span>, ada anggota keluarga yang ditinggal, atau yang pindah <span style="color: red"><i>"Kepala Keluarga dan sebagian
                            anggota keluarga"</i></span>, maka bagi anggota keluarga yang tidak pindah hanya ada dua pilihan yaitu <span style="color: green"><b>"Numpang KK"</b></span> jika dalam alamat
                            yang sama ada nomor KK yang lain, tetapi jika tidak ada maka harus <span style="color: green"><b>"Membuat KK Baru"</b></span>.
                        </li>
                        <li>
                            Jika yang pindah <span style="color: red"><i>"Anggota Keluarga"</i></span> maka pilihannya adalah <span style="color: green"><b>"Nomor KK Tetap"</b></span> karena Kepala Keluarga dan anggota keluarga yang lain
                            tidak pindah harus tetap menggunakan nomor KK yang ada.
                        </li>
                    </ul>
                    <p style="color: black"><b>Status KK Bagi Yang Pindah :</b></p>
                    <ul style="padding-left: 20px">
                        <li>Elemen data ini terkait dengan pengisian elemen data No. 3 (Jenis Kepindahan).</li>
                        <li>
                            Jika yang pindah <span style="color: red"><i>"Kepala Keluarga"</i></span> atau <span style="color: red"><i>"Kepala Keluarga dan sebagian anggota keluarga“</i></span> atau 
                            <span style="color: red"><i>"Kepala Keluarga dan seluruh anggota keluarga"</i></span> karena Kepala Keluarga wajib membawa Nomor KK, maka hanya ada satu pilihan yaitu <span style="color: green"><b>"Nomor KK Tetap"</b></span>.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    {{-- // Modal Info --}}


@endsection

@section('script')
<script src="{{ asset('assets/js/steps.js') }}"></script>
<script src="{{ asset('assets/js/layanan/pindahkeluar.js') }}"></script>
@endsection