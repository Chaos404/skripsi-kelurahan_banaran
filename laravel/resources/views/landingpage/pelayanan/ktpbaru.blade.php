@extends('layouts.landingpage')

@section('title')
<title>Banaran | KTP Baru</title>    
@endsection

@section('style')
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('pagetitle')
<section class="background-grey-1 padding-tb-25px text-grey-4">
    <div id="hapus" class="container">
        <h6 class="float-md-left font-2 mt-3">KTP Baru</h6>
        <ol class="breadcrumb float-md-right">
            <li><a href="#" class="text-grey-4">Home</a></li><i class="fa fa-angle-right"> </i>
            <li><a href="#" class="text-grey-4">Pelayanan</a></li><i class="fa fa-angle-right"> </i>
            <li class="active">KTP Baru</li>
        </ol>
        <div class="clearfix"></div>
    </div>
</section>
@endsection

@section('content')
    <!-- KTP Baru -->
    <section class="padding-tb-50px">
        <div class="container pad-top pad-bott">
            <div class="text-center margin-bottom-35px wow fadeInUp" style="padding-bottom: 50px;">
                <h1 class="font-weight-300 text-title-large font-3 text-main-color wow fadeInUp" data-wow-delay="0.2s">FORM KTP BARU</h1>
            </div>
            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="fa fa-check"></i> <strong>Berhasil! &nbsp;&nbsp;</strong>
                <strong>{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('error'))
            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="fa fa-times"></i> <strong>Bahaya! &nbsp;&nbsp;</strong>
                <strong>{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('warning'))
            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="fa fa-exclamation"></i> <strong>Perhatian! &nbsp;&nbsp;</strong>
                <strong>{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('info'))
            <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="fa fa-info"></i> <strong>Info! &nbsp;&nbsp;</strong> 
                <strong>{{ $message }}</strong>
            </div>
            @endif

            @if ($errors->any())
            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="fa fa-times"></i> <strong>Bahaya! &nbsp;&nbsp;</strong>
                Silakan periksa formulir di bawah ini untuk kesalahan <br>
                <strong>
                    <ul>
                        @foreach ($errors->all() as $message)
                            <li>{{$message}}</li>
                        @endforeach
                    </ul>
                </strong>
            </div>
            @endif

            @guest
            @elseif (Auth::user()->role == "unverified")
            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                <i class="fa fa-exclamation"></i> <strong>Perhatian! &nbsp;&nbsp;</strong>
                <strong>Akun anda belum terverifikasi.</strong>
            </div>
            <form class="padding-top-10px margin-top-10px border-top-1 border-grey-1" action="{{ route('createKtpBaru')}}" method="post" enctype="multipart/form-data">
                @csrf
                <h3><b>DATA PEMOHON</b></h3>
                <input type="hidden" name="role" value="{{ Auth::user()->role }}">
                <input type="hidden" class="form-control" name="id_user" id="inputName4" placeholder="Nama Lengkap" value="{{ Auth::user()->id }}">
                <div class="form-group">
                    <label >Nama Lengkap</label>
                    <input type="text" class="form-control" name="nama_lengkap" id="inputName4" placeholder="Nama Lengkap" value="{{ Auth::user()->name }}" disabled>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label >Tempat Lahir</label>
                        <input type="text" class="form-control" name="tempat_lahir" id="inputName4" placeholder="Tempat Lahir" value="{{ Auth::user()->tempat_lahir }}" disabled>
                    </div>
                    <div class="form-group col-md-6">
                        <label >Tanggal Lahir</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="datepicker" value="{{ Auth::user()->tgl_lahir }}" disabled>
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label >Jenis Kelamin</label>
                        <select name="jenis_kelamin" id="" class="form-control" disabled>
                            @if (Auth::user()->jeniskelamin ==  "Laki - laki")
                            <option <?php 'selected' ?> value= "Laki - laki"> Laki - Laki </option>
                            @else
                            <option <?php 'selected' ?> value="Perempuan"> Perempuan </option>
                            @endif
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Agama</label>
                        <select name="agama" id="" class="form-control" disabled>
                            @foreach ($agama as $row1)
                                <?php
                                $selected = ' ';
                                if(Auth::user()->t_agama_id == $row1['id']){
                                    $selected = 'selected';
                                }
                                ?>
                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['agama']; ?> </option>
                                ?>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label >Pekerjaan</label>
                        <select name="pekerjaan" id="" class="form-control" disabled>
                            @foreach ($pekerjaan as $row1)
                                <?php
                                $selected = ' ';
                                if(Auth::user()->t_pekerjaan_id == $row1['id']){
                                    $selected = 'selected';
                                }
                                ?>
                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['pekerjaan']; ?> </option>
                                ?>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label >Status Perkawinan</label>
                        <select name="status_perkawinan" id="" class="form-control" disabled>
                            @if (Auth::user()->statusperkawinan == "Belum Kawin")
                            <option <?php 'selected' ?> value="Belum Kawin"> Belum Kawin </option>
                            @elseif (Auth::user()->statusperkawinan == "Kawin")
                            <option <?php 'selected' ?> value="Kawin"> Kawin </option>
                            @else 
                            <option <?php 'selected' ?> value="Pernah Kawin"> Pernah Kawin </option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label >Dukuh</label>
                        <select name="dukuh" id="" class="form-control" disabled>
                            @foreach ($dukuh as $row1)
                                <?php
                                $selected = ' ';
                                if(Auth::user()->t_dukuh_id == $row1['id']){
                                    $selected = 'selected';
                                }
                                ?>
                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['dukuh']; ?> </option>
                                ?>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label >RT</label>
                        <select name="rt" id="" class="form-control" disabled>
                            @foreach ($rt as $row1)
                            <?php
                            $selected = ' ';
                            if(Auth::user()->t_rt_id == $row1['id']){
                                $selected = 'selected';
                            }
                            ?>
                            <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['rt']; ?> </option>
                            ?>
                                {{-- <option value="{{ $row1->id }}">{{ $row1->tag }}</option> --}}
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label >NIK</label>
                    <input type="text" class="form-control" id="inputName4" name="no_nik" placeholder="NIK" value="{{ Auth::user()->nik }}" disabled>
                </div>
                <div class="form-group">
                    <label >Nomor KK</label>
                    <input type="text" class="form-control" id="inputName4" name="no_kk" placeholder="Nomor KK" value="{{ Auth::user()->no_kk }}" disabled>
                </div>
                <label>Permohonan KTP</label>
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="male" name="permohonan_ktp" value="KTP Baru" class="custom-control-input" checked/>
                            <label class="custom-control-label" for="male">KTP Baru</label>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="female" name="permohonan_ktp" value="KTP Penggantian" class="custom-control-input"/>
                            <label class="custom-control-label" for="female">KTP Penggantian</label>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        
                    </div>
                </div>
                <div class="form-group">
                    <label >Lampiran Surat Pengantar RT</label>
                    <center padding-bottom: 100px;><img id="blah" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="300px" width="auto" name="lampiran_ktp"/></center><br>
                    <div class="custom-file">
                        <input type="file" name="lampiran_surat_pengantar_rt" class="custom-file-input @error('lampiran_surat_pengantar_rt') is-invalid @enderror" id="customFile" onchange="readURL(this);">
                        <label class="custom-file-label" for="customFile">Pilih Foto</label>
                    </div>
                </div>
                <br>
                <h3><b>KETERANGAN DAN LAIN - LAIN</b></h3>
                <div class="form-group">
                    {{-- <label>Keterangan dan lain - lain</label> --}}
                    <textarea name="keterangan" class="form-control @error('keterangan') is-invalid @enderror" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-primary btn-lg btn-block background-main-color text-white text-center font-weight-bold text-uppercase rounded-0 padding-15px">KIRIM</button>
            </form>
            @else
            <form class="padding-top-10px margin-top-10px border-top-1 border-grey-1" action="{{ route('createKtpBaru')}}" method="post" enctype="multipart/form-data">
                @csrf
                <h3><b>DATA PEMOHON</b></h3>
                <input type="hidden" name="role" value="{{ Auth::user()->role }}">
                <input type="hidden" class="form-control" name="id_user" id="inputName4" placeholder="Nama Lengkap" value="{{ Auth::user()->id }}">
                <div class="form-group">
                    <label >Nama Lengkap</label>
                    <input type="text" class="form-control" name="nama_lengkap" id="inputName4" placeholder="Nama Lengkap" value="{{ Auth::user()->name }}" disabled>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label >Tempat Lahir</label>
                        <input type="text" class="form-control" name="tempat_lahir" id="inputName4" placeholder="Tempat Lahir" value="{{ Auth::user()->tempat_lahir }}" disabled>
                    </div>
                    <div class="form-group col-md-6">
                        <label >Tanggal Lahir</label>
                        {{-- <input id="datepicker" name="tanggal_lahir"  placeholder="Tanggal Lahir" value="{{ uth::user()->tgl_lahir }}"> --}}
                        <div class="input-group">
                            <input type="text" class="form-control" name="datepicker" value="{{ Auth::user()->tgl_lahir }}" disabled>
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label >Jenis Kelamin</label>
                        <select name="jenis_kelamin" id="" class="form-control" disabled>
                            @if (Auth::user()->jeniskelamin ==  "Laki - laki")
                            <option <?php 'selected' ?> value= "Laki - laki"> Laki - Laki </option>
                            @else
                            <option <?php 'selected' ?> value="Perempuan"> Perempuan </option>
                            @endif
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Agama</label>
                        <select name="agama" id="" class="form-control" disabled>
                            @foreach ($agama as $row1)
                                <?php
                                $selected = ' ';
                                if(Auth::user()->t_agama_id == $row1['id']){
                                    $selected = 'selected';
                                }
                                ?>
                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['agama']; ?> </option>
                                ?>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label >Pekerjaan</label>
                        <select name="pekerjaan" id="" class="form-control" disabled>
                            @foreach ($pekerjaan as $row1)
                                <?php
                                $selected = ' ';
                                if(Auth::user()->t_pekerjaan_id == $row1['id']){
                                    $selected = 'selected';
                                }
                                ?>
                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['pekerjaan']; ?> </option>
                                ?>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label >Status Perkawinan</label>
                        <select name="status_perkawinan" id="" class="form-control" disabled>
                            @if (Auth::user()->statusperkawinan == "Belum Kawin")
                            <option <?php 'selected' ?> value="Belum Kawin"> Belum Kawin </option>
                            @elseif (Auth::user()->statusperkawinan == "Kawin")
                            <option <?php 'selected' ?> value="Kawin"> Kawin </option>
                            @else 
                            <option <?php 'selected' ?> value="Pernah Kawin"> Pernah Kawin </option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label >Dukuh</label>
                        <select name="dukuh" id="" class="form-control" disabled>
                            @foreach ($dukuh as $row1)
                                <?php
                                $selected = ' ';
                                if(Auth::user()->t_dukuh_id == $row1['id']){
                                    $selected = 'selected';
                                }
                                ?>
                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['dukuh']; ?> </option>
                                ?>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label >RT</label>
                        <select name="rt" id="" class="form-control" disabled>
                            @foreach ($rt as $row1)
                            <?php
                            $selected = ' ';
                            if(Auth::user()->t_rt_id == $row1['id']){
                                $selected = 'selected';
                            }
                            ?>
                            <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['rt']; ?> </option>
                            ?>
                                {{-- <option value="{{ $row1->id }}">{{ $row1->tag }}</option> --}}
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label >NIK</label>
                    <input type="text" class="form-control" id="inputName4" name="no_nik" placeholder="NIK" value="{{ Auth::user()->nik }}" disabled>
                </div>
                <div class="form-group">
                    <label >Nomor KK</label>
                    <input type="text" class="form-control" id="inputName4" name="no_kk" placeholder="Nomor KK" value="{{ Auth::user()->no_kk }}" disabled>
                </div>
                <label>Permohonan KTP</label>
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="male" name="permohonan_ktp" value="KTP Baru" class="custom-control-input" checked/>
                            <label class="custom-control-label" for="male">KTP Baru</label>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="female" name="permohonan_ktp" value="KTP Penggantian" class="custom-control-input"/>
                            <label class="custom-control-label" for="female">KTP Penggantian</label>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        
                    </div>
                </div>
                <div class="form-group">
                    <label >Lampiran Surat Pengantar RT</label>
                    <center padding-bottom: 100px;><img id="blah" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="300px" width="auto" name="lampiran_ktp"/></center><br>
                    <div class="custom-file">
                        <input type="file" name="lampiran_surat_pengantar_rt" class="custom-file-input @error('lampiran_surat_pengantar_rt') is-invalid @enderror" id="customFile" onchange="readURL(this);">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
                <br>
                <h3><b>KETERANGAN DAN LAIN - LAIN</b></h3>
                <div class="form-group">
                    {{-- <label>Keterangan dan lain - lain</label> --}}
                    <textarea name="keterangan" class="form-control @error('keterangan') is-invalid @enderror" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-primary btn-lg btn-block background-main-color text-white text-center font-weight-bold text-uppercase rounded-0 padding-15px">KIRIM</button>
            </form>
            @endguest
        </div>
    </section>
    <!-- // KTP Baru -->
@endsection

@section('script')
<script src="{{ asset('assets/js/layanan/ktp.js') }}"></script>
@endsection