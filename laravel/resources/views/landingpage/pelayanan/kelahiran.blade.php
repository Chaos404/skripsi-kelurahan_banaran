@extends('layouts.landingpage')

@section('title')
<title>Banaran | Akta Kelahiran</title>    
@endsection

@section('style')
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{ asset('assets/css/steps.css')}}">
@endsection

@section('pagetitle')
<section class="background-grey-1 padding-tb-25px text-grey-4">
    <div id="hapus" class="container">
        <h6 class="float-md-left font-2 mt-3">Akta Kelahiran</h6>
        <ol class="breadcrumb float-md-right">
            <li><a href="#" class="text-grey-4">Home</a></li><i class="fa fa-angle-right"> </i>
            <li><a href="#" class="text-grey-4">Pelayanan</a></li><i class="fa fa-angle-right"> </i>
            <li class="active">Akta Kelahiran</li>
        </ol>
        <div class="clearfix"></div>
    </div>
</section>
@endsection

@section('content')
    <!-- Akta Kelahiran -->
    <section class="padding-tb-50px">
        <div class="container pad-top pad-bott">
            <div class="text-center margin-bottom-35px wow fadeInUp" style="padding-bottom: 50px;">
                <h1 class="font-weight-300 text-title-large font-3 text-main-color wow fadeInUp" data-wow-delay="0.2s">FORM AKTA KELAHIRAN</h1>
            </div>
            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="fa fa-check"></i> <strong>Berhasil! &nbsp;&nbsp;</strong>
                <strong>{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('error'))
            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="fa fa-times"></i> <strong>Bahaya! &nbsp;&nbsp;</strong>
                <strong>{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('warning'))
            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="fa fa-exclamation"></i> <strong>Perhatian! &nbsp;&nbsp;</strong>
                <strong>{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('info'))
            <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="fa fa-info"></i> <strong>Info! &nbsp;&nbsp;</strong> 
                <strong>{{ $message }}</strong>
            </div>
            @endif

            @if ($errors->any())
            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="fa fa-times"></i> <strong>Bahaya! &nbsp;&nbsp;</strong>
                Silakan periksa formulir di bawah ini untuk kesalahan <br>
                <strong>
                    <ul>
                        @foreach ($errors->all() as $message)
                            <li>{{$message}}</li>
                        @endforeach
                    </ul>
                </strong>
            </div>
            @endif
            
            @guest
            @elseif (Auth::user()->role == "unverified")
            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                <i class="fa fa-exclamation"></i> <strong>Perhatian! &nbsp;&nbsp;</strong>
                <strong>Akun anda belum terverifikasi.</strong>
            </div>
            <form id="msform" class="padding-top-10px margin-top-10px border-top-1 border-grey-1" action="{{ route('createAktaKelahiran')}}" method="post" enctype="multipart/form-data">
                @csrf
                <!-- ----------- PELAPOR ----------- -->
                <input type="hidden" name="role" value="{{ Auth::user()->role }}">
                <input type="hidden" class="form-control" name="id_user" placeholder="Nama Lengkap" value="{{ Auth::user()->id }}">
                <input type="hidden" name="keperluan" id="" value="Akta Kelahiran">
                <input type="hidden" name="status" id="" value="proses">
                <input type="hidden" name="status" id="" value="proses">
                <!-- progressbar -->
                <ul id="progressbar">
                    <li class="active" id="account"><strong>Anak / Bayi</strong></li>
                    <li id="ortu"><strong>Orang Tua</strong></li>
                    <li id="saksi"><strong>Saksi</strong></li>
                    <li id="persyaratan"><strong>Pesyaratan</strong></li>
                    <li id="confirm"><strong>Finish</strong></li>
                </ul>
                <div class="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                </div> <br> <!-- fieldsets -->
                <fieldset>
                    <div class="form-card">
                        <div class="row">
                            <div class="col-7">
                                <h2 class="fs-title">Anak / Bayi :</h2>
                            </div>
                            <div class="col-5">
                                <h2 class="steps">Langkah 1 - 5</h2>
                            </div>
                        </div> 
                        <div class="form-group">
                            <label >Nama Lengkap</label>
                            <input type="text" name="nama_anak" class="form-control @error('nama_anak') is-invalid @enderror" placeholder="Nama Lengkap">
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Jenis Kelamin</label>
                                <select name="jenis_kelamin_anak" id="" class="form-control @error('jenis_kelamin_anak') is-invalid @enderror">
                                    <option value="">-- Pilih Jenis Kelamin --</option>
                                    <option value="Laki - laki">Laki - laki</option>
                                    <option value="Perempuan">Perempuan</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Tempat Dilahirkan</label>
                                {{-- <input type="text" name="tempat_lahir" class="form-control @error('keterangan') is-invalid @enderror" placeholder="Tempat Dilahirkan"> --}}
                                <select name="tempat_lahir" id="" class="form-control @error('tempat_lahir') is-invalid @enderror">
                                    <option value="">-- Pilih Tempat Kelahiran --</option>
                                    <option value="Rumah Sakit / Rumah Bersalin">Rumah Sakit / Rumah Bersalin</option>
                                    <option value="Puskesmas">Puskesmas</option>
                                    <option value="Polides">Polides</option>
                                    <option value="Rumah">Rumah</option>
                                    <option value="Lainnya">Lainnya</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Tanggal Lahir</label>
                                <input placeholder="Tanggal Lahir" name="tanggal_lahir" id="datepicker" class="form-control @error('tanggal_lahir') is-invalid @enderror"/>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Pukul</label>
                                <input id="timepicker" name="pukul_kelahiran" placeholder="Pukul Kelahiran" class="form-control @error('pukul_kelahiran') is-invalid @enderror" readonly/>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Nomor / Tempat Kelahiran</label>
                                <input type="text" name="no_atau_tempat_kelahiran" class="form-control @error('no_atau_tempat_kelahiran') is-invalid @enderror" placeholder="Nomor / Tempat Kelahiran">
                            </div>
                            <div class="form-group col-md-6">
                                <label >Jenis Kelahiran</label>
                                <select name="jenis_kelahiran" id="" class="form-control @error('jenis_kelahiran') is-invalid @enderror">
                                    <option value="">-- Pilih Jenis Kelahiran --</option>
                                    <option value="Tunggal">Tunggal</option>
                                    <option value="Kembar">Kembar</option>
                                    <option value="Kembar 3">Kembar 3</option>
                                    <option value="Kembar 4">Kembar 4</option>
                                    <option value="Lainnya">Lainnya</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Kelahiran Ke</label>
                                <input type="number" name="kelahiran_ke" min="1" max="10000.00" step="1" class="form-control @error('kelahiran_ke') is-invalid @enderror" placeholder="Kelahiran Ke">
                            </div>
                            <div class="form-group col-md-6">
                                <label >Penolong Kelahiran</label>
                                <select name="penolong_kelahiran" id="" class="form-control @error('penolong_kelahiran') is-invalid @enderror">
                                    <option value="">-- Pilih Penolong Kelahiran --</option>
                                    <option value="Dokter">Dokter</option>
                                    <option value="Bidan/Perawat">Bidan/Perawat</option>
                                    <option value="Dukun">Dukun</option>
                                    <option value="Lainnya">Lainnya</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Berat Lahir (Kg)</label>
                                <input type="text" name="berat_bayi" class="form-control @error('berat_bayi') is-invalid @enderror" placeholder="Berat Lahir Contoh: 2.5">
                            </div>
                            <div class="form-group col-md-6">
                                <label >Panjang Lahir (Cm)</label>
                                <input type="number" name="tinggi_bayi" min="1" max="10000.00" step="1" class="form-control @error('tinggi_bayi') is-invalid @enderror" placeholder="Panjang Lahir">
                            </div>
                        </div>
                    </div> <input type="button" name="next" class="next action-button" value="Lanjut" />
                </fieldset>
                <fieldset>
                    <div class="form-card">
                        <div class="row">
                            <div class="col-7">
                                <h2 class="fs-title">Orang Tua :</h2>
                            </div>
                            <div class="col-5">
                                <h2 class="steps">Langkah 2 - 5</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <!-- ----------- Data IBU ----------- -->
                                <h3><b>DATA IBU</b></h3>
                                <div class="form-group">
                                    <label >NIK</label>
                                    <input type="text" name="nik_ibu" class="form-control @error('nik_ibu') is-invalid @enderror" placeholder="NIK Ibu">
                                </div>
                                <div class="form-group">
                                    <label >Nama Lengkap</label>
                                    <input type="text" name="nama_ibu" class="form-control @error('nama_ibu') is-invalid @enderror" placeholder="Nama Lengkap Ibu">
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label >Tanggal Lahir</label>
                                        <input placeholder="Tanggal Lahir Ibu" name="tanggal_lahir_ibu" id="datepicker2" class="form-control @error('tanggal_lahir_ibu') is-invalid @enderror"/>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label >Pekerjaan</label>
                                        <select id="" name="pekerjaan_ibu" class="form-control @error('pekerjaan_ibu') is-invalid @enderror">
                                            <option value="">-- Pilih Pekerjaan --</option>
                                            @foreach ($pekerjaan as $row1)
                                                <?php
                                                $selected = ' ';
                                                ?>
                                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['pekerjaan']; ?> </option>
                                                ?>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label >Dukuh</label>
                                        <input class="form-control @error('dukuh_ibu') is-invalid @enderror" placeholder="Dukuh" name="dukuh_ibu"/>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label >RT</label>
                                        <input class="form-control @error('rt_ibu') is-invalid @enderror" placeholder="RT" name="rt_ibu"/>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label >Kelurahan</label>
                                        <input class="form-control @error('kelurahan_ibu') is-invalid @enderror" placeholder="Kelurahan" name="kelurahan_ibu"/>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label >Kecamatan</label>
                                        <input class="form-control @error('kecamatan_ibu') is-invalid @enderror" placeholder="Kecamatan" name="kecamatan_ibu"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label >Kabupaten</label>
                                    <input class="form-control @error('kabupaten_ibu') is-invalid @enderror" placeholder="Kabupaten" name="kabupaten_ibu"/>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <!-- ----------- Data AYAH ----------- -->
                                <h3><b>DATA AYAH</b></h3>
                                <div class="form-group">
                                    <label >NIK</label>
                                    <input type="text" name="nik_ayah" class="form-control @error('nik_ayah') is-invalid @enderror" placeholder="NIK Ayah">
                                </div>
                                <div class="form-group">
                                    <label >Nama Lengkap</label>
                                    <input type="text" name="nama_ayah" class="form-control @error('nama_ayah') is-invalid @enderror" placeholder="Nama Lengkap Ayah">
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label >Tanggal Lahir</label>
                                        <input placeholder="Tanggal Lahir Ayah" name="tanggal_lahir_ayah" id="datepicker3" class="form-control @error('tanggal_lahir_ayah') is-invalid @enderror"/>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label >Pekerjaan</label>
                                        <select name="pekerjaan_ayah" id="" class="form-control @error('pekerjaan_ayah') is-invalid @enderror">
                                            <option value="">-- Pilih Pekerjaan --</option>
                                            @foreach ($pekerjaan as $row1)
                                                <?php
                                                $selected = ' ';
                                                ?>
                                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['pekerjaan']; ?> </option>
                                                ?>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label >Dukuh</label>
                                        <input class="form-control @error('dukuh_ayah') is-invalid @enderror" placeholder="Dukuh" name="dukuh_ayah"/>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label >RT</label>
                                        <input class="form-control @error('rt_ayah') is-invalid @enderror" placeholder="RT" name="rt_ayah"/>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label >Kelurahan</label>
                                        <input class="form-control @error('kelurahan_ayah') is-invalid @enderror" placeholder="Kelurahan" name="kelurahan_ayah"/>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label >Kecamatan</label>
                                        <input class="form-control @error('kecamatan_ayah') is-invalid @enderror" placeholder="Kecamatan" name="kecamatan_ayah"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label >Kabupaten</label>
                                    <input class="form-control @error('kabupaten_ayah') is-invalid @enderror" placeholder="Kabupaten" name="kabupaten_ayah"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label >Tanggal Pencatatan Nikah</label>
                            <input placeholder="Tanggal Pencatatan Nikah" name="tanggal_pencatatan_nikah" id="datepicker4" class="form-control @error('tanggal_pencatatan_nikah') is-invalid @enderror"/>
                        </div>
                    </div> 
                    <input type="button" name="next" class="next action-button" value="Lanjut" /> <input type="button" name="previous" class="previous action-button-previous" value="Sebelumnya" />
                </fieldset>
                <fieldset>
                    <div class="form-card">
                        <div class="row">
                            <div class="col-7">
                                <h2 class="fs-title">Saksi :</h2>
                            </div>
                            <div class="col-5">
                                <h2 class="steps">Langkah 3 - 5</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <!-- ----------- Data SAKSI I ----------- -->
                                <h3><b>DATA SAKSI I</b></h3>
                                <div class="form-group">
                                    <label >NIK</label>
                                    <input type="text" name="nik_saksi1" class="form-control @error('nik_saksi1') is-invalid @enderror" placeholder="NIK Saksi I">
                                </div>
                                <div class="form-group">
                                    <label >Nama Lengkap</label>
                                    <input type="text" name="nama_saksi1" class="form-control @error('nama_saksi1') is-invalid @enderror" placeholder="Nama Lengkap Saksi I">
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label >Tanggal Lahir</label>
                                        <input placeholder="Tanggal Lahir Saksi I" name="tanggal_lahir_saksi1" id="datepicker5" class="form-control @error('tanggal_lahir_saksi1') is-invalid @enderror"/>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label >Jenis Kelamin</label>
                                        <select name="jenis_kelamin_saksi1" id="" class="form-control @error('jenis_kelamin_saksi1') is-invalid @enderror">
                                            <option value="">-- Pilih Jenis Kelamin --</option>
                                            <option value="Laki - Laki">Laki - Laki</option>
                                            <option value="Perempuan">Perempuan</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label >Pekerjaan</label>
                                    <select id="" name="pekerjaan_saksi1" class="form-control @error('pekerjaan_saksi1') is-invalid @enderror">
                                        <option value="">-- Pilih Pekerjaan --</option>
                                        @foreach ($pekerjaan as $row1)
                                            <?php
                                            $selected = ' ';
                                            ?>
                                            <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['pekerjaan']; ?> </option>
                                            ?>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label >Dukuh</label>
                                        <select name="dukuh_saksi1" id="dukuhnya_saksi1" class="form-control @error('dukuh_saksi1') is-invalid @enderror" onclick="Reset()">
                                            <option value="">-- Pilih Dukuh --</option>
                                            @foreach ($dukuh as $row1)
                                                <?php
                                                $selected = ' ';
                                                if($row1['t_dukuh_id'] == $row1['id']){
                                                    $selected = 'selected';
                                                }
                                                ?>
                                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['dukuh']; ?> </option>
                                                ?>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label >RT</label>
                                        <select name="carirtSaksi1" id="rtSaksi1" class="form-control @error('rt_saksi1') is-invalid @enderror">
                                            <option value="">-- Pilih RT --</option>
                                        </select>
                                        <input type="hidden" class="form-control @error('keterangan') is-invalid @enderror @error('rt_saksi1') is-invalid @enderror" id="rtnyaSaksi1" name="rt_saksi1">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <!-- ----------- Data SAKSI II ----------- -->
                                <h3><b>DATA SAKSI II</b></h3>
                                <div class="form-group">
                                    <label >NIK</label>
                                    <input type="text" name="nik_saksi2" class="form-control @error('nik_saksi2') is-invalid @enderror" placeholder="NIK Saksi II">
                                </div>
                                <div class="form-group">
                                    <label >Nama Lengkap</label>
                                    <input type="text" name="nama_saksi2" class="form-control @error('nama_saksi2') is-invalid @enderror" placeholder="Nama Lengkap Saksi II">
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label >Tanggal Lahir</label>
                                        <input placeholder="Tanggal Lahir Saksi II" name="tanggal_lahir_saksi2" id="datepicker6" class="form-control @error('tanggal_lahir_saksi2') is-invalid @enderror"/>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label >Jenis Kelamin</label>
                                        <select name="jenis_kelamin_saksi2" id="" class="form-control @error('jenis_kelamin_saksi2') is-invalid @enderror">
                                            <option value="">-- Pilih Jenis Kelamin --</option>
                                            <option value="Laki - Laki">Laki - Laki</option>
                                            <option value="Perempuan">Perempuan</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label >Pekerjaan</label>
                                    <select id="" name="pekerjaan_saksi2" class="form-control @error('pekerjaan_saksi2') is-invalid @enderror">
                                        <option value="">-- Pilih Pekerjaan --</option>
                                        @foreach ($pekerjaan as $row1)
                                            <?php
                                            $selected = ' ';
                                            ?>
                                            <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['pekerjaan']; ?> </option>
                                            ?>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label >Dukuh</label>
                                        <select name="dukuh_saksi2" id="dukuhnya_saksi2" class="form-control @error('dukuh_saksi2') is-invalid @enderror" onclick="Reset()">
                                            <option value="">-- Pilih Dukuh --</option>
                                            @foreach ($dukuh as $row1)
                                                <?php
                                                $selected = ' ';
                                                if($row1['t_dukuh_id'] == $row1['id']){
                                                    $selected = 'selected';
                                                }
                                                ?>
                                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['dukuh']; ?> </option>
                                                ?>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label >RT</label>
                                        <select name="carirtSaksi2" id="rtSaksi2" class="form-control @error('rt_saksi2') is-invalid @enderror">
                                            <option value="">-- Pilih RT --</option>
                                        </select>
                                        <input type="hidden" class="form-control @error('rt_saksi2') is-invalid @enderror" id="rtnyaSaksi2" name="rt_saksi2">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <input type="button" name="next" class="next action-button" value="Lanjut" /> <input type="button" name="previous" class="previous action-button-previous" value="Sebelumnya" />
                </fieldset>
                <fieldset>
                    <div class="form-card">
                        <div class="row">
                            <div class="col-7">
                                <h2 class="fs-title">Persyaratan :</h2>
                            </div>
                            <div class="col-5">
                                <h2 class="steps">Langkah 4 - 5</h2>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label >KTP Pelapor</label>
                                <center padding-bottom: 100px;><img id="blah8" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="auto" name="lampiran_ktp"/></center><br>
                                <div class="custom-file">
                                    <input type="file" name="lampiran_ktp_pelapor" class="custom-file-input @error('lampiran_ktp_pelapor') is-invalid @enderror" id="customFile" onchange="readURL8(this);">
                                    <label class="custom-file-label" for="customFile">Pilih Foto</label>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <label >KTP Ibu</label>
                                <center padding-bottom: 100px;><img id="blah" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="auto" name="lampiran_ktp"/></center><br>
                                <div class="custom-file">
                                    <input type="file" name="lampiran_ktp_ibu" class="custom-file-input @error('lampiran_ktp_ibu') is-invalid @enderror" id="customFile" onchange="readURL(this);">
                                    <label class="custom-file-label" for="customFile">Pilih Foto</label>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <label >KTP Ayah</label>
                                <center padding-bottom: 100px;><img id="blah1" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="auto" name="lampiran_ktp"/></center><br>
                                <div class="custom-file">
                                    <input type="file" name="lampiran_ktp_ayah" class="custom-file-input @error('lampiran_ktp_ayah') is-invalid @enderror" id="customFile" onchange="readURL1(this);">
                                    <label class="custom-file-label" for="customFile">Pilih Foto</label>
                                </div>
                            </div> 
                            <div class="form-group col-md-3">
                                <label >Buku Nikah</label>
                                <center padding-bottom: 100px;><img id="blah2" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="auto" name="lampiran_ktp"/></center><br>
                                <div class="custom-file">
                                    <input type="file" name="lampiran_buku_nikah" class="custom-file-input @error('lampiran_buku_nikah') is-invalid @enderror" id="customFile" onchange="readURL2(this);">
                                    <label class="custom-file-label" for="customFile">Pilih Foto</label>
                                </div>
                            </div> 
                        </div>
        
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label >Kartu Keluarga</label>
                                <center padding-bottom: 100px;><img id="blah3" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="auto" name="lampiran_ktp"/></center><br>
                                <div class="custom-file">
                                    <input type="file" name="lampiran_kartu_keluarga" class="custom-file-input @error('lampiran_kartu_keluarga') is-invalid @enderror" id="customFile" onchange="readURL3(this);">
                                    <label class="custom-file-label" for="customFile">Pilih Foto</label>
                                </div>
                            </div> 
                            <div class="form-group col-md-3">
                                <label >KTP Saksi I</label>
                                <center padding-bottom: 100px;><img id="blah4" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="auto" name="lampiran_ktp"/></center><br>
                                <div class="custom-file">
                                    <input type="file" name="lampiran_ktp_saksi_I" class="custom-file-input @error('lampiran_ktp_saksi_I') is-invalid @enderror" id="customFile" onchange="readURL4(this);">
                                    <label class="custom-file-label" for="customFile">Pilih Foto</label>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <label >KTP Saksi II</label>
                                <center padding-bottom: 100px;><img id="blah5" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="auto" name="lampiran_ktp"/></center><br>
                                <div class="custom-file">
                                    <input type="file" name="lampiran_ktp_saksi_II" class="custom-file-input @error('lampiran_ktp_saksi_II') is-invalid @enderror" id="customFile" onchange="readURL5(this);">
                                    <label class="custom-file-label" for="customFile">Pilih Foto</label>
                                </div>
                            </div> 
                            <div class="form-group col-md-3">
                                <label >Keterangan Lahir dari RS</label>
                                <center padding-bottom: 100px;><img id="blah6" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="auto" name="lampiran_ktp"/></center><br>
                                <div class="custom-file">
                                    <input type="file" name="lampiran_surat_keterangan_rs" class="custom-file-input @error('lampiran_surat_keterangan_rs') is-invalid @enderror" id="customFile" onchange="readURL6(this);">
                                    <label class="custom-file-label" for="customFile">Pilih Foto</label>
                                </div>
                            </div> 
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                {{-- <div class="form-group col-md-3"> --}}
                                    <label >Surat Pengantar RT</label>
                                    <center padding-bottom: 100px;><img id="blah7" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="auto" name="lampiran_ktp"/></center><br>
                                    <div class="custom-file">
                                        <input type="file" name="lampiran_surat_pengantar_rt" class="custom-file-input @error('lampiran_surat_pengantar_rt') is-invalid @enderror" id="customFile" onchange="readURL7(this);">
                                        <label class="custom-file-label" for="customFile">Pilih Foto</label>
                                    </div>
                                {{-- </div>  --}}
                            </div>
                        </div>
                        <br>
                        <h3><b>KETERANGAN DAN LAIN - LAIN</b></h3>
                        <div class="form-group">
                            {{-- <label>Keterangan dan lain - lain</label> --}}
                            <textarea name="keterangan" class="form-control @error('keterangan') is-invalid @enderror" id="exampleFormControlTextarea1" rows="3"></textarea>
                        </div>
                    </div>
                    <input type="button" name="next" class="next action-button" value="Lanjut" /> <input type="button" name="previous" class="previous action-button-previous" value="Sebelumnya" />
                </fieldset>
                <fieldset>
                    <div class="form-card">
                        <div class="row">
                            <div class="col-7">
                                <h2 class="fs-title">Selesai :</h2>
                            </div>
                            <div class="col-5">
                                <h2 class="steps">Langkah 5 - 5</h2>
                            </div>
                        </div> <br><br>
                        <h2 class="purple-text text-center"><strong>PERHATIAN !</strong></h2> <br>
                        <div class="row justify-content-center">
                            <div class="col-3"> <img src="{{ asset('assets/img/logo.png')}}" class="fit-image"> </div>
                        </div> <br><br>
                        <div class="row justify-content-center">
                            <div class="col-7 text-center">
                                <h5 class="purple-text text-center">Apakah anda yakin dengan data yang anda masukkan?</h5>
                            </div>
                        </div>
                    </div>
                    <input type="submit" name="next" class="next action-button" value="Kirim" /> <input type="button" name="previous" class="previous action-button-previous" value="Sebelumnya" />
                </fieldset>
            </form>
            @else
            <form id="msform" class="padding-top-10px margin-top-10px border-top-1 border-grey-1" action="{{ route('createAktaKelahiran')}}" method="post" enctype="multipart/form-data">
                @csrf
                <!-- ----------- PELAPOR ----------- -->
                <input type="hidden" name="role" value="{{ Auth::user()->role }}">
                <input type="hidden" class="form-control" name="id_user" placeholder="Nama Lengkap" value="{{ Auth::user()->id }}">
                <input type="hidden" name="keperluan" id="" value="Akta Kelahiran">
                <input type="hidden" name="status" id="" value="proses">
                <input type="hidden" name="status" id="" value="proses">
                <!-- progressbar -->
                <ul id="progressbar">
                    <li class="active" id="account"><strong>Anak / Bayi</strong></li>
                    <li id="ortu"><strong>Orang Tua</strong></li>
                    <li id="saksi"><strong>Saksi</strong></li>
                    <li id="persyaratan"><strong>Pesyaratan</strong></li>
                    <li id="confirm"><strong>Finish</strong></li>
                </ul>
                <div class="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                </div> <br> <!-- fieldsets -->
                <fieldset>
                    <div class="form-card">
                        <div class="row">
                            <div class="col-7">
                                <h2 class="fs-title">Anak / Bayi :</h2>
                            </div>
                            <div class="col-5">
                                <h2 class="steps">Langkah 1 - 5</h2>
                            </div>
                        </div> 
                        <div class="form-group">
                            <label >Nama Lengkap</label>
                            <input type="text" name="nama_anak" class="form-control @error('nama_anak') is-invalid @enderror" placeholder="Nama Lengkap">
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Jenis Kelamin</label>
                                <select name="jenis_kelamin_anak" id="" class="form-control @error('jenis_kelamin_anak') is-invalid @enderror">
                                    <option value="">-- Pilih Jenis Kelamin --</option>
                                    <option value="Laki - laki">Laki - laki</option>
                                    <option value="Perempuan">Perempuan</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Tempat Dilahirkan</label>
                                {{-- <input type="text" name="tempat_lahir" class="form-control @error('keterangan') is-invalid @enderror" placeholder="Tempat Dilahirkan"> --}}
                                <select name="tempat_lahir" id="" class="form-control @error('tempat_lahir') is-invalid @enderror">
                                    <option value="">-- Pilih Tempat Kelahiran --</option>
                                    <option value="Rumah Sakit / Rumah Bersalin">Rumah Sakit / Rumah Bersalin</option>
                                    <option value="Puskesmas">Puskesmas</option>
                                    <option value="Polides">Polides</option>
                                    <option value="Rumah">Rumah</option>
                                    <option value="Lainnya">Lainnya</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Tanggal Lahir</label>
                                <input placeholder="Tanggal Lahir" name="tanggal_lahir" id="datepicker" class="form-control @error('tanggal_lahir') is-invalid @enderror"/>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Pukul</label>
                                <input id="timepicker" name="pukul_kelahiran" placeholder="Pukul Kelahiran" class="form-control @error('pukul_kelahiran') is-invalid @enderror" readonly/>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Nomor / Tempat Kelahiran</label>
                                <input type="text" name="no_atau_tempat_kelahiran" class="form-control @error('no_atau_tempat_kelahiran') is-invalid @enderror" placeholder="Nomor / Tempat Kelahiran">
                            </div>
                            <div class="form-group col-md-6">
                                <label >Jenis Kelahiran</label>
                                <select name="jenis_kelahiran" id="" class="form-control @error('jenis_kelahiran') is-invalid @enderror">
                                    <option value="">-- Pilih Jenis Kelahiran --</option>
                                    <option value="Tunggal">Tunggal</option>
                                    <option value="Kembar">Kembar</option>
                                    <option value="Kembar 3">Kembar 3</option>
                                    <option value="Kembar 4">Kembar 4</option>
                                    <option value="Lainnya">Lainnya</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Kelahiran Ke</label>
                                <input type="number" name="kelahiran_ke" min="1" max="10000.00" step="1" class="form-control @error('kelahiran_ke') is-invalid @enderror" placeholder="Kelahiran Ke">
                            </div>
                            <div class="form-group col-md-6">
                                <label >Penolong Kelahiran</label>
                                <select name="penolong_kelahiran" id="" class="form-control @error('penolong_kelahiran') is-invalid @enderror">
                                    <option value="">-- Pilih Penolong Kelahiran --</option>
                                    <option value="Dokter">Dokter</option>
                                    <option value="Bidan/Perawat">Bidan/Perawat</option>
                                    <option value="Dukun">Dukun</option>
                                    <option value="Lainnya">Lainnya</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Berat Lahir (Kg)</label>
                                <input type="text" name="berat_bayi" class="form-control @error('berat_bayi') is-invalid @enderror" placeholder="Berat Lahir Contoh: 2.5">
                            </div>
                            <div class="form-group col-md-6">
                                <label >Panjang Lahir (Cm)</label>
                                <input type="number" name="tinggi_bayi" min="1" max="10000.00" step="1" class="form-control @error('tinggi_bayi') is-invalid @enderror" placeholder="Panjang Lahir">
                            </div>
                        </div>
                    </div> <input type="button" name="next" class="next action-button" value="Lanjut" />
                </fieldset>
                <fieldset>
                    <div class="form-card">
                        <div class="row">
                            <div class="col-7">
                                <h2 class="fs-title">Orang Tua :</h2>
                            </div>
                            <div class="col-5">
                                <h2 class="steps">Langkah 2 - 5</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <!-- ----------- Data IBU ----------- -->
                                <h3><b>DATA IBU</b></h3>
                                <div class="form-group">
                                    <label >NIK</label>
                                    <input type="text" name="nik_ibu" class="form-control @error('nik_ibu') is-invalid @enderror" placeholder="NIK Ibu">
                                </div>
                                <div class="form-group">
                                    <label >Nama Lengkap</label>
                                    <input type="text" name="nama_ibu" class="form-control @error('nama_ibu') is-invalid @enderror" placeholder="Nama Lengkap Ibu">
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label >Tanggal Lahir</label>
                                        <input placeholder="Tanggal Lahir Ibu" name="tanggal_lahir_ibu" id="datepicker2" class="form-control @error('tanggal_lahir_ibu') is-invalid @enderror"/>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label >Pekerjaan</label>
                                        <select id="" name="pekerjaan_ibu" class="form-control @error('pekerjaan_ibu') is-invalid @enderror">
                                            <option value="">-- Pilih Pekerjaan --</option>
                                            @foreach ($pekerjaan as $row1)
                                                <?php
                                                $selected = ' ';
                                                ?>
                                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['pekerjaan']; ?> </option>
                                                ?>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label >Dukuh</label>
                                        <input class="form-control @error('dukuh_ibu') is-invalid @enderror" placeholder="Dukuh" name="dukuh_ibu"/>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label >RT</label>
                                        <input class="form-control @error('rt_ibu') is-invalid @enderror" placeholder="RT" name="rt_ibu"/>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label >Kelurahan</label>
                                        <input class="form-control @error('kelurahan_ibu') is-invalid @enderror" placeholder="Kelurahan" name="kelurahan_ibu"/>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label >Kecamatan</label>
                                        <input class="form-control @error('kecamatan_ibu') is-invalid @enderror" placeholder="Kecamatan" name="kecamatan_ibu"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label >Kabupaten</label>
                                    <input class="form-control @error('kabupaten_ibu') is-invalid @enderror" placeholder="Kabupaten" name="kabupaten_ibu"/>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <!-- ----------- Data AYAH ----------- -->
                                <h3><b>DATA AYAH</b></h3>
                                <div class="form-group">
                                    <label >NIK</label>
                                    <input type="text" name="nik_ayah" class="form-control @error('nik_ayah') is-invalid @enderror" placeholder="NIK Ayah">
                                </div>
                                <div class="form-group">
                                    <label >Nama Lengkap</label>
                                    <input type="text" name="nama_ayah" class="form-control @error('nama_ayah') is-invalid @enderror" placeholder="Nama Lengkap Ayah">
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label >Tanggal Lahir</label>
                                        <input placeholder="Tanggal Lahir Ayah" name="tanggal_lahir_ayah" id="datepicker3" class="form-control @error('tanggal_lahir_ayah') is-invalid @enderror"/>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label >Pekerjaan</label>
                                        <select name="pekerjaan_ayah" id="" class="form-control @error('pekerjaan_ayah') is-invalid @enderror">
                                            <option value="">-- Pilih Pekerjaan --</option>
                                            @foreach ($pekerjaan as $row1)
                                                <?php
                                                $selected = ' ';
                                                ?>
                                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['pekerjaan']; ?> </option>
                                                ?>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label >Dukuh</label>
                                        <input class="form-control @error('dukuh_ayah') is-invalid @enderror" placeholder="Dukuh" name="dukuh_ayah"/>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label >RT</label>
                                        <input class="form-control @error('rt_ayah') is-invalid @enderror" placeholder="RT" name="rt_ayah"/>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label >Kelurahan</label>
                                        <input class="form-control @error('kelurahan_ayah') is-invalid @enderror" placeholder="Kelurahan" name="kelurahan_ayah"/>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label >Kecamatan</label>
                                        <input class="form-control @error('kecamatan_ayah') is-invalid @enderror" placeholder="Kecamatan" name="kecamatan_ayah"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label >Kabupaten</label>
                                    <input class="form-control @error('kabupaten_ayah') is-invalid @enderror" placeholder="Kabupaten" name="kabupaten_ayah"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label >Tanggal Pencatatan Nikah</label>
                            <input placeholder="Tanggal Pencatatan Nikah" name="tanggal_pencatatan_nikah" id="datepicker4" class="form-control @error('tanggal_pencatatan_nikah') is-invalid @enderror"/>
                        </div>
                    </div> 
                    <input type="button" name="next" class="next action-button" value="Lanjut" /> <input type="button" name="previous" class="previous action-button-previous" value="Sebelumnya" />
                </fieldset>
                <fieldset>
                    <div class="form-card">
                        <div class="row">
                            <div class="col-7">
                                <h2 class="fs-title">Saksi :</h2>
                            </div>
                            <div class="col-5">
                                <h2 class="steps">Langkah 3 - 5</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <!-- ----------- Data SAKSI I ----------- -->
                                <h3><b>DATA SAKSI I</b></h3>
                                <div class="form-group">
                                    <label >NIK</label>
                                    <input type="text" name="nik_saksi1" class="form-control @error('nik_saksi1') is-invalid @enderror" placeholder="NIK Saksi I">
                                </div>
                                <div class="form-group">
                                    <label >Nama Lengkap</label>
                                    <input type="text" name="nama_saksi1" class="form-control @error('nama_saksi1') is-invalid @enderror" placeholder="Nama Lengkap Saksi I">
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label >Tanggal Lahir</label>
                                        <input placeholder="Tanggal Lahir Saksi I" name="tanggal_lahir_saksi1" id="datepicker5" class="form-control @error('tanggal_lahir_saksi1') is-invalid @enderror"/>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label >Jenis Kelamin</label>
                                        <select name="jenis_kelamin_saksi1" id="" class="form-control @error('jenis_kelamin_saksi1') is-invalid @enderror">
                                            <option value="">-- Pilih Jenis Kelamin --</option>
                                            <option value="Laki - Laki">Laki - Laki</option>
                                            <option value="Perempuan">Perempuan</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label >Pekerjaan</label>
                                    <select id="" name="pekerjaan_saksi1" class="form-control @error('pekerjaan_saksi1') is-invalid @enderror">
                                        <option value="">-- Pilih Pekerjaan --</option>
                                        @foreach ($pekerjaan as $row1)
                                            <?php
                                            $selected = ' ';
                                            ?>
                                            <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['pekerjaan']; ?> </option>
                                            ?>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label >Dukuh</label>
                                        <select name="dukuh_saksi1" id="dukuhnya_saksi1" class="form-control @error('dukuh_saksi1') is-invalid @enderror" onclick="Reset()">
                                            <option value="">-- Pilih Dukuh --</option>
                                            @foreach ($dukuh as $row1)
                                                <?php
                                                $selected = ' ';
                                                if($row1['t_dukuh_id'] == $row1['id']){
                                                    $selected = 'selected';
                                                }
                                                ?>
                                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['dukuh']; ?> </option>
                                                ?>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label >RT</label>
                                        <select name="carirtSaksi1" id="rtSaksi1" class="form-control @error('rt_saksi1') is-invalid @enderror">
                                            <option value="">-- Pilih RT --</option>
                                        </select>
                                        <input type="hidden" class="form-control @error('keterangan') is-invalid @enderror @error('rt_saksi1') is-invalid @enderror" id="rtnyaSaksi1" name="rt_saksi1">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <!-- ----------- Data SAKSI II ----------- -->
                                <h3><b>DATA SAKSI II</b></h3>
                                <div class="form-group">
                                    <label >NIK</label>
                                    <input type="text" name="nik_saksi2" class="form-control @error('nik_saksi2') is-invalid @enderror" placeholder="NIK Saksi II">
                                </div>
                                <div class="form-group">
                                    <label >Nama Lengkap</label>
                                    <input type="text" name="nama_saksi2" class="form-control @error('nama_saksi2') is-invalid @enderror" placeholder="Nama Lengkap Saksi II">
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label >Tanggal Lahir</label>
                                        <input placeholder="Tanggal Lahir Saksi II" name="tanggal_lahir_saksi2" id="datepicker6" class="form-control @error('tanggal_lahir_saksi2') is-invalid @enderror"/>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label >Jenis Kelamin</label>
                                        <select name="jenis_kelamin_saksi2" id="" class="form-control @error('jenis_kelamin_saksi2') is-invalid @enderror">
                                            <option value="">-- Pilih Jenis Kelamin --</option>
                                            <option value="Laki - Laki">Laki - Laki</option>
                                            <option value="Perempuan">Perempuan</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label >Pekerjaan</label>
                                    <select id="" name="pekerjaan_saksi2" class="form-control @error('pekerjaan_saksi2') is-invalid @enderror">
                                        <option value="">-- Pilih Pekerjaan --</option>
                                        @foreach ($pekerjaan as $row1)
                                            <?php
                                            $selected = ' ';
                                            ?>
                                            <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['pekerjaan']; ?> </option>
                                            ?>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label >Dukuh</label>
                                        <select name="dukuh_saksi2" id="dukuhnya_saksi2" class="form-control @error('dukuh_saksi2') is-invalid @enderror" onclick="Reset()">
                                            <option value="">-- Pilih Dukuh --</option>
                                            @foreach ($dukuh as $row1)
                                                <?php
                                                $selected = ' ';
                                                if($row1['t_dukuh_id'] == $row1['id']){
                                                    $selected = 'selected';
                                                }
                                                ?>
                                                <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['dukuh']; ?> </option>
                                                ?>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label >RT</label>
                                        <select name="carirtSaksi2" id="rtSaksi2" class="form-control @error('rt_saksi2') is-invalid @enderror">
                                            <option value="">-- Pilih RT --</option>
                                        </select>
                                        <input type="hidden" class="form-control @error('rt_saksi2') is-invalid @enderror" id="rtnyaSaksi2" name="rt_saksi2">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <input type="button" name="next" class="next action-button" value="Lanjut" /> <input type="button" name="previous" class="previous action-button-previous" value="Sebelumnya" />
                </fieldset>
                <fieldset>
                    <div class="form-card">
                        <div class="row">
                            <div class="col-7">
                                <h2 class="fs-title">Persyaratan :</h2>
                            </div>
                            <div class="col-5">
                                <h2 class="steps">Langkah 4 - 5</h2>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label >KTP Pelapor</label>
                                <center padding-bottom: 100px;><img id="blah8" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="auto" name="lampiran_ktp"/></center><br>
                                <div class="custom-file">
                                    <input type="file" name="lampiran_ktp_pelapor" class="custom-file-input @error('lampiran_ktp_pelapor') is-invalid @enderror" id="customFile" onchange="readURL8(this);">
                                    <label class="custom-file-label" for="customFile">Pilih Foto</label>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <label >KTP Ibu</label>
                                <center padding-bottom: 100px;><img id="blah" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="auto" name="lampiran_ktp"/></center><br>
                                <div class="custom-file">
                                    <input type="file" name="lampiran_ktp_ibu" class="custom-file-input @error('lampiran_ktp_ibu') is-invalid @enderror" id="customFile" onchange="readURL(this);">
                                    <label class="custom-file-label" for="customFile">Pilih Foto</label>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <label >KTP Ayah</label>
                                <center padding-bottom: 100px;><img id="blah1" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="auto" name="lampiran_ktp"/></center><br>
                                <div class="custom-file">
                                    <input type="file" name="lampiran_ktp_ayah" class="custom-file-input @error('lampiran_ktp_ayah') is-invalid @enderror" id="customFile" onchange="readURL1(this);">
                                    <label class="custom-file-label" for="customFile">Pilih Foto</label>
                                </div>
                            </div> 
                            <div class="form-group col-md-3">
                                <label >Buku Nikah</label>
                                <center padding-bottom: 100px;><img id="blah2" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="auto" name="lampiran_ktp"/></center><br>
                                <div class="custom-file">
                                    <input type="file" name="lampiran_buku_nikah" class="custom-file-input @error('lampiran_buku_nikah') is-invalid @enderror" id="customFile" onchange="readURL2(this);">
                                    <label class="custom-file-label" for="customFile">Pilih Foto</label>
                                </div>
                            </div> 
                        </div>
        
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label >Kartu Keluarga</label>
                                <center padding-bottom: 100px;><img id="blah3" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="auto" name="lampiran_ktp"/></center><br>
                                <div class="custom-file">
                                    <input type="file" name="lampiran_kartu_keluarga" class="custom-file-input @error('lampiran_kartu_keluarga') is-invalid @enderror" id="customFile" onchange="readURL3(this);">
                                    <label class="custom-file-label" for="customFile">Pilih Foto</label>
                                </div>
                            </div> 
                            <div class="form-group col-md-3">
                                <label >KTP Saksi I</label>
                                <center padding-bottom: 100px;><img id="blah4" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="auto" name="lampiran_ktp"/></center><br>
                                <div class="custom-file">
                                    <input type="file" name="lampiran_ktp_saksi_I" class="custom-file-input @error('lampiran_ktp_saksi_I') is-invalid @enderror" id="customFile" onchange="readURL4(this);">
                                    <label class="custom-file-label" for="customFile">Pilih Foto</label>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <label >KTP Saksi II</label>
                                <center padding-bottom: 100px;><img id="blah5" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="auto" name="lampiran_ktp"/></center><br>
                                <div class="custom-file">
                                    <input type="file" name="lampiran_ktp_saksi_II" class="custom-file-input @error('lampiran_ktp_saksi_II') is-invalid @enderror" id="customFile" onchange="readURL5(this);">
                                    <label class="custom-file-label" for="customFile">Pilih Foto</label>
                                </div>
                            </div> 
                            <div class="form-group col-md-3">
                                <label >Keterangan Lahir dari RS</label>
                                <center padding-bottom: 100px;><img id="blah6" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="auto" name="lampiran_ktp"/></center><br>
                                <div class="custom-file">
                                    <input type="file" name="lampiran_surat_keterangan_rs" class="custom-file-input @error('lampiran_surat_keterangan_rs') is-invalid @enderror" id="customFile" onchange="readURL6(this);">
                                    <label class="custom-file-label" for="customFile">Pilih Foto</label>
                                </div>
                            </div> 
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                {{-- <div class="form-group col-md-3"> --}}
                                    <label >Surat Pengantar RT</label>
                                    <center padding-bottom: 100px;><img id="blah7" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="auto" name="lampiran_ktp"/></center><br>
                                    <div class="custom-file">
                                        <input type="file" name="lampiran_surat_pengantar_rt" class="custom-file-input @error('lampiran_surat_pengantar_rt') is-invalid @enderror" id="customFile" onchange="readURL7(this);">
                                        <label class="custom-file-label" for="customFile">Pilih Foto</label>
                                    </div>
                                {{-- </div>  --}}
                            </div>
                        </div>
                        <br>
                        <h3><b>KETERANGAN DAN LAIN - LAIN</b></h3>
                        <div class="form-group">
                            {{-- <label>Keterangan dan lain - lain</label> --}}
                            <textarea name="keterangan" class="form-control @error('keterangan') is-invalid @enderror" id="exampleFormControlTextarea1" rows="3"></textarea>
                        </div>
                    </div>
                    <input type="button" name="next" class="next action-button" value="Lanjut" /> <input type="button" name="previous" class="previous action-button-previous" value="Sebelumnya" />
                </fieldset>
                <fieldset>
                    <div class="form-card">
                        <div class="row">
                            <div class="col-7">
                                <h2 class="fs-title">Selesai :</h2>
                            </div>
                            <div class="col-5">
                                <h2 class="steps">Langkah 5 - 5</h2>
                            </div>
                        </div> <br><br>
                        <h2 class="purple-text text-center"><strong>PERHATIAN !</strong></h2> <br>
                        <div class="row justify-content-center">
                            <div class="col-3"> <img src="{{ asset('assets/img/logo.png')}}" class="fit-image"> </div>
                        </div> <br><br>
                        <div class="row justify-content-center">
                            <div class="col-7 text-center">
                                <h5 class="purple-text text-center">Apakah anda yakin dengan data yang anda masukkan?</h5>
                            </div>
                        </div>
                    </div>
                    <input type="submit" name="next" class="next action-button" value="Kirim" /> <input type="button" name="previous" class="previous action-button-previous" value="Sebelumnya" />
                </fieldset>
            </form>
            @endguest
        </div>
    </section>
    <!-- // Akta Kelahiran -->
@endsection

@section('script')
<script src="{{ asset('assets/js/steps.js') }}"></script>
<script src="{{ asset('assets/js/layanan/kelahiran.js') }}"></script>
@endsection