@extends('layouts.landingpage')

@section('title')
<title>Kelurahan Banaran | Struktur Pemerintahan</title>
@endsection

@section('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.2/css/swiper.min.css">
    <style>
        .swiper-container {
        width: 100%;
        /* padding-top: 50px; */
        padding-bottom: 50px;
        }

        .swiper-slide {
        background-position: center;
        background-size: cover;
        width: 320px;
        background-color: #fff;
        overflow: hidden;
        border-radius: 8px;
        }

        .picture {
        width: 320px;
        height: 320px;
        overflow: hidden;
        
        img {
            display: block;
            width: 100%;
            height: 100%;
            object-fit: cover;
        }
        }

        .detail {
        padding: 25px 20px;
        font-weight: 600;
        text-align: center;
        
        h3 {
            margin: 0;
            font-size: 20px;
        }
        
        span {
            display: block;
            font-size: 16px;
            color: #f44336;
        }
        }
    </style>
@endsection

@section('pagetitle')
<section class="background-grey-1 padding-tb-25px text-grey-4">
    <div id="hapus" class="container">
        <h6 class="float-md-left font-2 mt-3">Struktur Pemerintahan</h6>
        <ol class="breadcrumb float-md-right">
            <li><a href="#" class="text-grey-4">Profil </a></li><i class="fa fa-angle-right"></i>
            <li class="active">Struktur Pemerintahan</li>
        </ol>
        <div class="clearfix"></div>
    </div>
</section>

@endsection

@section('content')
<!-- Visi dan Misi -->
<section class="padding-tb-50px">
    <div class="container pad-top pad-bott">
        <div class="text-center margin-bottom-35px wow fadeInUp" style="padding-bottom: 50px;">
            <h1 class="font-weight-300 text-title-large font-3 text-main-color wow fadeInUp" data-wow-delay="0.2s">
                STRUKTUR PEMERINTAHAN</h1>
            <span class="opacity-7">Berikut Struktur Pemerintah Kelurahan Banaran</span>
        </div>
        <div class="row">
            <div class="col-lg-6 justify-content-center">
                <h3 class="text-main-color wow fadeInDown">Susunan Organisasi</h3>
                <a class="nav-link" style="cursor: pointer" data-toggle="modal" data-target="#lihat">
                    @foreach ($susunan as $row)
                    <img src="{{ URL::to('public') }}/assets/img/profil/susunanorganisasi/{{ $row->image }}" alt="">
                    @endforeach
                </a>
            </div>
            <div class="col-lg-6 justify-content-center">
                <h3 class="text-main-color wow fadeInDown">Perangkat Desa</h3>
                <!-- Slider main container -->
                <div class="swiper-container">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        @foreach ($perangkatdesa as $row)
                        <div class="swiper-slide">
                            <div class="picture">
                            <a href="{{ URL::to('public') }}/assets/img/profil/foto/{{ $row->image }}" target="_blank"><img src="{{ URL::to('public') }}/assets/img/profil/foto/{{ $row->image }}" ></a>
                            </div>
                            <div class="detail">
                            <h1>{{ $row->nama }}</h1>
                            <span>{{ $row->Jabatan['jabatan'] }}</span>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <!-- If we need pagination -->
                    <div class="swiper-pagination"></div>           
                </div>
            </div>
        </div>
    </div>

    <!-- modal lihat -->
    <div class="modal fade bd-example-modal-lg" id="lihat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title text-main-color" id="exampleModalLongTitle">Susunan Organisasi Pemerintah Desa Banaran</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    @foreach ($susunan as $row)
                    <img src="{{ URL::to('public') }}/assets/img/profil/susunanorganisasi/{{ $row->image }}" alt="">
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- end of modal1 -->
</section>
<!-- End Visi dan Misi -->

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.2/js/swiper.min.js"></script>
    <script>
        var swiper = new Swiper(".swiper-container", {
            effect: "coverflow",
            grabCursor: true,
            centeredSlides: true,
            slidesPerView: "auto",
            coverflowEffect: {
                rotate: 20,
                stretch: 0,
                depth: 350,
                modifier: 1,
                slideShadows: true
            },
            pagination: {
                el: ".swiper-pagination"
            }
        });

    </script>
@endsection
@endsection
