@extends('layouts.landingpage')

@section('title')
<title>Kelurahan Banaran | UMKM</title>    
@endsection

@section('pagetitle')
<section class="background-grey-1 padding-tb-25px text-grey-4">
    <div id="hapus" class="container">
        <h6 class="float-md-left font-2 mt-3">UMKM</h6>
        <ol class="breadcrumb float-md-right">
            <li><a href="#" class="text-grey-4">Home</a></li><i class="fa fa-angle-right"> </i>
            <li class="active">UMKM</li>
        </ol>
        <div class="clearfix"></div>
    </div>
</section>
    
@endsection

@section('content')
    <!-- UMKM -->
    <section class="padding-tb-50px">
        <div class="container pad-top pad-bott">
            <div class="text-center margin-bottom-35px wow fadeInUp" style="padding-bottom: 50px;">
                <h1 class="font-weight-300 text-title-large font-3 text-main-color wow fadeInUp" data-wow-delay="0.2s">UMKM</h1>
                <span class="opacity-7">UMKM yang berada di Kelurahan Banaran</span>
            </div>
            <div class="row">
                <div class="col-lg-12 ">
                    <section class="section-blog">
                        <div class="container">
                            <!-- UMKM -->
                            <div class="row">
                                @foreach ($umkm as $row)
                                    <div class="col-lg-4">
                                        <div class="card">
                                            <div class="card-img-wrapper">
                                                <img class="card-img-top" src="{{ URL::to('public') }}/assets/1119126/{{ $row->image }}" alt="Card image cap">
                                            </div>
                                            <div class="card-body">
                                                <a href="{{ url('/umkm'. '/' . $row->slug) }}" class="font-weight-bold d-block text-dark text-uppercase text-medium margin-bottom-10px font-weight-700">{{ $row->merk_usaha }}</a>    
                                                <span class="text-extra-small">SIUP :  <a href="{{ url('/umkm'. '/' . $row->slug) }}" class="text-main-color">{{ $row->no_siup }}</a></span>
                                                <p class="card-text">{!! \Illuminate\Support\Str::limit(strip_tags($row->keterangan), $limit = 180, $end = ' ......') !!}</p>
                                                <a href="{{ url('/umkm'. '/' . $row->slug) }}" class="btn btn-primary">Read More</a>
                                                {{-- <a style="float: right" target="_blank" href="https://api.whatsapp.com/send?phone={{ $row->no_whatsapp }}&text=Saya berminat dan bermaksud untuk order :)" class="btn btn-success"><i class="fa fa-whatsapp"></i></a> --}}
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <!-- // UMKM -->
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>
    <!-- End UMKM -->
@endsection