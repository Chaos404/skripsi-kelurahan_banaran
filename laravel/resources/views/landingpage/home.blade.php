@extends('layouts.landingpage')

@section('title')
<title>Kelurahan Banaran | Home</title>
@endsection

@section('content')
    <!-- Carousel -->
    <section>
        <div id="carouselExampleIndicators" id="jadibesar" class="carousel slide hero-carousel" data-ride="carousel">
            <div class="carousel-inner">
                @foreach ($banner1 as $row)
                <div class="carousel-item active">
                    <div class="carousel-image image-zoom" style="background-image: url('public/assets/img/homepage/bannerutama/{{ $row->banner }}');"></div>
                    <div class='slide-content'>
                        <div class='overlay'></div>
                        <h3>SELAMAT DATANG</h3>
                        <p class="paragraph">DESA BANARAN, KEC. KALIJAMBE <br> KAB. SRAGEN</p>
                    </div>
                </div>
                @endforeach
                @foreach ($banner2 as $row)
                <div class="carousel-item">
                    <div class="carousel-image image-zoom" style="background-image: url('public/assets/img/homepage/bannerutama/{{ $row->banner }}');"></div>
                    <div class='slide-content'>
                        <div class='overlay'></div>
                        {{-- <p class="paragraph"></P> --}}
                        <p class="paragraph">SISTEM <br> INFORMASI PELAYANAN PUBLIK</p>
                    </div>
                </div>
                @endforeach
                @foreach ($banner3 as $row)
                <div class="carousel-item">
                    <div class="carousel-image image-zoom" style="background-image: url('public/assets/img/homepage/bannerutama/{{ $row->banner }}');"></div>
                    <div class='slide-content'>
                        <div class='overlay'></div>
                        <p class="paragraph">PEMERINTAH DESA BANARAN</P>
                        <p>Wujud keterbukaan informasi publik dan transparansi penyelenggaraan pemerintah Desa</p>
                    </div>
                </div>
                @endforeach
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
            <div style="text-align: center">
                <a href="#sambutan">
                    <div class="scroll-down"></div>
                </a>
            </div>
        </div>
    </section>
    <!-- // Carousel -->

    <!-- // Tagline -->
    {{-- <div class="container" style="padding-top: 100px">
        <div class="row tagline">
            @foreach ($tagline as $row)
            <div class="col-lg-3 col-sm-6 col-xs-3 sm-mb-30px wow fadeInUp">
                <div class="card-tagline hvr-grow">
                    <div class="text-center ">
                        <h2 class="text-main-color">{{ $row->tagline }}</h2>
                        <p class="opacity">{{ $row->deskripsi }}</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div> --}}
    <!-- // Tagline -->

    <!-- Sambutan Lurah -->
    <section class="padding-tb-20px">
        <div id="sambutan" class="container kategori">
            <div  class="text-center wow fadeInUp">
                <h1 class="text-title-large font-3 text-main-color">SELAMAT DATANG</h1>
                <span class="opacity">Di Webiste Kelurahan Banaran</span>
            </div>
            <div class="row">
                @foreach ($kepaladesa as $row)
                <div class="col-lg-4 col-centered mt-5 my-auto">
                    <img class="rounded-circle" alt="100x100"
                        src="{{ URL::to('public') }}/assets/img/profil/foto/{{ $row->image }}" data-holder-rendered="true" height="200px" width="auto">
                    <h3 class="opacity text-center">{{ $row->nama }}</h3>
                </div>
                @endforeach
                <div class="col-lg-8">
                    @foreach ($sambutan as $row)
                    <p style="text-align: justify">{!! $row->sambutan !!}</p>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <!-- // Sambutan Lurah -->

    <!-- Visi dan Misi -->
    <section class="padding-tb-20px">
        <div class="container kategori">
            <div class="text-center mb-5 wow fadeInUp">
                <h1 class="text-title-large font-3 text-main-color">VISI & MISI</h1>
                <span class="opacity">Berikut Visi dan Misi Kelurahan Banaran</span>
            </div>
            <div class="row">
                <div class="col-lg-6 justify-content-center">
                    <h3 class="text-main-color wow fadeInDown">VISI</h3>
                    <ol class="font-2 text-justify">
                        @foreach ($visi as $row)
                            <li>{{ $row->deskripsi }}</li>
                        @endforeach
                    </ol>
                </div>
                <div class="col-lg-6 justify-content-center">
                    <h3 class="text-main-color wow fadeInDown">MISI</h3>
                    <ol class="font-2 text-justify">
                        @foreach ($misi as $row)
                            <li>{{ $row->deskripsi }}</li>
                        @endforeach
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- // Visi dan Misi -->

    <!-- QnA -->
    <section class="padding-tb-500px">
        <div class="container kategori">
            <div class="text-center mb-5 wow fadeInUp">
                <h1 class="text-title-large font-3 text-main-color">PETUNJUK PENGGUNAAN</h1>
                <span class="opacity">Tata Cara Dalam Pengajuan Pelayanan Online</span>
            </div>
            <div class="row">
                <div class="jadikecilya col-lg-4 my-auto justify-content-center">
                    <img alt="100x100" src="public/assets/img/ilustrator/it_support_monochromatic.svg"
                        data-holder-rendered="true">
                </div>
                <div class="col-lg-8 sm-mb-30px">
                    <div id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="card-qna">
                            <div class="card-header" role="tab" id="headingOne">
                                <h5 class="mb-0">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-1"
                                        aria-expanded="true" aria-controls="collapseOne"
                                        class="d-block text-dark text-capitalize text-up-small font-weight-700"><i
                                            class="fa fa-info margin-right-10px"></i> Apakah ini ? </a>
                                </h5>
                            </div>

                            <div id="collapse-1" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                                <div class="container padding-30px">
                                    Selamat datang di sistem administrasi kependudukan kelurahan Banaran. Sistem ini
                                    berfungsi sebagai media informasi dan pelayanan yang ditujukan kepada
                                    masyarakat Banaran guna dapat memberikan kemudahan dalam proses pengajuan berbagai
                                    administrasi yang bersangkutan dengan kelurahan Banaran.
                                </div>
                            </div>
                        </div>
                        <div class="card-qna">
                            <div class="card-header" role="tab" id="headingOne">
                                <h5 class="mb-0">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-2"
                                        aria-expanded="true" aria-controls="collapseOne"
                                        class="d-block text-dark text-capitalize text-up-small font-weight-700"><i
                                            class="fa fa-info margin-right-10px"></i> Bagaimana saya bisa menggunakan
                                        ?</a>
                                </h5>
                            </div>

                            <div id="collapse-2" class="collapse hide" role="tabpanel" aria-labelledby="headingOne">
                                <div class="container padding-30px">
                                    Anda dapat menggunakan sistem ini dengan mudah, untuk menggunakan pelayanan
                                    administrasi secara online anda dapat registrasi terlebih dahulu kesistem.
                                </div>
                            </div>
                        </div>
                        <div class="card-qna">
                            <div class="card-header" role="tab" id="headingOne">
                                <h5 class="mb-0">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-3"
                                        aria-expanded="true" aria-controls="collapseOne"
                                        class="d-block text-dark text-capitalize text-up-small font-weight-700"><i
                                            class="fa fa-info margin-right-10px"></i> Bagaimana mekanisme pengajuan
                                        pelayanan ?</a>
                                </h5>
                            </div>

                            <div id="collapse-3" class="collapse hide" role="tabpanel" aria-labelledby="headingOne">
                                <div class="container padding-30px">
                                    Anda dapat melakukan pengajuan administrasi kependudukan melalui sistem ini apabila
                                    anda berhasil registrasi dan akun anda <span
                                        style="color:green"><b>Terverifikasi</b></span> oleh petugas. Kemudian anda dapat mengajukan
                                    pelayanan sesuai keperluan melalui menu pelayanan.
                                </div>
                            </div>
                        </div>
                        <div class="card-qna">
                            <div class="card-header" role="tab" id="headingOne">
                                <h5 class="mb-0">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-4"
                                        aria-expanded="true" aria-controls="collapseOne"
                                        class="d-block text-dark text-capitalize text-up-small font-weight-700"><i
                                            class="fa fa-info margin-right-10px"></i> Kenapa akun saya tidak
                                        terverifikasi ?</a>
                                </h5>
                            </div>

                            <div id="collapse-4" class="collapse hide" role="tabpanel" aria-labelledby="headingOne">
                                <div class="container padding-30px">
                                    Akun anda tidak terverifikasi karena data diri yang anda masukkan tidak valid dengan
                                    data penduduk Banaran. Informasi lebih lanjut hubungi Administrator (0826232997967)
                                </div>
                            </div>
                        </div>
                        <div class="card-qna">
                            <div class="card-header" role="tab" id="headingOne">
                                <h5 class="mb-0">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-5"
                                        aria-expanded="true" aria-controls="collapseOne"
                                        class="d-block text-dark text-capitalize text-up-small font-weight-700"><i
                                            class="fa fa-info margin-right-10px"></i> Bagaimana cara mengetahui status
                                        pengajuan saya ? </a>
                                </h5>
                            </div>

                            <div id="collapse-5" class="collapse hide" role="tabpanel" aria-labelledby="headingOne">
                                <div class="container padding-30px">
                                    Status pengajuan administrasi akan diproses secepat mungkin oleh Administator dan
                                    akan ditolak apabila data pengajuan yang anda masukkan tidak valid. Status dapat
                                    dilihat dalam profil setelah anda melakukan login.
                                </div>
                            </div>
                        </div>
                        <div class="card-qna">
                            <div class="card-header" role="tab" id="headingOne">
                                <h5 class="mb-0">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-6"
                                        aria-expanded="true" aria-controls="collapseOne"
                                        class="d-block text-dark text-capitalize text-up-small font-weight-700"><i
                                            class="fa fa-info margin-right-10px"></i> Bagaimana melihat pengajuan saya yang
                                        terverifikasi ?</a>
                                </h5>
                            </div>

                            <div id="collapse-6" class="collapse hide" role="tabpanel" aria-labelledby="headingOne">
                                <div class="container padding-30px">
                                    Pengajuan yang berhasil terverifikasi dapat didownload dalam bentuk pdf pada profil
                                    menu Pengajuan dan dikirimkan ke email anda.
                                </div>
                            </div>
                        </div>
                        <div class="card-qna">
                            <div class="card-header" role="tab" id="headingOne">
                                <h5 class="mb-0">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-7"
                                        aria-expanded="true" aria-controls="collapseOne"
                                        class="d-block text-dark text-capitalize text-up-small font-weight-700"><i
                                            class="fa fa-info margin-right-10px"></i> Bagaimana langkah setelah saya
                                        mendapatkan surat ?</a>
                                </h5>
                            </div>

                            <div id="collapse-7" class="collapse hide" role="tabpanel" aria-labelledby="headingOne">
                                <div class="container padding-30px">
                                    Setelah anda mendapatkan surat dalam bentuk pdf, maka segera cetak surat tersebut
                                    dan siap digunakan dalam proses selanjutnya.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="jadibesarya col-lg-4 my-auto justify-content-center">
                    <img alt="100x100" src="public/assets/img/ilustrator/it_support_monochromatic.svg"
                        data-holder-rendered="true">
                </div>
            </div>
        </div>
    </section>
    <!-- // QnA -->

    <!-- Blog -->
    <section class="padding-tb-20px">
        <div class="container kategori">
            <div class="text-center mb-5 wow fadeInUp">
                <h1 class="text-title-large font-3 text-main-color">BERITA & INFORMASI</h1>
                <span class="opacity">Menyajikan Berita dan Informasi Terkini</span>
            </div>
            <div class="row">
                <div class="jadikecilya col-lg-4 col-md-4">
                    <!-- widget Latest -->
					<div class="widget">
                        <h4 class="widget-title clearfix"><span><i class="fa fa-search" aria-hidden="true"></i>&nbsp;<b>Search</b></span></h4>
                        <form action="{{ route('search') }}" method="GET" autocomplete="off">
                            <div class="input-group my-2">
                                <input type="text" class="form-control" placeholder="Search" aria-label="Search" name="search" required  style="border-color:#0093dd">
                                <div class="input-group-append">
                                    <button class="btn" type="submit" style="background-color:#FFFFFF; border-color:#0093dd">
                                        <i class="fa fa-search" style="color:#0093dd"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                        <br>
						<h4 class="widget-title clearfix"><span><i class="fa fa-file" aria-hidden="true"></i>&nbsp;<b>Terkini</b></span></h4>
						<ul class="last-posts">
							@foreach ($terkinis as $terkini)
                            <li>
								<a href="{{ url('/berita', [$terkini->slug]) }}" class="float-left margin-right-widget width-widget"><img src="{{ URL::to('public') }}/assets/1117942/{{ $terkini->image }}" alt=""></a>
								<a href="{{ url('/berita', [$terkini->slug]) }}" class="font-weight-bold d-block  text-uppercase text-medium text-dark font-weight-700">{{ \Illuminate\Support\Str::limit($terkini->judul, $limit = 20, $end = ' ...') }}</a>
								<span class="text-extra-small">Date :  <a href="{{ url('/berita', [$terkini->slug]) }}" class="text-main-color">{{ Carbon\Carbon::parse($terkini['created_at'])->isoFormat('D MMMM Y') }}</a></span>
							</li>
                            @endforeach
						</ul>

					</div>
					<!-- //  widget Latest -->

                    <!-- Informasi -->
                    <div class="widget widget_categories">
                        <h4 class="widget-title clearfix"><span><i class="fa fa-bullhorn"
                            aria-hidden="true"></i>&nbsp;<b>Informasi</b></span></h4>
                        <div class="row">
                            @foreach ($informasis as $informasi)
                            <a href="{{ url('/berita', [$informasi->slug]) }}"><div class="col-lg-6 col-6 col-md-6 mb-3 sm-mb-30px"><a href="{{ url('/berita', [$informasi->slug]) }}"><img src="{{ URL::to('public') }}/assets/1117942/{{ $informasi->image }}" alt=""></a></div></a>
                            @endforeach
                        </div>
                    </div>
                    <!-- //  Informasi -->
                </div>

                <div class="col-lg-8 ">
                    <div class="container">
                        <div class="row">
                            @foreach ($berita as $row)
                            <div class="col-lg-6" style="margin-bottom:35px">
                                <div class="card">
                                    <div class="card-img-wrapper">
                                        <img class="card-img-top" src="{{ URL::to('public') }}/assets/1117942/{{ $row->image }}" alt="Card image cap">
                                    </div>
                                    <div class="card-body" style="text-align: justify">
                                        <a href="{{ url('/berita', [$row->slug]) }}" class="font-weight-bold d-block text-dark text-uppercase text-medium margin-bottom-10px font-weight-700">{{ \Illuminate\Support\Str::limit($row->judul, $limit = 50, $end = ' ...') }}</a>    
                                        <span class="text-extra-small"><a href="{{ url('/berita', [$row->slug]) }}" class="text-main-color">{{ Carbon\Carbon::parse($row['created_at'])->isoFormat('D MMMM Y') }}</a></span>
                                        <p class="card-text">{!! \Illuminate\Support\Str::limit(strip_tags($row->deskripsi), $limit = 180, $end = ' ......') !!}</p>
                                        <a href="{{ url('/berita', [$row->slug]) }}" class="btn btn-primary">Read More</a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="jadibesarya col-lg-4 col-md-4">
                    <!-- widget Latest -->
					<div class="widget">
                        <h4 class="widget-title clearfix"><span><i class="fa fa-search" aria-hidden="true"></i>&nbsp;<b>Search</b></span></h4>
                        <form action="{{ route('search') }}" method="GET" autocomplete="off">
                            <div class="input-group my-2">
                                <input type="text" class="form-control" placeholder="Search" aria-label="Search" name="search" required  style="border-color:#0093dd">
                                <div class="input-group-append">
                                    <button class="btn" type="submit" style="background-color:#FFFFFF; border-color:#0093dd">
                                        <i class="fa fa-search" style="color:#0093dd"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                        <br>
						<h4 class="widget-title clearfix"><span><i class="fa fa-file" aria-hidden="true"></i>&nbsp;<b>Terkini</b></span></h4>
						<ul class="last-posts">
							@foreach ($terkinis as $terkini)
                            <li>
								<a href="{{ url('/berita', [$terkini->slug]) }}" class="float-left margin-right-widget width-widget"><img src="{{ URL::to('public') }}/assets/1117942/{{ $terkini->image }}" alt=""></a>
								<a href="{{ url('/berita', [$terkini->slug]) }}" class="font-weight-bold d-block  text-uppercase text-medium text-dark font-weight-700">{{ \Illuminate\Support\Str::limit($terkini->judul, $limit = 20, $end = ' ...') }}</a>
								<span class="text-extra-small">Date :  <a href="{{ url('/berita', [$terkini->slug]) }}" class="text-main-color">{{ Carbon\Carbon::parse($terkini['created_at'])->isoFormat('D MMMM Y') }}</a></span>
							</li>
                            @endforeach
						</ul>

					</div>
					<!-- //  widget Latest -->

                    <!-- Informasi -->
                    <div class="widget widget_categories">
                        <h4 class="widget-title clearfix"><span><i class="fa fa-bullhorn"
                            aria-hidden="true"></i>&nbsp;<b>Informasi</b></span></h4>
                        <div class="row">
                            @foreach ($informasis as $informasi)
                            <a href="{{ url('/berita', [$informasi->slug]) }}"><div class="col-lg-6 col-md-6 mb-3 sm-mb-30px"><a href="{{ url('/berita', [$informasi->slug]) }}"><img src="{{ URL::to('public') }}/assets/1117942/{{ $informasi->image }}" alt=""></a></div></a>
                            @endforeach
                        </div>
                    </div>
                    <!-- //  Informasi -->
                </div>

            </div>
        </div>
    </section>
    <!-- // Blog -->
@endsection