@extends('layouts.dashboard1')
@section('title', 'Dashboard | Buat Berita')

<!-- TagPage -->
@section('tagPage')
<div class="page-header" style="color: white">
    <h4 class="page-title" style="color: white">Buat Berita</h4>
    <ul class="breadcrumbs">
        <li class="nav-home">
            <a href="#">
                <i class="flaticon-home" style="color: white"></i>
            </a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="{{ '/dashboard-admin' }}" style="color: white">Dashboard</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="" style="color: white">Buat Berita</a>
        </li>
    </ul>
</div>
@endsection
<!-- End TagPage -->

@section('descPage','Halaman ini digunakan untuk mengelola Berita pada sistem')

@section('content')
<div class="page-inner mt--5">
    <div class="row mt--2">
        <div class="col-md-12">
            <div class="card full-height">
                <div class="card-body">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-check"></i> <strong>Success! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Danger! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('warning'))
                    <div class="alert alert-warning">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-exclamation"></i> <strong>Warning! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('info'))
                    <div class="alert alert-info">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-info"></i> <strong>Info! &nbsp;&nbsp;</strong> 
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Bahaya! &nbsp;&nbsp;</strong>
                        Silakan periksa formulir di bawah ini untuk kesalahan <br>
                        <strong>
                    <ul>
                        @foreach ($errors->all() as $message)
                            <li>{{$message}}</li>
                        @endforeach
                    </ul>
                </strong>
                    </div>
                    @endif

                    <form action="{{ url('dashboard-admin/berita/store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label>Judul</label>
                            <input type="text" name="judul" class="form-control" placeholder="Judul">
                        </div>
                        <div class="form-group">
                            <label>Kategori</label>
                            <select name="kategori" class="form-control">
                                <option value="">-- Pilih kategori --</option>
                                @foreach ($kategori as $row1)
                                    <?php
                                    $selected = ' ';
                                        $selected = 'selected';
                                    ?>
                                    <option value="{{ $row1->id }}"> <?php echo $row1['kategori']; ?> </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <center padding-bottom: 100px;><img id="blah" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="300px" width="350px" name="banner"/></center><br>
                            <input type='file' class="form-control @error('gambar') is-invalid @enderror" name="gambar" onchange="readURL(this);" />
                        </div>
                        <div class="form-group">
                            <label>Keterangan</label>
                            <textarea id="basic-example" class="form-control" data-validation="required" data-validation="custom" data-validation-regexp="^[a-zA-Z ]{2,30}$"  name="deskripsi"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary btn-lg btn-block background-main-color text-white text-center font-weight-bold text-uppercase rounded-0 padding-15px"><i class="far fa-save"></i>&nbsp; Simpan Data</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

<script>
    tinymce.init({
        selector: '#basic-example',
        height: 500,
        menubar: false,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table paste code help wordcount'
        ],
        toolbar: 'undo redo | formatselect | ' +
        'bold italic backcolor | alignleft aligncenter ' +
        'alignright alignjustify | bullist numlist outdent indent | ' +
        'removeformat | help',
        content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
    });
</script>
@endsection

