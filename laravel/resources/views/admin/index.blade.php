@extends('layouts.dashboard1')
@section('title', 'Kelurahan Banaran | Dashboard Admin')

<!-- Tagpage -->
@section('tagPage')
<div class="page-header" style="color: white">
    <h4 class="page-title" style="color: white">Dashboard Admin</h4>
</div>
@endsection
<!-- // Tagpage -->

@section('content')
<div class="page-inner mt--5">
    <div class="row mt--2">
        <div class="col-md-12">
            <div class="row">
                <div class="col-sm-6 col-lg-4">
                    <a href="">
                        <div class="card p-4">
                            <div class="d-flex align-items-center">
                                <span class="stamp stamp-md bg-secondary mr-3">
                                    <i class="fas fa-user-cog"></i>
                                </span>
                                <div>
                                    <h5 class="mb-1"><b><a href="#">{{ $petugas->count() }} <small>Petugas</small></a></b></h5>
                                    {{-- <small class="text-muted">12 waiting payments</small> --}}
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="card p-4">
                        <div class="d-flex align-items-center">
                            <span class="stamp stamp-md bg-success mr-3">
                                <i class="far fa-newspaper"></i>
                            </span>
                            <div>
                                <h5 class="mb-1"><b><a href="#">{{ $berita->count() }} <small>Berita & Informasi</small></a></b></h5>
                                {{-- <small class="text-muted">32 shipped</small> --}}
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="col-sm-6 col-lg-4">
                    <div class="card p-4">
                        <div class="d-flex align-items-center">
                            <span class="stamp stamp-md bg-warning mr-3">
                                <i class="fas fa-tags"></i>
                            </span>
                            <div>
                                <h5 class="mb-1"><b><a href="#">24 <small>UMK-M</small></a></b></h5>
                            </div>
                        </div>
                    </div>
                </div> --}}
                <div class="col-sm-6 col-lg-4">
                    <div class="card p-4">
                        <div class="d-flex align-items-center">
                            <span class="stamp stamp-md bg-danger mr-3">
                                <i class="far fa-file-alt"></i>
                            </span>
                            <div>
                                <h5 class="mb-1"><b>8<small> Layanan</small></b></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="card card-round">
                <div class="card-body">
                    <div class="card-title fw-mediumbold">Berita dan Informasi</div>
                    <div class="card-list">
                        @foreach ($berita as $row)
                        <div class="item-list">
                            <div class="avatar">
                                <img src="{{ URL::to('public') }}/assets/1117942/{{ $row->image }}" class="avatar-img">
                            </div>
                            <div class="info-user ml-3">
                                <div class="username">{{ \Illuminate\Support\Str::limit($row->judul, $limit = 50, $end = ' ...') }}</div>
                                <div class="status">{{ Carbon\Carbon::parse($row['created_at'])->isoFormat('D MMMM Y') }}</div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="" style="float: right; padding-right:10px">
                <a href="{{ url('dashboard-admin/berita') }}" class="btn btn-info btn-border btn-sm">
                    <span class="btn-label">
                        <i class="fa fas fa-arrow-alt-circle-right"></i>
                    </span>
                    More Detail
                </a>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card card-round">
                <div class="card-body">
                    <div class="card-title fw-mediumbold">Struktur Pemerintahan</div>
                    <div class="card-list">
                        @foreach ($pemerintahan as $row)
                        <div class="item-list">
                            <div class="avatar">
                                <img src="{{ URL::to('public') }}/assets/img/profil/foto/{{ $row->image }}" class="avatar-img rounded-circle">
                            </div>
                            <div class="info-user ml-3">
                                <div class="username">{{ $row->nama }}</div>
                                <div class="status">{{ $row->Jabatan['jabatan'] }}</div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="" style="float: right; padding-right:10px">
                <a href="{{ url('dashboard-admin/profil/strukturpemerintahan') }}" class="btn btn-info btn-border btn-sm">
                    <span class="btn-label">
                        <i class="fa fas fa-arrow-alt-circle-right"></i>
                    </span>
                    More Detail
                </a>
            </div>
        </div>
    </div>
    
</div>
@endsection
    
@section('script')
<!-- jQuery Scrollbar -->
<script src="{{ asset('assets/assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js')}}"></script>


<!-- Chart JS -->
<script src="{{ asset('assets/assets/js/plugin/chart.js/chart.min.js')}}"></script>

<!-- jQuery Sparkline -->
<script src="{{ asset('assets/assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js')}}"></script>

<!-- Chart Circle -->
<script src="{{ asset('assets/assets/js/plugin/chart-circle/circles.min.js')}}"></script>

<!-- Datatables -->
<script src="{{ asset('assets/assets/js/plugin/datatables/datatables.min.js')}}"></script>

<!-- Bootstrap Notify -->
<script src="{{ asset('assets/assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js')}}"></script>

<!-- jQuery Vector Maps -->
<script src="{{ asset('assets/assets/js/plugin/jqvmap/jquery.vmap.min.js')}}"></script>
<script src="{{ asset('assets/assets/js/plugin/jqvmap/maps/jquery.vmap.world.js')}}"></script>

<!-- Sweet Alert -->
<script src="{{ asset('assets/assets/js/plugin/sweetalert/sweetalert.min.js')}}"></script>

<script>
    Circles.create({
        id:'circles-1',
        radius:45,
        value:60,
        maxValue:100,
        width:7,
        text: 5,
        colors:['#f1f1f1', '#FF9E27'],
        duration:400,
        wrpClass:'circles-wrp',
        textClass:'circles-text',
        styleWrapper:true,
        styleText:true
    })

    Circles.create({
        id:'circles-2',
        radius:45,
        value:70,
        maxValue:100,
        width:7,
        text: 36,
        colors:['#f1f1f1', '#2BB930'],
        duration:400,
        wrpClass:'circles-wrp',
        textClass:'circles-text',
        styleWrapper:true,
        styleText:true
    })

    Circles.create({
        id:'circles-3',
        radius:45,
        value:40,
        maxValue:100,
        width:7,
        text: 12,
        colors:['#f1f1f1', '#F25961'],
        duration:400,
        wrpClass:'circles-wrp',
        textClass:'circles-text',
        styleWrapper:true,
        styleText:true
    })

    var totalIncomeChart = document.getElementById('totalIncomeChart').getContext('2d');

    var mytotalIncomeChart = new Chart(totalIncomeChart, {
        type: 'bar',
        data: {
            labels: ["S", "M", "T", "W", "T", "F", "S", "S", "M", "T"],
            datasets : [{
                label: "Total Income",
                backgroundColor: '#ff9e27',
                borderColor: 'rgb(23, 125, 255)',
                data: [6, 4, 9, 5, 4, 6, 4, 3, 8, 10],
            }],
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            legend: {
                display: false,
            },
            scales: {
                yAxes: [{
                    ticks: {
                        display: false //this will remove only the label
                    },
                    gridLines : {
                        drawBorder: false,
                        display : false
                    }
                }],
                xAxes : [ {
                    gridLines : {
                        drawBorder: false,
                        display : false
                    }
                }]
            },
        }
    });

    $('#lineChart').sparkline([105,103,123,100,95,105,115], {
        type: 'line',
        height: '70',
        width: '100%',
        lineWidth: '2',
        lineColor: '#ffa534',
        fillColor: 'rgba(255, 165, 52, .14)'
    });
</script>
@endsection