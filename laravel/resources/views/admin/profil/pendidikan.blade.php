<div class="form-group col-md-6">
    <label >Pendidikan Terakhir</label>
    <select name="pendidikan_terakhir" id="" class="form-control">
        @if ($row->pendidikan_terakhir == "SMA / SMK Sederajat")
            <option <?php echo $selected = 'selected'; ?> value="SMA / SMK Sederajat">SMA / SMK Sederajat</option>
            <option value="Sarjana">Sarjana</option>
            <option value="Magister">Magister</option>
            <option value="Doktor">Doktor</option>
        @elseif ($row->pendidikan_terakhir == "Sarjana")
            <option value="SMA / SMK Sederajat">SMA / SMK Sederajat</option>
            <option <?php echo $selected = 'selected'; ?> value="Sarjana">Sarjana</option>
            <option value="Magister">Magister</option>
            <option value="Doktor">Doktor</option>
        @elseif ($row->pendidikan_terakhir == "Magister")
            <option value="SMA / SMK Sederajat">SMA / SMK Sederajat</option>
            <option value="Sarjana">Sarjana</option>
            <option <?php echo $selected = 'selected'; ?>  value="Magister">Magister</option>
            <option value="Doktor">Doktor</option>
        @else
            <option value="SMA / SMK Sederajat">SMA / SMK Sederajat</option>
            <option value="Sarjana">Sarjana</option>
            <option value="Magister">Magister</option>
            <option <?php echo $selected = 'selected'; ?>  value="Doktor">Doktor</option>
        @endif
        
    </select>
</div>