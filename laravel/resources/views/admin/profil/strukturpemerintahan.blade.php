@extends('layouts.dashboard1')
@section('title', 'Dashboard | Struktur Pemerintahan')

<!-- TagPage -->
@section('tagPage')
<div class="page-header" style="color: white">
    <h4 class="page-title" style="color: white">Struktur Pemerintahan</h4>
    <ul class="breadcrumbs">
        <li class="nav-home">
            <a href="#">
                <i class="flaticon-home" style="color: white"></i>
            </a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="{{ '/dashboard-admin' }}" style="color: white">Dashboard</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="" style="color: white">Profil</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="" style="color: white">Struktur Pemerintahan</a>
        </li>
    </ul>
</div>
@endsection
<!-- End TagPage -->

@section('descPage','Halaman ini digunakan untuk mengelola Struktur Pemerintahan kelurahan Banaran')

@section('content')
<div class="page-inner mt--5">
    <div class="row mt--2">
        <div class="col-md-12">
            <div class="card full-height">
                <div class="card-body">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-check"></i> <strong>Success! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Danger! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('warning'))
                    <div class="alert alert-warning">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-exclamation"></i> <strong>Warning! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('info'))
                    <div class="alert alert-info">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-info"></i> <strong>Info! &nbsp;&nbsp;</strong> 
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Bahaya! &nbsp;&nbsp;</strong>
                        Silakan periksa formulir di bawah ini untuk kesalahan <br>
                        <strong>
                            <ul>
                                @foreach ($errors->all() as $message)
                                    <li>{{$message}}</li>
                                @endforeach
                            </ul>
                        </strong>
                    </div>
                    @endif

                    <div class="text-right" data-toggle="modal" data-target="#create" style="margin-bottom: -2rem">
                        <a class="btn btn-info btn-round pull-right">
                          <i class="fas fa-plus-circle"></i>&nbsp; Add Data
                        </a>
                    </div>
                    <div class="table-responsive" style="padding-top: 10px">
                        <table id="basic-datatables" class="display table table-striped table-hover" >
                            <thead>
                                <tr>
                                    <th class="text-center" width="3%">#</th>
                                    <th class="text-center" width="20%">Nama</th>
                                    {{-- <th class="text-center" width="20%">NIK</th> --}}
                                    <th class="text-center" width="20%">Jabatan</th>
                                    <th class="text-center" width="20%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                            $no = 1;
                            @endphp
                            @foreach ($perangkatdesa as $row)
                            <tr>
                                <td class="text-center">{{ $no++ }}</td>
                                <td class="text-center">{{ $row->nama }}</td>
                                {{-- <td class="text-center">{{ $row->nik }}</td> --}}
                                <td class="text-center">
                                    @if ($row->jabatans_id == 1)
                                        Kepala Desa
                                    @elseif ($row->jabatans_id == 2)
                                        Sekretaris Desa
                                    @elseif ($row->jabatans_id == 3)
                                        Kaur Keuangan
                                    @elseif ($row->jabatans_id == 4)
                                        Kaur TU Umum
                                    @elseif ($row->jabatans_id == 5)
                                        Kaur Perencanaan
                                    @elseif ($row->jabatans_id == 6)
                                        Kasi Pemerintahan
                                    @elseif ($row->jabatans_id == 7)
                                        Kasi Kesra
                                    @elseif ($row->jabatans_id == 8)
                                        Kasi Pelayanan
                                    @elseif ($row->jabatans_id == 9)
                                        Kebayanan I
                                    @elseif ($row->jabatans_id == 10)
                                        Kebayanan II
                                    @else
                                        Kebayanan III
                                    @endif
                                </td>
                                <td class="text-center">
                                    <a data="{{$row->id}}" id="lihat" data-toggle="modal" data-target="#dataLihat{{ $row->id }}" class="btn btn-sm btn-info"><i class="fas fa-eye" style="color: white;" ></i></a>
                                    &nbsp;&nbsp;
                                    <a data="{{$row->id}}" id="edit" data-toggle="modal" data-target="#dataEdit{{ $row->id }}" class="btn btn-sm btn-primary"><i class="fas fa-edit" style="color: white;" ></i></a>
                                    &nbsp;&nbsp;
                                    <form action="{{url('dashboard-admin/profil/strukturpemerintahan/delete', [$row->id])}}" method="post" class="d-inline">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" onclick="return confirm('Anda yakin menghapus?');" class="btn btn-sm btn-danger" height:100px><i class="far fa-trash-alt"></i></button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>         
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Lihat Modal -->
@foreach ($perangkatdesa as $row)
<div class="modal fade bd-example-modal-lg" id="dataLihat{{ $row->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    {{-- <form action="{{url('dashboard-admin/profil/strukturpemerintahan/update', [$row->id])}}" method="post" enctype="multipart/form-data"> --}}
        @csrf
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="page-title" id="myModalLabel">Detail Perangkat Desa</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>Nama Lengkap</label>
                            <input type="text" name="nama" class="form-control" placeholder="Nama Lengkap" value="{{ $row->nama }}" disabled>
                        </div>
                    </div>
                    {{-- <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>NIK</label>
                            <input type="text" name="nik" class="form-control" placeholder="NIK" value="{{ $row->nik }}" disabled>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Tempat Lahir</label>
                            <input type="text" name="tempat_lahir" class="form-control" placeholder="Tempat Lahir" value="{{ $row->tempat_lahir }}" disabled>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Tanggal Lahir</label>
                            <input type="text" class="form-control" name="tanggal_lahir" data-toggle="datepicker" placeholder="Tanggal Lahir" value="{{ $row->tgl_lahir }}" disabled>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label >Tanggal Pengangkatan</label>
                            <input type="text" class="form-control" name="tanggal_pengangkatan" data-toggle="datepicker" placeholder="Tanggal Pengangkatan" value="{{ $row->tanggal_pengangkatan }}" disabled>
                        </div>
                        <div class="form-group col-md-6">
                            <label >Nomor Hp</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">+62</span>
                                </div>
                                <input type="text" class="form-control" name="nomor_hp" placeholder="Nomor HP" value="{{ $row->nohp }}" aria-describedby="basic-addon1" disabled>
                            </div>
                        </div>
                    </div> --}}
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label >Jenis Kelamin</label>
                            <select name="jenis_kelamin" id="" class="form-control" disabled>
                                @if ($row['jeniskelamin'] == "Laki - laki")
                                    <option <?php echo $selected = 'selected'; ?> value="Laki - laki">Laki - laki</option>
                                    <option  value="Perempuan">Perempuan</option>
                                @elseif ($row['jeniskelamin'] == "Perempuan")
                                    <option <?php echo $selected = 'selected'; ?> value="Perempuan">Perempuan</option>
                                    <option  value="Laki - laki">Laki - laki</option>
                                @else
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label >Foto</label>
                            <center padding-bottom: 100px;><a href="{{ URL::to('public') }}/assets/img/profil/foto/{{ $row->image }}" target="_blank"><img src="{{ URL::to('public') }}/assets/img/profil/foto/{{ $row->image }}" height="150px" width="200px"></a></center><br>
                        </div>
                        {{-- <div class="form-group col-md-6">
                            <label >SK Pengangkatan</label>
                            <center padding-bottom: 100px;><a href="{{ URL::to('public') }}/assets/img/profil/sk/{{ $row->sk_pengangkatan }}" target="_blank"><i class="far fa-file-pdf fa-10x"></i></a></center><br>
                        </div> --}}
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label >Jabatan</label>
                            <select id="" name="jabatan" class="form-control" disabled>
                                @foreach ($jabatan as $row1)
                                    <?php
                                    $selected = ' ';
                                    if($row['jabatans_id'] == $row1['id']){
                                        $selected = 'selected';
                                    }
                                    ?>
                                    <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['jabatan']; ?> </option>
                                    ?>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {{-- <button type="submit" class="btn btn-primary"><i class="far fa-save"></i>&nbsp; Simpan Data</button> --}}
                </div>
            </div>
        </div>
    {{-- </form> --}}
</div>
@endforeach
<!-- // Lihat Modal -->

<!-- Edit Modal -->
@foreach ($perangkatdesa as $row)
<div class="modal fade bd-example-modal-lg" id="dataEdit{{ $row->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <form action="{{url('dashboard-admin/profil/strukturpemerintahan/update', [$row->id])}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="page-title" id="myModalLabel">Edit Data Perangkat Desa</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>Nama Lengkap</label>
                            <input type="text" name="nama" class="form-control" placeholder="Nama Lengkap" value="{{ $row->nama }}">
                        </div>
                    </div>
                    {{-- <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>NIK</label>
                            <input type="text" name="nik" class="form-control" placeholder="NIK" value="{{ $row->nik }}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Tempat Lahir</label>
                            <input type="text" name="tempat_lahir" class="form-control" placeholder="Tempat Lahir" value="{{ $row->tempat_lahir }}">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Tanggal Lahir</label>
                            <input type="text" class="form-control" name="tanggal_lahir" data-toggle="datepicker" placeholder="Tanggal Lahir" value="{{ $row->tgl_lahir }}">
                        </div>
                    </div> --}}
                    {{-- <div class="form-row">
                        <div class="form-group col-md-6">
                            <label >Tanggal Pengangkatan</label>
                            <input type="text" class="form-control" name="tanggal_pengangkatan" data-toggle="datepicker" placeholder="Tanggal Pengangkatan" value="{{ $row->tanggal_pengangkatan }}">
                        </div>
                        <div class="form-group col-md-6">
                            <label >Nomor Hp</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">+62</span>
                                </div>
                                <input type="text" class="form-control" name="nomor_hp" placeholder="Nomor HP" value="{{ $row->nohp }}" aria-describedby="basic-addon1">
                            </div>
                        </div>
                    </div> --}}
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label >Jenis Kelamin</label>
                            <select name="jenis_kelamin" id="" class="form-control">
                                @if ($row['jeniskelamin'] == "Laki - laki")
                                    <option <?php echo $selected = 'selected'; ?> value="Laki - laki">Laki - laki</option>
                                    <option  value="Perempuan">Perempuan</option>
                                @elseif ($row['jeniskelamin'] == "Perempuan")
                                    <option <?php echo $selected = 'selected'; ?> value="Perempuan">Perempuan</option>
                                    <option  value="Laki - laki">Laki - laki</option>
                                @else
                                @endif
                            </select>
                        </div>
                        
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label >Foto</label>
                            <center padding-bottom: 100px;><img id="blah{{ $row->id }}" src="{{ URL::to('public') }}/assets/img/profil/foto/{{ $row->image }}" alt="jpg, jpeg, png." height="150px" width="150px" name="foto"/></center><br>
                            <input type='file' class="form-control" name="foto" onchange="readURL{{ $row->id }}(this);" />
                        </div>
                        {{-- <div class="form-group col-md-6">
                            <label >SK Pengangkatan</label><br>
                            <center padding-bottom: 100px;><a href="{{ URL::to('public') }}/assets/img/profil/sk/{{ $row->sk_pengangkatan }}" target="_blank"><i class="far fa-file-pdf fa-9x"></i></a></center><br>
                            <br><input type='file' class="form-control" name="sk_pengangkatan" />
                        </div> --}}
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label >Jabatan</label>
                            <select id="" name="jabatan" class="form-control">
                                @foreach ($jabatan as $row1)
                                    <?php
                                    $selected = ' ';
                                    if($row['jabatans_id'] == $row1['id']){
                                        $selected = 'selected';
                                    }
                                    ?>
                                    <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['jabatan']; ?> </option>
                                    ?>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary"><i class="far fa-save"></i>&nbsp; Simpan Data</button>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    function readURL{{ $row->id }}(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah{{ $row->id }}')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endforeach
<!-- // Edit Modal -->

<!-- Create Modal -->
<div class="modal fade bd-example-modal-lg" id="create" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <form action="{{ url('dashboard-admin/profil/strukturpemerintahan/create') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="modal-dialog modal-lg" style="; ">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="page-title" id="myModalLabel">Tambah Struktur Pemerintahan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>Nama Lengkap</label>
                            <input type="text" name="nama" class="form-control" placeholder="Nama Lengkap">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label >Jenis Kelamin</label>
                            <select name="jenis_kelamin" id="" class="form-control">
                                <option value="">-- Pilih Jenis Kelamin --</option>
                                <option value="Laki - laki">Laki - laki</option>
                                <option value="Perempuan">Perempuan</option>
                            </select>
                        </div>
                        {{-- <div class="form-group col-md-6">
                            <label >Pendidikan Terakhir</label>
                            <select name="pendidikan_terakhir" id="" class="form-control">
                                <option value="">-- Pilih Pendidikan Terakhir --</option>
                                <option value="SMA / SMK Sederajat">SMA / SMK Sederajat</option>
                                <option value="Sarjana">Sarjana</option>
                                <option value="Magister">Magister</option>
                                <option value="Doktor">Doktor</option>
                            </select>
                        </div> --}}
                    </div>
                    {{-- <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>NIK</label>
                            <input type="text" name="nik" class="form-control" placeholder="NIK">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Tempat Lahir</label>
                            <input type="text" name="tempat_lahir" class="form-control" placeholder="Tempat Lahir">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Tanggal Lahir</label>
                            <input type="text" class="form-control" name="tanggal_lahir" data-toggle="datepicker" placeholder="Tanggal Lahir">
                        </div>
                    </div> --}}
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label >Foto</label>
                            <center padding-bottom: 100px;><img id="blah1" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="300px" width="auto" name="foto"/></center><br>
                            <input type='file' class="form-control @error('foto') is-invalid @enderror" name="foto" onchange="readURL1(this);" />
                        </div>
                        {{-- <div class="form-group col-md-6">
                            <label >Nomor Hp</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">+62</span>
                                </div>
                                <input type="text" class="form-control" name="nomor_hp" placeholder="Nomor HP" aria-describedby="basic-addon1">
                            </div>
                        </div> --}}
                    </div>
                    {{-- <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Tanggal Pengangkatan</label>
                            <input type="text" class="form-control" name="tanggal_pengangkatan" data-toggle="datepicker" placeholder="Tanggal Lahir">
                        </div>
                        <div class="form-group col-md-6">
                            <label >SK Pengangkatan</label>
                            <input type='file' class="form-control" name="sk_pengangkatan" />
                        </div>
                    </div> --}}
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label >Jabatan</label>
                            <select id="" name="jabatan" class="form-control">
                                <option value="">-- Pilih Jabatan --</option>
                                @foreach ($jabatan as $row1)
                                    <?php
                                    $selected = ' ';
                                    ?>
                                    <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['jabatan']; ?> </option>
                                    ?>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary"><i class="far fa-save"></i>&nbsp; Simpan Data</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- // Create Modal -->
@endsection

@section('script')
<script>
    $(function() {
        $('[data-toggle="datepicker"]').datepicker({
            autoHide: true,
            zIndex: 2048,
            format: 'yyyy-mm-dd'
        });
    });
</script>

<script>
    

    function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah1')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

</script>
@endsection
