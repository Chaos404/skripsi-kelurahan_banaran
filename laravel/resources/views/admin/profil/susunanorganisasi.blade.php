@extends('layouts.dashboard1')
@section('title', 'Dashboard | Susunan Organisasi')

<!-- TagPage -->
@section('tagPage')
<div class="page-header" style="color: white">
    <h4 class="page-title" style="color: white">Susunan Organisasi</h4>
    <ul class="breadcrumbs">
        <li class="nav-home">
            <a href="#">
                <i class="flaticon-home" style="color: white"></i>
            </a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="{{ '/dashboard-admin' }}" style="color: white">Dashboard</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="" style="color: white">Homepage</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="" style="color: white">Susunan Organisasi</a>
        </li>
    </ul>
</div>
@endsection
<!-- End TagPage -->

@section('descPage','Halaman ini digunakan untuk mengelola Susunan Organisasi pada sistem')

@section('content')
<div class="page-inner mt--5">
    <div class="row mt--2">
        <div class="col-md-12">
            <div class="card full-height">
                <div class="card-body">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-check"></i> <strong>Success! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Danger! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('warning'))
                    <div class="alert alert-warning">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-exclamation"></i> <strong>Warning! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('info'))
                    <div class="alert alert-info">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-info"></i> <strong>Info! &nbsp;&nbsp;</strong> 
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Bahaya! &nbsp;&nbsp;</strong>
                        Silakan periksa formulir di bawah ini untuk kesalahan <br>
                        <strong>
                            <ul>
                                @foreach ($errors->all() as $message)
                                    <li>{{$message}}</li>
                                @endforeach
                            </ul>
                        </strong>
                    </div>
                    @endif

                    {{-- <div class="text-right" data-toggle="modal" data-target="#create" style="margin-bottom: -2rem">
                        <a class="btn btn-info btn-round pull-right">
                          <i class="fas fa-plus-circle"></i>&nbsp; Add Data
                        </a>
                    </div> --}}
                    <div class="table-responsive" style="padding-top: 10px">
                        <table id="basic-datatables" class="display table table-striped table-hover" >
                            <thead>
                                <tr>
                                    <th class="text-center" width="3%">#</th>
                                    <th class="text-center" width="57%">Susunan Organisasi</th>
                                    <th class="text-center" width="20%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                            $no = 1;
                            @endphp
                            @foreach ($susunan as $row)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td><center><a href="{{ URL::to('public') }}/assets/img/profil/susunanorganisasi/{{ $row->image }}" target="_blank"><img src="{{ URL::to('public') }}/assets/img/profil/susunanorganisasi/{{ $row->image }}" class="img-thumbnail" height="100px" width="150px"></a></center></td>
                                <td class="text-center">
                                    <a data="{{$row->id}}" id="edit" data-toggle="modal" data-target="#dataEdit{{ $row->id }}" class="btn btn-sm btn-primary"><i class="fas fa-edit" style="color: white;" ></i></a>
                                    {{-- &nbsp;&nbsp;
                                    <a href="{{url('dashboard-petugas/pendaftaran/masuk/delete', [$row->id])}}" onclick="return confirm('Anda yakin menghapus?');" class="btn btn-sm btn-danger" height:100px><i class="far fa-trash-alt"></i></a> --}}
                                </td>
                            </tr>
                            @endforeach
                            </tbody>         
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Edit Modal -->
@foreach ($susunan as $row)
<div class="modal fade bd-example-modal-lg" id="dataEdit{{ $row->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <form action="{{url('dashboard-admin/profil/susunanorganisasi/update', [$row->id])}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="page-title" id="myModalLabel">Edit Susunan Organisasi</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <center><a href="{{ URL::to('public') }}/assets/img/profil/susunanorganisasi/{{ $row->image }}" target="_blank"><center padding-bottom: 100px;><img id="blah{{ $row->id }}" src="{{ URL::to('public') }}/assets/img/profil/susunanorganisasi/{{ $row->image }}" alt="jpg, jpeg, png." height="150px" width="150px" name="banner"/></center><br></a></center><br>
                        <input type='file' class="form-control @error('image') is-invalid @enderror" name="image" onchange="readURL{{ $row->id }}(this);" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary"><i class="far fa-save"></i>&nbsp; Simpan Data</button>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    function readURL{{ $row->id }}(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah{{ $row->id }}')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endforeach
<!-- // Edit Modal -->
@endsection