@extends('layouts.dashboard1')
@section('title', 'Dashboard | Kependudukan')

<!-- TagPage -->
@section('tagPage')
<div class="page-header" style="color: white">
    <h4 class="page-title" style="color: white">Kependudukan</h4>
    <ul class="breadcrumbs">
        <li class="nav-home">
            <a href="#">
                <i class="flaticon-home" style="color: white"></i>
            </a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="{{ '/dashboard-admin' }}" style="color: white">Dashboard</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="" style="color: white">Profil</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="" style="color: white">Kependudukan</a>
        </li>
    </ul>
</div>
@endsection
<!-- End TagPage -->

@section('descPage','Halaman ini digunakan untuk mengelola Kependudukan kelurahan Banaran')

@section('content')
<div class="page-inner mt--5">
    <div class="row mt--2">
        <div class="col-md-12">
            <div class="card full-height">
                <div class="card-body">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-check"></i> <strong>Success! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Danger! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('warning'))
                    <div class="alert alert-warning">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-exclamation"></i> <strong>Warning! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('info'))
                    <div class="alert alert-info">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-info"></i> <strong>Info! &nbsp;&nbsp;</strong> 
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Bahaya! &nbsp;&nbsp;</strong>
                        Silakan periksa formulir di bawah ini untuk kesalahan <br>
                        <strong>
                            <ul>
                                @foreach ($errors->all() as $message)
                                    <li>{{$message}}</li>
                                @endforeach
                            </ul>
                        </strong>
                    </div>
                    @endif

                    <div class="table-responsive" style="padding-top: 10px">
                        <table id="basic-datatables" class="display table table-striped table-hover" >
                            <thead>
                                <tr>
                                    <th class="text-center" width="3%">#</th>
                                    <th class="text-center" width="20%">Jumlah Penduduk</th>
                                    <th class="text-center" width="20%">Laki - laki</th>
                                    <th class="text-center" width="20%">Perempuan</th>
                                    <th class="text-center" width="20%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                            $no = 1;
                            @endphp
                            @foreach ($kependudukan as $row)
                            <tr>
                                <td class="text-center">{{ $no++ }}</td>
                                <td class="text-center">{{ $row->jumlah_penduduk }}</td>
                                <td class="text-center">{{ $row->penduduk_laki }}</td>
                                <td class="text-center">{{ $row->penduduk_perempuan }}</td>
                                <td class="text-center">
                                    <a data="{{$row->id}}" id="edit" data-toggle="modal" data-target="#dataEdit{{ $row->id }}" class="btn btn-sm btn-primary"><i class="fas fa-edit" style="color: white;" ></i></a>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>         
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Edit Modal -->
@foreach ($kependudukan as $row)
<div class="modal fade bd-example-modal-lg" id="dataEdit{{ $row->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <form action="{{url('dashboard-admin/profil/kependudukan/update', [$row->id])}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="page-title" id="myModalLabel">Edit Kependudukan Kelurahan Banaran</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Kependudukan</label>
                        <input type="text" name="jumlah_penduduk" class="form-control" value="{{ $row->jumlah_penduduk }}" required>
                    </div>
                    <div class="form-group">
                        <label>Penduduk Laki - laki</label>
                        <input type="text" name="penduduk_laki" class="form-control" value="{{ $row->penduduk_laki }}" required>
                    </div>
                    <div class="form-group">
                        <label>Penduduk Perempuan</label>
                        <input type="text" name="penduduk_perempuan" class="form-control" value="{{ $row->penduduk_perempuan }}" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary"><i class="far fa-save"></i>&nbsp; Simpan Data</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endforeach
<!-- // Edit Modal -->

<!-- Create Modal -->
<div class="modal fade bd-example-modal-lg" id="create" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <form action="{{ url('dashboard-admin/profil/tupoksi/create') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="modal-dialog modal-lg" style="; ">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="page-title" id="myModalLabel">Tambah Tupoksi</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Tugas Pokok dan Fungsi</label>
                        <textarea row="5" type="text" class="form-control" name="tupoksi" placeholder="Tugas pokok dan fungsi"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary"><i class="far fa-save"></i>&nbsp; Simpan Data</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- // Create Modal -->
@endsection
