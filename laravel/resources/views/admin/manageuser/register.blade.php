@extends('layouts.dashboard1')
@section('title', 'Dashboard | Manage User')

<!-- TagPage -->
@section('tagPage')
<div class="page-header" style="color: white">
    <h4 class="page-title" style="color: white">Manage User</h4>
    <ul class="breadcrumbs">
        <li class="nav-home">
            <a href="#">
                <i class="flaticon-home" style="color: white"></i>
            </a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="{{ '/dashboard-admin' }}" style="color: white">Dashboard</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="" style="color: white">Manage User</a>
        </li>
    </ul>
</div>
@endsection
<!-- End TagPage -->

@section('descPage','Halaman ini digunakan untuk mengelola Petugas pada sistem')

@section('content')
<div class="page-inner mt--5">
    <div class="row mt--2">
        <div class="col-md-12">
            <div class="card full-height">
                <div class="card-body">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-check"></i> <strong>Success! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Danger! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('warning'))
                    <div class="alert alert-warning">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-exclamation"></i> <strong>Warning! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('info'))
                    <div class="alert alert-info">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-info"></i> <strong>Info! &nbsp;&nbsp;</strong> 
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Bahaya! &nbsp;&nbsp;</strong>
                        Silakan periksa formulir di bawah ini untuk kesalahan <br>
                        <strong>
                            <ul>
                                @foreach ($errors->all() as $message)
                                    <li>{{$message}}</li>
                                @endforeach
                            </ul>
                        </strong>
                    </div>
                    @endif

                    <div class="text-right" data-toggle="modal" data-target="#create" style="margin-bottom: -2rem">
                        <a class="btn btn-info btn-round pull-right">
                          <i class="fas fa-plus-circle"></i>&nbsp; Add Data
                        </a>
                    </div>
                    <div class="table-responsive" style="padding-top: 10px">
                        <table id="basic-datatables" class="display table table-striped table-hover" >
                            <thead>
                                <tr>
                                    <th class="text-center" width="3%">#</th>
                                    <th class="text-center" width="57%">Email</th>
                                    {{-- <th class="text-center" width="20%">NOMOR KK</th>
                                    <th class="text-center" width="20%">NAMA</th>
                                    <th class="text-center" width="20%">RT</th> --}}
                                    <th class="text-center" width="20%">Level User</th>
                                    <th class="text-center" width="20%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                            $no = 1;
                            @endphp
                            @foreach ($user as $row)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $row->email }}</td>
                                    {{-- <td class="text-center">{{ $row->no_kk }}</td>   
                                    <td class="text-center">{{ $row->name }}</td>    
                                    <td class="text-center">{{ $row->t_rt_id }}</td> --}}
                                    <td class="text-center" style="text-transform: uppercase;">{{ $row->role }}</td>  
                                    <td class="text-center">
                                        {{-- <a data="{{$row->id}}" id="edit" data-toggle="modal" data-target="#dataEdit{{ $row->id }}" class="btn btn-sm btn-primary"><i class="fas fa-edit" style="color: white;" ></i></a>
                                        &nbsp;&nbsp; --}}
                                        <a href="{{url('dashboard-admin/manage-user/delete', [$row->id])}}" onclick="return confirm('Are you sure to delete this?')" class="btn btn-sm btn-danger" height:100px><i class="far fa-trash-alt"></i></a>
                                    </td>
                                </tr>
                            @endforeach   
                            </tbody>         
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Edit Modal -->
{{-- @foreach ($user as $row)
<div class="modal fade bd-example-modal-lg" id="dataEdit{{ $row->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <form action="{{url('dashboard-admin/manage-user/update', [$row->id])}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="page-title" id="myModalLabel">Edit Level User</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="hidden"  name="role" class="form-control"  value="petugas">
                        <label for="email" class="placeholder"><b>Email</b></label>
                        <input id="email" type="email" class="form-control" name="email" value="{{ $row->email }}" required autofocus>
                    </div>
                    <div class="form-group">
                        <label for="passwordsignin" class="placeholder"><b>Password</b></label>
                        <div class="position-relative">
                            <input id="password" type="password" class="form-control" name="current_password" autocomplete="current-password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="confirmpassword" class="placeholder"><b>Password Baru</b></label>
                        <div class="position-relative">
                            <input id="new_password" type="password" class="form-control" name="new_password" autocomplete="current-password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="confirmpassword" class="placeholder"><b>Confirm Password Baru</b></label>
                        <div class="position-relative">
                            <input id="new_confirm_password" type="password" class="form-control" name="new_confirm_password" autocomplete="current-password">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary"><i class="far fa-save"></i>&nbsp; Simpan Data</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endforeach --}}
<!-- // Edit Modal -->

<!-- Create Modal -->
<div class="modal fade bd-example-modal-lg" id="create" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <form action="{{ url('dashboard-admin/manage-user/create') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="modal-dialog modal-lg" style="; ">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="page-title" id="myModalLabel">Tambah Petugas</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="hidden"  name="role" class="form-control"  value="petugas">
                        <label for="email" class="placeholder"><b>Email</b></label>
                        <input  id="email" name="email" type="email" class="form-control @error('email') is-invalid @enderror"  placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="passwordsignin" class="placeholder"><b>Password</b></label>
                        <div class="position-relative">
                            <input  id="passwordsignin" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password" placeholder="Password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="confirmpassword" class="placeholder"><b>Confirm Password</b></label>
                        <div class="position-relative">
                            <input  id="password-confirm" type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation"  autocomplete="new-password" placeholder="Confirm Password">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary"><i class="far fa-save"></i>&nbsp; Simpan Data</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- // Create Modal -->

@endsection


@section('script')
    <script>
        $('#datepicker').datetimepicker({
            format: 'YYYY-DD-MM',
        });
    </script>
@endsection
