<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Kelurahan Banaran | Registrasi Akun</title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <link rel="icon" href="{{ asset('assets/img/logo2.png')}}" type="image/x-icon" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

   <!-- Fonts and icons -->
	<script src="{{ asset('assets/assets/js/plugin/webfont/webfont.min.js')}}"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['{{ asset('assets/assets/css/fonts.min.css')}}']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
    </script>
    
	
	<!-- CSS Files -->
	<link rel="stylesheet" href="{{ asset('assets/assets/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{ asset('assets/assets/css/atlantis.css')}}">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>
    <style>
        .error {
            color: red;
        }
    </style>

</head>

<body class="login d-flex">
    <div class="row" style="margin-right: 0 !important">
        <div class="col-lg-6 text-center d-flex flex-column" style="position:relative; width:100%; background-color: #0093dd">
            <div class="login-aside w-100 d-flex flex-column align-items-center justify-content-center text-center" style="position: sticky; top:40%;">
                <h1 class="title fw-bold text-white mb-3">Sistem Informasi Pelayanan Publik</h1>
                <p class="subtitle text-white op-7">Harap masukkan seluruh data anda dengan benar pada form yang tersedia, agar akun anda dapat diverifikasi dan dapat melakukan pengajuan pelayanan administrasi secara online melalui sistem ini.</p>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="container container-login container-transparent animated fadeIn">
                <h3 class="text-center mt-5">Registrasi Akun</h3>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-check"></i> <strong>Berhasil! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Danger! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('warning'))
                    <div class="alert alert-warning">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-exclamation"></i> <strong>Warning! &nbsp;&nbsp;</strong>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('info'))
                    <div class="alert alert-info">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-info"></i> <strong>Info! &nbsp;&nbsp;</strong> 
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="fa fa-times"></i> <strong>Bahaya! &nbsp;&nbsp;</strong>
                        Silakan periksa formulir di bawah ini untuk kesalahan <br>
                        <strong>
                            <ul>
                                @foreach ($errors->all() as $message)
                                    <li>{{$message}}</li>
                                @endforeach
                            </ul>
                        </strong>
                    </div>
                    @endif
				<div class="login-form">
                    <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="fullname" class="placeholder"><b>Nama Lengkap</b></label>
                                <input type="text" class="form-control @error('nama_lengkap') is-invalid @enderror" name="nama_lengkap" id="inputName4" placeholder="Nama Lengkap" >
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Tempat Lahir</label>
                                <input type="text" class="form-control @error('tempat_lahir') is-invalid @enderror" name="tempat_lahir" id="inputName4" placeholder="Tempat Lahir">
                            </div>
                            <div class="form-group col-md-6">
                                <label >Tanggal Lahir</label>
                                <div class="input-group">
                                    <input type="text" class="form-control @error('tanggal_lahir') is-invalid @enderror" id="datepicker" placeholder="Tanggal Lahir" name="tanggal_lahir">
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="fa fa-calendar-check"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Jenis Kelamin</label>
                                <select name="jenis_kelamin" id="" class="form-control @error('jenis_kelamin') is-invalid @enderror">
                                    <option value="">-- Pilih Jenis Kelamin --</option>
                                    <option value="Laki - Laki">Laki - Laki</option>
                                    <option value="Perempuan">Perempuan</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Agama</label>
                                <select name="agama" id="" class="form-control @error('agama') is-invalid @enderror">
                                    <option value="">-- Pilih Agama --</option>
                                    @foreach ($agama as $row1)
                                        <?php
                                        $selected = ' ';
                                        if($row1['t_agama_id'] == $row1['id']){
                                            $selected = 'selected';
                                        }
                                        ?>
                                        <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['agama']; ?> </option>
                                        ?>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="email" class="placeholder"><b>Email</b></label>
                                <input  id="email" name="email" type="email" class="form-control @error('email') is-invalid @enderror"  placeholder="Email">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="passwordsignin" class="placeholder"><b>Password</b></label>
                                <div class="position-relative">
                                    <input  id="passwordsignin" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password" placeholder="Password (Minimal 8 Karakter)">
                                    <div class="show-password">
                                        <i class="icon-eye"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="confirmpassword" class="placeholder"><b>Confirm Password</b></label>
                                <div class="position-relative">
                                    <input  id="password-confirm" type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation"  autocomplete="new-password" placeholder="Confirm Password">
                                    <div class="show-password">
                                        <i class="icon-eye"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Pekerjaan</label>
                                <select name="pekerjaan" id="" class="form-control @error('pekerjaan') is-invalid @enderror">
                                    <option value="">-- Pilih Pekerjaan --</option>
                                    @foreach ($pekerjaan as $row1)
                                        <?php
                                        $selected = ' ';
                                        if($row1['t_pekerjaan_id'] == $row1['id']){
                                            $selected = 'selected';
                                        }
                                        ?>
                                        <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['pekerjaan']; ?> </option>
                                        ?>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label >Status Perkawinan</label>
                                <select name="status_perkawinan" id="" class="form-control @error('status_perkawinan') is-invalid @enderror">
                                    <option value="">-- Pilih Status Perkawinan --</option>
                                    <option value="Belum Kawin">Belum Kawin</option>
                                    <option value="Kawin">Kawin</option>
                                    <option value="Pernah Kawin">Pernah Kawin</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label >Dukuh</label>
                                <select name="dukuh" id="dukuhnya" class="form-control @error('dukuh') is-invalid @enderror" onclick="Reset()">
                                    <option value="">-- Pilih Dukuh --</option>
                                    @foreach ($dukuh as $row1)
                                        <?php
                                        $selected = ' ';
                                        if($row1['t_dukuh_id'] == $row1['id']){
                                            $selected = 'selected';
                                        }
                                        ?>
                                        <option <?php echo $selected; ?> value="{{ $row1->id }}"> <?php echo $row1['dukuh']; ?> </option>
                                        ?>
                                            {{-- <option value="{{ $row1->id }}">{{ $row1->tag }}</option> --}}
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label >RT</label>
                                <select name="carirt" id="rt" class="form-control @error('rt') is-invalid @enderror">
                                    <option value="">-- Pilih RT --</option>
                                </select>
                                <input type="hidden" class="form-control @error('rt') is-invalid @enderror" id="rtnya" name="rt">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label >NIK</label>
                                <input type="text" class="form-control @error('nik') is-invalid @enderror" id="inputName4" name="nik" placeholder="NIK (16 Digit)">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label >Nomor KK</label>
                                <input type="text" class="form-control @error('no_kk') is-invalid @enderror" id="inputName4" name="no_kk" placeholder="Nomor KK (16 Digit)">
                            </div>
                        </div>
                        <div class="form-row">
                            {{-- <div class="form-group col-md-6">
                                <label >Lampiran KTP</label>
                                <center padding-bottom: 100px;><img id="blah" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="150px" name="lampiran_ktp"/></center><br>
                                <input type='file' class="form-control @error('lampiran_ktp') is-invalid @enderror" name="lampiran_ktp" onchange="readURL(this);" />
                            </div> --}}
                            <div class="form-group col-md-12">
                                <label >Lampiran KK</label>
                                {{-- <input type="file" class="form-control" id="inputName4" name="lampiran_kk" placeholder="NIK"> --}}
                                <center padding-bottom: 100px;><img id="blah2" src="http://placehold.it/150x150" alt="jpg, jpeg, png." height="150px" width="150px" name="lampiran_kk"/></center><br>
                                <input type='file' class="form-control @error('lampiran_kk') is-invalid @enderror" name="lampiran_kk" onchange="readURL2(this);" />
                            </div>
                        </div>
                        {{-- <div class="row form-sub m-0">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="agree" id="agree">
                                <label class="custom-control-label" for="agree">I Agree the terms and conditions.</label>
                            </div>
                        </div> --}}
                        <div class="form-group">
                            {{-- <label>Captcha</label> --}}
                            {!! NoCaptcha::renderJs() !!}
                            {!! NoCaptcha::display() !!}
                            {{-- <span class="text-danger">{{ $errors->first('g-recaptcha-response') }}</span> --}}
                        </div>
                        {{-- <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" name="remember" id="rememberme" {{ old('remember') ? 'checked' : '' }}>
                                <span class="form-check-sign" for="rememberme">I Agree the terms and conditions.</span>
                            </label>
                        </div> --}}
                        <div class="row form-action text-center">
                            <div class="col-md-6">
                                <a href="{{ url('/login') }}" id="show-signin" class="btn btn-danger btn-link btn-login mr-3">Cancel</a>
                            </div>
                            <div class="col-md-6 mb-5">
                                <button type="submit" class="btn btn-info w-100 fw-bold">Daftar</a>
                            </div>
                        </div>
                    </form>
				</div>
			</div>
        </div>
    </div>


    <script src="{{ asset('assets/assets/js/core/jquery.3.2.1.min.js')}}"></script>
    <script src="{{ asset('assets/assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js')}}"></script>
    <script src="{{ asset('assets/assets/js/core/popper.min.js')}}"></script>
    <script src="{{ asset('assets/assets/js/core/bootstrap.min.js')}}"></script>

    <!-- Moment JS -->
    <script src="{{ asset('assets/assets/js/plugin/moment/moment.min.js')}}"></script>
    <!-- jQuery Scrollbar -->
    <script src="{{ asset('assets/assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js')}}"></script>
    <!-- DateTimePicker -->
    <script src="{{ asset('assets/assets/js/plugin/datepicker/bootstrap-datetimepicker.min.js')}}"></script>
    <!-- Atlantis JS -->
    <script src="{{ asset('assets/assets/js/atlantis.min.js')}}"></script>

    <script>
        $('#datepicker').datetimepicker({
            format: 'YYYY-MM-DD',
        });

    </script>
    <script>
        $(document).ready(function () {
            $('#dukuhnya').click(function () {
                $("input[name=rt]").val("");
            });
        });

        $(document).ready(function () {
            $('select[name="dukuh"]').on('change', function () {
                $('select[name="carirt"]').on('change', function () {
                    var nomorRt = $("#rt option:selected").attr("namaRt");
                    $("#rtnya").val(nomorRt);
                });

                let idRt = $(this).val();
                if (idRt) {
                    jQuery.ajax({
                        url: '/get_RT/' + idRt,
                        type: "GET",
                        dataType: "json",
                        success: function (response) {
                            $('select[name="carirt"]').empty();
                            $('select[name="carirt"]').append(
                                '<option value="">-- Pilih RT --</option>');
                            $.each(response, function (key, value) {
                                $('select[name="carirt"]').append(
                                    '<option value="' + key + '"namaRt="' +
                                    value.rt + '">' + value.rt + '</option>');
                            });
                        },
                    });
                } else {
                    // $('select[name="dukuh"]').append('<option value="">-- pilih kota asal --</option>');
                }
            });
        });

    </script>

    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        function readURL2(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah2')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>
</body>

</html>
