<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<!-- Yield Title -->
	<title>@yield('title')</title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="{{ asset('assets/img/logo2.png')}}" type="image/x-icon"/>
	
	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<!-- Fonts and icons -->
	<script src="{{ asset('assets/assets/js/plugin/webfont/webfont.min.js')}}"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['{{ asset('assets/assets/css/fonts.min.css')}}']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	<link rel="stylesheet" href="{{ asset('assets/assets/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{ asset('assets/assets/css/atlantis.min.css')}}">

	{{-- <script src="/path/to/jquery.js"></script><!-- jQuery is required --> --}}
	<link  href="{{ asset('assets/assets/css/datepicker.css')}}" rel="stylesheet">	

	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link rel="stylesheet" href="{{ asset('assets/assets/css/demo.css')}}">
	
	<!-- Yield Style-->
	@yield('style')
</head>
<body>
	<div class="wrapper">
		<div class="main-header">
			<!-- Logo Header -->
			<div class="logo-header" data-background-color="blue">
				
				<a href="{{ url('/dashboard-admin') }}" class="logo">
					<img src="{{ asset('assets/img/logo2.png')}}" alt="navbar brand" class="navbar-brand" height="50px" width="auto">
				</a>
				<button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon">
						<i class="icon-menu"></i>
					</span>
				</button>
				<button class="topbar-toggler more"><i class="icon-options-vertical"></i></button>
				<div class="nav-toggle">
					<button class="btn btn-toggle toggle-sidebar">
						<i class="icon-menu"></i>
					</button>
				</div>
			</div>
			<!-- End Logo Header -->

			<!-- Navbar Header -->
			<nav class="navbar navbar-header navbar-expand-lg" data-background-color="blue2">
				
				<div class="container-fluid">
					<div class="collapse" id="search-nav">
					</div>
					<ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
						
						<li class="nav-item dropdown hidden-caret">
							<a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
								<div class="avatar-sm">
									<img src="{{ asset('assets/assets/img/profile.png')}}" alt="..." class="avatar-img rounded-circle">
								</div>
							</a>
							<ul class="dropdown-menu dropdown-user animated fadeIn">
								<div class="dropdown-user-scroll scrollbar-outer">
									<li>
										<div class="user-box">
											<div class="avatar-lg"><img src="{{ asset('assets/assets/img/profile.png')}}" alt="image profile" class="avatar-img rounded"></div>
											<div class="u-text">
												<h4>Administrator</h4>
												<p class="text-muted">{{ Auth::user()->email }}</p>
											</div>
										</div>
									</li>
									<li>
										{{-- LOGOUT LARAVEL --}}
                                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><span class="dropdown-item fa fa-sign-out fa-fw"></span>{{ __(' Logout') }}</a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
									</li>
								</div>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
			<!-- End Navbar -->
		</div>

		<!-- Sidebar -->
		<div class="sidebar sidebar-style-2">			
			<div class="sidebar-wrapper scrollbar scrollbar-inner">
				<div class="sidebar-content">
					<div class="user">
						<div class="avatar-sm float-left mr-2">
							<img src="{{ asset('assets/assets/img/profile.png')}}" alt="..." class="avatar-img rounded-circle">
						</div>
						<div class="info">
							<a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
								<span>
									{{-- {{ Auth::user()->name }} --}}
									<span class="user-level"><h3>Administrator</h3></span>
								</span>
							</a>
							<div class="clearfix"></div>
						</div>
					</div>
					<ul class="nav nav-primary">
						<li class="nav-item {{ (request()->is('dashboard-admin')) ? 'active':''  }}">
							<a href="{{ url('dashboard-admin') }}">
								<i class="fas fa-home"></i>
									<p>Dashboard</p>
								
								</a>
							</a>
						</li>
						<li class="nav-section">
							<span class="sidebar-mini-icon">
								<i class="fa fa-ellipsis-h"></i>
							</span>
							<h4 class="text-section">Website</h4>
						</li>
						<li class="nav-item {{ request()->is('dashboard-admin/manage-user') ? 'active submenu' : '' }}">
							<a data-toggle="collapse" href="#user">
								<i class="fas fa-user-cog"></i>
								<p>Role User Petugas</p>
								<span class="caret"></span>
							</a>
							<div class="collapse
							{{ request()->is('dashboard-admin/manage-user') ? 'show' : '' }}" id="user">
								<ul class="nav nav-collapse">
									<li class="{{ Request::is('dashboard-admin/manage-user') ? 'active' : '' }}">
										<a href="{{ url('/dashboard-admin/manage-user') }}">
											<span class="sub-item">Petugas</span>
										</a>
									</li>
								</ul>
							</div>
						</li>
						<li class="nav-item
							{{ request()->is('dashboard-admin/homepage/banner-utama') ? 'active submenu' : '' }}
							{{ request()->is('dashboard-admin/homepage/tagline') ? 'active submenu' : '' }}
							{{ request()->is('dashboard-admin/homepage/sambutan') ? 'active submenu' : '' }}
							">
							<a data-toggle="collapse" href="#base">
								<i class="fas fa-layer-group"></i>
								<p>Homepage</p>
								<span class="caret"></span>
							</a>
							<div class="collapse
								{{ request()->is('dashboard-admin/homepage/banner-utama') ? 'show' : '' }}
								{{ request()->is('dashboard-admin/homepage/tagline') ? 'show' : '' }}
								{{ request()->is('dashboard-admin/homepage/sambutan') ? 'show' : '' }}
								" id="base">
								<ul class="nav nav-collapse">
									<li class="{{ Request::is('dashboard-admin/homepage/banner-utama') ? 'active' : '' }}">
										<a href="{{ url('dashboard-admin/homepage/banner-utama') }}">
											<span class="sub-item">Banner Utama</span>
										</a>
									</li>
									{{-- <li class="{{ Request::is('dashboard-admin/homepage/tagline') ? 'active' : '' }}">
										<a href="{{ url('dashboard-admin/homepage/tagline') }}">
											<span class="sub-item">Tagline</span>
										</a>
									</li> --}}
									<li class="{{ Request::is('dashboard-admin/homepage/sambutan') ? 'active' : '' }}">
										<a href="{{ url('dashboard-admin/homepage/sambutan') }}">
											<span class="sub-item">Sambutan Kepala Desa</span>
										</a>
									</li>
								</ul>
							</div>
						</li>
						<li class="nav-item
							{{ request()->is('dashboard-admin/profil/visi-misi') ? 'active submenu' : '' }}
							{{ request()->is('dashboard-admin/profil/tupoksi') ? 'active submenu' : '' }}
							{{ request()->is('dashboard-admin/profil/susunanorganisasi') ? 'active submenu' : '' }}
							{{ request()->is('dashboard-admin/profil/strukturpemerintahan') ? 'active submenu' : '' }}
							{{ request()->is('dashboard-admin/profil/kampungkb') ? 'active submenu' : '' }}
							{{ request()->is('dashboard-admin/profil/kependudukan') ? 'active submenu' : '' }}
							">
							<a data-toggle="collapse" href="#sidebarLayouts">
								<i class="far fa-building"></i>
								<p>Profil</p>
								<span class="caret"></span>
							</a>
							<div class="collapse
								{{ request()->is('dashboard-admin/profil/visi-misi') ? 'show' : '' }}
								{{ request()->is('dashboard-admin/profil/tupoksi') ? 'show' : '' }}
								{{ request()->is('dashboard-admin/profil/susunanorganisasi') ? 'show' : '' }}
								{{ request()->is('dashboard-admin/profil/strukturpemerintahan') ? 'show' : '' }}
								{{ request()->is('dashboard-admin/profil/kampungkb') ? 'show' : '' }}
								{{ request()->is('dashboard-admin/profil/kependudukan') ? 'show' : '' }}
								" id="sidebarLayouts">
								<ul class="nav nav-collapse">
									<li class="{{ Request::is('dashboard-admin/profil/visi-misi') ? 'active' : '' }}">
										<a href="{{ url('dashboard-admin/profil/visi-misi') }}">
											<span class="sub-item">Visi & Misi</span>
										</a>
									</li>
									<li class="{{ Request::is('dashboard-admin/profil/tupoksi') ? 'active' : '' }}">
										<a href="{{ url('dashboard-admin/profil/tupoksi') }}">
											<span class="sub-item">Tupoksi</span>
										</a>
									</li>
									<li class="{{ Request::is('dashboard-admin/profil/susunanorganisasi') ? 'active' : '' }}">
										<a href="{{ url('dashboard-admin/profil/susunanorganisasi') }}">
											<span class="sub-item">Susunan Organisasi</span>
										</a>
									</li>
									<li class="{{ Request::is('dashboard-admin/profil/strukturpemerintahan') ? 'active' : '' }}">
										<a href="{{ url('dashboard-admin/profil/strukturpemerintahan') }}">
											<span class="sub-item">Struktur Pemerintahan</span>
										</a>
									</li>
									<li class="{{ Request::is('dashboard-admin/profil/kampungkb') ? 'active' : '' }}">
										<a href="{{ url('dashboard-admin/profil/kampungkb') }}">
											<span class="sub-item">Kampung KB</span>
										</a>
									</li>
									<li class="{{ Request::is('dashboard-admin/profil/kependudukan') ? 'active' : '' }}">
										<a href="{{ url('dashboard-admin/profil/kependudukan') }}">
											<span class="sub-item">Kependudukan</span>
										</a>
									</li>
								</ul>
							</div>
						</li>
						<li class="nav-item
							{{ request()->is('dashboard-admin/berita') ? 'active submenu' : '' }}
							{{ request()->is('dashboard-admin/berita/create') ? 'active submenu' : '' }}">
							<a data-toggle="collapse" href="#forms">
								<i class="far fa-newspaper"></i>
								<p>Berita</p>
								<span class="caret"></span>
							</a>
							<div class="collapse {{ request()->is('dashboard-admin/berita') ? 'show' : '' }}" id="forms">
								<ul class="nav nav-collapse">
									<li class="{{ Request::is('dashboard-admin/berita') ? 'active' : '' }}">
										<a href="{{ url('dashboard-admin/berita') }}">
											<span class="sub-item">Konten Berita</span>
										</a>
									</li>
								</ul>
							</div>
						</li>
						<li class="nav-item {{ request()->is('dashboard-admin/alamat') ? 'active submenu' : '' }}">
							<a data-toggle="collapse" href="#maps">
								<i class="fas fa-map-marked-alt"></i>
								<p>Alamat</p>
								<span class="caret"></span>
							</a>
							<div class="collapse {{ request()->is('dashboard-admin/alamat') ? 'show' : '' }}" id="maps">
								<ul class="nav nav-collapse">
									<li class="{{ Request::is('dashboard-admin/alamat') ? 'active' : '' }}">
										<a href="{{ url('dashboard-admin/alamat') }}">
											<span class="sub-item">Kelurahan Banaran</span>
										</a>
									</li>
								</ul>
							</div>
						</li>
						{{-- <li class="nav-item">
							<a data-toggle="collapse" href="#charts">
								<i class="far fa-chart-bar"></i>
								<p>Charts</p>
								<span class="caret"></span>
							</a>
							<div class="collapse" id="charts">
								<ul class="nav nav-collapse">
									<li>
										<a href="charts/charts.html">
											<span class="sub-item">Chart Js</span>
										</a>
									</li>
									<li>
										<a href="charts/sparkline.html">
											<span class="sub-item">Sparkline</span>
										</a>
									</li>
								</ul>
							</div>
						</li>
						<li class="nav-item">
							<a href="widgets.html">
								<i class="fas fa-desktop"></i>
								<p>Widgets</p>
								<span class="badge badge-success">4</span>
							</a>
						</li>
						<li class="nav-item">
							<a data-toggle="collapse" href="#submenu">
								<i class="fas fa-bars"></i>
								<p>Menu Levels</p>
								<span class="caret"></span>
							</a>
							<div class="collapse" id="submenu">
								<ul class="nav nav-collapse">
									<li>
										<a data-toggle="collapse" href="#subnav1">
											<span class="sub-item">Level 1</span>
											<span class="caret"></span>
										</a>
										<div class="collapse" id="subnav1">
											<ul class="nav nav-collapse subnav">
												<li>
													<a href="#">
														<span class="sub-item">Level 2</span>
													</a>
												</li>
												<li>
													<a href="#">
														<span class="sub-item">Level 2</span>
													</a>
												</li>
											</ul>
										</div>
									</li>
									<li>
										<a data-toggle="collapse" href="#subnav2">
											<span class="sub-item">Level 1</span>
											<span class="caret"></span>
										</a>
										<div class="collapse" id="subnav2">
											<ul class="nav nav-collapse subnav">
												<li>
													<a href="#">
														<span class="sub-item">Level 2</span>
													</a>
												</li>
											</ul>
										</div>
									</li>
									<li>
										<a href="#">
											<span class="sub-item">Level 1</span>
										</a>
									</li>
								</ul>
							</div>
						</li>
						<li class="mx-4 mt-2">
							<a href="http://themekita.com/atlantis-bootstrap-dashboard.html" class="btn btn-primary btn-block"><span class="btn-label mr-2"> <i class="fa fa-heart"></i> </span>Buy Pro</a> 
						</li> --}}
					</ul>
				</div>
			</div>
		</div>
		<!-- End Sidebar -->

        <!-- Content -->
		<div class="main-panel">
			<div class="content">
				<div class="panel-header bg-primary-gradient">
					<div class="page-inner py-5">
						<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
							<div>
								@yield('tagPage')
								<h5 class="text-white op-7 mb-2">@yield('descPage')</h5>
							</div>
							<div class="ml-md-auto py-2 py-md-0">
								{{-- <a href="#" class="btn btn-white btn-border btn-round mr-2">Manage</a>
								<a href="#" class="btn btn-secondary btn-round">Add Customer</a> --}}
							</div>
						</div>
					</div>
				</div>
		@yield('content')
		<!-- // Content -->
		</div>
		

			<footer class="footer">
                <div class="container-fluid">
                    <nav class="pull-left">
                        <ul class="nav">
                            <li class="nav-item">
                                <a class="nav-link" href="#">
                                    <b>Created alvicky.id&copy;{{ Carbon\Carbon::now()->year }}</b>
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <div class="copyright ml-auto">
                        {{ Carbon\Carbon::now()->year }}, made with <i class="fa fa-heart heart text-danger"></i> by <a href="https://www.themekita.com">ThemeKita</a>
                    </div>				
                </div>
            </footer>
        </div>
        <!-- // Footer -->
	</div>
	<!--   Core JS Files   -->
	<script src="{{ asset('assets/assets/js/core/jquery.3.2.1.min.js')}}"></script>
	<script src="{{ asset('assets/assets/js/core/popper.min.js')}}"></script>
	<script src="{{ asset('assets/assets/js/core/bootstrap.min.js')}}"></script>

	<!-- jQuery UI -->
	<script src="{{ asset('assets/assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js')}}"></script>
	<script src="{{ asset('assets/assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js')}}"></script>

	<!-- jQuery Scrollbar -->
	<script src="{{ asset('assets/assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js')}}"></script>

	<!-- Chart JS -->
	<script src="{{ asset('assets/assets/js/plugin/chart.js/chart.min.js')}}"></script>

	<!-- jQuery Sparkline -->
	<script src="{{ asset('assets/assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js')}}"></script>

	<!-- Chart Circle -->
	<script src="{{ asset('assets/assets/js/plugin/chart-circle/circles.min.js')}}"></script>

	<!-- Datatables -->
	<script src="{{ asset('assets/assets/js/plugin/datatables/datatables.min.js')}}"></script>

	<!-- Bootstrap Notify -->
	<script src="{{ asset('assets/assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js')}}"></script>

	<!-- jQuery Vector Maps -->
	<script src="{{ asset('assets/assets/js/plugin/jqvmap/jquery.vmap.min.js')}}"></script>
	<script src="{{ asset('assets/assets/js/plugin/jqvmap/maps/jquery.vmap.world.js')}}"></script>

	<!-- Sweet Alert -->
	<script src="{{ asset('assets/assets/js/plugin/sweetalert/sweetalert.min.js')}}"></script>

	<!-- Atlantis JS -->
	<script src="{{ asset('assets/assets/js/atlantis.min.js')}}"></script>

	<!-- Atlantis DEMO methods, don't include it in your project! -->
	<script src="{{ asset('assets/assets/js/setting-demo.js')}}"></script>

	<!-- DateTimePicker -->
	<script src="{{ asset('assets/assets/js/datepicker/datepicker.js')}}"></script>

	<!-- TinyMCE JS -->
	<script src="{{ asset('assets/assets/tinymce/tinymce.min.js')}}"></script>
	
	<script>
		$('#basic-datatables').DataTable();
		$('#basic-datatables2').DataTable();
	</script>
    
    <!-- Yield Script -->
    @yield('script')
</body>
</html>