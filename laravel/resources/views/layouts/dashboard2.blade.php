<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
     <!-- Yield Title -->
	<title>@yield('title')</title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <link rel="icon" href="{{ asset('assets/img/logo2.png')}}" type="image/x-icon"/>
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

	<!-- Fonts and icons -->
	<script src="{{ asset('assets/assets/js/plugin/webfont/webfont.min.js')}}"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['{{ asset('assets/assets/css/fonts.min.css')}}']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	<link rel="stylesheet" href="{{ asset('assets/assets/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{ asset('assets/assets/css/atlantis.min.css')}}">

	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link rel="stylesheet" href="{{ asset('assets/assets/css/demo.css')}}">
	
    <!-- Yield Style-->
    @yield('style')
</head>
<body>
	<div class="wrapper">
		<div class="main-header">
			<!-- Logo Header -->
			<div class="logo-header" data-background-color="blue">
				
				<a href="{{ url('/dashboard-petugas') }}" class="logo">
					<img src="{{ asset('assets/img/logo2.png')}}" alt="navbar brand" class="navbar-brand" height="50px" width="auto">
				</a>
				<button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon">
						<i class="icon-menu"></i>
					</span>
				</button>
				<button class="topbar-toggler more"><i class="icon-options-vertical"></i></button>
				<div class="nav-toggle">
					<button class="btn btn-toggle toggle-sidebar">
						<i class="icon-menu"></i>
					</button>
				</div>
			</div>
			<!-- End Logo Header -->

			<!-- Navbar Header -->
			<nav class="navbar navbar-header navbar-expand-lg" data-background-color="blue2">
				
				<div class="container-fluid">
					<ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
						<li class="nav-item dropdown hidden-caret">
							<a class="nav-link dropdown-toggle" href="#" id="messageDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-paste"></i>
								<span class="notification">{{ $pengajuansurat->count() }}</span>
							</a>
							<ul class="dropdown-menu messages-notif-box animated fadeIn" aria-labelledby="messageDropdown">
								<li>
									<div class="dropdown-title d-flex justify-content-between align-items-center">
										Pengajuan Surat 									
									</div>
								</li>
								<li>
									<div class="message-notif-scroll scrollbar-outer">
										<div class="notif-center">
											@foreach ($pengajuansurat as $row)
											<a href="#">
												<div class="notif-icon notif-primary"> <i class="fa fa-paste"></i> </div>
												<div class="notif-content">
													<span class="block">
														@if(strlen($row->Keperluan['keperluan'])>25)                                     
															{{substr(strip_tags($row->Keperluan['keperluan']),0,25)}} ...
														@else
															{{$row->Keperluan['keperluan']}}
														@endif
													</span>
													<span class="time">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($row->created_at))->diffForHumans() }}</span> 
												</div>
											</a>
											@endforeach
										</div>
									</div>
								</li>
								<li>
									<a class="see-all" href="{{ url('dashboard-petugas/layanan/masuk') }}">See all messages<i class="fa fa-angle-right"></i> </a>
								</li>
							</ul>
						</li>
						<li class="nav-item dropdown hidden-caret">
							<a class="nav-link dropdown-toggle" href="#" id="messageDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-tag"></i>
								<span class="notification">{{ $pengajuanumkm->count() }}</span>
							</a>
							<ul class="dropdown-menu messages-notif-box animated fadeIn" aria-labelledby="messageDropdown">
								<li>
									<div class="dropdown-title d-flex justify-content-between align-items-center">
										Pengajuan UMKM 									
									</div>
								</li>
								<li>
									<div class="message-notif-scroll scrollbar-outer">
										
										<div class="notif-center">
											@foreach ($pengajuanumkm as $row)
											<a href="#">
												<div class="notif-icon notif-primary"> <i class="fa fa-tag"></i> </div>
												<div class="notif-content">
													<span class="block">
														@if(strlen($row['merk_usaha'])>25)                                     
															{{substr(strip_tags($row['merk_usaha']),0,25)}} ...
														@else
															{{$row['merk_usaha']}}
														@endif
													</span>
													<span class="time">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($row->created_at))->diffForHumans() }}</span> 
												</div>
											</a>
											@endforeach
										</div>
									</div>
								</li>
								<li>
									<a class="see-all" href="{{ url('dashboard-petugas/pendaftaran/masuk') }}">See all messages<i class="fa fa-angle-right"></i> </a>
								</li>
							</ul>
						</li>
						<li class="nav-item dropdown hidden-caret">
							<a class="nav-link dropdown-toggle" href="#" id="notifDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fas fa-user-cog"></i>
								<span class="notification">{{ $unverified->count() }}</span>
							</a>
							<ul class="dropdown-menu notif-box animated fadeIn" aria-labelledby="notifDropdown">
								<li>
									<div class="dropdown-title">You have {{ $unverified->count() }} new user registered</div>
								</li>
								<li>
									<div class="notif-scroll scrollbar-outer">
										<div class="notif-center">
											@foreach ($unverified as $row)
											<a href="#">
												<div class="notif-icon notif-primary"> <i class="fa fa-user-plus"></i> </div>
												<div class="notif-content">
													<span class="block">
														@if(strlen($row['name'])>25)                                     
															{{substr(strip_tags($row['name']),0,25)}} ...
														@else
															{{$row['name']}}
														@endif
													</span>
													<span class="time">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($row->created_at))->diffForHumans() }}</span> 
												</div>
											</a>
											@endforeach
										</div>
									</div>
								</li>
								<li>
									<a class="see-all" href="{{ url('dashboard-petugas/manage-users') }}">See all notifications<i class="fa fa-angle-right"></i> </a>
								</li>
							</ul>
						</li>
						<li class="nav-item dropdown hidden-caret">
							<a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
								<div class="avatar-sm">
									<img src="{{ asset('assets/assets/img/profile2.png')}}" alt="..." class="avatar-img rounded-circle">
								</div>
							</a>
							<ul class="dropdown-menu dropdown-user animated fadeIn">
								<div class="dropdown-user-scroll scrollbar-outer">
									<li>
										<div class="user-box">
											<div class="avatar-lg"><img src="{{ asset('assets/assets/img/profile2.png')}}" alt="image profile" class="avatar-img rounded"></div>
											<div class="u-text">
												<h4>Petugas</h4>
												<p class="text-muted">{{ Auth::user()->email }}</p>
											</div>
										</div>
									</li>
									<li>
										{{-- LOGOUT LARAVEL --}}
                                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><span class="dropdown-item fa fa-sign-out fa-fw"></span>{{ __(' Logout') }}</a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
									</li>
								</div>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
			<!-- End Navbar -->
		</div>

		<!-- Sidebar -->
		<div class="sidebar sidebar-style-2">			
			<div class="sidebar-wrapper scrollbar scrollbar-inner">
				<div class="sidebar-content">
					<div class="user">
						<div class="avatar-sm float-left mr-2">
							<img src="{{ asset('assets/assets/img/profile2.png')}}" alt="..." class="avatar-img rounded-circle">
						</div>
						<div class="info">
							<a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
								<span>
									<span class="user-level"><h3>Petugas</h3></span>
								</span>
							</a>
							<div class="clearfix"></div>
						</div>
					</div>
					<ul class="nav nav-primary">
						<li class="nav-item {{ (request()->is('dashboard-petugas')) ? 'active':''  }}">
							<a href="{{ url('dashboard-petugas') }}">
								<i class="fas fa-home"></i>
									<p>Dashboard</p>
								
								</a>
							</a>
						</li>
						<li class="nav-section">
							<span class="sidebar-mini-icon">
								<i class="fa fa-ellipsis-h"></i>
							</span>
							<h4 class="text-section">Pelayanan</h4>
						</li>
						<li class="nav-item 
							{{ request()->is('dashboard-petugas/manage-users') ? 'active submenu' : '' }}
							{{ request()->is('dashboard-petugas/manage-users/verified') ? 'active submenu' : '' }}
							">
							<a data-toggle="collapse" href="#Kependudukan">
								<i class="fas fa-user-cog"></i>
								<p>Kependudukan</p>
								<span class="caret"></span>
							</a>
							<div class="collapse
								{{ request()->is('dashboard-petugas/manage-users') ? 'show' : '' }}
								{{ request()->is('dashboard-petugas/manage-users/verified') ? 'show' : '' }}
								" id="Kependudukan">
								<ul class="nav nav-collapse">
									<li class="{{ Request::is('dashboard-petugas/manage-users') ? 'active' : '' }}">
										<a href="{{ url('/dashboard-petugas/manage-users') }}">
											<span class="sub-item">Registrasi Baru</span>
										</a>
									</li>
									<li class="{{ Request::is('dashboard-petugas/manage-users/verified') ? 'active' : '' }}">
										<a href="{{ url('/dashboard-petugas/manage-users/verified') }}">
											<span class="sub-item">Penduduk Terdaftar</span>
										</a>
									</li>
								</ul>
							</div>
						</li>
						<li class="nav-item
							{{ request()->is('dashboard-petugas/layanan/masuk') ? 'active submenu' : '' }}
							{{ request()->is('dashboard-petugas/layanan/verify') ? 'active submenu' : '' }}">
							<a data-toggle="collapse" href="#layanan">
								<i class="fas fa-paste"></i>
								<p>Layanan Surat</p>
								<span class="caret"></span>
							</a>
							<div class="collapse
								{{ request()->is('dashboard-petugas/layanan/masuk') ? 'show' : '' }}
								{{ request()->is('dashboard-petugas/layanan/verify') ? 'show' : '' }}
								" id="layanan">
								<ul class="nav nav-collapse">
									<li class="{{ Request::is('dashboard-petugas/layanan/masuk') ? 'active' : '' }}">
										<a href="{{ url('/dashboard-petugas/layanan/masuk') }}">
											<span class="sub-item">Pengajuan Masuk</span>
										</a>
									</li>
									<li class="{{ Request::is('dashboard-petugas/layanan/verify') ? 'active' : '' }}">
										<a href="{{ url('/dashboard-petugas/layanan/verify') }}">
											<span class="sub-item">Pengajuan Diverifikasi</span>
										</a>
									</li>
								</ul>
							</div>
						</li>
						<li class="nav-item
							{{ request()->is('dashboard-petugas/pendaftaran/masuk') ? 'active submenu' : '' }}
							{{ request()->is('dashboard-petugas/pendaftaran/verify') ? 'active submenu' : '' }}">
							<a data-toggle="collapse" href="#pendaftaran-umkm">
								<i class="fas fa-tag"></i>
								<p>Pendaftaran UMKM</p>
								<span class="caret"></span>
							</a>
							<div class="collapse
								{{ request()->is('dashboard-petugas/pendaftaran/masuk') ? 'show' : '' }}
								{{ request()->is('dashboard-petugas/pendaftaran/verify') ? 'show' : '' }}
								" id="pendaftaran-umkm">
								<ul class="nav nav-collapse">
									<li class="{{ Request::is('dashboard-petugas/pendaftaran/masuk') ? 'active' : '' }}">
										<a href="{{ url('/dashboard-petugas/pendaftaran/masuk') }}">
											<span class="sub-item">Pendaftaran Masuk</span>
										</a>
									</li>
									<li class="{{ Request::is('dashboard-petugas/pendaftaran/verify') ? 'active' : '' }}">
										<a href="{{ url('/dashboard-petugas/pendaftaran/verify') }}">
											<span class="sub-item">Pendaftaran Diverifikasi</span>
										</a>
									</li>
								</ul>
							</div>
						</li>
						<li class="nav-item
								{{ request()->is('dashboard-petugas/laporan/layanan-surat') ? 'active submenu' : '' }}
								{{ request()->is('dashboard-petugas/laporan/umkm') ? 'active submenu' : '' }}">
							<a data-toggle="collapse" href="#laporan">
								<i class="far fa-folder-open"></i>
								<p>Laporan</p>
								<span class="caret"></span>
							</a>
							<div class="collapse
									{{ request()->is('dashboard-petugas/laporan/layanan-surat') ? 'show' : '' }}
									{{ request()->is('dashboard-petugas/laporan/umkm') ? 'show' : '' }}" id="laporan">
								<ul class="nav nav-collapse">
									<li class="{{ Request::is('dashboard-petugas/laporan/layanan-surat') ? 'active' : '' }}">
										<a href="{{ url('/dashboard-petugas/laporan/layanan-surat') }}">
											<span class="sub-item">Layanan Surat</span>
										</a>
									</li>
									<li class="{{ Request::is('dashboard-petugas/laporan/umkm') ? 'active' : '' }}">
										<a href="{{ url('/dashboard-petugas/laporan/umkm') }}">
											<span class="sub-item">UMKM</span>
										</a>
									</li>
								</ul>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- End Sidebar -->

        <!-- Content -->
		<div class="main-panel">
			<div class="content">
				<div class="panel-header bg-primary-gradient">
					<div class="page-inner py-5">
						<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
							<div>
                                 <!-- Yield Tagpage -->
								@yield('tagPage')
								<h5 class="text-white op-7 mb-2">@yield('descPage')</h5>
							</div>
							<div class="ml-md-auto py-2 py-md-0">
								{{-- <a href="#" class="btn btn-white btn-border btn-round mr-2">Manage</a>
								<a href="#" class="btn btn-secondary btn-round">Add Customer</a> --}}
							</div>
						</div>
					</div>
                </div>
                @yield('content')
			</div>
			<footer class="footer">
                <div class="container-fluid">
                    <nav class="pull-left">
                        <ul class="nav">
                            <li class="nav-item">
                                <a class="nav-link" href="#">
                                    <b>Created alvicky.id&copy;{{ Carbon\Carbon::now()->year }}</b>
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <div class="copyright ml-auto">
                        {{ Carbon\Carbon::now()->year }}, made with <i class="fa fa-heart heart text-danger"></i> by <a href="https://www.themekita.com">ThemeKita</a>
                    </div>				
                </div>
            </footer>
        </div>
        <!-- // Content -->
		
		
	</div>
	<!--   Core JS Files   -->
	<script src="{{ asset('assets/assets/js/core/jquery.3.2.1.min.js')}}"></script>
	<script src="{{ asset('assets/assets/js/core/popper.min.js')}}"></script>
	<script src="{{ asset('assets/assets/js/core/bootstrap.min.js')}}"></script>

	<!-- jQuery UI -->
	<script src="{{ asset('assets/assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js')}}"></script>
	<script src="{{ asset('assets/assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js')}}"></script>

	<!-- jQuery Scrollbar -->
	<script src="{{ asset('assets/assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js')}}"></script>

	<!-- Chart JS -->
	<script src="{{ asset('assets/assets/js/plugin/chart.js/chart.min.js')}}"></script>

	<!-- jQuery Sparkline -->
	<script src="{{ asset('assets/assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js')}}"></script>

	<!-- Chart Circle -->
	<script src="{{ asset('assets/assets/js/plugin/chart-circle/circles.min.js')}}"></script>

	<!-- Datatables -->
	<script src="{{ asset('assets/assets/js/plugin/datatables/datatables.min.js')}}"></script>

	<!-- Bootstrap Notify -->
	<script src="{{ asset('assets/assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js')}}"></script>

	<!-- jQuery Vector Maps -->
	<script src="{{ asset('assets/assets/js/plugin/jqvmap/jquery.vmap.min.js')}}"></script>
	<script src="{{ asset('assets/assets/js/plugin/jqvmap/maps/jquery.vmap.world.js')}}"></script>

	<!-- Sweet Alert -->
	<script src="{{ asset('assets/assets/js/plugin/sweetalert/sweetalert.min.js')}}"></script>

	<!-- Atlantis JS -->
	<script src="{{ asset('assets/assets/js/atlantis.min.js')}}"></script>

	<!-- Atlantis DEMO methods, don't include it in your project! -->
	<script src="{{ asset('assets/assets/js/setting-demo.js')}}"></script>

	<!-- Moment JS -->
    <script src="{{ asset('assets/assets/js/plugin/moment/moment.min.js')}}"></script>

	<!-- DateTimePicker -->
	<script src="{{ asset('assets/assets/js/plugin/datepicker/bootstrap-datetimepicker.min.js')}}"></script>

	<!-- TinyMCE JS -->
	<script src="{{ asset('assets/assets/tinymce/tinymce.min.js')}}"></script>
    
    <!-- Yield Script -->
	@yield('script')
	
	<script>
		$('#basic-datatables').DataTable();
	</script>

	<script>
		Circles.create({
			id:'circles-1',
			radius:45,
			value:60,
			maxValue:100,
			width:7,
			text: 5,
			colors:['#f1f1f1', '#FF9E27'],
			duration:400,
			wrpClass:'circles-wrp',
			textClass:'circles-text',
			styleWrapper:true,
			styleText:true
		})

		Circles.create({
			id:'circles-2',
			radius:45,
			value:70,
			maxValue:100,
			width:7,
			text: 36,
			colors:['#f1f1f1', '#2BB930'],
			duration:400,
			wrpClass:'circles-wrp',
			textClass:'circles-text',
			styleWrapper:true,
			styleText:true
		})

		Circles.create({
			id:'circles-3',
			radius:45,
			value:40,
			maxValue:100,
			width:7,
			text: 12,
			colors:['#f1f1f1', '#F25961'],
			duration:400,
			wrpClass:'circles-wrp',
			textClass:'circles-text',
			styleWrapper:true,
			styleText:true
		})

		var totalIncomeChart = document.getElementById('totalIncomeChart').getContext('2d');

		var mytotalIncomeChart = new Chart(totalIncomeChart, {
			type: 'bar',
			data: {
				labels: ["S", "M", "T", "W", "T", "F", "S", "S", "M", "T"],
				datasets : [{
					label: "Total Income",
					backgroundColor: '#ff9e27',
					borderColor: 'rgb(23, 125, 255)',
					data: [6, 4, 9, 5, 4, 6, 4, 3, 8, 10],
				}],
			},
			options: {
				responsive: true,
				maintainAspectRatio: false,
				legend: {
					display: false,
				},
				scales: {
					yAxes: [{
						ticks: {
							display: false //this will remove only the label
						},
						gridLines : {
							drawBorder: false,
							display : false
						}
					}],
					xAxes : [ {
						gridLines : {
							drawBorder: false,
							display : false
						}
					}]
				},
			}
		});

		$('#lineChart').sparkline([105,103,123,100,95,105,115], {
			type: 'line',
			height: '70',
			width: '100%',
			lineWidth: '2',
			lineColor: '#ffa534',
			fillColor: 'rgba(255, 165, 52, .14)'
		});
	</script>
</body>
</html>