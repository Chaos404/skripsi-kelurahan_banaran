<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('assets/img/logo2.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/img/logo2.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('assets/img/logo2.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/img/logo2.png')}}">

     <!-- Bootstrap CSS -->
     <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
     <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
     <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}">
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
 
     <!-- google fonts -->
     <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800%7CPoppins:300i,400,700,400i,500%7CDosis:300" rel="stylesheet">

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ asset('assets/js/vendor/jquery-3.4.1.min.js')}}"></script>
    <script src="{{ asset('assets/js/vendor/popper.min.js')}}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('assets/js/jquery.nicescroll.min.js')}}"></script>
    
     <!-- Custom -->
     <script src="{{ asset('assets/js/jquery.nicescroll.min.js')}}"></script>
     <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
     <link rel="stylesheet" href="{{ asset('assets/css/animate.css')}}" />
     <link rel="stylesheet" href="{{ asset('assets/css/carousel-custom.css')}}">
     <link rel="stylesheet" href="{{ asset('assets/css/blog-custom.css')}}">
     <link rel="stylesheet" href="{{ asset('assets/css/hover-min.css')}}">

     @yield('style')

    {{-- <title>Document</title> --}}
    @yield('title')

</head>
<body class="d-flex flex-column h-100">

    <!-- Page title --->
    @yield('pagetitle')
	<!-- // Page title --->

    <!-- Content -->
    @yield('content')
        <!-- Back to top button -->
        <a href="#" id="scroll" style="display: none;"><span></span></a>
    <!-- End of Content -->
    
    <!--  <script  src="assets/js/numscroller-1.0.js"></script> -->
    <script src="{{ asset('assets/js/sticky-sidebar.js')}}"></script>
    <script src="{{ asset('assets/js/YouTubePopUp.jquery.js')}}"></script>
    <script src="{{ asset('assets/js/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('assets/js/imagesloaded.min.js')}}"></script>
    <script src="{{ asset('assets/js/wow.min.js')}}"></script>
    <script src="{{ asset('assets/js/custom.js')}}"></script>

    @yield('script')

    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>
    <script>
        // On Top
        $(document).ready(function(){ 
            $(window).scroll(function(){ 
                if ($(this).scrollTop() > 100) { 
                    $('#scroll').fadeIn(); 
                } else { 
                    $('#scroll').fadeOut(); 
                } 
            }); 
            $('#scroll').click(function(){ 
                $("html, body").animate({ scrollTop: 0 }, 600); 
                return false; 
            }); 
        });
        
        // Active Nav
        $(document).ready(function () {
            var url = window.location;
            $('ul.navbar-nav li a[href="' + url + '"]').parent().addClass('active');
            $('ul.navbar-nav li a').filter(function () {
                return this.href == url;
            }).parent().addClass('active');
        });
        
        
        // Nicescroll
        $("body").niceScroll({
            cursorcolor: "rgb(25, 208, 240)",
            cursorborder: "1px solid white",
            smoothscroll: true,
            zindex: "1"
        });

        // Dropdown Hover
        const $dropdown = $(".dropdown");
            const $dropdownToggle = $(".dropdown-toggle");
            const $dropdownMenu = $(".dropdown-menu");
            const showClass = "show";

            $(window).on("load resize", function() {
            if (this.matchMedia("(min-width: 768px)").matches) {
                $dropdown.hover(
                function() {
                    const $this = $(this);
                    $this.addClass(showClass);
                    $this.find($dropdownToggle).attr("aria-expanded", "true");
                    $this.find($dropdownMenu).addClass(showClass);
                },
                function() {
                    const $this = $(this);
                    $this.removeClass(showClass);
                    $this.find($dropdownToggle).attr("aria-expanded", "false");
                    $this.find($dropdownMenu).removeClass(showClass);
                }
                );
            } else {
                $dropdown.off("mouseenter mouseleave");
            }
        });
    </script>


    
</body>
</html>