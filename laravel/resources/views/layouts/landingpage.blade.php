<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('assets/img/logo2.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/img/logo2.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('assets/img/logo2.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/img/logo2.png')}}">

     <!-- Bootstrap CSS -->
     <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
     <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
     <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}">
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
 
     <!-- google fonts -->
     <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800%7CPoppins:300i,400,700,400i,500%7CDosis:300" rel="stylesheet">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ asset('assets/js/vendor/jquery-3.4.1.min.js')}}"></script>
    <script src="{{ asset('assets/js/vendor/popper.min.js')}}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('assets/js/jquery.nicescroll.min.js')}}"></script>
    
     <!-- Custom -->
     <link rel="stylesheet" href="{{ asset('assets/css/animate.css')}}" />
     <link rel="stylesheet" href="{{ asset('assets/css/carousel-custom.css')}}">
     {{-- <link rel="stylesheet" href="{{ asset('assets/css/blog-custom.css')}}"> --}}
     <link rel="stylesheet" href="{{ asset('assets/css/hover-min.css')}}">

     @yield('style')

    {{-- <title>Document</title> --}}
    @yield('title')

</head>
<body class="d-flex flex-column h-100">
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-expand-md navbar-banaran bg-navbar-banaran" id="mainNav">
        <div class="container">
            <a class="navbar-brand" href="/">
                <img src="{{ asset('assets/img/logo2.png')}}" width="150px" height="auto" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar"
                aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbar">
                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/') }}">HOME <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item dropdown 
                        {{ (request()->is('profil/visimisi')) ? 'active':''  }}
                        {{ (request()->is('profil/tupoksi')) ? 'active':''  }}
                        {{ (request()->is('profil/strukturpemerintahan')) ? 'active':''  }}
                        {{ (request()->is('profil/kampungkb')) ? 'active':''  }}
                        {{ (request()->is('profil/geografisdankependudukan')) ? 'active':''  }}">
                        <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                            PROFIL
                        </a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item {{ (request()->is('profil/visimisi')) ? 'active':''  }}" href="{{ url('/profil/visimisi') }}">Visi & Misi</a>
                            <a class="dropdown-item {{ (request()->is('profil/tupoksi')) ? 'active':''  }}" href="{{ url('/profil/tupoksi') }}">Tupoksi</a>
                            <a class="dropdown-item {{ (request()->is('profil/strukturpemerintahan')) ? 'active':''  }}" href="{{ url('/profil/strukturpemerintahan') }}">Struktur Pemerintahan</a>
                            @foreach ($kelurahan as $row)
                                <a class="dropdown-item {{ (request()->is('profil/kampungkb')) ? 'active':''  }}" href="{{ $row->link_kampungkb }}">Kampung KB</a>
                            @endforeach
                            <a class="dropdown-item {{ (request()->is('profil/geografisdankependudukan')) ? 'active':''  }}" href="{{ url('/profil/geografisdankependudukan') }}">Geografis dan Kependududkan</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/berita') }}">BERITA</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/umkm') }}">UMKM</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/pelayanan') }}">PELAYANAN</a>
                    </li>
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/login') }}"><span id="hapus" style="color: #fff;">|&nbsp;&nbsp;&nbsp; </span><i class="fa fa-user"></i>&nbsp; LOGIN</a>
                        </li>
                    @elseif (Auth::user()->role == "petugas")
                        <script>
                            document.location.href='/dashboard-petugas';
                        </script>
                        {{-- <li class="nav-item dropdown 
                            {{ (request()->is('dashboard')) ? 'active':''}}">
                            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                                <span id="hapus" style="color: #fff;">|&nbsp;&nbsp;&nbsp; </span><i class="fa fa-user"></i>&nbsp; {{ Auth::user()->name }}
                            </a>
                            <div class="dropdown-menu" style="margin-left:50px;">
                                <!-- Logout -->
                                <a class="dropdown-item" style="font-size:14px" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();"> <i class="fa fa-sign-out"></i>
                                    {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li> --}}
                    @elseif (Auth::user()->role == "admin")
                        <script>
                            document.location.href='/dashboard-admin';
                        </script>
                        {{-- <li class="nav-item dropdown 
                            {{ (request()->is('dashboard')) ? 'active':''}}">
                            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                                <span id="hapus" style="color: #fff;">|&nbsp;&nbsp;&nbsp; </span><i class="fa fa-user"></i>&nbsp; {{ Auth::user()->name }}
                            </a>
                            <div class="dropdown-menu" style="margin-left:50px;">
                                <!-- Logout -->
                                <a class="dropdown-item" style="font-size:14px" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();"> <i class="fa fa-sign-out"></i>
                                    {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li> --}}
                    @else
                        <li class="nav-item dropdown 
                            {{ (request()->is('dashboard')) ? 'active':''}}">
                            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                                <span id="hapus" style="color: #fff;">|&nbsp;&nbsp;&nbsp; </span><i class="fa fa-user"></i>&nbsp; {{ Auth::user()->name }}
                            </a>
                            <div class="dropdown-menu" style="margin-left:50px;">
                                <a class="dropdown-item {{ (request()->is('dashboard')) ? 'active':''  }}" href="{{ url('dashboard/' . Auth::user()->id) }}">Profil</a>
                                <a class="dropdown-item {{ (request()->is('mendaftar/umkm')) ? 'active':''  }}" href="{{ url('/mendaftar/umkm') }}">Mendaftar UMKM</a>
                                <!-- Logout -->
                                <a class="dropdown-item" style="font-size:14px" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();"> <i class="fa fa-sign-out"></i>
                                    {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>
    <!-- End of Navbar -->

    <!-- Page title --->
    @yield('pagetitle')
	<!-- // Page title --->

    <!-- Content -->
    @yield('content')
        <!-- Back to top button -->
        <a href="#" id="scroll" style="display: none;"><span></span></a>
    <!-- End of Content -->

    <!-- Footer -->
    <footer class="footer mt-auto page-footer font-small blue pt-4">
        <div class="container text-center text-md-left">
            <!-- Grid row -->
            <div class="row justify-content-md-center">
                <!-- Grid column -->
                <div class="my-auto col-md-2">
                    <!-- Content -->
                    <a class="navbar-brand" href="#">
                        <img src="{{ asset('assets/img/logo2.png')}}" width="auto" height="70px" alt="">
                    </a>
                </div>
                <!-- Grid column -->
                <hr class="clearfix w-100 d-md-none pb-0">
                <!-- Grid column -->
                <div class="col-md-5 mb-md-0 mb-0">
                    <ul class="list-unstyled">
                        <li class="nav-item">
                            <a class="link-footer" style="color:white"><b> KONTAK KAMI</b> </a>
                        </li>
                        @foreach ($kelurahan as $row)
                        <li>
                            <a href="https://goo.gl/maps/JTrcwDos3NmeW5wF6" target="_blank" class="link-footer" style="color:white"><i class="fa fa-home"></i> {{ $row->alamat }}</a>
                        </li>
                        <li>
                            <a href="tel:{{ $row->nohp }}" class="link-footer" style="color:white"><i class="fa fa-phone"></i> {{ $row->nohp }} </a>
                        </li>
                        <li>
                            <a href="mailto:{{ $row->email }}" class="link-footer" style="color:white"><i class="fa fa-envelope"></i> {{ $row->email }} </a>
                        </li>
                        @endforeach
                    </ul>
                    <!--Instagram-->
                    <a class="ins-ic">
                        <i class="link-footer fab fa-instagram fa-lg white-text mr-md-3 mr-3 fa-1x"> </i>
                    </a>
                    <!-- Twitter -->
                    <a class="tw-ic">
                        <i class="link-footer fab fa-twitter fa-lg white-text mr-md-3 mr-3 fa-1x"> </i>
                    </a>
                    <!-- Facebook -->
                    <a class="fb-ic">
                        <i class="link-footer fab fa-facebook-f fa-lg white-text mr-md-3 mr-3 fa-1x"> </i>
                    </a>
                    <!-- Browser +-->
                    <a class="gplus-ic">
                        <i class="link-footer fa fa-globe fa-lg white-text mr-md-2 mr-3 fa-1x"> </i>
                    </a>
                </div>
                <hr class="clearfix w-100 d-md-none pb-0">
                <!-- Grid column -->
                <div class="col-md-2 mb-md-0 mb-0 ">
                    <ul class="list-unstyled">
                        <li class="nav-item">
                            <a class="link-footer" style="color:white"><b> NAVIGASI SITUS</b> </a>
                        </li>
                        <li>
                            <a class="link-footer" href="{{ '/' }}"> Home </a>
                        </li>
                        <li>
                            <a class="link-footer" href="{{ '/profil/visimisi' }}"> Visi & Misi </a>
                        </li>
                        <li>
                            <a class="link-footer" href="{{ '/profil/tupoksi' }}"> Tupoksi </a>
                        </li>
                        <li>
                            <a class="link-footer" href="{{ '/profil/strukturpemerintahan' }}"> Struktur Pemerintah </a>
                        </li>
                        <li>
                            @foreach ($kelurahan as $row)
                            <a class="link-footer" href="{{ $row->link_kampungkb }}"> Kampung KB </a>
                            @endforeach
                        </li>
                    </ul>
                </div>
                <div class="col-md-3 mb-md-0 mb-0 ">
                    <ul class="list-unstyled">
                        <li class="nav-item">
                            <a class="link-footer" style="color:#0093dd">X</a>
                        </li>
                        <li>
                            <a class="link-footer" href="{{ '/profil/geografisdankependudukan' }}"> Geografis dan Kependududkan </a>
                        </li>

                        <li>
                            <a class="link-footer" href="{{ '/berita' }}"> Berita </a>
                        </li>
                        <li>
                            <a class="link-footer" href="{{ '/umkm' }}"> UMKM </a>
                        </li>
                        <li>
                            <a class="link-footer" href="{{ '/pelayanan' }}"> Pelayanan </a>
                        </li>
                    </ul>
                </div>
                {{-- <hr class="clearfix w-100 d-md-none pb-0">
                <!-- Grid column -->
                <div class="my-auto col-md-3 text-md-center">
                    <!--Instagram-->
                    <a class="ins-ic" href="#">
                        <i class="link-footer fab fa-instagram fa-lg white-text mr-md-3 mr-3 fa-1x"> </i>
                    </a>
                    <!-- Twitter -->
                    <a class="tw-ic" href="#">
                        <i class="link-footer fab fa-twitter fa-lg white-text mr-md-3 mr-3 fa-1x"> </i>
                    </a>
                    <!-- Facebook -->
                    <a class="fb-ic" href="#">
                        <i class="link-footer fab fa-facebook-f fa-lg white-text mr-md-3 mr-3 fa-1x"> </i>
                    </a>
                    <!-- Browser +-->
                    <a class="gplus-ic" href="#">
                        <i class="link-footer fa fa-globe fa-lg white-text mr-md-2 mr-3 fa-1x"> </i>
                    </a>
                </div> --}}
            </div>
            <!-- Grid column -->
        </div>
    </footer>
    
    <script src="{{ asset('assets/js/sticky-sidebar.js')}}"></script>
    <script src="{{ asset('assets/js/YouTubePopUp.jquery.js')}}"></script>
    <script src="{{ asset('assets/js/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('assets/js/imagesloaded.min.js')}}"></script>
    <script src="{{ asset('assets/js/wow.min.js')}}"></script>
    <script src="{{ asset('assets/js/custom.js')}}"></script>

    <!-- TinyMCE JS -->
    <script src="{{ asset('assets/assets/tinymce/tinymce.min.js')}}"></script>

    @yield('script')

    <script>
        // On Top
        $(document).ready(function(){ 
            $(window).scroll(function(){ 
                if ($(this).scrollTop() > 100) { 
                    $('#scroll').fadeIn(); 
                } else { 
                    $('#scroll').fadeOut(); 
                } 
            }); 
            $('#scroll').click(function(){ 
                $("html, body").animate({ scrollTop: 0 }, 600); 
                return false; 
            }); 
        });
        
        // Active Nav
        $(document).ready(function () {
            var url = window.location;
            $('ul.navbar-nav li a[href="' + url + '"]').parent().addClass('active');
            $('ul.navbar-nav li a').filter(function () {
                return this.href == url;
            }).parent().addClass('active');
        });
        

        // Dropdown Hover
        const $dropdown = $(".dropdown");
            const $dropdownToggle = $(".dropdown-toggle");
            const $dropdownMenu = $(".dropdown-menu");
            const showClass = "show";

            $(window).on("load resize", function() {
            if (this.matchMedia("(min-width: 768px)").matches) {
                $dropdown.hover(
                function() {
                    const $this = $(this);
                    $this.addClass(showClass);
                    $this.find($dropdownToggle).attr("aria-expanded", "true");
                    $this.find($dropdownMenu).addClass(showClass);
                },
                function() {
                    const $this = $(this);
                    $this.removeClass(showClass);
                    $this.find($dropdownToggle).attr("aria-expanded", "false");
                    $this.find($dropdownMenu).removeClass(showClass);
                }
                );
            } else {
                $dropdown.off("mouseenter mouseleave");
            }
        });
    </script>
</body>
</html>