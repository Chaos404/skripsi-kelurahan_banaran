<!DOCTYPE html>
<html>
<head>
<title>Kelurahan Banaran</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        
<style type="text/css">
    body{
        font-family: "Times New Roman";
    }
   .left    { text-align: left;}
   .right   { text-align: right;}
   .center  { text-align: center;}
   .justify { text-align: justify;}

   .jaraknya {
       height: 50px;
   }   
    
    #cap {
      position: fixed;
      z-index: 20;
      padding-left: 500px;
      padding-top: 800px;
    }

    #ttd {
      position: fixed;
      z-index: 20;
      padding-left: 550px;
      padding-top: 800px;
    }
</style>
</head>
<body>
    <div class="row">
        <div class="my-auto col-lg-2">
        </div>
        <div class="my-auto col-lg-8" align="center">
            <h5 style="font-weight: bold;">PEMERINTAH DESA BANARAN</h5>
            <h5>UMKM <b><span style="color: red">{{ $umkmsend->merk_usaha }}</span> yang anda daftarkan berhasil <b><span style="color: green">terverifikasi</span></b>.</h5>   
        </div>
        <div class="my-auto col-lg-2" align="right">
        </div>
    </div>
</body>
</html>