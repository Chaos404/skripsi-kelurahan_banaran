<!DOCTYPE html>
<html lang="en">
<head>
  <title>Keterangan Lahir</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<style type="text/css">
    body{
        /* font-family: "Times New Roman"; */
        /* font-size: 9px; */
    }
   .left    { text-align: left;}
   .right   { text-align: right;}
   .center  { text-align: center;}
   .justify { text-align: justify;}

   .jaraknya {
       height: 50px;
   }   

    /* table */
    .custom { 
        line-height: 17px; 
    }

    .bold {
        font-weight: bold;
    }

    /* Typho */
    h6 {
        line-height: 10px;
    }

    p   {
        line-height: 1px;
    }

    .garis {
        display: block;
        margin-top: 0.2em;
        margin-bottom: 0.2em;
        margin-left: auto;
        margin-right: auto;
        border-style: inset;
        border-width: 1px;
    }

    .vl {
        border-left: 2px dotted black;
        height: 100hv;
        position: absolute;
        left: 50%;
        margin-left: 50px;
        top: 0;
    }

    .kapital {
        text-transform: uppercase;
    }

</style>
</head>
<body>
    <table cellpadding="0" cellspacing="0" style="font-size: 11px;width:100%;border-collapse: collapse;  border-spacing: 0;line-height:12px">
        <tr>
            <td style="width:22.8%;">Pemerintah Desa/Kelurahan</td>
            <td style="width:1%;">:</td>
            <td style="width:18%;" >BANARAN</td>
            <td style="width: 40%"></td>
            <td rowspan="2" align="center" style="padding-left:30px;padding-top:0px;">
                <input class="bold" style="border: 1px solid rgb(0,0,0);width: 80px;" type="text" name="" id="" value="KODE. F-2.01">
            </td>
        </tr>
        <tr>
            <td style="width:21%;">Kecamatan</td>
            <td style="width:1%;">:</td>
            <td >KALIJAMBE</td>
            <td style="width: 30%"></td>
        </tr>
        <tr>
            <td style="width:21%;">Kabupaten/Kota</td>
            <td style="width:1%;">:</td>
            <td >SRAGEN</td>
            <td style="width: 30%"></td>
            <td>
                <span style="font-size: 8px;text-align:right">Lembar 1 : UPTD/Instansi Pelaksana</span>
            </td>
        </tr>
        <tr>
            <td style="width:21%;">Kode Wilayah</td>
            <td style="width:1%;">:</td>
            <td >3314</td>
            <td style="width: 30%"></td>
            <td>
                <span style="font-size: 8px;text-align:right">Lembar 2 : Untuk yang bersangkutan</span>
                <span style="font-size: 8px;text-align:right">Lembar 3 : Desa/Kelurahan</span>
                <span style="font-size: 8px;text-align:right">Lembar 4 : Kecamatan</span>
            </td>
        </tr>
    </table>
    <br>
    @foreach ($suratnya as $row)
    @foreach ($kelahiran as $kelahirans)
    <p  class="center bold" style="line-height:2px"><u>SURAT KETERANGAN KELAHIRAN</u></p>
    <table cellpadding="0" cellspacing="0" style="font-size: 11px;width:100%;border-collapse: collapse;  border-spacing: 0;line-height:14px">
        @foreach ($ayah as $ayahs)
        @if ($ayahs->form_kelahirans_id == $kelahirans['id'])
        <tr>
            <td style="width:22.8%;">Nama Kepala Keluarga</td>
            <td style="width:2%;">:</td>
            <td>{{ $ayahs['nama'] }}</td>
        </tr>
        @endif
        @endforeach
        <tr>
            <td style="width:22.8%;">Nomor Kartu Keluarga</td>
            <td style="width:2%;">:</td>
            <td>{{ $row->User['no_kk'] }}</td>
        </tr>
    </table>
    <br>
    <table border="1" cellpadding="0" cellspacing="0" style="font-size: 11px;width:100%;border-collapse: collapse;  border-spacing: 0;line-height:16px">
        <tr>
            <td style="padding: 5 5 5 5">
                <table cellpadding="0" cellspacing="0" style="font-size: 11px;width:100%;border-collapse: collapse;  border-spacing: 0;line-height:16px">
                    <tr>
                        <td class="bold" colspan="4"><span style="font-size: 14px">BAYI / ANAK</span></td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">1. </td>
                        <td style="width:22.8%;">Nama</td>
                        <td style="width:2%;">:</td>
                        <td>{{ $kelahirans['nama'] }}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">2. </td>
                        <td style="width:22.8%;">Jenis Kelamin</td>
                        <td style="width:2%;">:</td>
                        <td>{{ $kelahirans['jeniskelamin'] }}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">3. </td>
                        <td style="width:22.8%;">Tempat Dilahirkan</td>
                        <td style="width:2%;">:</td>
                        <td>{{$kelahirans['tempat_lahir']}}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">4. </td>
                        <td style="width:22.8%;">Nomor / Tempat Kelahiran</td>
                        <td style="width:2%;">:</td>
                        <td>{{  $kelahirans['nomor_tempat_kelahiran']  }}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">5. </td>
                        <td style="width:22.8%;">Hari dan Tanggal Lahir</td>
                        <td style="width:2%;">:</td>
                        <td>{{ Carbon\Carbon::parse($kelahirans['tgl_lahir'])->isoFormat('dddd, D MMMM Y') }}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">6. </td>
                        <td style="width:22.8%;">Pukul</td>
                        <td style="width:2%;">:</td>
                        <td>{{ Carbon\Carbon::parse($kelahirans['pukul'])->format('H:i') }}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">7. </td>
                        <td style="width:22.8%;">Jenis Kelahiran</td>
                        <td style="width:2%;">:</td>
                        <td>{{ $kelahirans['jeniskelahiran'] }}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">8. </td>
                        <td style="width:22.8%;">Kelahiran Ke-</td>
                        <td style="width:2%;">:</td>
                        <td>{{ $kelahirans['kelahiran_ke'] }}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">9. </td>
                        <td style="width:22.8%;">Penolong Kelahiran</td>
                        <td style="width:2%;">:</td>
                        <td>{{ $kelahirans['penolong_kelahiran'] }}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">10. </td>
                        <td style="width:22.8%;">Berat Bayi</td>
                        <td style="width:2%;">:</td>
                        <td>{{ $kelahirans['berat'] }} Kg</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">11. </td>
                        <td style="width:22.8%;">Panjang Bayi</td>
                        <td style="width:2%;">:</td>
                        <td>{{ $kelahirans['tinggi'] }} Cm</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            @foreach ($ibu as $ibus)
            @if ($ibus->form_kelahirans_id == $kelahirans['id'])
            <td style="padding: 5 5 5 5">
                <table cellpadding="0" cellspacing="0" style="font-size: 11px;width:100%;border-collapse: collapse;  border-spacing: 0;line-height:16px">
                    <tr>
                        <td class="bold" colspan="4"><span style="font-size: 14px">IBU</span></td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">1. </td>
                        <td style="width:24.3%;">NIK</td>
                        <td style="width:2.5%;">:</td>
                        <td>{{ $ibus['nik'] }}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">2. </td>
                        <td style="width:24.3%;">Nama Lengkap</td>
                        <td style="width:2.5%;">:</td>
                        <td>{{ $ibus['nama'] }}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">3. </td>
                        <td style="width:24.3%;">Tanggal Lahir / Umur</td>
                        <td style="width:2.5%;">:</td>
                        <td>{{ Carbon\Carbon::parse($ibus['tgl_lahir'])->translatedFormat('d F Y') }} / {{ Carbon\Carbon::now()->translatedFormat('Y') - Carbon\Carbon::parse($ibus['tgl_lahir'])->translatedFormat('Y') }}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">4. </td>
                        <td style="width:24.3%;">Pekerjaan</td>
                        <td style="width:2.5%;">:</td>
                        <td>{{ $ibus->Pekerjaan['pekerjaan'] }}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">5. </td>
                        <td style="width:24.3%;">Alamat</td>
                        <td style="width:2.5%;">:</td>
                        <td>{{ $ibus['dukuh'] }} Rt. {{ $ibus['rt'] }} {{ $ibus['kelurahan'] }}, {{ $ibus['kecamatan'] }}, {{ $ibus['kabupaten'] }}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">6. </td>
                        <td style="width:24.3%;">Kewarganegaraan</td>
                        <td style="width:2.5%;">:</td>
                        <td>WNI</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">7. </td>
                        <td style="width:24.3%;">Kebangsaan</td>
                        <td style="width:2.5%;">:</td>
                        <td>INDONESIA</td>
                    </tr>
                    @foreach ($ayah as $ayahs)
                    @if ($ayahs->form_kelahirans_id == $kelahirans['id'])
                    <tr>
                        <td style="width: 10px" align="center">8. </td>
                        <td style="width:24.3%;">Tgl. Pencatatan Perkawinan</td>
                        <td style="width:2.5%;">:</td>
                        <td>{{ Carbon\Carbon::parse($ayahs['tgl_pencatatankawin'])->translatedFormat('d F Y') }}</td>
                    </tr>
                    @endif
                    @endforeach
                </table>
            </td>
            @endif
            @endforeach
        </tr>
        <tr>
            @foreach ($ayah as $ayahs)
            @if ($ayahs->form_kelahirans_id == $kelahirans['id'])
            <td style="padding: 5 5 5 5">
                <table cellpadding="0" cellspacing="0" style="font-size: 11px;width:100%;border-collapse: collapse;  border-spacing: 0;line-height:16px">
                    <tr>
                        <td class="bold" colspan="4"><span style="font-size: 14px">AYAH</span></td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">1. </td>
                        <td style="width:24.3%;">NIK</td>
                        <td style="width:2.5%;">:</td>
                        <td>{{ $ayahs['nik'] }}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">2. </td>
                        <td style="width:24.3%;">Nama Lengkap</td>
                        <td style="width:2.5%;">:</td>
                        <td>{{ $ayahs['nama'] }}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">3. </td>
                        <td style="width:24.3%;">Tanggal Lahir / Umur</td>
                        <td style="width:2.5%;">:</td>
                        <td>{{ Carbon\Carbon::parse($ayahs['tgl_lahir'])->translatedFormat('d F Y') }} / {{ Carbon\Carbon::now()->translatedFormat('Y') - Carbon\Carbon::parse($ayahs['tgl_lahir'])->translatedFormat('Y') }}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">4. </td>
                        <td style="width:24.3%;">Pekerjaan</td>
                        <td style="width:2.5%;">:</td>
                        <td>{{ $ayahs->Pekerjaan['pekerjaan'] }}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">5. </td>
                        <td style="width:24.3%;">Alamat</td>
                        <td style="width:2.5%;">:</td>
                        <td>{{ $ayahs['dukuh'] }} Rt. {{ $ayahs['rt'] }} {{ $ayahs['kelurahan'] }}, {{ $ayahs['kecamatan'] }}, {{ $ayahs['kabupaten'] }}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">6. </td>
                        <td style="width:24.3%;">Kewarganegaraan</td>
                        <td style="width:2.5%;">:</td>
                        <td>WNI</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">7. </td>
                        <td style="width:24.3%;">Kebangsaan</td>
                        <td style="width:2%;">:</td>
                        <td>INDONESIA</td>
                    </tr>
                </table>
            </td>
            @endif
            @endforeach
        </tr>
        <tr>
            <td style="padding: 5 5 5 5">
                <table cellpadding="0" cellspacing="0" style="font-size: 11px;width:100%;border-collapse: collapse;  border-spacing: 0;line-height:16px">
                    <tr>
                        <td class="bold" colspan="4"><span style="font-size: 14px">PELAPOR</span></td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">1. </td>
                        <td style="width:23.4%;">NIK</td>
                        <td style="width:1%;">:</td>
                        <td>{{ $row->User['nik'] }}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">2. </td>
                        <td style="width:23.4%;">Nama Lengkap</td>
                        <td style="width:1%;">:</td>
                        <td>{{ $row->User['name'] }}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">3. </td>
                        <td style="width:23.4%;">Umur</td>
                        <td style="width:1%;">:</td>
                        <td>{{ Carbon\Carbon::now()->translatedFormat('Y') - Carbon\Carbon::parse($row->User['tgl_lahir'])->translatedFormat('Y') }}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">4. </td>
                        <td style="width:23.4%;">Jenis Kelamin</td>
                        <td style="width:1%;">:</td>
                        <td>{{$row->User['jeniskelamin']}}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">5. </td>
                        <td style="width:23.4%;">Pekerjaan</td>
                        <td style="width:1%;">:</td>
                        <td>{{ $row->User->Pekerjaan['pekerjaan'] }}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">6. </td>
                        <td style="width:23.4%;">Alamat</td>
                        <td style="width:1%;">:</td>
                        <td>{{ $row->User->Dukuh['dukuh'] }}, Rt. {{ $row->User['rt'] }} Banaran, Kalijambe, Sragen</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            @foreach ($saksi1 as $saksi1s)
            @if ($saksi1s->form_kelahirans_id == $kelahirans['id'])
            <td style="padding: 5 5 5 5">
                <table cellpadding="0" cellspacing="0" style="font-size: 11px;width:100%;border-collapse: collapse;  border-spacing: 0;line-height:16px">
                    <tr>
                        <td class="bold" colspan="4"><span style="font-size: 14px">SAKSI I</span></td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">1. </td>
                        <td style="width:24.3%;">NIK</td>
                        <td style="width:2.5%;">:</td>
                        <td>{{ $saksi1s['nik'] }}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">2. </td>
                        <td style="width:24.3%;">Nama Lengkap</td>
                        <td style="width:2.5%;">:</td>
                        <td>{{ $saksi1s['nama'] }}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">3. </td>
                        <td style="width:24.3%;">Umur</td>
                        <td style="width:2.5%;">:</td>
                        <td>{{ Carbon\Carbon::now()->translatedFormat('Y') - Carbon\Carbon::parse($saksi1s['tgl_lahir'])->translatedFormat('Y') }}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">4. </td>
                        <td style="width:24.3%;">Jenis Kelamin</td>
                        <td style="width:2.5%;">:</td>
                        <td>{{ $saksi1s['jeniskelamin'] }}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">5. </td>
                        <td style="width:24.3%;">Pekerjaan</td>
                        <td style="width:2.5%;">:</td>
                        <td>{{ $saksi1s->Pekerjaan['pekerjaan'] }}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">6. </td>
                        <td style="width:24.3%;">Alamat</td>
                        <td style="width:2.5%;">:</td>
                        <td>{{ $saksi1s->Dukuh['dukuh'] }}, Rt. {{ $saksi1s['rt'] }} Banaran, Kalijambe, Sragen</td>
                    </tr>
                </table>
            </td>
            @endif
            @endforeach
        </tr>
        <tr>
            @foreach ($saksi2 as $saksi2s)
            @if ($saksi2s->form_kelahirans_id == $kelahirans['id'])
            <td style="padding: 5 5 5 5">
                <table cellpadding="0" cellspacing="0" style="font-size: 11px;width:100%;border-collapse: collapse;  border-spacing: 0;line-height:16px">
                    <tr>
                        <td class="bold" colspan="4"><span style="font-size: 14px">SAKSI II</span></td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">1. </td>
                        <td style="width:24.3%;">NIK</td>
                        <td style="width:2.5%;">:</td>
                        <td>{{ $saksi2s['nik'] }}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">2. </td>
                        <td style="width:24.3%;">Nama Lengkap</td>
                        <td style="width:2.5%;">:</td>
                        <td>{{ $saksi2s['nama'] }}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">3. </td>
                        <td style="width:24.3%;">Umur</td>
                        <td style="width:2.5%;">:</td>
                        <td>{{ Carbon\Carbon::now()->translatedFormat('Y') - Carbon\Carbon::parse($saksi2s['tgl_lahir'])->translatedFormat('Y') }}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">4. </td>
                        <td style="width:24.3%;">Jenis Kelamin</td>
                        <td style="width:2.5%;">:</td>
                        <td>{{$saksi2s['jeniskelamin']}}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">5. </td>
                        <td style="width:24.3%;">Pekerjaan</td>
                        <td style="width:2.5%;">:</td>
                        <td>{{ $saksi2s->Pekerjaan['pekerjaan'] }}</td>
                    </tr>
                    <tr>
                        <td style="width: 10px" align="center">6. </td>
                        <td style="width:24.3%;">Alamat</td>
                        <td style="width:2.5%;">:</td>
                        <td>{{ $saksi2s->Dukuh['dukuh'] }}, Rt. {{ $saksi1s['rt'] }} Banaran, Kalijambe, Sragen</td>
                    </tr>
                </table>
            </td>
            @endif
            @endforeach
        </tr>
    </table>
    @endforeach
    @endforeach
    
    <div class="bold" style="font-size: 11px;padding-top:10px;">
        <table align="center" style="width: 100%">
            <tr>
                <td style="width:40%;" align="center"><span style="font-size: 10px">Mengetahui,</span><br> Kepala Desa/Kelurahan</td>
                <td style="width:20%"></td>
                <td style="width:40%;" align="center">
                    @foreach ($suratnya as $data)
                    <span style="font-size: 10px">Sragen, {{ Carbon\Carbon::parse($data->updated_at)->translatedFormat('d F Y') }}</span><br>
                    @endforeach 
                    Pelapor</td>
            </tr>
            <tr>
                <td align="center" style="height:80px;"><p style="color: white;padding-top:60px">X</p>
                    ( ..................................................... )</td>
                <td></td>
                <td align="center" style="height:80px;"><p style="color: white;padding-top:60px">X</p>
                    @foreach ($suratnya as $data)
                    ( {{ $data->User['name'] }} )
                    @endforeach </td>
            </tr>
        </table>
    </div>
</body>
</html>