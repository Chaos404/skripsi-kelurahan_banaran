<!DOCTYPE html>
<html lang="en">
<head>
  <title>SUKET</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<style type="text/css">
    body{
        font-family: "Times New Roman";
        /* font-size: 9px; */
    }
   .left    { text-align: left;}
   .right   { text-align: right;}
   .center  { text-align: center;}
   .justify { text-align: justify;}

   .jaraknya {
       height: 50px;
   }   
    
    #cap {
      position: fixed;
      z-index: 20;
      padding-left: 500px;
      padding-top: 800px;
    }

    #ttd {
      position: fixed;
      z-index: 20;
      padding-left: 550px;
      padding-top: 800px;
    }

    /* heading */

    h1 { font: bold 100% sans-serif; letter-spacing: 0.5em; text-align: center; text-transform: uppercase; }

    /* table */
    .custom { 
        line-height: 17px; 
    }

    .bold {
        font-weight: bold;
    }

    /* Typho */
    h6 {
        line-height: 10px;
    }

    p   {
        line-height: 1px;
    }

    .garis {
        display: block;
        margin-top: 0.2em;
        margin-bottom: 0.2em;
        margin-left: auto;
        margin-right: auto;
        border-style: dashed;
        border-width: 1px;
    }

    .vl {
        border-left: 2px dotted black;
        height: 100hv;
        position: absolute;
        left: 50%;
        margin-left: 50px;
        top: 0;
    }

    .kapital {
        text-transform: uppercase;
    }

</style>
</head>
<body>
    <div class="row">
        <div class="col-sm-6">
            <div class="row" style="height: 60px">
                <div class="my-auto col-lg-2">
                    <img src="{{ storage_path('img/Sragen Logo.png') }}" alt="" width="50" height="50">
                </div>
                <div class="my-auto col-lg-8" align="center">
                        <h6 style="font-weight: bold;">PEMERINTAH KABUPATEN SRAGEN</h6>
                        <h6 style="font-weight: bold;">KECAMATAN KALIJAMBE</h6>
                        <h6 style="font-weight: bold;">DESA / KELURAHAN BANARAN</h6>
                        <hr class="garis" style="width:100%;border: 2px solid rgb(0, 0, 0);">
                </div>
                <div class="my-auto col-lg-2"> 
                </div>
            </div>
        
            <div class="row custom" style="height:90px;">
                <div class="col-lg-3">
                    <b>No. Kode Desa / Kelurahan :</b><br>
                    33141 / 2009
                    <div style="width: 30%;">
                        <hr>
                    </div>
                </div>
                <div class="my-auto col-lg-5">
                    
                </div>
                <div class="col-lg-4">
                    <table align="right" style="font-weight: bold">
                        <tr>
                            <td class="text-center" rowspan="2">SURAT</td>
                            <td class="text-center" width="100%"><u>KETERANGAN</u>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center" style="line-height: 13px">PENGANTAR</td>
                        </tr>
                    </table>
                    <div class="bold" style="padding-left: 73%; padding-top:20px;">
                        <hr>
                        @foreach ($suratnya as $data)
                        <p style="line-height: 0px">Nomor : {{ $data->no_surat }}</p>
                        @endforeach
                    </div>
                </div>
            </div>
            
            <p class="center"><b>Yang bertanda tangan dibawah ini, menerangkan bahwa :</b></p>
            <table class="custom" style="width: 100%">
                @foreach ($suratnya as $data)
                <tr>
                    <td class="text-center">1.</td>
                    <td>Nama</td>
                    <td>:</td>
                    <td class="bold"><span class="kapital">{{ $data->User['name'] }}</span>
                        @if ($data->User['jeniskelamin'] == "Laki - laki")
                            Lk.
                        @else
                            Pr.
                        @endif
                    
                    </td>
                </tr>
                <tr>
                    <td class="text-center">2.</td>
                    <td>Tempat & Tanggal lahir</td>
                    <td>:</td>
                    <td class="bold"><span class="kapital">{{ $data->User['tempat_lahir'] }}</span>, {{ Carbon\Carbon::parse($data->User['tgl_lahir'])->translatedFormat('d F Y') }}</td>
                </tr>
                <tr>
                    <td class="text-center">3.</td>
                    <td>Kewarganegaraan & Agama</td>
                    <td>:</td>
                    <td class="bold">WNI / {{ $data->User->Agama['agama'] }}</td>
                </tr>
                <tr>
                    <td class="text-center">4.</td>
                    <td>Pekerjaan</td>
                    <td>:</td>
                    <td class="bold kapital">{{ $data->User->Pekerjaan['pekerjaan'] }}</td>
                </tr>
                <tr>
                    <td class="text-center">5.</td>
                    <td>Tempat Tinggal</td>
                    <td>:</td>
                    <td class="bold">Dk. <span class="kapital">{{ $data->User->Dukuh['dukuh'] }}</span> RT/RW {{ $data->User['rt'] }}/- <br> Ds. BANARAN Kec. KALIJAMBE</td>
                </tr>
                <tr>
                    <td class="text-center"></td>
                    <td>Kabupaten</td>
                    <td>:</td>
                    <td class="bold">Sragen Provinsi Jawa Tengah</td>
                </tr>
                <tr>
                    <td class="text-center">6.</td>
                    <td>Surat Bukti Diri</td>
                    <td>:</td>
                    <td class="bold">KTP No. {{ $data->User['nik'] }} <br> KK &nbsp; No. {{ $data->User['no_kk'] }}</td>
                </tr>
                <tr>
                    <td class="text-center">7.</td>
                    <td>Status Perkawinan</td>
                    <td>:</td>
                    <td class="bold kapital">{{ $data->User['statusperkawinan'] }}</td>
                </tr>
                <tr>
                    <td class="text-center">8.</td>
                    <td>Keperluan</td>
                    <td>:</td>
                    <td class="bold">{{ $data->Keperluan['keperluan'] }}</td>
                </tr>
                <tr>
                    <td class="text-center">9.</td>
                    <td>Berlaku Mulai</td>
                    <td>:</td>
                    <td class="bold">{{ Carbon\Carbon::parse($data->updated_at)->translatedFormat('d F Y') }} s/d Selesai</td>
                </tr>
                <tr>
                    <td class="text-center">10.</td>
                    <td>Keterangan lain - lain *)</td>
                    <td>:</td>
                    <td class="bold">{{ $data->keterangan }}</td>
                </tr>
                @endforeach
            </table>
            <br>
            <p class="center bold">Demikian untuk menjadikan maklum bagi yang berkepentingan</p>
            <p class="bold" style="padding-left: 30%">Nomor &nbsp;&nbsp;&nbsp; :</p>
            <p class="bold" style="padding-left: 30%">Tanggal &nbsp; : </p>
            <p class="right bold" style="line-height: 1px;padding-right:20px">BANARAN, {{ Carbon\Carbon::parse($data->updated_at)->translatedFormat('d F Y') }}</p>
            <p class="right bold" style="line-height: 0px;padding-right:20px">----------------------------------------</p>
        
            <table class="bold" style="line-height: 15px;">
                @if (strlen($data->User['name']) <= 18)
                    <tr>
                        <td class="text-center"><div style="padding-left:5px;">Tanda Tangan</div></td>
                        <td class="text-center"><div style="padding-left:60px;">Camat</div></td>
                        <td class="text-center"><div style="padding-left:40px;">Kepala Desa BANARAN</div></td>
                    </tr>
                    <tr>
                        <td class="text-center"><div style="padding-left:5px;">Pemegang</div></td>
                        <td class="text-center"><div style="padding-left:60px;">KALIJAMBE</div></td>
                        <td class="text-center"><div style="padding-left:40px;">Sekretaris BANARAN</div></td>
                    </tr>
                    <tr>
                        <td><div class="jaraknya"></div></td>
                        <td><div class="jaraknya"></div></td>
                        <td><div style="padding-left:60px;"><img id="cap" src="{{ storage_path('img/legalitas/cap.png') }}" width="110" height="0" alt=""><img id="ttd" src="{{ storage_path('img/legalitas/ttd.png') }}" width="150" height="0" alt=""></div></td>
                    </tr>
                    <tr>
                        <td class="text-center kapital">
                            <div style="padding-left:5px;">
                                @foreach ($suratnya as $data)
                                <b>{{ $data->User['name'] }}</b>
                                @endforeach
                                <p style="line-height: 8px;">------------------------------</p>
                            </div>
                        </td>
                        <td class="text-center"><div style="padding-left:60px;"><br>------------------------------ <br><p style="line-height: 10px; float:left">&nbsp;&nbsp;&nbsp;<b> NIP.</b></p></div></td>
                        <td class="text-center"><div style="padding-left:40px;"><b></b><br><p style="line-height: 8px;">------------------------------</p></div></td>
                    </tr>
                @else
                    <tr>
                        <td class="text-center"><div style="padding-left:5px;">Tanda Tangan</div></td>
                        <td class="text-center"><div style="padding-left:60px;">Camat</div></td>
                        <td class="text-center"><div style="padding-left:0px;">Kepala Desa BANARAN</div></td>
                    </tr>
                    <tr>
                        <td class="text-center"><div style="padding-left:5px;">Pemegang</div></td>
                        <td class="text-center"><div style="padding-left:60px;">KALIJAMBE</div></td>
                        <td class="text-center"><div style="padding-left:0px;">Sekretaris BANARAN</div></td>
                    </tr>
                    <tr>
                        <td><div class="jaraknya"></div></td>
                        <td><div class="jaraknya"></div></td>
                        <td><div style="padding-left:60px;"><img id="cap" src="{{ storage_path('img/legalitas/cap.png') }}" width="110" height="0" alt=""><img id="ttd" src="{{ storage_path('img/legalitas/ttd.png') }}" width="150" height="0" alt=""></div></td>
                    </tr>
                    <tr>
                        <td class="text-center kapital">
                            <div style="padding-left:5px;">
                                @foreach ($suratnya as $data)
                                <b>{{ $data->User['name'] }}</b>
                                @endforeach
                                <br><p style="line-height: 8px;">------------------------------</p>
                            </div>
                        </td>
                        <td class="text-center"><div style="padding-left:60px;"><br><br> ------------------------------ <br><p style="line-height: 10px; float:left">&nbsp;&nbsp;&nbsp;<b> NIP.</b></p></div></td>
                        <td class="text-center"><div style="padding-left:0px;"><br><b></b><br><p style="line-height: 8px;">------------------------------</p></div></td>
                    </tr>
                @endif
            </table>
            <p class="center" style="font-size: 11px; font-weight:bold;padding-top:14px;">Catatan : *) Apabila ruangan ini tidak mencukupi, harap ditulis sebaliknya, dan dibubuhi stempel Desa/Kelurahan.</p>
        </div>
        <div class="vl"></div>
        <div class="col-sm-6">
            
        </div>
    </div>
</body>
</html>
