<!DOCTYPE html>
<html lang="en">
<head>
  <title>KTP</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<style type="text/css">
    body{
        /* font-family: "Times New Roman"; */
        /* font-size: 9px; */
    }
   .left    { text-align: left;}
   .right   { text-align: right;}
   .center  { text-align: center;}
   .justify { text-align: justify;}

   .jaraknya {
       height: 50px;
   }   

    /* table */
    .custom { 
        line-height: 17px; 
    }

    .bold {
        font-weight: bold;
    }

    /* Typho */
    h6 {
        line-height: 10px;
    }

    p   {
        line-height: 1px;
    }

    .garis {
        display: block;
        margin-top: 0.2em;
        margin-bottom: 0.2em;
        margin-left: auto;
        margin-right: auto;
        border-style: inset;
        border-width: 1px;
    }

    .vl {
        border-left: 2px dotted black;
        height: 100hv;
        position: absolute;
        left: 50%;
        margin-left: 50px;
        top: 0;
    }

    .kapital {
        text-transform: uppercase;
    }

</style>
</head>
<body>
    <div class="bold" style="float:right">
        <input style="border: 1px solid rgb(0,0,0);" type="text" name="" id="" value="F-1.21">
    </div>
    <br><br>
    <p  class="center bold" style="line-height:1px">FORMULIR PERMOHONAN KARTU PENDUDUK (KTP) WARGA NEGARA INDONESIA</p>
    <table border="1" style="font-size: 11px;width:100%;">
        <tr>
            <td>
                <div class="" style="margin: 3px 3px 3px 3px;">
                    Perhatian : <br>
                    1. Harap diisi dengan huruf cetak dan menggunakan tinta hitam. <br>
                    2. Untuk kolom pilihan, harap memberi tanda silang (x) pada kotak pilihan. <br>
                    3. Setelah formulir ini diisi dan ditandatangani, harap diserahkan kembali ke Kantor Desa/Kelurahan.
                </div>
            </td>
        </tr>
    </table>
    <br>
    <table cellpadding="0" cellspacing="0" class="bold" style="font-size: 12px;width:90%;border-collapse: collapse;  border-spacing: 0;">
        <tr>
            <td style="width:40%;">PEMERINTAH PROPINSI</td>
            <td style="width:1%;">:</td>
            <td style="width:15%;"><div style="border: 1px solid rgb(0,0,0); padding-left: 2px; width: 30px; text-align:left">33</div></td>
            <td class="bold"><div style="border: 1px solid rgb(0,0,0); padding-left: 2px; text-align:left">JAWA TENGAH</div></td>
        </tr>
        <tr>
            <td style="width:40%;">PEMERINTAH KABUPATEN/KOTA</td>
            <td style="width:1%;">:</td>
            <td style="width:15%;"><div style="border: 1px solid rgb(0,0,0); padding-left: 2px; width: 30px; text-align:left">14</div></td>
            <td class="bold"><div style="border: 1px solid rgb(0,0,0); padding-left: 2px; text-align:left">SRAGEN</div></td>
        </tr>
        <tr>
            <td style="width:40%;">KECAMATAN</td>
            <td style="width:1%;">:</td>
            <td style="width:15%;"><div style="border: 1px solid rgb(0,0,0); padding-left: 2px; width: 30px; text-align:left">01</div></td>
            <td class="bold"><div style="border: 1px solid rgb(0,0,0); padding-left: 2px; text-align:left">KALIJAMBE</div></td>
        </tr>
        <tr>
            <td style="width:40%;">KELURAHAN/DESA</td>
            <td style="width:1%;">:</td>
            <td style="width:15%;"><div style="border: 1px solid rgb(0,0,0); padding-left: 2px; width: 30px; text-align:left">2009</div></td>
            <td class="bold"><div style="border: 1px solid rgb(0,0,0); padding-left: 2px; text-align:left">BANARAN</div></td>
        </tr>
    </table>
    <br>
    @foreach ($suratnya as $data)
    <table cellpadding="0" cellspacing="0" style="font-size: 12px;width:90%;border-collapse: collapse;  border-spacing: 0;">
        <tr>
            <td class="bold" style="width:20%;"><i><u>PERMOHONAN KTP</u></i></td>
            <td style="width:10%;"><div style="border: 1px solid rgb(0,0,0); width: 20px; text-align:center"><span style="color: white">X</span></td>
            @if ($data->permohonan_ktp == "KTP Baru")
                <td style="width:1%;"><div style="border: 1px solid rgb(0,0,0); width: 20px; text-align:center"><b>V</b></td>
                <td style="width:15%;"><div style="border: 1px solid rgb(0,0,0); padding-left: 2px; width: 60px; text-align:left">A. Baru</div></td>
                <td style="width:1%;"><div style="border: 1px solid rgb(0,0,0); width: 20px; text-align:center"><span style="color: white">X</span></td>
                <td style="width:15%;"><div style="border: 1px solid rgb(0,0,0); padding-left: 2px; width: 100px; text-align:left">B. Perpanjangan</div></td>
                <td style="width:1%;"><div style="border: 1px solid rgb(0,0,0); width: 20px; text-align:center"><span style="color: white">X</span></td>
                <td style="width:15%;"><div style="border: 1px solid rgb(0,0,0); padding-left: 2px; width: 100px; text-align:left">C. Penggantian</div></td>
            @else
                <td style="width:1%;"><div style="border: 1px solid rgb(0,0,0); width: 20px; text-align:center"><span style="color: white">X</span></td>
                <td style="width:15%;"><div style="border: 1px solid rgb(0,0,0); padding-left: 2px; width: 60px; text-align:left">A. Baru</div></td>
                <td style="width:1%;"><div style="border: 1px solid rgb(0,0,0); width: 20px; text-align:center"><span style="color: white">X</span></td>
                <td style="width:15%;"><div style="border: 1px solid rgb(0,0,0); padding-left: 2px; width: 100px; text-align:left">B. Perpanjangan</div></td>
                <td style="width:1%;"><div style="border: 1px solid rgb(0,0,0); width: 20px; text-align:center"><b>V</b></td>
                <td style="width:15%;"><div style="border: 1px solid rgb(0,0,0); padding-left: 2px; width: 100px; text-align:left">C. Penggantian</div></td>
            @endif
        </tr>
    </table>
    <br>
    <table style="font-size: 11px;width:100%;">
            <tr>
                <td style="width:30%;"><div style="border: 1px solid rgb(0,0,0); padding-left: 2px; text-align:left">1. Nama Lengkap</div></td>4
                <td style="width:1%;"></td>
                <td class="kapital" colspan="7"><div style="border: 1px solid rgb(0,0,0); padding-left: 2px; text-align:left">{{ $data->User['name'] }}</div></td>
            </tr>
            <tr>
                <td style="width:30%;"><div style="border: 1px solid rgb(0,0,0); padding-left: 2px; text-align:left">3. No. KK</div></td>
                <td style="width:1%;"></td>
                <td colspan="7"><div style="border: 1px solid rgb(0,0,0); padding-left: 2px; width: 250px; text-align:left">{{ $data->User['no_kk'] }}</div></td>
            </tr>
            <tr>
                <td style="width:30%;"><div style="border: 1px solid rgb(0,0,0); padding-left: 2px; text-align:left">5. NIK</div></td>
                <td style="width:1%;"></td>
                <td colspan="7"><div style="border: 1px solid rgb(0,0,0); padding-left: 2px; width: 250px; text-align:left">{{ $data->User['nik'] }}</div></td>
            </tr>
            <tr>
                <td style="width:30%;"><div style="border: 1px solid rgb(0,0,0); padding-left: 2px; text-align:left">7. Alamat</div></td>
                <td style="width:1%;"></td>
                <td class="kapital" colspan="7"><div style="border: 1px solid rgb(0,0,0); padding-left: 2px; text-align:left">{{ $data->User->Dukuh['dukuh'] }}</div></td>
            </tr>
            <tr>
                <td style="width:30%;"></td>
                <td style="width:1%;"></td>
                <td class="kapital" colspan="7"><div style="border: 1px solid rgb(0,0,0); padding-left: 2px; text-align:left"><span style="color: white">X</span></div></td>
            </tr>
            <tr>
                <td style="width:30%;"></td>
                <td style="width:1%;"></td>
                <td><div style="border: 1px solid rgb(0,0,0); width: 20px; text-align:center">RT</div></td>
                <td style="padding-right:10px"><div style="border: 1px solid rgb(0,0,0); width: 40px; text-align:center">{{ $data->User['rt'] }}</div></td>
                <td style="margin-right: 10px"><div style="border: 1px solid rgb(0,0,0); width: 20px; text-align:center">RW</div></td>
                <td style="padding-right:200px"><div style="border: 1px solid rgb(0,0,0); width: 40px; text-align:center">-</div></td>
            </tr>
        @endforeach
    </table>
    <div class="row">
        <div class="col-lg-8">
            <table border="1" bordercolor="#000000" style="font-size: 11px;width:30%;">
                <tr>
                    <td style="width:20%; text-align:center">Pas Photo (2x3)</td>
                    <td style="width:20%; text-align:center">Cap Jempol</td>
                </tr>
                <tr>
                    <td style="height:110px"></td>
                    <td style="height:110px"></td>
                </tr>
            </table>
        </div>
        <div class="col-lg-4">
            <div class="row">
                <div class="col-lg-6" style="font-size: 11px">
                    <table border="1" bordercolor="#000000" style="padding-left:215.5px;">
                        <tr>
                            <td style="width:180px; text-align:center">Specimen Tanda Tangan</td>
                        </tr>
                        <tr>
                            <td style="height:80px"></td>
                        </tr>
                    </table>
                    <br>
                    <p style="padding-left: 218px;">Ket : Cap Jempol / Tanda Tangan</p>
                </div>
                <div class="col-lg-6">
                    @foreach ($suratnya as $data)
                    <div class="center" style="font-size: 11px;padding-left:550px;padding-top:10px">
                            <p>Banaran, {{ Carbon\Carbon::parse($data->updated_at)->translatedFormat('d F Y') }}</p>
                            <p >Pemohon,</p>
                            <br><br><br><br>
                            <p>( {{ $data->User['name'] }} )</p>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="center" style="font-size: 11px;padding-top:150px;">
            <p>Mengetahui,</p>
            <div style="font-size: 11px;">
                <table align="center" style="width: 80%">
                    <tr>
                        <td style="width:40%;" align="center">Camat KALIJAMBE</td>
                        <td style="width:10%"></td>
                        <td style="width:40%;" align="center">Kelurahan Banaran</td>
                    </tr>
                    <tr>
                        <td align="center" style="height:200px;">( ..................................................... )</td>
                        <td></td>
                        <td align="center" style="height:200px;">( ..................................................... )</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</body>
</html>