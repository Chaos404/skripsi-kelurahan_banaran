<!DOCTYPE html>
<html lang="en">
<head>
  <title>PINDAH KELUAR</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<style type="text/css">
    body{
        /* font-family: "Times New Roman"; */
        /* font-size: 9px; */
    }
   .left    { text-align: left;}
   .right   { text-align: right;}
   .center  { text-align: center;}
   .justify { text-align: justify;}

   .jaraknya {
       height: 50px;
   }   

    /* table */
    .custom { 
        line-height: 17px; 
    }

    .bold {
        font-weight: bold;
    }

    /* Typho */
    h6 {
        line-height: 10px;
    }

    p   {
        line-height: 1px;
    }

    .garis {
        display: block;
        margin-top: 0.2em;
        margin-bottom: 0.2em;
        margin-left: auto;
        margin-right: auto;
        border-style: inset;
        border-width: 1px;
    }

    .vl {
        border-left: 2px dotted black;
        height: 100hv;
        position: absolute;
        left: 50%;
        margin-left: 50px;
        top: 0;
    }

    .kapital {
        text-transform: uppercase;
    }

</style>
</head>
<body>
    @foreach ($suratnya as $row)
    @foreach ($pindahkeluar as $pindahkeluars)
    <div class="bold" style="float:right">
        @if ($pindahkeluars['t_jeniskepindahans_id'] == 1)
            <input style="border: 1px solid rgb(0,0,0);" type="text" name="" id="" value="F.1-25">
        @elseif ($pindahkeluars['t_jeniskepindahans_id'] == 2)
            <input style="border: 1px solid rgb(0,0,0);" type="text" name="" id="" value="F.1-29">
        @else
            <input style="border: 1px solid rgb(0,0,0);" type="text" name="" id="" value="F.1-34">
        @endif
    </div>
    <br><br>
    <table cellpadding="0" cellspacing="0" class="bold" style="font-size: 11px;width:90%;border-collapse: collapse;  border-spacing: 0;">
        <tr>
            <td style="width:30%;">PROPINSI</td>
            <td style="width:1%;">:</td>
            <td style="width:20%;" class="bold">JAWA TENGAH</td>
            <td>*)</td>
        </tr>
        <tr>
            <td style="width:30%;">KABUPATEN/KOTA</td>
            <td style="width:1%;">:</td>
            <td class="bold">SRAGEN</td>
            <td>*)</td>
        </tr>
        <tr>
            <td style="width:30%;">KECAMATAN</td>
            <td style="width:1%;">:</td>
            <td class="bold">KALIJAMBE</td>
            <td>*)</td>
        </tr>
        <tr>
            <td style="width:30%;">DESA/KELURAHAN</td>
            <td style="width:1%;">:</td>
            <td class="bold">BANARAN</td>
            <td>*)</td>
        </tr>
        <tr>
            <td style="width:30%;">DUSUN/DUKUH/KAMPUNG</td>
            <td style="width:1%;">:</td>
            <td class="bold kapital">
                {{ $row->User->Dukuh['dukuh'] }}
            </td>
            <td>*)</td>
        </tr>
    </table>
    <br>
    <p  class="center bold" style="line-height:1px">FORMULIR PERMOHONAN PINDAH WNI</p>
    <P class="center bold" style="font-size: 11px">
    @if ($pindahkeluars['t_jeniskepindahans_id'] == 1)
        Antar Desa/Kelurahan Dalam Satu Kecamatan
    @elseif ($pindahkeluars['t_jeniskepindahans_id'] == 2)
        Antar Kecamatan Dalam Satu Kabupaten/Kota
    @else
        Antar Kabupaten/Kota atau Antar Provinsi
    @endif
    </P>
    <P class="center bold" style="font-size: 11px">No. {{ $row->no_suratformulir }}</P>
    <br>
    <span class="bold" style="font-size: 16px">DATA DAERAH ASAL</span>
    <table class="bold" style="font-size: 11px;width:100%;">
        <tr>
            <td style="width: 10px">1. </td>
            <td style="width:30%;">Nomor Kartu Keluarga</td>
            <td colspan="5">{{ $row->User['no_kk'] }}</td>
        </tr>
        <tr>
            <td>2. </td>
            <td style="width:30%;">Nama Kepala Keluarga</td>
            <td colspan="5">{{ $pindahkeluars['kepala_keluarga'] }}</td>
        </tr>
        <tr>
            <td>3. </td>
            <td style="width:30%;">Alamat</td>
            <td style="width: 43%">{{ $row->User->Dukuh['dukuh'] }}</td>
            <td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;RT &nbsp;&nbsp; {{ $row->User['rt'] }} &nbsp;  &nbsp;&nbsp;&nbsp;&nbsp; RW &nbsp;&nbsp; -</td>
            {{-- <td style="width: 5%">{{ $row->User['rt'] }}</td>
            <td style="width: 5%">RW</td> --}}
            {{-- <td>-</td> --}}
        </tr>
        <tr>
            <td></td>
            <td style="width:30%;"></td>
            <td colspan="5">Dusun/Dukuh/Kampung &nbsp;&nbsp;&nbsp;&nbsp;{{ $row->User->Dukuh['dukuh'] }}</td>
        </tr>
        <tr>
            <td></td>
            <td style="padding-left: 30px" style="width:30%;">a. Desa/Kelurahan</td>
            {{-- <td colspan="5">BANARAN <span style="float: right; padding-right:99px"> c. Kab/Kota &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SRAGEN</span></td> --}}
            <td style="width: 43%">BANARAN</td>
            <td style="width: 15%">c. Kab/Kota</td>
            <td colspan="3">SRAGEN</td>
        </tr>
        <tr>
            <td></td>
            <td style="padding-left: 30px" style="width:30%;">b. Kecamatan</td>
            {{-- <td colspan="5">KALIJAMBE <span style="float: right; padding-right:65px"> d. Provinsi &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; JAWA TENGAH</span></td> --}}
            <td style="width: 43%">KALIJAMBE</td>
            <td style="width: 15%">d. Provinsi</td>
            <td colspan="3">JAWA TENGAH</td>
        </tr>
        <tr>
            <td></td>
            <td style="width:30%;"></td>
            {{-- <td colspan="5">Kode Pos  &nbsp;&nbsp;&nbsp;&nbsp; 57275 <span style="float: right;padding-right:60px"> Telepon  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +6282266335773</span></td> --}}
            <td style="width: 43%">57275</td>
            <td style="width: 15%">Telepon</td>
            <td colspan="3">-</td>
        </tr>
        <tr>
            <td>4. </td>
            <td style="width:30%;">NIK Pemohon</td>
            <td colspan="5">{{ $row->User['nik'] }}</td>
        </tr>
        <tr>
            <td>5. </td>
            <td style="width:30%;">Nama Lengkap</td>
            <td colspan="5">{{ $row->User['name'] }}</td>
        </tr>
        @endforeach
    </table>
    <span class="bold" style="font-size: 16px">DATA KEPINDAHAN</span>
    <table class="bold" style="font-size: 11px;width:100%;">
        @foreach ($suratnya as $row)
        <tr>
            <td style="width: 10px">1. </td>
            <td style="width:30%;">Alasan Pindah</td>
            @if ($pindahkeluars['alasan_pindah'] != null )
                <td colspan="5">{{ $pindahkeluars['alasan_pindah'] }}</td>
            @else
                <td colspan="5">{{ $pindahkeluars['alasan_pindah2'] }}</td>
            @endif
        </tr>
        @foreach ($tinggal as $tinggals)
        @if ($tinggals->form_perpindahans_id == $pindahkeluars['id'])
        <tr>
            <td>2. </td>
            <td style="width:30%;">Alamat Tujuan Pindah</td>
            <td style="width: 43%">{{ $tinggals['dukuh'] }}</td>
            <td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;RT &nbsp;&nbsp; {{ $tinggals['rt'] }} &nbsp;  &nbsp;&nbsp;&nbsp;&nbsp; RW &nbsp;&nbsp; {{ $tinggals['rw'] }}</td> --}}</td>
            {{-- <td style="width: 5%">{{ $tinggals['rt'] }}</td>
            <td style="width: 5%">RW</td>
            <td>{{ $tinggals['rw'] }}</td> --}}
        </tr>
        <tr>
            <td></td>
            <td style="width:30%;"></td>
            <td colspan="5">Dusun/Dukuh/Kampung &nbsp;&nbsp;&nbsp;&nbsp; {{ $tinggals['dukuh'] }}</td>
        </tr>
        <tr>
            <td></td>
            <td style="padding-left: 30px" style="width:30%;">a. Desa/Kelurahan</td>
            {{-- <td colspan="5"><span class="kapital">{{ $tinggals['kelurahan'] }}</span><span style="float: right; padding-right:78px"> c. Kab/Kota &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="kapital">{{ $tinggals['kabupaten'] }}</span></span></td> --}}
            <td style="width: 43%" class="kapital">{{ $tinggals['kelurahan'] }}</td>
            <td style="width: 10%">c. Kab/Kota</td>
            <td colspan="3" class="kapital">{{ $tinggals['kabupaten'] }}</td>
        </tr>
        <tr>
            <td></td>
            <td style="padding-left: 30px" style="width:30%;">b. Kecamatan</td>
            {{-- <td colspan="5"><span class="kapital">{{ $tinggals['kecamatan'] }}</span><span style="float: right; padding-right:65px"> d. Provinsi &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; JAWA TENGAH</span></td> --}}
            <td style="width: 43%" class="kapital">{{ $tinggals['kecamatan'] }}</td>
            <td style="width: 10%">d. Provinsi</td>
            <td colspan="3" class="kapital">{{ $tinggals['provinsi'] }}</td>
        </tr>
        <tr>
            <td></td>
            <td style="width:30%;"></td>
            {{-- <td colspan="5">Kode Pos  &nbsp;&nbsp;&nbsp;&nbsp; {{ $tinggals['kodepos'] }}  <span style="float: right;padding-right:60px"> Telepon  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +6282266335773</span></td> --}}
            <td style="width: 43%">{{ $tinggals['kodepos'] }}</td>
            <td style="width: 15%">Telepon</td>
            <td colspan="3">-</td>
        </tr>
        @endif
        @endforeach
        <tr>
            <td>3. </td>
            <td style="width:30%;">Jenis Kepindahan</td>
            <td colspan="5">
                @if ($pindahkeluars['t_jeniskepindahans_id'] == 1)
                    Antar Kelurahan dalam Satu Kecamatan
                @elseif ($pindahkeluars['t_jeniskepindahans_id'] == 2)
                    Antar Kecamatan dalam Satu Kabupaten
                @else
                    Antar Kabupaten / Kota
                @endif
            </td>
        </tr>
        <tr>
            <td>4. </td>
            <td style="width:30%;">Status KK <br> Bagi Yang Tidak Pindah</td>
            <td colspan="5">{{ $pindahkeluars['statuskk_tidakpindah'] }}</td>
        </tr>
        <tr>
            <td>5. </td>
            <td style="width:30%;">Status KK <br> Bagi Yang Pindah</td>
            <td colspan="5">{{ $pindahkeluars['statuskk_pindah'] }}</td>
        </tr>
        <tr>
            <td>6. </td>
            <td style="width:30%;">Keluarga Yang Pindah</td>
            <td colspan="5"></td>
        </tr>
        <table border="1" align="center" style="width: 100%">
            <tr>
                <td style="width:50%;height:25px" align="center">NIK</td>
                <td style="width:50%;" align="center">NAMA</td>
            </tr>
            @foreach ($keluarga as $keluargas)
            @if ($keluargas->form_perpindahans_id == $pindahkeluars['id'])
            <tr>
                <td class="text-center" style="height: 15px">{{ $keluargas->nik }}</td>
                <td class="text-center" style="height: 15px">{{ $keluargas->nama }}</td>
            </tr>
            {{-- <tr>
                <td style="height: 15px"></td>
                <td style="height: 15px"></td>
            </tr>
            <tr>
                <td style="height: 15px"></td>
                <td style="height: 15px"></td>
            </tr>
            <tr>
                <td style="height: 15px"></td>
                <td style="height: 15px"></td>
            </tr>
            <tr>
                <td style="height: 15px"></td>
                <td style="height: 15px"></td>
            </tr>
            <tr>
                <td style="height: 15px"></td>
                <td style="height: 15px"></td>
            </tr> --}}
            @elseif ($keluargas->form_perpindahans_id != $pindahkeluars['id'])
            @endif
            @endforeach
        </table>
        @endforeach
        @endforeach
    </table>
    <div class="bold" style="font-size: 11px;padding-top:40px;">
        <table align="center" style="width: 100%">
            <tr>
                <td style="width:40%;" align="center"><span style="color: white">X</span><br> Petugas Registrasi</td>
                <td style="width:20%"></td>
                <td style="width:40%;" align="center">
                    @foreach ($suratnya as $row)
                    <span style="font-size: 10px">Sragen, {{ Carbon\Carbon::parse($row->updated_at)->translatedFormat('d F Y') }}</span><br>
                    @endforeach 
                    Pemohon</td>
            </tr>
            <tr>
                <td align="center" style="height:200px;">( ..................................................... )</td>
                <td></td>
                <td align="center" style="height:200px;">
                    @foreach ($suratnya as $row)
                    ( {{ $row->User['name'] }} )
                    @endforeach </td>
            </tr>
        </table>
    </div>
</body>
</html>