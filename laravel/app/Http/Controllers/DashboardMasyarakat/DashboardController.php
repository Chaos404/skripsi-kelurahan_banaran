<?php

namespace App\Http\Controllers\DashboardMasyarakat;
use App\User;
use App\Http\Controllers\Controller;
use App\Model\Surat;
use App\Model\Umkm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard($id)
    {
        $userLogin = Auth::user()->id;
        if($userLogin != $id) {
            return abort(404);
        } else {
            $user = User::where('id', $id)->get();
            $surat = Surat::where('t_users_id', $id)->where('status', 'terverifikasi')->orderBy('created_at', 'desc')->count();
            $umkm = Umkm::where('t_users_id', $id)->where('status', 'terverifikasi')->orderBy('created_at', 'desc')->count();

            return view('masyarakat.index', compact('user', 'surat', 'umkm'));
        }
    }
}
