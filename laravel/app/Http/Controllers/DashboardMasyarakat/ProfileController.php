<?php

namespace App\Http\Controllers\DashboardMasyarakat;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use App\Model\Agama;
use App\Model\Dukuh;
use App\Model\Pekerjaan;
use App\Model\Rt;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id_u)
    {
        $userLogin = Auth::user()->id;
        if($userLogin != $id_u) {
            return abort(404);
        } else {
            $user = User::where('id', $id_u)->get();
            $pekerjaan = Pekerjaan::all();
            $dukuh = Dukuh::all();
            $rt = Rt::all();
            $agama = Agama::all();
            return view('masyarakat.profile', compact('user', 'pekerjaan', 'dukuh', 'rt', 'agama'));
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function rt($id_u, $id)
    {
        $rt = Rt::select('rt','id')->where('t_dukuh_id', $id)->get();
        return response()->json($rt);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array(
            'id'=>$id,
        );

        $data = array_merge($data, $request->all());

        if ($request->hasFile('lampiran_kk')) {
            $validator = Validator::make($data, [
                'id'=>'required|numeric|exists:t_users,id',
                'nama_lengkap' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255'],
                'pekerjaan' => ['required', 'string', 'max:255'],
                'dukuh' => ['required', 'string', 'max:255'],
                'rt' => ['required', 'string', 'max:255'],
                'agama' => ['required', 'string', 'max:255'],
                'tempat_lahir' => ['required', 'string', 'max:255'],
                'tanggal_lahir' => ['required', 'string', 'max:255'],
                'jenis_kelamin' => ['required', 'string', 'max:255'],
                'nik' => ['required', 'numeric', 'digits:16'],
                'no_kk' => ['required','numeric', 'digits:16'],
                'status_perkawinan' => ['required', 'string', 'max:255'],
                'lampiran_kk' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048']
            ]);
            } else {
                $validator = Validator::make($data, [
                'id'=>'required|numeric|exists:t_users,id',
                'nama_lengkap' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255'],
                'pekerjaan' => ['required', 'string', 'max:255'],
                'dukuh' => ['required', 'string', 'max:255'],
                'rt' => ['required', 'string', 'max:255'],
                'agama' => ['required', 'string', 'max:255'],
                'tempat_lahir' => ['required', 'string', 'max:255'],
                'tanggal_lahir' => ['required', 'string', 'max:255'],
                'jenis_kelamin' => ['required', 'string', 'max:255'],
                'nik' => ['required', 'numeric', 'digits:16'],
                'no_kk' => ['required','numeric', 'digits:16'],
                'status_perkawinan' => ['required', 'string', 'max:255'],
                // 'lampiran_kk' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048']
            ]);
                }
        
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            if ($request->hasFile('lampiran_kk')) {
                $kk = User::where('id',$id)->first();
                
                // check file exist
                if (file_exists('public/assets/1117688/'.$kk['lamp_kk']) AND $kk['lamp_kk'] != "") {
                    unlink('public/assets/1117688/'.$kk['lamp_kk']);
                }

                // Upload and change file
                $file = $request->file('lampiran_kk');

                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $file->getClientOriginalName())) {
                    return redirect()->back()->with('errors','invalid file name.');
                }

                $folder_destination = 'public/assets/1117688';

                // Available alpha caracters
                $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

                // generate a pin based on 2 * 7 digits + a random character
                $pin = mt_rand(1000000, 9999999)
                    . mt_rand(1000000, 9999999)
                    . $characters[rand(0, strlen($characters) - 1)];

                $filenya = str_shuffle($pin) . '-' . $file->getClientOriginalName();
                $filetowrite = $folder_destination . '/' . $filenya;
                $file->move($folder_destination, $filetowrite);    


                $user = User::where('id',$id)->update([
                    'name' => request('nama_lengkap'),
                    'email' => request('email'),
                    't_pekerjaan_id' => request('pekerjaan'),
                    't_dukuh_id' => request('dukuh'),
                    'rt' => request('rt'),
                    't_agama_id' => request('agama'),
                    'tempat_lahir' => request('tempat_lahir'),
                    'tgl_lahir' => request('tanggal_lahir'),
                    'jeniskelamin' => request('jenis_kelamin'),
                    'nik' => request('nik'),
                    'no_kk' => request('no_kk'),
                    'statusperkawinan' => request('status_perkawinan'),
                    // 'lamp_ktp'=> $filenya1,
                    'lamp_kk'=> $filenya,
                    'role' => 'unverified'
                ]);
            } else {
                $user = User::where('id',$id)->update([
                    'name' => request('nama_lengkap'),
                    'email' => request('email'),
                    't_pekerjaan_id' => request('pekerjaan'),
                    't_dukuh_id' => request('dukuh'),
                    'rt' => request('rt'),
                    't_agama_id' => request('agama'),
                    'tempat_lahir' => request('tempat_lahir'),
                    'tgl_lahir' => request('tanggal_lahir'),
                    'jeniskelamin' => request('jenis_kelamin'),
                    'nik' => request('nik'),
                    'no_kk' => request('no_kk'),
                    'statusperkawinan' => request('status_perkawinan'),
                    // 'lamp_ktp'=> $filenya1,
                    'role' => 'unverified'
                ]);
            }

            if ($user) {
                return redirect()->back()->with('success', 'Edit profil dan data diri berhasil.');
            } else {
                return redirect()->back()->with('errors', 'Edit profil dan data diri gagal, harap ulangi kembali.');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
