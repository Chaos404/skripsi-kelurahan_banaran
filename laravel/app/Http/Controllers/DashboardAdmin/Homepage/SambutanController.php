<?php

namespace App\Http\Controllers\DashboardAdmin\Homepage;

use App\Http\Controllers\Controller;
use App\Model\Homepage\t_sambutan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SambutanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.homepage.sambutan', [
            'sambutan' => t_sambutan::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sambutan' => ['required', 'string']
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            $sambutan = t_sambutan::insert([
                'sambutan' => request('sambutan')
            ]);

            if($sambutan){
                return redirect()->back()->with('success', 'Tambah sambutan berhasil.');
            } else {
                return redirect()->back()->with('errors', 'Tambah sambutan gagal, harap ulangi kembali.');
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array(
            'id'=>$id,
        );

        $data = array_merge($data, $request->all());

        $validator = Validator::make($data, [
            'id'=>'required|numeric|exists:t_sambutans,id',
            'sambutan' => ['required', 'string']
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            $sambutan = t_sambutan::where('id',$id)->update([
                'sambutan'=> request('sambutan')
            ]);

            if ($sambutan) {
                return redirect()->back()->with('success', 'Edit sambutan berhasil.');
            } else {
                return redirect()->back()->with('errors', 'Edit sambutan gagal, harap ulangi kembali.');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
