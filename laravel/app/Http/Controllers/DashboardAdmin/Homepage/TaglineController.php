<?php

namespace App\Http\Controllers\DashboardAdmin\Homepage;

use App\Http\Controllers\Controller;
use App\Model\Homepage\t_tagline;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TaglineController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.homepage.tagline', [
            'tagline' => t_tagline::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'judul' => ['required', 'string', 'max:255'],
            'deskripsi' => ['required', 'string', 'max:255']
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            $tagline = t_tagline::insert([
                'tagline' => request('judul'),
                'deskripsi' => request('deskripsi')
            ]);

            if($tagline){
                return redirect()->back()->with('success', 'Tambah tagline berhasil.');
            } else {
                return redirect()->back()->with('errors', 'Tambah tagline gagal, harap ulangi kembali.');
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array(
            'id'=>$id,
        );

        $data = array_merge($data, $request->all());

        $validator = Validator::make($data, [
            'id'=>'required|numeric|exists:t_taglines,id',
            'judul' => ['required', 'string','max:2048'],
            'deskripsi' => ['required', 'string','max:2048']
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            $tagline = t_tagline::where('id',$id)->update([
                'tagline'=> request('judul'),
                'deskripsi' => request('deskripsi')
            ]);

            if ($tagline) {
                return redirect()->back()->with('success', 'Edit tagline berhasil.');
            } else {
                return redirect()->back()->with('errors', 'Edit tagline gagal, harap ulangi kembali.');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
