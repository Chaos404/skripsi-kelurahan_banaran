<?php

namespace App\Http\Controllers\DashboardAdmin\Homepage;

use App\Http\Controllers\Controller;
use App\Model\Homepage\t_banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BannerutamaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.homepage.bannerutama', [
            'banner' => t_banner::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'banner' => ['required', 'file', 'image', 'mimes:jpeg,png,jpg', 'max:2048'],
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            // Available alpha caracters
            $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

            // generate a pin based on 2 * 7 digits + a random character
            $pin = mt_rand(1000000, 9999999)
                . mt_rand(1000000, 9999999)
                . $characters[rand(0, strlen($characters) - 1)];

            $file = $request->file('banner');

            if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file->getClientOriginalName())) {
                return redirect()->back()->with('errors','invalid file name.');
            }
            $folder_destination ='public/assets/img/homepage/bannerutama';

            $filenya = str_shuffle($pin) . '-' . $file->getClientOriginalName();
            $filetowrite = $folder_destination . '/' . $filenya;
            $file->move($folder_destination, $filetowrite);

            $banner = t_banner::insert([
                'banner' => $filenya
            ]);

            if($banner){
                return redirect()->back()->with('success', 'Upload banner utama berhasil.');
            } else {
                return redirect()->back()->with('errors', 'Upload banner utama gagal, harap ulangi kembali.');
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array(
            'id'=>$id,
        );

        $data = array_merge($data, $request->all());

        $validator = Validator::make($data, [
            'id'=>'required|numeric|exists:t_banners,id',
            'banner' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048']
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            $banner = t_banner::where('id',$id)->first();
            
            // check file exist
            if (file_exists('public/assets/img/homepage/bannerutama/'.$banner['banner']) AND $banner['banner'] != "") {
                unlink('public/assets/img/homepage/bannerutama/'.$banner['banner']);
            }

            // Upload and change file
            $file = $request->file('banner');

            if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $file->getClientOriginalName())) {
                return redirect()->back()->with('errors','invalid file name.');
            }

            $folder_destination = 'public/assets/img/homepage/bannerutama';

            // Available alpha caracters
            $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

            // generate a pin based on 2 * 7 digits + a random character
            $pin = mt_rand(1000000, 9999999)
                . mt_rand(1000000, 9999999)
                . $characters[rand(0, strlen($characters) - 1)];

            $filenya = str_shuffle($pin) . '-' . $file->getClientOriginalName();
            $filetowrite = $folder_destination . '/' . $filenya;
            $file->move($folder_destination, $filetowrite);      
            
            
            $banner = t_banner::where('id',$id)->update([
                'banner'=> $filenya
            ]);

            if ($banner) {
                return redirect()->back()->with('success', 'Edit banner utama berhasil.');
            } else {
                return redirect()->back()->with('errors', 'Edit banner utama gagal, harap ulangi kembali.');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
