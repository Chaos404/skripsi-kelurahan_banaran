<?php

namespace App\Http\Controllers\DashboardAdmin\Berita;

use App\Http\Controllers\Controller;
use App\Model\Berita\t_berita;
use App\Model\Berita\t_kategoriberita;
use Illuminate\Http\Request;
use Illuminate\Support\Str; 
use Illuminate\Support\Facades\Validator;

class BeritaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.berita.kontenberita', [
            'berita' => t_berita::orderBy('id', 'DESC')->get(),
            'kategori' => t_kategoriberita::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.berita.createkontenberita', [
            'berita' => t_berita::all(),
            'kategori' => t_kategoriberita::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'judul' => ['required', 'string', 'max:255'],
            'kategori' => ['required', 'string', 'max:255'],
            'gambar' => ['required', 'file', 'image', 'mimes:jpeg,png,jpg', 'max:2048'],
            'deskripsi' => ['required', 'min:200']
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            // Available alpha caracters
            $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

            // generate a pin based on 2 * 7 digits + a random character
            $pin = mt_rand(1000000, 9999999)
                . mt_rand(1000000, 9999999)
                . $characters[rand(0, strlen($characters) - 1)];

            $file = $request->file('gambar');

            if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file->getClientOriginalName())) {
                return redirect()->back()->with('errors','invalid file name.');
            }
            $folder_destination ='public/assets/1117942';

            $filenya = str_shuffle($pin) . '-' . $file->getClientOriginalName();
            $filetowrite = $folder_destination . '/' . $filenya;
            $file->move($folder_destination, $filetowrite);

            $berita = t_berita::insert([
                'judul' => request('judul'),
                'slug' => Str::slug($request->judul),
                'id_kategoriberitas' => request('kategori'),
                'image' => $filenya,
                'deskripsi' => request('deskripsi')
            ]);

            if($berita){
                return redirect('dashboard-admin/berita')->with('success', 'Tambah berita berhasil.');
            } else {
                return redirect()->back()->with('errors', 'Tambah berita gagal, harap ulangi kembali.');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.berita.editkontenberita', [
            'berita' => t_berita::where('id', $id)->firstOrFail(),
            'kategori' => t_kategoriberita::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array(
            'id'=>$id,
        );

        $data = array_merge($data, $request->all());

        if ($request->hasFile('gambar')){
            $validator = Validator::make($data, [
                'id'=>'required|numeric|exists:t_beritas,id',
                'judul' => ['required', 'string', 'max:255'],
                'kategori' => ['required', 'string', 'max:255'],
                'gambar' => ['required', 'file', 'image', 'mimes:jpeg,png,jpg', 'max:2048'],
                'deskripsi' => ['required', 'string']
            ]);
    
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            } else {
                $berita = t_berita::where('id',$id)->first();
                // check file exist
                if (file_exists('public/assets/1117942/'.$berita['image']) AND $berita['image'] != "") {
                    unlink('public/assets/1117942/'.$berita['image']);
                }
    
                // Upload and change file
                $file = $request->file('gambar');
    
                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $file->getClientOriginalName())) {
                    return redirect()->back()->with('errors','invalid file name.');
                }
    
                $folder_destination = 'public/assets/1117942';
    
                 // Available alpha caracters
                $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

                // generate a pin based on 2 * 7 digits + a random character
                $pin = mt_rand(1000000, 9999999)
                    . mt_rand(1000000, 9999999)
                    . $characters[rand(0, strlen($characters) - 1)];

                $filenya = str_shuffle($pin) . '-' . $file->getClientOriginalName();
                $filetowrite = $folder_destination . '/' . $filenya;
                $file->move($folder_destination, $filetowrite);                 
                
                $berita = t_berita::where('id',$id)->update([
                    'judul' => request('judul'),
                    'slug' => Str::slug($request->judul),
                    'id_kategoriberitas' => request('kategori'),
                    'image' => $filenya,
                    'deskripsi' => request('deskripsi')
                ]);
    
                if ($berita) {
                    return redirect('dashboard-admin/berita')->with('success', 'Edit berita berhasil.');
                } else {
                    return redirect()->back()->with('errors', 'Edit berita gagal, harap ulangi kembali.');
                } 
            }
        } else {
                $validator = Validator::make($data, [
                    'id'=>'required|numeric|exists:t_beritas,id',
                    'judul' => ['required', 'string', 'max:255'],
                    'kategori' => ['required', 'string', 'max:255'],
                    'deskripsi' => ['required', 'string']
                ]);
        
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator);
                } else {
                    $berita = t_berita::where('id',$id)->first();
                    
                    $berita = t_berita::where('id',$id)->update([
                        'judul' => request('judul'),
                        'slug' => Str::slug($request->judul),
                        'id_kategoriberitas' => request('kategori'),
                        'deskripsi' => request('deskripsi')
                    ]);
        
                    if ($berita) {
                        return redirect('dashboard-admin/berita')->with('success', 'Edit berita berhasil.');
                    } else {
                        return redirect()->back()->with('errors', 'Edit berita gagal, harap ulangi kembali.');
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = array(
            'id'=>$id
        );

        $validator = Validator::make($data, [
            'id'=>'required|numeric|exists:t_beritas,id',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $berita = t_berita::where('id',$id)->first();

        // check file exist
        if (file_exists('public/assets/1117942'.$berita['image'])) {
            unlink('public/assets/1117942'.$berita['image']);
        }

        $delete= t_berita::where('id', $id)->delete();

        if ($delete) {
            return redirect()->back()->with('success','Berita berhasil dihapus.');
        } else {
            return redirect()->back()->with('errors','Berita gagal dihapus, harap ulangi kembali.');
        }
    }
}
