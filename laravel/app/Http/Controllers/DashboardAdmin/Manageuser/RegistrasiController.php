<?php

namespace App\Http\Controllers\DashboardAdmin\Manageuser;

use App\Http\Controllers\Controller;
use App\Model\Agama;
use App\Model\Dukuh;
use App\Model\Pekerjaan;
use App\Model\Rt;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Rules\MatchOldPassword;

class RegistrasiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.manageuser.register', [
            'user'=> User::where('role', 'petugas')->get(),
            'pekerjaan' => Pekerjaan::all(),
            'dukuh' => Dukuh::all(),
            'rt' => Rt::all(),
            'agama' => Agama::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'email' => 'required|string|email|max:255|unique:t_users',
            'password' => 'required|string|min:8|confirmed',
            'password_confirmation' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            $petugas = User::insert([
                'email' => request('email'),
                'password' => Hash::make(request('password')),
                'role' => request('role')
            ]);

            if($petugas){
                return redirect()->back()->with('success', 'Tambah petugas berhasil.');
            } else{
                return redirect()->back()->with('errors', 'Tambah petugas gagal, harap coba kembali.');
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ]);
        $user = User::find(auth()->user()->id)->update([            
            'password'=> Hash::make($request->new_password)
            ]);
        // dd('Password change successfully.');

        if ($user) {
            return redirect()->back()->with('success', 'Update password successfully.');
        } else {
            return redirect()->back()->with('errors', 'The update password failed, please try again.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = array(
            'id'=>$id
        );

        $validator = Validator::make($data, [
            'id'=>'required|numeric|exists:t_users,id',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $user = User::where('id',$id)->first();

        $delete=User::where('id', $id)->delete();
        if ($delete) {
            return redirect()->back()->with('success','Petugas berhasil dihapus.');
        } else {
            return redirect()->back()->with('errors','Petugas gagal di hapus, harap coba kembali.');
        }
    }
}
