<?php

namespace App\Http\Controllers\DashboardAdmin;

use App\Http\Controllers\Controller;
use App\Model\Berita\t_berita;
use App\Model\Profil\t_perangkatdesa;
use Illuminate\Http\Request;
use App\User;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.index', [
            'petugas'=> User::where('role', 'petugas')->get(),

            'berita' => t_berita::orderBy('id', 'DESC')->limit(4)->get(),
            'pemerintahan' => t_perangkatdesa::orderBy('id', 'ASC')->limit(4)->get(),
        ]);
    }
}
