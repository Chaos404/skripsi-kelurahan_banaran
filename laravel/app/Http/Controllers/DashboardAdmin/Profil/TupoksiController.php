<?php

namespace App\Http\Controllers\DashboardAdmin\Profil;

use App\Http\Controllers\Controller;
use App\Model\Profil\t_tupoksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TupoksiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.profil.tupoksi', [
            'tupoksi' => t_tupoksi::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tupoksi' => ['required', 'string', 'max:255']
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            $tupoksi = t_tupoksi::insert([
                'tupoksi' => request('tupoksi'),
            ]);

            if($tupoksi){
                return redirect()->back()->with('success', 'Tambah tupoksi berhasil.');
            } else {
                return redirect()->back()->with('errors', 'Tambah tupoksi gagal, harap ulangi kembali.');
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array(
            'id'=>$id,
        );

        $data = array_merge($data, $request->all());

        $validator = Validator::make($data, [
            'id'=>'required|numeric|exists:t_tupoksis,id',
            'tupoksi' => ['required']
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            $tupoksi = t_tupoksi::where('id',$id)->update([
                'tupoksi' => request('tupoksi')
            ]);

            if ($tupoksi) {
                return redirect()->back()->with('success', 'Edit tupoksi berhasil.');
            } else {
                return redirect()->back()->with('errors', 'Edit tupoksi gagal, harap ulangi kembali.');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = array(
            'id'=>$id
        );

        $validator = Validator::make($data, [
            'id'=>'required|numeric|exists:t_tupoksis,id',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $tupoksi = t_tupoksi::where('id', $id)->delete();

        if ($tupoksi) {
            return redirect()->back()->with('success','Tupoksi berhasil dihapus.');
        } else {
            return redirect()->back()->with('errors','Tupoksi gagal dihapus, harap ulangi kembali.');
        }

    }
}
