<?php

namespace App\Http\Controllers\DashboardAdmin\Profil;

use App\Http\Controllers\Controller;
use App\Model\Profil\t_perangkatdesastruktur;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SusunanorganisasiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.profil.susunanorganisasi', [
            'susunan' => t_perangkatdesastruktur::all()
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array(
            'id'=>$id,
        );

        $data = array_merge($data, $request->all());

        $validator = Validator::make($data, [
            'id'=>'required|numeric|exists:t_perangkatdesastrukturs,id',
            'image' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048']
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            $image = t_perangkatdesastruktur::where('id',$id)->first();
            
            // check file exist
            if (file_exists('public/assets/img/profil/susunanorganisasi/'.$image['image']) AND $image['image'] != "") {
                unlink('public/assets/img/profil/susunanorganisasi/'.$image['image']);
            }

            // Upload and change file
            $file = $request->file('image');

            if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $file->getClientOriginalName())) {
                return redirect()->back()->with('errors','invalid file name.');
            }

            $folder_destination = 'public/assets/img/profil/susunanorganisasi';

            // Available alpha caracters
            $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

            // generate a pin based on 2 * 7 digits + a random character
            $pin = mt_rand(1000000, 9999999)
                . mt_rand(1000000, 9999999)
                . $characters[rand(0, strlen($characters) - 1)];

            $filenya = str_shuffle($pin) . '-' . $file->getClientOriginalName();
            $filetowrite = $folder_destination . '/' . $filenya;
            $file->move($folder_destination, $filetowrite);      
            
            
            $image = t_perangkatdesastruktur::where('id',$id)->update([
                'image'=> $filenya
            ]);

            if ($image) {
                return redirect()->back()->with('success', 'Edit susunan organisasi berhasil.');
            } else {
                return redirect()->back()->with('errors', 'Edit susunan organisasi gagal, harap ulangi kembali.');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
