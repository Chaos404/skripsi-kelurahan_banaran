<?php

namespace App\Http\Controllers\DashboardAdmin\Profil;

use App\Http\Controllers\Controller;
use App\Model\Profil\t_visimisi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VisimisiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.profil.visidanmisi', [
          'visi' => t_visimisi::where('jenis', 'visi')->get(),
          'misi' => t_visimisi::where('jenis', 'misi')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->jenis == "visi") {
            $validator = Validator::make($request->all(), [
                'deskripsi' => ['required', 'string', 'max:255'],
                'jenis' => ['required', 'string', 'max:255']
            ]);
    
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            } else {
                $visi = t_visimisi::insert([
                    'jenis' => request('jenis'),
                    'deskripsi' => request('deskripsi')
                ]);
    
                if($visi){
                    return redirect()->back()->with('success', 'Tambah visi berhasil.');
                } else {
                    return redirect()->back()->with('errors', 'Tambah visi gagal, harap ulangi kembali.');
                }
            }
        } else {
            $validator = Validator::make($request->all(), [
                'deskripsi' => ['required', 'string', 'max:255'],
                'jenis' => ['required', 'string', 'max:255']
            ]);
    
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            } else {
                $misi = t_visimisi::insert([
                    'jenis' => request('jenis'),
                    'deskripsi' => request('deskripsi')
                ]);
    
                if($misi){
                    return redirect()->back()->with('success', 'Tambah misi berhasil.');
                } else {
                    return redirect()->back()->with('errors', 'Tambah misi gagal, harap ulangi kembali.');
                }
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array(
            'id'=>$id,
        );

        $data = array_merge($data, $request->all());

        if ($request->jenis == "visi") {
            $validator = Validator::make($data, [
                'id'=>'required|numeric|exists:t_visimisis,id',
                'deskripsi' => ['required', 'string','max:2048']
            ]);
            
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            } else {
                $visi = t_visimisi::where('id',$id)->where('jenis', 'visi')->update([
                    'deskripsi' => request('deskripsi')
                ]);
    
                if ($visi) {
                    return redirect()->back()->with('success', 'Edit visi berhasil.');
                } else {
                    return redirect()->back()->with('errors', 'Edit visi gagal, harap ulangi kembali.');
                }
            }
        } else {
            $validator = Validator::make($data, [
                'id'=>'required|numeric|exists:t_visimisis,id',
                'deskripsi' => ['required', 'string','max:2048']
            ]);
            
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            } else {
                $misi = t_visimisi::where('id',$id)->where('jenis', 'misi')->update([
                    'deskripsi' => request('deskripsi')
                ]);
    
                if ($misi) {
                    return redirect()->back()->with('success', 'Edit misi berhasil.');
                } else {
                    return redirect()->back()->with('errors', 'Edit misi gagal, harap ulangi kembali.');
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = array(
            'id'=>$id
        );

        $validator = Validator::make($data, [
            'id'=>'required|numeric|exists:t_visimisis,id',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $delete= t_visimisi::where('id', $id)->delete();

        if ($delete) {
            return redirect()->back()->with('success','Visi atau Misi berhasil dihapus.');
        } else {
            return redirect()->back()->with('errors','Visi atau Misi gagal dihapus, harap ulangi kembali.');
        }

    }
}
