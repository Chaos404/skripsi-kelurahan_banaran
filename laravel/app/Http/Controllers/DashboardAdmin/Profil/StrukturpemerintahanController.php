<?php

namespace App\Http\Controllers\DashboardAdmin\Profil;

use App\Http\Controllers\Controller;
use App\Model\Profil\t_jabatan;
use App\Model\Profil\t_perangkatdesa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StrukturpemerintahanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.profil.strukturpemerintahan', [
            'jabatan' => t_jabatan::all(),
            'perangkatdesa' => t_perangkatdesa::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => ['required', 'string', 'max:255'],
            // 'nik' => ['required', 'numeric', 'unique:t_perangkatdesas', 'digits_between:16,16'],
            // 'tempat_lahir' => ['required', 'string', 'max:255'],
            // 'tanggal_lahir' => ['required', 'string', 'max:255'],
            'foto' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
            // 'nomor_hp' => ['required', 'string', 'max:13'],
            'jenis_kelamin' => ['required', 'string', 'max:255'],
            // 'pendidikan_terakhir' => ['required', 'string', 'max:255'],
            // 'sk_pengangkatan' => ['required','mimetypes:application/pdf','max:10000'],
            // 'tanggal_pengangkatan' => ['required', 'string', 'max:255'],
            'jabatan' => ['required', 'string', 'max:255']
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            // Available alpha caracters
            $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

            // generate a pin based on 2 * 7 digits + a random character
            $pin = mt_rand(1000000, 9999999)
                . mt_rand(1000000, 9999999)
                . $characters[rand(0, strlen($characters) - 1)];


            $file = $request->file('foto');

            if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file->getClientOriginalName())) {
                return redirect()->back()->with('errors','invalid file name.');
            }
            $folder_destination ='public/assets/img/profil/foto';

            $filenya = str_shuffle($pin) . '-' . $file->getClientOriginalName();
            $filetowrite = $folder_destination . '/' . $filenya;
            $file->move($folder_destination, $filetowrite);

            // $file1 = $request->file('sk_pengangkatan');

            // if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file1->getClientOriginalName())) {
            //     return redirect()->back()->with('errors','invalid file name.');
            // }
            // $folder_destination ='public/assets/img/profil/sk';

            // $filenya1 = str_shuffle($pin) . '-' . $file1->getClientOriginalName();
            // $filetowrite = $folder_destination . '/' . $filenya1;
            // $file1->move($folder_destination, $filetowrite);


            $perangkatdesa = t_perangkatdesa::insert([
                'nama' => request('nama'),
                'nik' => request('nik'),
                'image' => $filenya,
                'tempat_lahir' => request('tempat_lahir'),
                'tgl_lahir' => request('tanggal_lahir'),
                'nohp' => request('nomor_hp'),
                'jeniskelamin' => request('jenis_kelamin'),
                'pendidikan_terakhir' => request('pendidikan_terakhir'),
                // 'sk_pengangkatan' => $filenya1,
                'tanggal_pengangkatan' => request('tanggal_pengangkatan'),
                'jabatans_id' => request('jabatan'),
            ]);

            if($perangkatdesa){
                return redirect()->back()->with('success', 'Tambah perangakt desa berhasil.');
            } else {
                return redirect()->back()->with('errors', 'Tambah perangakt desa gagal, harap ulangi kembali.');
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array(
            'id'=>$id,
        );

        $data = array_merge($data, $request->all());

        if ($request->hasFile('foto')) {
            $validator = Validator::make($data, [
                'id'=>'required|numeric|exists:t_perangkatdesas,id',
                // 'nama' => ['required', 'string', 'max:255'],
                // 'nik' => ['required', 'numeric', 'digits_between:16,16'],
                // 'tempat_lahir' => ['required', 'string', 'max:255'],
                // 'tanggal_lahir' => ['required', 'string', 'max:255'],
                // 'foto' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
                // 'nomor_hp' => ['required', 'string', 'max:13'],
                // 'jenis_kelamin' => ['required', 'string', 'max:255'],
                // 'pendidikan_terakhir' => ['required', 'string', 'max:255'],
                // 'tanggal_pengangkatan' => ['required', 'string', 'max:255'],
                // 'jabatan' => ['required', 'string', 'max:255']

                'nama' => ['required', 'string', 'max:255'],
                // 'nik' => ['required', 'numeric', 'unique:t_perangkatdesas', 'digits_between:16,16'],
                // 'tempat_lahir' => ['required', 'string', 'max:255'],
                // 'tanggal_lahir' => ['required', 'string', 'max:255'],
                'foto' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
                // 'nomor_hp' => ['required', 'string', 'max:13'],
                'jenis_kelamin' => ['required', 'string', 'max:255'],
                // 'pendidikan_terakhir' => ['required', 'string', 'max:255'],
                // 'sk_pengangkatan' => ['required','mimetypes:application/pdf','max:10000'],
                // 'tanggal_pengangkatan' => ['required', 'string', 'max:255'],
                'jabatan' => ['required', 'string', 'max:255']
            ]);
        } elseif ($request->hasFile('sk_pengangkatan')) {
            $validator = Validator::make($data, [
                'id'=>'required|numeric|exists:t_perangkatdesas,id',
                // 'nama' => ['required', 'string', 'max:255'],
                // 'nik' => ['required', 'numeric', 'digits_between:16,16'],
                // 'tempat_lahir' => ['required', 'string', 'max:255'],
                // 'tanggal_lahir' => ['required', 'string', 'max:255'],
                // 'nomor_hp' => ['required', 'string', 'max:13'],
                // 'jenis_kelamin' => ['required', 'string', 'max:255'],
                // 'pendidikan_terakhir' => ['required', 'string', 'max:255'],
                // 'sk_pengangkatan' => ['required','mimetypes:application/pdf','max:10000'],
                // 'tanggal_pengangkatan' => ['required', 'string', 'max:255'],
                // 'jabatan' => ['required', 'string', 'max:255']

                'nama' => ['required', 'string', 'max:255'],
                // 'nik' => ['required', 'numeric', 'unique:t_perangkatdesas', 'digits_between:16,16'],
                // 'tempat_lahir' => ['required', 'string', 'max:255'],
                // 'tanggal_lahir' => ['required', 'string', 'max:255'],
                // 'foto' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
                // 'nomor_hp' => ['required', 'string', 'max:13'],
                'jenis_kelamin' => ['required', 'string', 'max:255'],
                // 'pendidikan_terakhir' => ['required', 'string', 'max:255'],
                // 'sk_pengangkatan' => ['required','mimetypes:application/pdf','max:10000'],
                // 'tanggal_pengangkatan' => ['required', 'string', 'max:255'],
                'jabatan' => ['required', 'string', 'max:255']
            ]);
        } else {
            $validator = Validator::make($data, [
                'id'=>'required|numeric|exists:t_perangkatdesas,id',
                // 'nama' => ['required', 'string', 'max:255'],
                // 'nik' => ['required', 'numeric', 'digits_between:16,16'],
                // 'tempat_lahir' => ['required', 'string', 'max:255'],
                // 'tanggal_lahir' => ['required', 'string', 'max:255'],
                // 'nomor_hp' => ['required', 'string', 'max:13'],
                // 'jenis_kelamin' => ['required', 'string', 'max:255'],
                // 'pendidikan_terakhir' => ['required', 'string', 'max:255'],
                // 'tanggal_pengangkatan' => ['required', 'string', 'max:255'],
                // 'jabatan' => ['required', 'string', 'max:255']

                'nama' => ['required', 'string', 'max:255'],
                // 'nik' => ['required', 'numeric', 'unique:t_perangkatdesas', 'digits_between:16,16'],
                // 'tempat_lahir' => ['required', 'string', 'max:255'],
                // 'tanggal_lahir' => ['required', 'string', 'max:255'],
                // 'foto' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
                // 'nomor_hp' => ['required', 'string', 'max:13'],
                'jenis_kelamin' => ['required', 'string', 'max:255'],
                // 'pendidikan_terakhir' => ['required', 'string', 'max:255'],
                // 'sk_pengangkatan' => ['required','mimetypes:application/pdf','max:10000'],
                // 'tanggal_pengangkatan' => ['required', 'string', 'max:255'],
                'jabatan' => ['required', 'string', 'max:255']
            ]);
        }

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            if ($request->hasFile('foto')) {
                // Foto
                $foto = t_perangkatdesa::where('id',$id)->first();
                // check file exist
                if (file_exists('public/assets/img/profil/foto/'.$foto['image']) AND $foto['image'] != "") {
                    unlink('public/assets/img/profil/foto/'.$foto['image']);
                }

                // Upload and change file
                $file = $request->file('foto');

                // Available alpha caracters
                $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

                // generate a pin based on 2 * 7 digits + a random character
                $pin = mt_rand(1000000, 9999999)
                    . mt_rand(1000000, 9999999)
                    . $characters[rand(0, strlen($characters) - 1)];
                    

                // $filetowrite = $folder_destination . '/' .str_shuffle($pin) . '-' . $file->getClientOriginalName();
                
                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $file->getClientOriginalName())) {
                    return redirect()->back()->with('errors','invalid file name.');
                }

                $folder_destination = 'public/assets/img/profil/foto';

                $filenya = str_shuffle($pin) . '-' . $file->getClientOriginalName();
                $filetowrite = $folder_destination . '/' . $filenya;
                $file->move($folder_destination, $filetowrite); 

                $perangkatdesa = t_perangkatdesa::where('id',$id)->update([
                    'nama' => request('nama'),
                    'nik' => request('nik'),
                    'image' => $filenya,
                    'tempat_lahir' => request('tempat_lahir'),
                    'tgl_lahir' => request('tanggal_lahir'),
                    'nohp' => request('nomor_hp'),
                    'jeniskelamin' => request('jenis_kelamin'),
                    'pendidikan_terakhir' => request('pendidikan_terakhir'),
                    'tanggal_pengangkatan' => request('tanggal_pengangkatan'),
                    'jabatans_id' => request('jabatan'),
                ]);
            } elseif ($request->hasFile('sk_pengangkatan')) {
                // SK
                $sk = t_perangkatdesa::where('id',$id)->first();
                // check file exist
                if (file_exists('public/assets/img/profil/sk/'.$sk['sk_pengangkatan']) AND $sk['sk_pengangkatan'] != "") {
                    unlink('public/assets/img/profil/sk/'.$sk['sk_pengangkatan']);
                }

                // Upload and change file
                $file1 = $request->file('sk_pengangkatan');

                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $file1->getClientOriginalName())) {
                    return redirect()->back()->with('errors','invalid file name.');
                }

                $folder_destination = 'public/assets/img/profil/sk';

                // Available alpha caracters
                $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

                // generate a pin based on 2 * 7 digits + a random character
                $pin = mt_rand(1000000, 9999999)
                    . mt_rand(1000000, 9999999)
                    . $characters[rand(0, strlen($characters) - 1)];

                $filenya1 = str_shuffle($pin) . '-' . $file1->getClientOriginalName();
                $filetowrite = $folder_destination . '/' . $filenya1;
                $file1->move($folder_destination, $filetowrite);

                $perangkatdesa = t_perangkatdesa::where('id',$id)->update([
                    'nama' => request('nama'),
                    'nik' => request('nik'),
                    'tempat_lahir' => request('tempat_lahir'),
                    'tgl_lahir' => request('tanggal_lahir'),
                    'nohp' => request('nomor_hp'),
                    'jeniskelamin' => request('jenis_kelamin'),
                    'pendidikan_terakhir' => request('pendidikan_terakhir'),
                    'sk_pengangkatan' => $filenya1,
                    'tanggal_pengangkatan' => request('tanggal_pengangkatan'),
                    'jabatans_id' => request('jabatan'),
                ]);
            } else {
                $perangkatdesa = t_perangkatdesa::where('id',$id)->update([
                    'nama' => request('nama'),
                    'nik' => request('nik'),
                    'tempat_lahir' => request('tempat_lahir'),
                    'tgl_lahir' => request('tanggal_lahir'),
                    'nohp' => request('nomor_hp'),
                    'jeniskelamin' => request('jenis_kelamin'),
                    'pendidikan_terakhir' => request('pendidikan_terakhir'),
                    'tanggal_pengangkatan' => request('tanggal_pengangkatan'),
                    'jabatans_id' => request('jabatan'),
                ]);
            }

            if ($perangkatdesa) {
                return redirect()->back()->with('success', 'Edit perangkat desa successfully.');
            } else {
                return redirect()->back()->with('errors', 'The perangkat desa failed to edit, please try again.');
            }
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = array(
            'id'=>$id
        );

        $validator = Validator::make($data, [
            'id'=>'required|numeric|exists:t_perangkatdesas,id',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $perangkatdesa =  t_perangkatdesa::where('id',$id)->first();

        // check file exist
        if (file_exists('public/assets/img/profil/foto/'.$perangkatdesa['image'])) {
            unlink('public/assets/img/profil/foto/'.$perangkatdesa['image']);
        }

        // check file exist
        if (file_exists('public/assets/img/profil/sk/'.$perangkatdesa['sk_pengangkatan'])) {
            unlink('public/assets/img/profil/sk/'.$perangkatdesa['sk_pengangkatan']);
        }

        $delete= t_perangkatdesa::where('id', $id)->delete();
        
        if ($delete) {
            return redirect()->back()->with('success','Perangkat desa berhasil dihapus.');
        } else {
            return redirect()->back()->with('errors','Perangkat desa gagal dihapus, harap ulangi kembali.');
        }
    }
}
