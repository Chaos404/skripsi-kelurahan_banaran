<?php

namespace App\Http\Controllers\DashboardAdmin\Profil;

use App\Http\Controllers\Controller;
use App\Model\Profil\t_kelurahan;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class KependudukanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.profil.kependudukan', [
            'kependudukan' => t_kelurahan::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array(
            'id'=>$id,
        );

        $data = array_merge($data, $request->all());

        $validator = Validator::make($data, [
            'id'=>'required|numeric|exists:t_kelurahans,id',
            'jumlah_penduduk' => ['required', 'numeric'],
            'penduduk_laki' => ['required', 'numeric'],
            'penduduk_perempuan' => ['required', 'numeric']
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            $link = t_kelurahan::where('id',$id)->update([
                'jumlah_penduduk' => request('jumlah_penduduk'),
                'penduduk_laki' => request('penduduk_laki'),
                'penduduk_perempuan' => request('penduduk_perempuan')
            ]);

            if ($link) {
                return redirect()->back()->with('success', 'Edit Kependudukan berhasil.');
            } else {
                return redirect()->back()->with('errors', 'Edit Kependudukan gagal, harap ulangi kembali.');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
