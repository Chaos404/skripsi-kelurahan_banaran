<?php

namespace App\Http\Controllers\Landingpage;

use App\Http\Controllers\Controller;
use App\Model\Profil\t_kelurahan;
use App\Model\Profil\t_tupoksi;

class TupoksiController extends Controller
{
    public function index(){
        return view('landingpage.tupoksi', [
            'tupoksi' => t_tupoksi::all(),

            'kelurahan' => t_kelurahan::all()
        ]);
    }
}
