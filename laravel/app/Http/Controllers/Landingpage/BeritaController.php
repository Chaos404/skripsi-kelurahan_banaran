<?php

namespace App\Http\Controllers\Landingpage;

use App\Http\Controllers\Controller;
use App\Model\Berita\t_berita;
use App\Model\Berita\t_kategoriberita;
use App\Model\Profil\t_kelurahan;
use Illuminate\Http\Request;

class BeritaController extends Controller
{
    public function index(){
        return view('landingpage.berita', [
            'berita' => t_berita::orderBy('id', 'DESC')->paginate(6),
            'paginate' => t_berita::orderBy('id', 'DESC')->paginate(6),

            'terkinis' => t_berita::where('id_kategoriberitas', 1)->orderBy('id', 'DESC')->limit(3)->get(),
            'informasis' => t_berita::where('id_kategoriberitas', 2)->orderBy('id', 'DESC')->limit(4)->get(),

            'kelurahan' => t_kelurahan::all()
        ]);
    }

    public function search(Request $request){
        $kategori = t_kategoriberita::all();
        $paginate = t_berita::orderBy('id', 'DESC')->paginate(6);
        $terkinis = t_berita::where('id_kategoriberitas', 1)->orderBy('id', 'DESC')->limit(3)->get();
        $informasis = t_berita::where('id_kategoriberitas', 2)->orderBy('id', 'DESC')->limit(4)->get();
        $kelurahan = t_kelurahan::all();

        $q = $request->input('search');
        $berita = t_berita::orderBy('id', 'DESC')
            ->where('judul', 'LIKE', '%' .$q. '%')
            ->orWhere('deskripsi', 'LIKE', '%' .$q. '%')
            ->paginate(6);
        return view('landingpage.search.searchBerita', compact('berita', 'kategori', 'paginate', 'terkinis', 'informasis', 'kelurahan'));
    }
}
