<?php

namespace App\Http\Controllers\Landingpage;

use App\Http\Controllers\Controller;
use App\Model\Berita\t_berita;
use App\Model\Berita\t_kategoriberita;
use App\Model\Profil\t_kelurahan;

class DetailberitaController extends Controller
{
    public function index($slug){
        return view('landingpage.detail-berita', [
            'berita' => t_berita::where('slug', $slug)->firstOrFail(),
            'kategori' => t_kategoriberita::all(),

            'terkinis' => t_berita::where('id_kategoriberitas', 1)->orderBy('id', 'DESC')->limit(3)->get(),
            'informasis' => t_berita::where('id_kategoriberitas', 2)->orderBy('id', 'DESC')->limit(4)->get(),

            'kelurahan' => t_kelurahan::all()
        ]);
    }
}
