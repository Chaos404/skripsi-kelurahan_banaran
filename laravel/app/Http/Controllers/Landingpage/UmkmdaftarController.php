<?php

namespace App\Http\Controllers\Landingpage;

use App\Http\Controllers\Controller;
use App\Model\Agama;
use App\Model\Dukuh;
use App\Model\Keperluan;
use App\Model\Pekerjaan;
use App\Model\Profil\t_kelurahan;
use App\Model\Rt;
use App\Model\Umkm;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class UmkmdaftarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('landingpage.umkm_daftar',[
            'user'=> User::all(),
            'dukuh' => Dukuh::all(),
            'rt' => Rt::all(),

            'kelurahan' => t_kelurahan::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->role == "unverified"){
            return redirect()->back()->with('warning', 'Pengajuan gagal, akun anda belum terverifikasi.');
        } else {
            $validator = Validator::make($request->all(), [
                'merk_usaha' => ['required', 'max:255'],
                'nomor_siup' => ['required', 'max:255'],
                'nomor_npwp' => ['required', 'max:255'],
                'nomor_whatsapp' =>['required', 'string', 'max:15'],
                'foto_produk' => ['required', 'file', 'image', 'mimes:jpeg,png,jpg', 'max:2048'],
                'lampiran_siup' => ['required', 'file', 'image', 'mimes:jpeg,png,jpg', 'max:2048'],
                'lampiran_npwp' => ['required', 'file', 'image', 'mimes:jpeg,png,jpg', 'max:2048'],
                'keterangan' => ['required', 'min:200']
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            } else {
                    // Available alpha caracters
                    $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

                    // generate a pin based on 2 * 7 digits + a random character
                    $pin = mt_rand(1000000, 9999999)
                        . mt_rand(1000000, 9999999)
                        . $characters[rand(0, strlen($characters) - 1)];
                        
                $file1 = $request->file('foto_produk');

                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file1->getClientOriginalName())) {
                    return redirect()->back()->with('errors','invalid file name.');
                }
                $folder_destination ='public/assets/1119126';

                $filenya1 = str_shuffle($pin) . '-' . $file1->getClientOriginalName();
                $filetowrite = $folder_destination . '/' . $filenya1;
                $file1->move($folder_destination, $filetowrite);

                $file2 = $request->file('lampiran_siup');

                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file2->getClientOriginalName())) {
                    return redirect()->back()->with('errors','invalid file name.');
                }
                $folder_destination ='public/assets/1119126';

                $filenya2 = str_shuffle($pin) . '-' . $file2->getClientOriginalName();
                $filetowrite = $folder_destination . '/' . $filenya2;
                $file2->move($folder_destination, $filetowrite);

                $file3 = $request->file('lampiran_npwp');

                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file3->getClientOriginalName())) {
                    return redirect()->back()->with('errors','invalid file name.');
                }
                $folder_destination ='public/assets/1119126';

                $filenya3 = str_shuffle($pin) . '-' . $file3->getClientOriginalName();
                $filetowrite = $folder_destination . '/' . $filenya3;
                $file3->move($folder_destination, $filetowrite);

                $umkm = Umkm::insert([
                    't_users_id' => request('id_user'),
                    'merk_usaha' => request('merk_usaha'),
                    'slug' => Str::slug($request->merk_usaha, '-'),
                    'no_siup' => request('nomor_siup'),
                    'no_npwp' => request('nomor_npwp'),
                    'no_whatsapp' => '+62'.request('nomor_whatsapp'),
                    'image' => $filenya1,
                    'lamp_siup' => $filenya2,
                    'lamp_npwp' => $filenya3,
                    'keterangan' => request('keterangan')
                ]);

                if($umkm){
                    return redirect()->back()->with('success', 'Pendaftaran UMKM berhasil terkirim, tunggu verifikasi dari petugas.');
                } else {
                    return redirect()->back()->with('errors', 'Pendaftaran UMKM gagal, harap ulangi kembali.');
                }
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}
