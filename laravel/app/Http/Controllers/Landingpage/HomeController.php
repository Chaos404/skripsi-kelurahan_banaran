<?php

namespace App\Http\Controllers\Landingpage;

use App\Http\Controllers\Controller;
use App\Model\Berita\t_berita;
use App\Model\Homepage\t_banner;
use App\Model\Profil\t_perangkatdesa;
use App\Model\Homepage\t_sambutan;
use App\Model\Homepage\t_tagline;
use App\Model\Profil\t_kelurahan;
use App\Model\Profil\t_visimisi;

class HomeController extends Controller
{
    public function index(){
        return view('landingpage.home', [
            'banner1' => t_banner::where('id', 1)->get(),
            'banner2' => t_banner::where('id', 2)->get(),
            'banner3' => t_banner::where('id', 3)->get(),

            'tagline' => t_tagline::all(),

            'kepaladesa' => t_perangkatdesa::where('jabatans_id', 1)->get(),
            'sambutan' => t_sambutan::all(),


            'visi' => t_visimisi::where('jenis', 'visi')->get(),
            'misi' => t_visimisi::where('jenis', 'misi')->get(),

            'berita' => t_berita::orderBy('id', 'DESC')->limit(4)->get(),
            'terkinis' => t_berita::where('id_kategoriberitas', 1)->orderBy('id', 'DESC')->limit(3)->get(),
            'informasis' => t_berita::where('id_kategoriberitas', 2)->orderBy('id', 'DESC')->limit(4)->get(),

            'kelurahan' => t_kelurahan::all()

        ]);
    }
}
