<?php

namespace App\Http\Controllers\Landingpage;

use App\Model\Berita\t_berita;
use App\Model\Umkm;
use App\Http\Controllers\Controller;
use App\Model\Profil\t_kelurahan;

class DetailumkmController extends Controller
{
    public function index($slug){
        $umkm = Umkm::where('slug', $slug)->firstOrFail();
        $terkinis = t_berita::where('id_kategoriberitas', 1)->orderBy('id', 'DESC')->limit(3)->get();
        $informasis = t_berita::where('id_kategoriberitas', 2)->orderBy('id', 'DESC')->limit(4)->get();

        $kelurahan = t_kelurahan::all();

        return view('landingpage.detail-umkm', compact('umkm', 'terkinis', 'informasis', 'kelurahan'));
    }
}
