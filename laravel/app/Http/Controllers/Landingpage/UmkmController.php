<?php

namespace App\Http\Controllers\Landingpage;
use App\Model\Umkm;
use App\Http\Controllers\Controller;
use App\Model\Profil\t_kelurahan;

class UmkmController extends Controller
{
    public function index(){
        return view('landingpage.umkm', [
            'umkm' => Umkm::where('status', 'terverifikasi')->orderBy('created_at', 'DESC')->get(),

            'kelurahan' => t_kelurahan::all()
        ]);
    }
}
