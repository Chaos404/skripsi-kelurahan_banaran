<?php

namespace App\Http\Controllers\Landingpage;

use App\Http\Controllers\Controller;
use App\Model\Profil\t_kelurahan;
use App\Model\Profil\t_visimisi;

class VisimisiController extends Controller
{
    public function index(){
        return view('landingpage.visimisi', [
            'visi' => t_visimisi::where('jenis', 'visi')->get(),
            'misi' => t_visimisi::where('jenis', 'misi')->get(),

            'kelurahan' => t_kelurahan::all()
        ]);
    }
}
