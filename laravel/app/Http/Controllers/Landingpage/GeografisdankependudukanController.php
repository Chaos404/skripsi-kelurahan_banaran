<?php

namespace App\Http\Controllers\Landingpage;

use App\Http\Controllers\Controller;
use App\Model\Profil\t_kelurahan;

class GeografisdankependudukanController extends Controller
{
    public function index(){
        return view('landingpage.geografisdankependudukan' , [


            'kelurahan' => t_kelurahan::all()
        ]);
    }
}
