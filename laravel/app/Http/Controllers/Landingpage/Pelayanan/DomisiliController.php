<?php

namespace App\Http\Controllers\Landingpage\Pelayanan;

use App\Http\Controllers\Controller;
use App\Model\Agama;
use App\Model\Dukuh;
use App\Model\Keperluan;
use App\Model\Pekerjaan;
use App\Model\Profil\t_kelurahan;
use App\Model\Rt;
use App\Model\Surat;
use App\Model\Umkm;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Mail;

class DomisiliController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('landingpage.pelayanan.domisili',[
            'user'=> User::all(),
            'pekerjaan' => Pekerjaan::all(),
            'dukuh' => Dukuh::all(),
            'rt' => Rt::all(),
            'agama' => Agama::all(),
            'keperluan' => Keperluan::all(),

            'kelurahan' => t_kelurahan::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->role == "unverified"){
            return redirect()->back()->with('warning', 'Pengajuan gagal, akun anda belum terverifikasi.');
        } else {
            $validator = Validator::make($request->all(), [
                'id_user'=>['required', 'max:255'],
                'lampiran_ktp' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
                'lampiran_surat_pengantar_rt' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
                'keterangan'=>['required', 'string']
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            } else {
                // Available alpha caracters
                $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

                // generate a pin based on 2 * 7 digits + a random character
                $pin = mt_rand(1000000, 9999999)
                    . mt_rand(1000000, 9999999)
                    . $characters[rand(0, strlen($characters) - 1)];
                    
                $file = $request->file('lampiran_surat_pengantar_rt');

                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file->getClientOriginalName())) {
                    return redirect()->back()->with('errors','invalid file name.');
                }

                $folder_destination = 'public/assets/lampiran/pengantar_RT';

                $filenya = str_shuffle($pin) . '-' . $file->getClientOriginalName();
                $filetowrite = $folder_destination . '/' . $filenya;
                $file->move($folder_destination, $filetowrite);

                //KTP PELAPOR 
                $file1 = $request->file('lampiran_ktp');

                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file1->getClientOriginalName())) {
                    return redirect()->back()->with('errors','invalid file name.');
                }

                $folder_destination = 'public/assets/lampiran/ktp_pelapor';

                $filenya1 = str_shuffle($pin) . '-' . $file1->getClientOriginalName();
                $filetowrite = $folder_destination . '/' . $filenya1;
                $file1->move($folder_destination, $filetowrite);

                // Available alpha caracters
                $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

                // generate a pin based on 2 * 7 digits + a random character
                $pin = mt_rand(1000000, 9999999)
                    . mt_rand(1000000, 9999999)
                    . $characters[rand(0, strlen($characters) - 1)];
                    
                $surat = Surat::insert([
                    't_users_id' => request('id_user'),
                    'id_pengajuan' => str_shuffle($pin),
                    't_keperluan_id' => 7,
                    'keterangan' => request('keterangan'),
                    'lamp_pengantarRT'=> $filenya,
                    'ktp_pelapor' => $filenya1
                ]);

                if ($surat) {
                    return redirect()->back()->with('success', 'Pengajuan berhasil terkirim, tunggu verifikasi dari petugas.');
                } else {
                    return redirect()->back()->with('errors', 'Pengajuan gagal, harap ulangi kembali.');
                }
            }

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Surat::where('id',$id)->get();
        $pekerjaan = Pekerjaan::all();
        $dukuh = Dukuh::all();
        $rt = Rt::all();
        $agama = Agama::all();
       
        $pengajuansurat = Surat::where('status', 'proses')->orderBy('created_at', 'desc')->get();
        $pengajuanumkm = Umkm::where('status', 'proses')->orderBy('created_at', 'desc')->get();
        $unverified =  User::where('role', 'unverified')->orderBy('created_at', 'desc')->get();
        return view('petugas.layanan_surat.edit.domisili', compact('data', 'pengajuansurat', 'pengajuanumkm', 'pekerjaan', 'dukuh', 'rt', 'agama', 'unverified'));
    }

    public function lihat($id)
    {
        $data = Surat::where('id',$id)->get();
        $pekerjaan = Pekerjaan::all();
        $dukuh = Dukuh::all();
        $rt = Rt::all();
        $agama = Agama::all();
       
        $pengajuansurat = Surat::where('status', 'proses')->orderBy('created_at', 'desc')->get();
        $pengajuanumkm = Umkm::where('status', 'proses')->orderBy('created_at', 'desc')->get();
        $unverified =  User::where('role', 'unverified')->orderBy('created_at', 'desc')->get();
        return view('petugas.layanan_surat.lihat.domisili', compact('data', 'pengajuansurat', 'pengajuanumkm', 'pekerjaan', 'dukuh', 'rt', 'agama', 'unverified'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array(
            'id'=>$id,
        );

        $ubah = array(
            'id_pengajuan'=>$id,
        );

        $data = array_merge($data, $request->all());

        $validator = Validator::make($data, [
            'id'=>'required|numeric|exists:t_surat,id',
            'no_surat'=>'required|max:255|string|unique:t_surat',
            'keterangan'=>'required|string',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {              
            $surat = Surat::where('id',$id)->update([
                'status'=> "terverifikasi",
                'no_surat'=>$request->no_surat,
                'keterangan' => $request->keterangan,

            ]);
            
            // //   Create qr
            // $qrstring = 'Pengajuan #' . $request->id_pengajuan.'/'.$request->id_name.'/'.$request->nik.'/'.$request->keperluannya;
            // // Passing data
            // view()->share(['surat' => $surat, 'qrstring' => $qrstring]);

            $ubah = Surat::find($ubah);

            view()->share([
                'suratnya' => $ubah,
            ]);
            
            $pdf = PDF::loadView('mail.suket', $ubah)->setPaper('legal', 'landscape');
            $path = 'public/assets/1197268';
            $fileName =  ($request->id_pengajuan) . '-Suket.' . 'pdf' ;
            $pdf->save($path . '/' . $fileName);

            // $pdf2 = PDF::loadView('mail.form_ktp', $ubah)->setPaper('legal', 'potrait');
            // $path2 = 'public/assets/1197824';
            // $fileName2 =  ($request->id_pengajuan) . '-Lampiran.' . 'pdf' ;
            // $pdf2->save($path2 . '/' . $fileName2);
            
            // return $pdf2->stream('test_pdf.pdf');

            Mail::send('mail.content', ['surat' => $surat], function($message) use ($pdf, $request){
                $message->from('alvicky98@gmail.com');
                $message->to($request->email);
                $message->subject('Terverifikasi ID #' .$request->id_pengajuan );
                $message->attachData($pdf->output(), $request->id_pengajuan . '-Suket.' . 'pdf');
            });

            if ($surat) {
                return redirect('dashboard-petugas/layanan/masuk')->with('success', 'Verifikasi surat pengajuan berhasil.');
            } else {
                return redirect('dashboard-petugas/layanan/masuk')->with('errors', 'Verifikasi surat pengajuan gagal, harap ulangi kembali..');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = array(
            'id'=>$id
        );

        $validator = Validator::make($data, [
            'id'=>'required|numeric|exists:t_surat,id',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $pengantarRt = Surat::where('id',$id)->first();
        Mail::send('mail.refuse', ['pengantarRt' => $pengantarRt], function($message) use ($pengantarRt) {
            $message->from('alvicky98@gmail.com');
            $message->to($pengantarRt->User['email']);
            $message->subject('Pengajuan Tertolak ID #' .$pengantarRt->id_pengajuan );
        });

        // check file exist
        if (file_exists('public/assets/lampiran/pengantar_RT/'.$pengantarRt['lamp_pengantarRT'])) {
            unlink('public/assets/lampiran/pengantar_RT/'.$pengantarRt['lamp_pengantarRT']);
        }

        $delete=Surat::where('id', $id)->delete();
        
        if ($delete) {
            return redirect()->back()->with('success','Pengajuan berhasil dihapus.');
        } else {
            return redirect()->back()->with('errors','Pengajuan gagal dihapus, harap ulangi kembali.');
        }
    }
}
