<?php

namespace App\Http\Controllers\Landingpage\Pelayanan;

use App\Http\Controllers\Controller;
use App\Model\Agama;
use App\Model\Dukuh;
use App\Model\Form\form_perpindahan;
use App\Model\Form\t_jeniskepindahan;
use App\Model\Form\t_keluarga;
use App\Model\Form\t_tinggal;
use App\Model\Keperluan;
use App\Model\Pekerjaan;
use App\Model\Profil\t_kelurahan;
use App\Model\Rt;
use App\Model\Surat;
use App\Model\Umkm;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Mail;

class PindahMasukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('landingpage.pelayanan.pindahmasuk',[
            'user'=> User::all(),
            'pekerjaan' => Pekerjaan::all(),
            'dukuh' => Dukuh::all(),
            'rt' => Rt::all(),
            'agama' => Agama::all(),
            'keperluan' => Keperluan::all(),
            'jeniskepindahan' => t_jeniskepindahan::all(),
            'kelurahan' => t_kelurahan::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->role == "unverified"){
            return redirect()->back()->with('warning', 'Pengajuan gagal, akun anda belum terverifikasi.');
        } else {
            if ($request->status_kk_keluarga_pindah == "Membuat KK Baru") {
                $validator = Validator::make($request->all(), [
                    'id_user'=>['required', 'max:255'],
                    'keterangan'=>['required', 'string'],
                    'lampiran_ktp_pelapor' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
                    'lampiran_surat_pindah_dari_tempat_asal' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
                    // DATA KEPINDAHAN
                    'kepindahan'=>['required', 'string', 'max:255'],
                    'nama_kepala_keluarga'=>['required', 'string', 'max:255'],
                    'nik_kepala_keluarga'=>['required', 'numeric', 'digits:16'],
                    'dukuh_asal'=>['required', 'string', 'max:255'],
                    'rt_asal'=>['required', 'string', 'max:255'],

                    'tanggal_kedatangan'=>['required', 'string', 'max:255'],
                    'status_kk_keluarga_pindah'=>['required', 'string', 'max:255'],

                    'nik_anggota_keluarga_pindah'=>['required', 'array', 'min:1'],
                    'nik_anggota_keluarga_pindah.*'=>['required', 'numeric', 'digits:16'],
                    'nama_anggota_keluarga_pindah'=>['required', 'array', 'min:1'],
                    'nama_anggota_keluarga_pindah.*'=>['required', 'string', 'max:255'],
                    'shdk'=>['required', 'array', 'min:1'],
                    'shdk.*'=>['required', 'string', 'max:255'],

                    // DATA TINGGAL KE
                    'provinsi_kepindahan'=>['required', 'string', 'max:255'],
                    'kota_atau_kabupaten_kepindahan'=>['required', 'string', 'max:255'],
                    'kecamatan_kepindahan'=>['required', 'string', 'max:255'],
                    'kelurahan_kepindahan'=>['required', 'string', 'max:255'],
                    'dukuh_kepindahan'=>['required', 'string', 'max:255'],
                    'rt_kepindahan'=>['required', 'string', 'max:255'],
                    'rw_kepindahan'=>['required', 'string', 'max:255'],
                    'kode_pos_kepindahan'=>['required', 'numeric', 'digits:5'],

                    // KELUARGA PINDAH
                    // 'nik_anggota_keluarga_pindah'=>['required', 'numeric', 'digits:16'],
                    // 'nama_anggota_keluarga_pindah'=>['required', 'string', 'max:255'],

                                    
                    // 'lampiran_KTP_pelapor' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
                    // 'lampiran_KK' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
                    'lampiran_akta_kelahiran_pelapor' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
                    'lampiran_ijazah_terakhir_pelapor' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
                ]);
            } else {
                $validator = Validator::make($request->all(), [
                    'id_user'=>['required', 'max:255'],
                    'keterangan'=>['required', 'string'],
                    'lampiran_ktp_pelapor' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
                    'lampiran_surat_pindah_dari_tempat_asal' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
                    // DATA KEPINDAHAN
                    'kepindahan'=>['required', 'string', 'max:255'],
                    'nama_kepala_keluarga'=>['required', 'string', 'max:255'],
                    'nomor_kk' =>['required', 'numeric', 'digits:16'],
                    'nik_kepala_keluarga'=>['required', 'numeric', 'digits:16'],
                    'dukuh_asal'=>['required', 'string', 'max:255'],
                    'rt_asal'=>['required', 'string', 'max:255'],
    
                    'tanggal_kedatangan'=>['required', 'string', 'max:255'],
                    'status_kk_keluarga_pindah'=>['required', 'string', 'max:255'],
    
                    'nik_anggota_keluarga_pindah'=>['required', 'array', 'min:1'],
                    'nik_anggota_keluarga_pindah.*'=>['required', 'numeric', 'digits:16'],
                    'nama_anggota_keluarga_pindah'=>['required', 'array', 'min:1'],
                    'nama_anggota_keluarga_pindah.*'=>['required', 'string', 'max:255'],
                    'shdk'=>['required', 'array', 'min:1'],
                    'shdk.*'=>['required', 'string', 'max:255'],
    
                    // DATA TINGGAL KE
                    'provinsi_kepindahan'=>['required', 'string', 'max:255'],
                    'kota_atau_kabupaten_kepindahan'=>['required', 'string', 'max:255'],
                    'kecamatan_kepindahan'=>['required', 'string', 'max:255'],
                    'kelurahan_kepindahan'=>['required', 'string', 'max:255'],
                    'dukuh_kepindahan'=>['required', 'string', 'max:255'],
                    'rt_kepindahan'=>['required', 'string', 'max:255'],
                    'rw_kepindahan'=>['required', 'string', 'max:255'],
                    'kode_pos_kepindahan'=>['required', 'numeric', 'digits:5'],
    
                    // KELUARGA PINDAH
                    // 'nik_anggota_keluarga_pindah'=>['required', 'numeric', 'digits:16'],
                    // 'nama_anggota_keluarga_pindah'=>['required', 'string', 'max:255'],
    
                                    
                    // 'lampiran_KTP_pelapor' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
                    // 'lampiran_KK' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
                    'lampiran_akta_kelahiran_pelapor' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
                    'lampiran_ijazah_terakhir_pelapor' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
                ]);
            }

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            } else {
                // Available alpha caracters
                $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

                // generate a pin based on 2 * 7 digits + a random character
                $pin = mt_rand(1000000, 9999999)
                    . mt_rand(1000000, 9999999)
                    . $characters[rand(0, strlen($characters) - 1)];

                // Pengantar Surat Pindah
                $file = $request->file('lampiran_surat_pindah_dari_tempat_asal');

                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file->getClientOriginalName())) {
                    return redirect()->back()->with('errors','invalid file name.');
                }

                $folder_destination = 'public/assets/lampiran/surat_pindah';

                $filenya = str_shuffle($pin) . '-' . $file->getClientOriginalName();
                $filetowrite = $folder_destination . '/' . $filenya;
                $file->move($folder_destination, $filetowrite);

                // Akta Kelahiran 
                $file2 = $request->file('lampiran_akta_kelahiran_pelapor');

                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file2->getClientOriginalName())) {
                    return redirect()->back()->with('errors','invalid file name.');
                }

                $folder_destination = 'public/assets/lampiran/akta_kelahiran';

                $filenya2 = str_shuffle($pin) . '-' . $file2->getClientOriginalName();
                $filetowrite = $folder_destination . '/' . $filenya2;
                $file2->move($folder_destination, $filetowrite);

                // Ijazah Terakhir
                $file3 = $request->file('lampiran_ijazah_terakhir_pelapor');

                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file3->getClientOriginalName())) {
                    return redirect()->back()->with('errors','invalid file name.');
                }

                $folder_destination = 'public/assets/lampiran/ijazah_terakhir';

                $filenya3 = str_shuffle($pin) . '-' . $file3->getClientOriginalName();
                $filetowrite = $folder_destination . '/' . $filenya3;
                $file3->move($folder_destination, $filetowrite);

                if ($request->hasFile('lampiran_akta_nikah')) {
                    // Buku Nikah
                    $file4 = $request->file('lampiran_akta_nikah');

                    if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file4->getClientOriginalName())) {
                        return redirect()->back()->with('errors','invalid file name.');
                    }

                    $folder_destination = 'public/assets/lampiran/buku_nikah';

                    $filenya4 = str_shuffle($pin) . '-' . $file4->getClientOriginalName();
                    $filetowrite = $folder_destination . '/' . $filenya4;
                    $file4->move($folder_destination, $filetowrite);
                }

                //KTP PELAPOR 
                $file5 = $request->file('lampiran_ktp_pelapor');

                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file5->getClientOriginalName())) {
                    return redirect()->back()->with('errors','invalid file name.');
                }

                $folder_destination = 'public/assets/lampiran/ktp_pelapor';

                $filenya5 = str_shuffle($pin) . '-' . $file5->getClientOriginalName();
                $filetowrite = $folder_destination . '/' . $filenya5;
                $file5->move($folder_destination, $filetowrite);

                // Available alpha caracters
                $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

                // generate a pin based on 2 * 7 digits + a random character
                $pin = mt_rand(1000000, 9999999)
                    . mt_rand(1000000, 9999999)
                    . $characters[rand(0, strlen($characters) - 1)];

                if ($request->status_kk_keluarga_pindah == "Membuat KK Baru") {
                    // Create Kepindahan
                    if ($request->hasFile('lampiran_akta_nikah')) {
                        // Surat
                        $surat = Surat::insert([
                            't_users_id' => request('id_user'),
                            't_keperluan_id' => 4,
                            'keterangan' => request('keterangan'),
                            'id_pengajuan' => str_shuffle($pin),
                            'ktp_pelapor' => $filenya5
                        ]);

                        // Create Pindah Masuk
                        $create = form_perpindahan::insert([    
                            't_surat_id' => DB::table('t_surat')->orderBy('id', 'DESC')->limit(1)->value('id'),

                            't_jeniskepindahans_id' => request('kepindahan'),
                            'kepala_keluarga' => request('nama_kepala_keluarga'),
                            'nik_kepalakeluarga' => request('nik_kepala_keluarga'),
                            'rt' => request('rt_asal'),
                            't_dukuh_id' => request('dukuh_asal'),
                            'alasan_pindah' => request('alasan_pindah'),
                            'alasan_pindah2' => request('alasan_pindah2'),
                            'jenis_kepindahan' => request('jenis_kepindahan'),
                            'statuskk_tidakpindah' => request('status_KK_keluarga_tidak_pindah'),
                            'statuskk_pindah' => request('status_kk_keluarga_pindah'),
                            'tanggal_kedatangan' => request('tanggal_kedatangan'),
        
                            'lamp_aktakelahiran' => $filenya2,
                            'lamp_ijazahterakhir' => $filenya3,            
                            'lamp_aktanikah' => $filenya4,
                            'lamp_suratpindah' => $filenya,
                        ]);

                        // Create Tinggal Kepindahan
                        $kepindahan = t_tinggal::insert([
                            'form_perpindahans_id' => DB::table('form_perpindahans')->orderBy('id', 'DESC')->limit(1)->value('id'),

                            'provinsi' => request('provinsi_kepindahan'),
                            'kabupaten' => request('kota_atau_kabupaten_kepindahan'),
                            'kecamatan' => request('kecamatan_kepindahan'),
                            'kelurahan' => request('kelurahan_kepindahan'),
                            'dukuh' => request('dukuh_kepindahan'),
                            'rt' => request('rt_kepindahan'),
                            'rw' => request('rw_kepindahan'),
                            'kodepos' => request('kode_pos_kepindahan'),
                        ]); 

                        // Create Keluarga
                        $nik = request('nik_anggota_keluarga_pindah');
                        $nama = request('nama_anggota_keluarga_pindah');
                        $shdk = request('shdk');
                        for ($i = 0; $i < count($nik); $i++) {
                            $keluarga = t_keluarga::insert([
                                'form_perpindahans_id' => DB::table('form_perpindahans')->orderBy('id', 'DESC')->limit(1)->value('id'),

                                'nik' => $nik[$i],
                                'nama' => $nama[$i],
                                'shdk' => $shdk[$i],
                            ]);
                        }
                        
                    } else {
                        // Surat
                        $surat = Surat::insert([
                            't_users_id' => request('id_user'),
                            't_keperluan_id' => 4,
                            'keterangan' => request('keterangan'),
                            'id_pengajuan' => str_shuffle($pin),
                            'ktp_pelapor' => $filenya5
                        ]);

                        $create = form_perpindahan::insert([                
                            't_surat_id' => DB::table('t_surat')->orderBy('id', 'DESC')->limit(1)->value('id'),

                            't_jeniskepindahans_id' => request('kepindahan'),
                            'kepala_keluarga' => request('nama_kepala_keluarga'),
                            'nomor_kk' => request('nomor_kk'),
                            'nik_kepalakeluarga' => request('nik_kepala_keluarga'),
                            'rt' => request('rt_asal'),
                            't_dukuh_id' => request('dukuh_asal'),
                            'alasan_pindah' => request('alasan_pindah'),
                            'alasan_pindah2' => request('alasan_pindah2'),
                            'jenis_kepindahan' => request('jenis_kepindahan'),
                            'statuskk_tidakpindah' => request('status_KK_keluarga_tidak_pindah'),
                            'statuskk_pindah' => request('status_kk_keluarga_pindah'),
                            'tanggal_kedatangan' => request('tanggal_kedatangan'),
        
                            'lamp_aktakelahiran' => $filenya2,
                            'lamp_ijazahterakhir' => $filenya3,
                            'lamp_suratpindah' => $filenya,
                            // 'lamp_aktanikah' => $file4->getClientOriginalName()
                        ]);

                        // Create Tinggal Kepindahan
                        $kepindahan = t_tinggal::insert([
                            'form_perpindahans_id' => DB::table('form_perpindahans')->orderBy('id', 'DESC')->limit(1)->value('id'),

                            'provinsi' => request('provinsi_kepindahan'),
                            'kabupaten' => request('kota_atau_kabupaten_kepindahan'),
                            'kecamatan' => request('kecamatan_kepindahan'),
                            'kelurahan' => request('kelurahan_kepindahan'),
                            'dukuh' => request('dukuh_kepindahan'),
                            'rt' => request('rt_kepindahan'),
                            'rw' => request('rw_kepindahan'),
                            'kodepos' => request('kode_pos_kepindahan'),
                        ]);

                        // Create Keluarga
                        $nik = request('nik_anggota_keluarga_pindah');
                        $nama = request('nama_anggota_keluarga_pindah');
                        $shdk = request('shdk');
                        for ($i = 0; $i < count($nik); $i++) {
                            $keluarga = t_keluarga::insert([
                                'form_perpindahans_id' => DB::table('form_perpindahans')->orderBy('id', 'DESC')->limit(1)->value('id'),

                                'nik' => $nik[$i],
                                'nama' => $nama[$i],
                                'shdk' => $shdk[$i],
                            ]);
                        }
                    }
                } else {
                    // Create Kepindahan
                    if ($request->hasFile('lampiran_akta_nikah')) {
                        // Surat
                        $surat = Surat::insert([
                            't_users_id' => request('id_user'),
                            't_keperluan_id' => 4,
                            'keterangan' => request('keterangan'),
                            'id_pengajuan' => str_shuffle($pin),
                            'ktp_pelapor' => $filenya5
                        ]);

                        // Create Pindah Masuk
                        $create = form_perpindahan::insert([    
                            't_surat_id' => DB::table('t_surat')->orderBy('id', 'DESC')->limit(1)->value('id'),

                            't_jeniskepindahans_id' => request('kepindahan'),
                            'kepala_keluarga' => request('nama_kepala_keluarga'),
                            'nomor_kk' => request('nomor_kk'),
                            'nik_kepalakeluarga' => request('nik_kepala_keluarga'),
                            'rt' => request('rt_asal'),
                            't_dukuh_id' => request('dukuh_asal'),
                            'alasan_pindah' => request('alasan_pindah'),
                            'alasan_pindah2' => request('alasan_pindah2'),
                            'jenis_kepindahan' => request('jenis_kepindahan'),
                            'statuskk_tidakpindah' => request('status_KK_keluarga_tidak_pindah'),
                            'statuskk_pindah' => request('status_kk_keluarga_pindah'),
                            'tanggal_kedatangan' => request('tanggal_kedatangan'),
        
                            'lamp_aktakelahiran' => $filenya2,
                            'lamp_ijazahterakhir' => $filenya3,            
                            'lamp_aktanikah' => $filenya4,
                            'lamp_suratpindah' => $filenya,
                        ]);

                        // Create Tinggal Kepindahan
                        $kepindahan = t_tinggal::insert([
                            'form_perpindahans_id' => DB::table('form_perpindahans')->orderBy('id', 'DESC')->limit(1)->value('id'),

                            'provinsi' => request('provinsi_kepindahan'),
                            'kabupaten' => request('kota_atau_kabupaten_kepindahan'),
                            'kecamatan' => request('kecamatan_kepindahan'),
                            'kelurahan' => request('kelurahan_kepindahan'),
                            'dukuh' => request('dukuh_kepindahan'),
                            'rt' => request('rt_kepindahan'),
                            'rw' => request('rw_kepindahan'),
                            'kodepos' => request('kode_pos_kepindahan'),
                        ]); 

                        // Create Keluarga
                        $nik = request('nik_anggota_keluarga_pindah');
                        $nama = request('nama_anggota_keluarga_pindah');
                        $shdk = request('shdk');
                        for ($i = 0; $i < count($nik); $i++) {
                            $keluarga = t_keluarga::insert([
                                'form_perpindahans_id' => DB::table('form_perpindahans')->orderBy('id', 'DESC')->limit(1)->value('id'),

                                'nik' => $nik[$i],
                                'nama' => $nama[$i],
                                'shdk' => $shdk[$i],
                            ]);
                        }
                        
                    } else {
                        // Surat
                        $surat = Surat::insert([
                            't_users_id' => request('id_user'),
                            't_keperluan_id' => 4,
                            'keterangan' => request('keterangan'),
                            'id_pengajuan' => str_shuffle($pin),
                            'ktp_pelapor' => $filenya5
                        ]);

                        $create = form_perpindahan::insert([                
                            't_surat_id' => DB::table('t_surat')->orderBy('id', 'DESC')->limit(1)->value('id'),

                            't_jeniskepindahans_id' => request('kepindahan'),
                            'kepala_keluarga' => request('nama_kepala_keluarga'),
                            'nomor_kk' => request('nomor_kk'),
                            'nik_kepalakeluarga' => request('nik_kepala_keluarga'),
                            'rt' => request('rt_asal'),
                            't_dukuh_id' => request('dukuh_asal'),
                            'alasan_pindah' => request('alasan_pindah'),
                            'alasan_pindah2' => request('alasan_pindah2'),
                            'jenis_kepindahan' => request('jenis_kepindahan'),
                            'statuskk_tidakpindah' => request('status_KK_keluarga_tidak_pindah'),
                            'statuskk_pindah' => request('status_kk_keluarga_pindah'),
                            'tanggal_kedatangan' => request('tanggal_kedatangan'),
        
                            'lamp_aktakelahiran' => $filenya2,
                            'lamp_ijazahterakhir' => $filenya3,
                            'lamp_suratpindah' => $filenya,
                            // 'lamp_aktanikah' => $file4->getClientOriginalName()
                        ]);

                        // Create Tinggal Kepindahan
                        $kepindahan = t_tinggal::insert([
                            'form_perpindahans_id' => DB::table('form_perpindahans')->orderBy('id', 'DESC')->limit(1)->value('id'),

                            'provinsi' => request('provinsi_kepindahan'),
                            'kabupaten' => request('kota_atau_kabupaten_kepindahan'),
                            'kecamatan' => request('kecamatan_kepindahan'),
                            'kelurahan' => request('kelurahan_kepindahan'),
                            'dukuh' => request('dukuh_kepindahan'),
                            'rt' => request('rt_kepindahan'),
                            'rw' => request('rw_kepindahan'),
                            'kodepos' => request('kode_pos_kepindahan'),
                        ]);

                        // Create Keluarga
                        $nik = request('nik_anggota_keluarga_pindah');
                        $nama = request('nama_anggota_keluarga_pindah');
                        $shdk = request('shdk');
                        for ($i = 0; $i < count($nik); $i++) {
                            $keluarga = t_keluarga::insert([
                                'form_perpindahans_id' => DB::table('form_perpindahans')->orderBy('id', 'DESC')->limit(1)->value('id'),

                                'nik' => $nik[$i],
                                'nama' => $nama[$i],
                                'shdk' => $shdk[$i],
                            ]);
                        }
                    }
                }

                if ($create) {
                    return redirect()->back()->with('success', 'Pengajuan berhasil terkirim, tunggu verifikasi dari petugas.');
                } else {
                    return redirect()->back()->with('errors', 'Pengajuan gagal, harap ulangi kembali.');
                }
            }

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Surat::where('id',$id)->get();
        $pindahdatang = form_perpindahan::where('t_surat_id',$id)->get();
        $keluarga = t_keluarga::all();
        $tinggal = t_tinggal::all();

        $pekerjaan = Pekerjaan::all();
        $jeniskepindahan = t_jeniskepindahan::all();
        $keluarga = t_keluarga::all();
        $dukuh = Dukuh::all();
        $rt = Rt::all();
        $agama = Agama::all();
        $keperluan = Keperluan::all();
        $pengajuansurat = Surat::where('status', 'proses')->orderBy('created_at', 'desc')->get();
        $pengajuanumkm = Umkm::where('status', 'proses')->orderBy('created_at', 'desc')->get();
        $unverified =  User::where('role', 'unverified')->orderBy('created_at', 'desc')->get();
        return view('petugas.layanan_surat.edit.pindahmasuk', compact('data', 'pindahdatang', 'tinggal', 'pekerjaan', 'jeniskepindahan', 'keluarga', 'dukuh', 'rt', 'agama', 'keperluan', 'pengajuansurat', 'pengajuanumkm', 'unverified'));
    }

    public function lihat($id)
    {
        $data = Surat::where('id',$id)->get();
        $pindahdatang = form_perpindahan::where('t_surat_id',$id)->get();
        $keluarga = t_keluarga::all();
        $tinggal = t_tinggal::all();

        $pekerjaan = Pekerjaan::all();
        $jeniskepindahan = t_jeniskepindahan::all();
        $keluarga = t_keluarga::all();
        $dukuh = Dukuh::all();
        $rt = Rt::all();
        $agama = Agama::all();
        $keperluan = Keperluan::all();
        $pengajuansurat = Surat::where('status', 'proses')->orderBy('created_at', 'desc')->get();
        $pengajuanumkm = Umkm::where('status', 'proses')->orderBy('created_at', 'desc')->get();
        $unverified =  User::where('role', 'unverified')->orderBy('created_at', 'desc')->get();
        return view('petugas.layanan_surat.lihat.pindahmasuk', compact('data', 'pindahdatang', 'tinggal', 'pekerjaan', 'jeniskepindahan', 'keluarga', 'dukuh', 'rt', 'agama', 'keperluan', 'pengajuansurat', 'pengajuanumkm', 'unverified'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array(
            'id'=>$id,
        );

        $ubah = array(
            'id_pengajuan'=>$id,
        );

        $data = array_merge($data, $request->all());

        $validator = Validator::make($data, [
            'id'=>'required|numeric|exists:t_surat,id',
            'no_surat'=>'required|max:255|string|unique:t_surat',
            'no_suratformulir'=>'required|max:255|string|unique:t_surat',
            'keterangan'=>'required|string'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            $surat = Surat::where('id',$id)->update([
                'status'=> "terverifikasi",
                'no_surat'=>$request->no_surat,
                'no_suratformulir'=>$request->no_suratformulir,
                'keterangan' => $request->keterangan,

            ]);

            $pindahdatang = form_perpindahan::where('t_surat_id',$id)->update([
                'nomor_kk'=> $request->nomor_kk
            ]);
            
            //   Create qr
            $qrstring = 'Pengajuan #' . $request->id_pengajuan.'/'.$request->id_name.'/'.$request->nik.'/'.$request->keperluannya;
            // Passing data
            view()->share(['surat' => $surat, 'qrstring' => $qrstring]);

            $ubah = Surat::find($ubah);
            $pindahdatang = form_perpindahan::where('t_surat_id',$id)->get();
            $tinggal = t_tinggal::all();
            $keluarga = t_keluarga::all();
            $jeniskepindahan = t_jeniskepindahan::all();
            view()->share([
                'suratnya' => $ubah,
                'keluarga' => $keluarga,
                'jeniskepindahan' => $jeniskepindahan,
                'tinggal' => $tinggal,
                'pindahdatang' => $pindahdatang
            ]);
            
            $pdf = PDF::loadView('mail.suket', $ubah)->setPaper('legal', 'landscape');
            $path = 'public/assets/1197268';
            $fileName =  ($request->id_pengajuan) . '-Suket.' . 'pdf' ;
            $pdf->save($path . '/' . $fileName);

            $pdf2 = PDF::loadView('mail.form_datang', $ubah)->setPaper('legal', 'potrait');
            $path2 = 'public/assets/1197824';
            $fileName2 =  ($request->id_pengajuan) . '-Lampiran.' . 'pdf' ;
            $pdf2->save($path2 . '/' . $fileName2);
            
            // return $pdf->download($fileName);
            // return $pdf2->stream('test-kematian_pdf.pdf');

            Mail::send('mail.content', ['surat' => $surat], function($message) use ($pdf, $pdf2, $request){
                $message->from('alvicky98@gmail.com');
                $message->to($request->email);
                $message->subject('Terverifikasi ID #' .$request->id_pengajuan );
                $message->attachData($pdf->output(), $request->id_pengajuan . '-Suket.' . 'pdf');
                $message->attachData($pdf2->output(), $request->id_pengajuan . '-Lampiran.' . 'pdf');
            });

            if ($surat) {
                return redirect('dashboard-petugas/layanan/masuk')->with('success', 'Verifikasi surat pengajuan berhasil.');
            } else {
                return redirect('dashboard-petugas/layanan/masuk')->with('errors', 'Verifikasi surat pengajuan gagal, harap ulangi kembali..');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = array(
            'id'=>$id
        );

        $validator = Validator::make($data, [
            'id'=>'required|numeric|exists:t_surat,id',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $pengantarRt = Surat::where('id',$id)->first();
        Mail::send('mail.refuse', ['pengantarRt' => $pengantarRt], function($message) use ($pengantarRt) {
            $message->from('alvicky98@gmail.com');
            $message->to($pengantarRt->User['email']);
            $message->subject('Pengajuan Tertolak ID #' .$pengantarRt->id_pengajuan );
        });
        
        $pengantarAsal = form_perpindahan::where('t_surat_id',$id)->first();
        $akta = form_perpindahan::where('t_surat_id',$id)->first();
        $ijazah = form_perpindahan::where('t_surat_id',$id)->first();
        $aktanikah = form_perpindahan::where('t_surat_id',$id)->first();
        $aktanikah2 = form_perpindahan::where('t_surat_id',$id)->where('lamp_aktanikah', null)->first();

        if ($aktanikah2 != null) {
            if (file_exists('public/assets/lampiran/akta_kelahiran/'.$akta['lamp_aktakelahiran'])) {
                unlink('public/assets/lampiran/akta_kelahiran/'.$akta['lamp_aktakelahiran']);
            }

            if (file_exists('public/assets/lampiran/ijazah_terakhir/'.$ijazah['lamp_ijazahterakhir'])) {
                unlink('public/assets/lampiran/ijazah_terakhir/'.$ijazah['lamp_ijazahterakhir']);
            }

            if (file_exists('public/assets/lampiran/surat_pindah/'.$pengantarAsal['lamp_suratpindah'])) {
                unlink('public/assets/lampiran/surat_pindah/'.$pengantarAsal['lamp_suratpindah']);
            }

        } else {
            if (file_exists('public/assets/lampiran/akta_kelahiran/'.$akta['lamp_aktakelahiran'])) {
                unlink('public/assets/lampiran/akta_kelahiran/'.$akta['lamp_aktakelahiran']);
            }

            if (file_exists('public/assets/lampiran/ijazah_terakhir/'.$ijazah['lamp_ijazahterakhir'])) {
                unlink('public/assets/lampiran/ijazah_terakhir/'.$ijazah['lamp_ijazahterakhir']);
            }

            if (file_exists('public/assets/lampiran/buku_nikah/'.$aktanikah['lamp_aktanikah'])) {
                unlink('public/assets/lampiran/buku_nikah/'.$aktanikah['lamp_aktanikah']);
            }

            if (file_exists('public/assets/lampiran/surat_pindah/'.$pengantarAsal['lamp_suratpindah'])) {
                unlink('public/assets/lampiran/surat_pindah/'.$pengantarAsal['lamp_suratpindah']);
            }
        }

        $delete=Surat::where('id', $id)->delete();
        
        if ($delete) {
            return redirect()->back()->with('success','Pengajuan berhasil dihapus.');
        } else {
            return redirect()->back()->with('errors','Pengajuan gagal dihapus, harap ulangi kembali.');
        }
    }
}
