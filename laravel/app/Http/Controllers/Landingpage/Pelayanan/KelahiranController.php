<?php

namespace App\Http\Controllers\Landingpage\Pelayanan;

use App\Http\Controllers\Controller;
use App\Model\Agama;
use App\Model\Dukuh;
use App\Model\Form\form_kelahiran;
use App\Model\Form\t_ayah;
use App\Model\Form\t_ibu;
use App\Model\Form\t_saksi1;
use App\Model\Form\t_saksi2;
use App\Model\Keperluan;
use App\Model\Pekerjaan;
use App\Model\Profil\t_kelurahan;
use App\Model\Rt;
use App\Model\Surat;
use App\Model\Umkm;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Mail;

class KelahiranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('landingpage.pelayanan.kelahiran',[
            'user'=> User::all(),
            'pekerjaan' => Pekerjaan::all(),
            'dukuh' => Dukuh::all(),
            'rt' => Rt::all(),
            'agama' => Agama::all(),
            'keperluan' => Keperluan::all(),
            
            'kelurahan' => t_kelurahan::all()
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRt($id)
    {
        $rt = Rt::select('rt','id')->where('t_dukuh_id', $id)->get();
        return response()->json($rt);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->role == "unverified"){
            return redirect()->back()->with('warning', 'Pengajuan gagal, akun anda belum terverifikasi.');
        } else {
            $validator = Validator::make($request->all(), [
                'id_user'=>['required', 'max:255'],
                'keterangan'=>['required', 'string'],
                'lampiran_ktp_pelapor' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],

                'lampiran_surat_pengantar_rt' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
                // ANAK
                'nama_anak'=>['required', 'string', 'max:255'],
                'jenis_kelamin_anak'=>['required', 'string', 'max:255'],
                'tempat_lahir'=>['required', 'string', 'max:255'],
                'no_atau_tempat_kelahiran'=>['required', 'string', 'max:255'],
                'tanggal_lahir'=>['required', 'max:255'],
                'pukul_kelahiran'=>['required', 'max:255'],
                'jenis_kelahiran'=>['required', 'string', 'max:255'],
                'kelahiran_ke'=>['required', 'numeric', 'max:255'],
                'penolong_kelahiran'=>['required', 'string', 'max:255'],
                'berat_bayi'=>['required', 'string', 'max:255'],
                'tinggi_bayi'=>['required', 'numeric', 'max:255'],

                'lampiran_surat_keterangan_rs' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
                // IBU
                'nik_ibu'=>['required', 'numeric', 'digits:16'],
                'nama_ibu'=>['required', 'string', 'max:255'],
                'tanggal_lahir_ibu'=>['required', 'string', 'max:255'],
                'pekerjaan_ibu'=>['required', 'max:255'],
                'dukuh_ibu'=>['required', 'string', 'max:255'],
                'rt_ibu'=>['required', 'string', 'max:255'],
                'kelurahan_ibu'=>['required', 'string', 'max:255'],
                'kecamatan_ibu'=>['required', 'string', 'max:255'],
                'kabupaten_ibu'=>['required', 'string', 'max:255'],

                'lampiran_ktp_ibu' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
                // AYAH 
                'nik_ayah'=>['required', 'numeric', 'digits:16'],
                'nama_ayah'=>['required', 'string', 'max:255'],
                'tanggal_lahir_ayah'=>['required', 'string', 'max:255'],
                'pekerjaan_ayah'=>['required', 'max:255'],
                'dukuh_ayah'=>['required', 'string', 'max:255'],
                'rt_ayah'=>['required', 'string', 'max:255'],
                'kelurahan_ayah'=>['required', 'string', 'max:255'],
                'kecamatan_ayah'=>['required', 'string', 'max:255'],
                'kabupaten_ayah'=>['required', 'string', 'max:255'],
                'tanggal_pencatatan_nikah'=>['required', 'string', 'max:255'],

                'lampiran_ktp_ayah' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
                'lampiran_kartu_keluarga' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
                'lampiran_buku_nikah' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],

                // SAKSI I
                'nik_saksi1'=>['required', 'numeric', 'digits_between:16,16'],
                'nama_saksi1'=>['required', 'string', 'max:255'],
                'tanggal_lahir_saksi1'=>['required', 'string', 'max:255'],
                'dukuh_saksi1'=>['required', 'string', 'max:255'],
                'pekerjaan_saksi1'=>['required', 'max:255'],
                'jenis_kelamin_saksi1'=>['required', 'string', 'max:255'],
                'rt_saksi1'=>['required', 'string', 'max:255'],

                'lampiran_ktp_saksi_I' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],

                // SAKSI I
                'nik_saksi2'=>['required', 'numeric', 'digits_between:16,16'],
                'nama_saksi2'=>['required', 'string', 'max:255'],
                'tanggal_lahir_saksi2'=>['required', 'string', 'max:255'],
                'dukuh_saksi2'=>['required', 'string', 'max:255'],
                'pekerjaan_saksi2'=>['required', 'max:255'],
                'jenis_kelamin_saksi2'=>['required', 'string', 'max:255'],
                'rt_saksi2'=>['required', 'string', 'max:255'],

                'lampiran_ktp_saksi_II' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            } else {
                // Available alpha caracters
                $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

                // generate a pin based on 2 * 7 digits + a random character
                $pin = mt_rand(1000000, 9999999)
                    . mt_rand(1000000, 9999999)
                    . $characters[rand(0, strlen($characters) - 1)];
                    
                // Pengantar RT
                $file = $request->file('lampiran_surat_pengantar_rt');

                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file->getClientOriginalName())) {
                    return redirect()->back()->with('errors','invalid file name.');
                }

                $folder_destination = 'public/assets/lampiran/pengantar_RT';

                $filenya = str_shuffle($pin) . '-' . $file->getClientOriginalName();
                $filetowrite = $folder_destination . '/' . $filenya;
                $file->move($folder_destination, $filetowrite);

                // KTP IBU
                $file2 = $request->file('lampiran_ktp_ibu');

                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file2->getClientOriginalName())) {
                    return redirect()->back()->with('errors','invalid file name.');
                }

                $folder_destination = 'public/assets/lampiran/ktp_ibu';

                $filenya2 = str_shuffle($pin) . '-' . $file2->getClientOriginalName();
                $filetowrite = $folder_destination . '/' . $filenya2;
                $file2->move($folder_destination, $filetowrite);

                // KTP AYAH
                $file3 = $request->file('lampiran_ktp_ayah');

                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file3->getClientOriginalName())) {
                    return redirect()->back()->with('errors','invalid file name.');
                }

                $folder_destination = 'public/assets/lampiran/ktp_ayah';

                $filenya3 = str_shuffle($pin) . '-' . $file3->getClientOriginalName();
                $filetowrite = $folder_destination . '/' . $filenya3;
                $file3->move($folder_destination, $filetowrite);

                // Kartu Keluarga
                $file4 = $request->file('lampiran_kartu_keluarga');

                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file4->getClientOriginalName())) {
                    return redirect()->back()->with('errors','invalid file name.');
                }

                $folder_destination = 'public/assets/lampiran/kartu_keluarga';

                $filenya4 = str_shuffle($pin) . '-' . $file4->getClientOriginalName();
                $filetowrite = $folder_destination . '/' . $filenya4;
                $file4->move($folder_destination, $filetowrite);

                // Buku Nikah
                $file5 = $request->file('lampiran_buku_nikah');

                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file5->getClientOriginalName())) {
                    return redirect()->back()->with('errors','invalid file name.');
                }

                $folder_destination = 'public/assets/lampiran/buku_nikah';

                $filenya5 = str_shuffle($pin) . '-' . $file5->getClientOriginalName();
                $filetowrite = $folder_destination . '/' . $filenya5;
                $file5->move($folder_destination, $filetowrite);

                // Surat Keterangan RS
                $file6 = $request->file('lampiran_surat_keterangan_rs');

                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file6->getClientOriginalName())) {
                    return redirect()->back()->with('errors','invalid file name.');
                }

                $folder_destination = 'public/assets/lampiran/keterangan_RS';

                $filenya6 = str_shuffle($pin) . '-' . $file6->getClientOriginalName();
                $filetowrite = $folder_destination . '/' . $filenya6;
                $file6->move($folder_destination, $filetowrite);

                // KTP SAKSI I
                $file7 = $request->file('lampiran_ktp_saksi_I');

                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file7->getClientOriginalName())) {
                    return redirect()->back()->with('errors','invalid file name.');
                }

                $folder_destination = 'public/assets/lampiran/saksi_I';

                $filenya7 = str_shuffle($pin) . '-' . $file7->getClientOriginalName();
                $filetowrite = $folder_destination . '/' . $filenya7;
                $file7->move($folder_destination, $filetowrite);

                // KTP SAKSI II
                $file8 = $request->file('lampiran_ktp_saksi_II');

                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file8->getClientOriginalName())) {
                    return redirect()->back()->with('errors','invalid file name.');
                }

                $folder_destination = 'public/assets/lampiran/saksi_II';

                $filenya8 = str_shuffle($pin) . '-' . $file8->getClientOriginalName();
                $filetowrite = $folder_destination . '/' . $filenya8;
                $file8->move($folder_destination, $filetowrite);

                //KTP PELAPOR 
                $file9 = $request->file('lampiran_ktp_pelapor');

                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file9->getClientOriginalName())) {
                    return redirect()->back()->with('errors','invalid file name.');
                }

                $folder_destination = 'public/assets/lampiran/ktp_pelapor';

                $filenya9 = str_shuffle($pin) . '-' . $file9->getClientOriginalName();
                $filetowrite = $folder_destination . '/' . $filenya9;
                $file9->move($folder_destination, $filetowrite);


                // Available alpha caracters
                $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

                // generate a pin based on 2 * 7 digits + a random character
                $pin = mt_rand(1000000, 9999999)
                    . mt_rand(1000000, 9999999)
                    . $characters[rand(0, strlen($characters) - 1)];
                 
                
                // Surat
                $surat = Surat::insert([
                    't_users_id' => request('id_user'),
                    't_keperluan_id' => 1,
                    'id_pengajuan' => str_shuffle($pin),
                    'keterangan' => request('keterangan'),
                    'lamp_pengantarRT' => $filenya,
                    'ktp_pelapor' => $filenya9
                ]);

                // Create Anak
                $create = form_kelahiran::insert([
                    't_surat_id' => DB::table('t_surat')->orderBy('id', 'DESC')->limit(1)->value('id'),

                    'nama' => request('nama_anak'),
                    'jeniskelamin' => request('jenis_kelamin_anak'),
                    'nomor_tempat_kelahiran' => request('no_atau_tempat_kelahiran'),
                    'tempat_lahir' => request('tempat_lahir'),
                    'tgl_lahir' => request('tanggal_lahir'),
                    'pukul' => request('pukul_kelahiran'),
                    'jeniskelahiran' => request('jenis_kelahiran'),
                    'kelahiran_ke' => request('kelahiran_ke'),
                    'penolong_kelahiran' => request('penolong_kelahiran'),
                    'berat' => request('berat_bayi'),
                    'tinggi' => request('tinggi_bayi'),
                    'lamp_KeteranganRS' => $filenya6
                ]);

                // Create Ibu
                $ibu = t_ibu::insert([
                    'form_kelahirans_id' => DB::table('form_kelahirans')->orderBy('id', 'DESC')->limit(1)->value('id'),

                    'nik' => request('nik_ibu'),
                    'nama' => request('nama_ibu'),
                    'tgl_lahir' => request('tanggal_lahir_ibu'),
                    't_pekerjaan_id' => request('pekerjaan_ibu'),
                    'dukuh'  => request('dukuh_ibu'),
                    'rt' => request('rt_ibu'),
                    'kelurahan' => request('kelurahan_ibu'),
                    'kecamatan' => request('kecamatan_ibu'),
                    'kabupaten' => request('kabupaten_ibu'),
                    'lamp_ktp' => $filenya2
                ]);

                // Create Ayah
                $ayah = t_ayah::insert([
                    'form_kelahirans_id' => DB::table('form_kelahirans')->orderBy('id', 'DESC')->limit(1)->value('id'),

                    'nik' => request('nik_ayah'),
                    'nama' => request('nama_ayah'),
                    'tgl_lahir' => request('tanggal_lahir_ayah'),
                    't_pekerjaan_id' => request('pekerjaan_ayah'),
                    'dukuh'  => request('dukuh_ayah'),
                    'rt' => request('rt_ayah'),
                    'kelurahan' => request('kelurahan_ayah'),
                    'kecamatan' => request('kecamatan_ayah'),
                    'kabupaten' => request('kabupaten_ayah'),
                    'tgl_pencatatankawin' => request('tanggal_pencatatan_nikah'),
                    'lamp_ktp' => $filenya3,
                    'lamp_kk' => $filenya4,
                    'lamp_bukunikah' => $filenya5
                ]);

                // Create Saksi I
                $saksiI = t_saksi1::insert([
                    'form_kelahirans_id' => DB::table('form_kelahirans')->orderBy('id', 'DESC')->limit(1)->value('id'),

                    'nik' => request('nik_saksi1'),
                    'nama' => request('nama_saksi1'),
                    'tgl_lahir' => request('tanggal_lahir_saksi1'),
                    'jeniskelamin' => request('jenis_kelamin_saksi1'),
                    't_pekerjaan_id' => request('pekerjaan_saksi1'),
                    't_dukuh_id' => request('dukuh_saksi1'),
                    'rt' => request('rt_saksi1'),
                    'lamp_ktp' => $filenya7
                ]);

                // Create Saksi II
                $saksiII = t_saksi2::insert([
                    'form_kelahirans_id' => DB::table('form_kelahirans')->orderBy('id', 'DESC')->limit(1)->value('id'),

                    'nik' => request('nik_saksi2'),
                    'nama' => request('nama_saksi2'),
                    'tgl_lahir' => request('tanggal_lahir_saksi2'),
                    'jeniskelamin' => request('jenis_kelamin_saksi2'),
                    't_pekerjaan_id' => request('pekerjaan_saksi2'),
                    't_dukuh_id' => request('dukuh_saksi2'),
                    'rt' => request('rt_saksi2'),
                    'lamp_ktp' => $filenya8
                ]);

                // // Surat
                // $surat = Surat::insert([
                //     't_users_id' => request('id_user'),
                //     't_keperluan_id' => 1,
                //     'id_pengajuan' => str_shuffle($pin),
                //     // 'form_kelahirans_id' => DB::table('form_kelahirans')->orderBy('id', 'DESC')->limit(1)->value('id'),
                //     'lamp_pengantarRT' => $file->getClientOriginalName()
                // ]);

                if ($create) {
                    return redirect()->back()->with('success', 'Pengajuan berhasil terkirim, tunggu verifikasi dari petugas.');
                } else {
                    return redirect()->back()->with('errors', 'Pengajuan gagal, harap ulangi kembali.');
                }
            }

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Surat::where('id',$id)->get();
        $kelahiran = form_kelahiran::where('t_surat_id',$id)->get();
        $ibu = t_ibu::all();
        $ayah = t_ayah::all();
        $saksi1 = t_saksi1::all();
        $saksi2 = t_saksi2::all();

        $pekerjaan = Pekerjaan::all();
        $dukuh = Dukuh::all();
        $rt = Rt::all();
        $agama = Agama::all();
        $keperluan = Keperluan::all();
        $pengajuansurat = Surat::where('status', 'proses')->orderBy('created_at', 'desc')->get();
        $pengajuanumkm = Umkm::where('status', 'proses')->orderBy('created_at', 'desc')->get();
        $unverified =  User::where('role', 'unverified')->orderBy('created_at', 'desc')->get();
        return view('petugas.layanan_surat.edit.kelahiran', compact('data', 'kelahiran', 'ibu', 'ayah', 'saksi1', 'saksi2', 'pekerjaan', 'dukuh', 'rt', 'agama', 'keperluan', 'pengajuansurat', 'pengajuanumkm', 'unverified'));
    }

    public function lihat($id)
    {
        $data = Surat::where('id',$id)->get();
        $kelahiran = form_kelahiran::where('t_surat_id',$id)->get();
        $ibu = t_ibu::all();
        $ayah = t_ayah::all();
        $saksi1 = t_saksi1::all();
        $saksi2 = t_saksi2::all();

        $pekerjaan = Pekerjaan::all();
        $dukuh = Dukuh::all();
        $rt = Rt::all();
        $agama = Agama::all();
        $keperluan = Keperluan::all();
        $pengajuansurat = Surat::where('status', 'proses')->orderBy('created_at', 'desc')->get();
        $pengajuanumkm = Umkm::where('status', 'proses')->orderBy('created_at', 'desc')->get();
        $unverified =  User::where('role', 'unverified')->orderBy('created_at', 'desc')->get();
        return view('petugas.layanan_surat.lihat.kelahiran', compact('data', 'kelahiran', 'ibu', 'ayah', 'saksi1', 'saksi2', 'pekerjaan', 'dukuh', 'rt', 'agama', 'keperluan', 'pengajuansurat', 'pengajuanumkm', 'unverified'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array(
            'id'=>$id,
        );

        $ubah = array(
            'id_pengajuan'=>$id,
        );

        $data = array_merge($data, $request->all());

        $validator = Validator::make($data, [
            'id'=>'required|numeric|exists:t_surat,id',
            'no_surat'=>'required|max:255|string|unique:t_surat',
            'keterangan'=>'required|string',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {              
            $surat = Surat::where('id',$id)->update([
                'status'=> "terverifikasi",
                'no_surat'=>$request->no_surat,
                'keterangan' => $request->keterangan,

            ]);
            
            // //   Create qr
            // $qrstring = 'Pengajuan #' . $request->id_pengajuan.'/'.$request->id_name.'/'.$request->nik.'/'.$request->keperluannya;
            // // Passing data
            // view()->share(['surat' => $surat, 'qrstring' => $qrstring]);

            $ubah = Surat::find($ubah);
            $kelahiran = form_kelahiran::where('t_surat_id',$id)->get();
            $ibu = t_ibu::all();
            $ayah = t_ayah::all();
            $saksi1 = t_saksi1::all();
            $saksi2 = t_saksi2::all();
            view()->share([
                'suratnya' => $ubah,
                'kelahiran'=> $kelahiran,
                'ibu' => $ibu,
                'ayah' => $ayah,
                'saksi1' => $saksi1,
                'saksi2' => $saksi2
            ]);
            
            $pdf = PDF::loadView('mail.suket', $ubah)->setPaper('legal', 'landscape');
            $path = 'public/assets/1197268';
            $fileName =  ($request->id_pengajuan) . '-Suket.' . 'pdf' ;
            $pdf->save($path . '/' . $fileName);

            $pdf2 = PDF::loadView('mail.form_ketlahir', $ubah)->setPaper('legal', 'potrait');
            $path2 = 'public/assets/1197824';
            $fileName2 =  ($request->id_pengajuan) . '-Lampiran.' . 'pdf' ;
            $pdf2->save($path2 . '/' . $fileName2);
            
            // return $pdf->stream('test_pdf.pdf');

            Mail::send('mail.content', ['surat' => $surat], function($message) use ($pdf, $pdf2, $request){
                $message->from('alvicky98@gmail.com');
                $message->to($request->email);
                $message->subject('Terverifikasi ID #' .$request->id_pengajuan );
                $message->attachData($pdf->output(), $request->id_pengajuan . '-Suket.' . 'pdf');
                $message->attachData($pdf2->output(), $request->id_pengajuan . '-Lampiran.' . 'pdf');
            });

            if ($surat) {
                return redirect('dashboard-petugas/layanan/masuk')->with('success', 'Verifikasi surat pengajuan berhasil.');
            } else {
                return redirect('dashboard-petugas/layanan/masuk')->with('errors', 'Verifikasi surat pengajuan gagal, harap ulangi kembali..');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = array(
            'id'=>$id
        );

        $data2 = array(
            't_surat_id'=>$id,
        );

        $validator = Validator::make($data, [
            'id'=>'required|numeric|exists:t_surat,id',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $pengantarRt = Surat::where('id',$id)->first();
        Mail::send('mail.refuse', ['pengantarRt' => $pengantarRt], function($message) use ($pengantarRt) {
            $message->from('alvicky98@gmail.com');
            $message->to($pengantarRt->User['email']);
            $message->subject('Pengajuan Tertolak ID #' .$pengantarRt->id_pengajuan );
        });
        
        $kelahiran = DB::table('form_kelahirans')->where('t_surat_id', $data2)->limit(1)->value('id');

        $ibu = t_ibu::where('form_kelahirans_id', $kelahiran)->first();
        $ayah = t_ayah::where('form_kelahirans_id', $kelahiran)->first();
        $nikah = t_ayah::where('form_kelahirans_id', $kelahiran)->first();
        $kk = t_ayah::where('form_kelahirans_id', $kelahiran)->first();
        $saksi1 = t_saksi1::where('form_kelahirans_id', $kelahiran)->first();
        $saksi2 = t_saksi2::where('form_kelahirans_id', $kelahiran)->first();
        $keterangan = form_kelahiran::where('t_surat_id',$id)->first();

        // check file exist
        if (file_exists('public/assets/lampiran/pengantar_RT/'.$pengantarRt['lamp_pengantarRT'])) {
            unlink('public/assets/lampiran/pengantar_RT/'.$pengantarRt['lamp_pengantarRT']);
        }

        if (file_exists('public/assets/lampiran/ktp_ibu/'.$ibu['lamp_ktp'])) {
            unlink('public/assets/lampiran/ktp_ibu/'.$ibu['lamp_ktp']);
        }

        if (file_exists('public/assets/lampiran/ktp_ayah/'.$ayah['lamp_ktp'])) {
            unlink('public/assets/lampiran/ktp_ayah/'.$ayah['lamp_ktp']);
        }

        if (file_exists('public/assets/lampiran/saksi_I/'.$saksi1['lamp_ktp'])) {
            unlink('public/assets/lampiran/saksi_I/'.$saksi1['lamp_ktp']);
        }

        if (file_exists('public/assets/lampiran/saksi_II/'.$saksi2['lamp_ktp'])) {
            unlink('public/assets/lampiran/saksi_II/'.$saksi2['lamp_ktp']);
        }

        if (file_exists('public/assets/lampiran/kartu_keluarga/'.$kk['lamp_kk'])) {
            unlink('public/assets/lampiran/kartu_keluarga/'.$kk['lamp_kk']);
        }

        if (file_exists('public/assets/lampiran/keterangan_RS/'.$keterangan['lamp_KeteranganRS'])) {
            unlink('public/assets/lampiran/keterangan_RS/'.$keterangan['lamp_KeteranganRS']);
        }

        if (file_exists('public/assets/lampiran/buku_nikah/'.$nikah['lamp_bukunikah'])) {
            unlink('public/assets/lampiran/buku_nikah/'.$nikah['lamp_bukunikah']);
        }

        $delete=Surat::where('id', $id)->delete();
        
        if ($delete) {
            return redirect()->back()->with('success','Pengajuan berhasil dihapus.');
        } else {
            return redirect()->back()->with('errors','Pengajuan gagal dihapus, harap ulangi kembali.');
        }
    }
}
