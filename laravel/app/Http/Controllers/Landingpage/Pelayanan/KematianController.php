<?php

namespace App\Http\Controllers\Landingpage\Pelayanan;

use App\Http\Controllers\Controller;
use App\Model\Agama;
use App\Model\Dukuh;
use App\Model\Form\form_kematian;
use App\Model\Form\t_ayah;
use App\Model\Form\t_ibu;
use App\Model\Form\t_saksi1;
use App\Model\Form\t_saksi2;
use App\Model\Keperluan;
use App\Model\Pekerjaan;
use App\Model\Profil\t_kelurahan;
use App\Model\Rt;
use App\Model\Surat;
use App\Model\Umkm;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Mail;

class KematianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('landingpage.pelayanan.kematian',[
            'user'=> User::all(),
            'pekerjaan' => Pekerjaan::all(),
            'dukuh' => Dukuh::all(),
            'rt' => Rt::all(),
            'agama' => Agama::all(),
            'keperluan' => Keperluan::all(),

            'kelurahan' => t_kelurahan::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->role == "unverified"){
            return redirect()->back()->with('warning', 'Pengajuan gagal, akun anda belum terverifikasi.');
        } else {
            $validator = Validator::make($request->all(), [
                'id_user'=>['required', 'max:255'],
                'keterangan'=>'required|string',
                'lampiran_ktp_pelapor' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
                'lampiran_surat_pengantar_rt' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
                // JENAZAH
                'nik_jenazah'=>['required', 'numeric', 'digits:16'],
                'nama_jenazah'=>['required', 'string', 'max:255'],
                'jenis_kelamin_jenazah'=>['required', 'string', 'max:255'],
                'tempat_lahir_jenazah'=>['required', 'string', 'max:255'],
                'tanggal_lahir_jenazah'=>['required', 'max:255'],
                'rt_jenazah'=>['required', 'string', 'max:255'],
                'tanggal_kematian'=>['required', 'string', 'max:255'],
                'pukul_kematian'=>['required', 'max:255'],
                'sebab_kematian'=>['required', 'string', 'max:255'],
                'tempat_kematian'=>['required', 'string', 'max:255'],
                'menerangkan'=>['required', 'string', 'max:255'],

                'dukuh_jenazah'=>['required', 'string', 'max:255'],
                'pekerjaan_jenazah'=>['required', 'string', 'max:255'],
                'agama_jenazah'=>['required', 'string', 'max:255'],

                'lampiran_ktp_jenazah' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
                // 'lampiran_surat_keterangan_rs' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
                // IBU
                'nik_ibu'=>['required', 'numeric', 'digits:16'],
                'nama_ibu'=>['required', 'string', 'max:255'],
                'tanggal_lahir_ibu'=>['required', 'string', 'max:255'],
                'pekerjaan_ibu'=>['required', 'max:255'],
                'dukuh_ibu'=>['required', 'string', 'max:255'],
                'rt_ibu'=>['required', 'string', 'max:255'],
                'kelurahan_ibu'=>['required', 'string', 'max:255'],
                'kecamatan_ibu'=>['required', 'string', 'max:255'],
                'kabupaten_ibu'=>['required', 'string', 'max:255'],

                'lampiran_ktp_ibu' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
                // AYAH 
                'nik_ayah'=>['required', 'numeric', 'digits:16'],
                'nama_ayah'=>['required', 'string', 'max:255'],
                'tanggal_lahir_ayah'=>['required', 'string', 'max:255'],
                'pekerjaan_ayah'=>['required', 'max:255'],
                'dukuh_ayah'=>['required', 'string', 'max:255'],
                'rt_ayah'=>['required', 'string', 'max:255'],
                'kelurahan_ayah'=>['required', 'string', 'max:255'],
                'kecamatan_ayah'=>['required', 'string', 'max:255'],
                'kabupaten_ayah'=>['required', 'string', 'max:255'],

                'lampiran_ktp_ayah' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
                'lampiran_kartu_keluarga' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
                // SAKSI I
                'nik_saksi1'=>['required', 'numeric', 'digits:16'],
                'nama_saksi1'=>['required', 'string', 'max:255'],
                'tanggal_lahir_saksi1'=>['required', 'string', 'max:255'],
                'dukuh_saksi1'=>['required', 'string', 'max:255'],
                'pekerjaan_saksi1'=>['required', 'max:255'],
                'jenis_kelamin_saksi1'=>['required', 'string', 'max:255'],
                'rt_saksi1'=>['required', 'string', 'max:255'],

                'lampiran_ktp_saksi_I' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],

                // SAKSI I
                'nik_saksi2'=>['required', 'numeric', 'digits:16'],
                'nama_saksi2'=>['required', 'string', 'max:255'],
                'tanggal_lahir_saksi2'=>['required', 'string', 'max:255'],
                'dukuh_saksi2'=>['required', 'string', 'max:255'],
                'pekerjaan_saksi2'=>['required', 'max:255'],
                'jenis_kelamin_saksi2'=>['required', 'string', 'max:255'],
                'rt_saksi2'=>['required', 'string', 'max:255'],

                'lampiran_ktp_saksi_II' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            } else {
                // Available alpha caracters
                $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

                // generate a pin based on 2 * 7 digits + a random character
                $pin = mt_rand(1000000, 9999999)
                    . mt_rand(1000000, 9999999)
                    . $characters[rand(0, strlen($characters) - 1)];
                    
                // Pengantar RT
                $file = $request->file('lampiran_surat_pengantar_rt');

                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file->getClientOriginalName())) {
                    return redirect()->back()->with('errors','invalid file name.');
                }

                $folder_destination = 'public/assets/lampiran/pengantar_RT';

                $filenya = str_shuffle($pin) . '-' . $file->getClientOriginalName();
                $filetowrite = $folder_destination . '/' . $filenya;
                $file->move($folder_destination, $filetowrite);

                // KTP IBU
                $file2 = $request->file('lampiran_ktp_ibu');

                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file2->getClientOriginalName())) {
                    return redirect()->back()->with('errors','invalid file name.');
                }

                $folder_destination = 'public/assets/lampiran/ktp_ibu';

                $filenya2 = str_shuffle($pin) . '-' . $file2->getClientOriginalName();
                $filetowrite = $folder_destination . '/' . $filenya2;
                $file2->move($folder_destination, $filetowrite);

                // KTP AYAH
                $file3 = $request->file('lampiran_ktp_ayah');

                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file3->getClientOriginalName())) {
                    return redirect()->back()->with('errors','invalid file name.');
                }

                $folder_destination = 'public/assets/lampiran/ktp_ayah';

                $filenya3 = str_shuffle($pin) . '-' . $file3->getClientOriginalName();
                $filetowrite = $folder_destination . '/' . $filenya3;
                $file3->move($folder_destination, $filetowrite);

                // Kartu Keluarga
                $file4 = $request->file('lampiran_kartu_keluarga');

                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file4->getClientOriginalName())) {
                    return redirect()->back()->with('errors','invalid file name.');
                }

                $folder_destination = 'public/assets/lampiran/kartu_keluarga';

                $filenya4 = str_shuffle($pin) . '-' . $file4->getClientOriginalName();
                $filetowrite = $folder_destination . '/' . $filenya4;
                $file4->move($folder_destination, $filetowrite);

                // KTP Jenazah
                $file5 = $request->file('lampiran_ktp_jenazah');

                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file5->getClientOriginalName())) {
                    return redirect()->back()->with('errors','invalid file name.');
                }

                $folder_destination = 'public/assets/lampiran/ktp_jenazah';

                $filenya5 = str_shuffle($pin) . '-' . $file5->getClientOriginalName();
                $filetowrite = $folder_destination . '/' . $filenya5;
                $file5->move($folder_destination, $filetowrite);

                if ($request->hasFile('lampiran_surat_keterangan_rs')) {
                    // Surat Keterangan RS
                    $file6 = $request->file('lampiran_surat_keterangan_rs');

                    if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file6->getClientOriginalName())) {
                        return redirect()->back()->with('errors','invalid file name.');
                    }

                    $folder_destination = 'public/assets/lampiran/keterangan_kematian_RS';

                    $filenya6 = str_shuffle($pin) . '-' . $file6->getClientOriginalName();
                    $filetowrite = $folder_destination . '/' . $filenya6;
                    $file6->move($folder_destination, $filetowrite);
                }

                // KTP SAKSI I
                $file7 = $request->file('lampiran_ktp_saksi_I');

                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file7->getClientOriginalName())) {
                    return redirect()->back()->with('errors','invalid file name.');
                }

                $folder_destination = 'public/assets/lampiran/saksi_I';

                $filenya7 = str_shuffle($pin) . '-' . $file7->getClientOriginalName();
                $filetowrite = $folder_destination . '/' . $filenya7;
                $file7->move($folder_destination, $filetowrite);

                // KTP SAKSI II
                $file8 = $request->file('lampiran_ktp_saksi_II');

                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file8->getClientOriginalName())) {
                    return redirect()->back()->with('errors','invalid file name.');
                }

                $folder_destination = 'public/assets/lampiran/saksi_II';

                $filenya8 = str_shuffle($pin) . '-' . $file8->getClientOriginalName();
                $filetowrite = $folder_destination . '/' . $filenya8;
                $file8->move($folder_destination, $filetowrite);

                //KTP PELAPOR 
                $file9 = $request->file('lampiran_ktp_pelapor');

                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file9->getClientOriginalName())) {
                    return redirect()->back()->with('errors','invalid file name.');
                }

                $folder_destination = 'public/assets/lampiran/ktp_pelapor';

                $filenya9 = str_shuffle($pin) . '-' . $file9->getClientOriginalName();
                $filetowrite = $folder_destination . '/' . $filenya9;
                $file9->move($folder_destination, $filetowrite);


                // Available alpha caracters
                $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

                // generate a pin based on 2 * 7 digits + a random character
                $pin = mt_rand(1000000, 9999999)
                    . mt_rand(1000000, 9999999)
                    . $characters[rand(0, strlen($characters) - 1)];

                if ($request->hasFile('lampiran_surat_keterangan_rs')) {
                    // Surat
                    $surat = Surat::insert([
                        't_users_id' => request('id_user'),
                        't_keperluan_id' => 2,
                        'id_pengajuan' => str_shuffle($pin),
                        'keterangan' => request('keterangan'),
                        'lamp_pengantarRT' => $filenya,
                        'ktp_pelapor' => $filenya9
                    ]);
                    
                    // Create Jenazah
                    $create = form_kematian::insert([
                        't_surat_id' => DB::table('t_surat')->orderBy('id', 'DESC')->limit(1)->value('id'),
                        
                        'nik' => request('nik_jenazah'),
                        'nama' => request('nama_jenazah'),
                        'jeniskelamin' => request('jenis_kelamin_jenazah'),
                        'tempat_lahir' => request('tempat_lahir_jenazah'),
                        'tgl_lahir' => request('tanggal_lahir_jenazah'),
                        'tgl_mati' => request('tanggal_kematian'),
                        'pukul' => request('pukul_kematian'),
                        'sebab' => request('sebab_kematian'),
                        'tempat_kematian' => request('tempat_kematian'),
                        'menerangkan' => request('menerangkan'),
                        'rt' => request('rt_jenazah'),
                        
                        't_agama_id'=> request('agama_jenazah'),
                        't_pekerjaan_id'=> request('pekerjaan_jenazah'),
                        't_dukuh_id'=> request('dukuh_jenazah'),

                        'lamp_ktp' => $filenya5,
                        'lamp_keterangankematian' => $filenya6
                    ]);

                    // Create Ibu
                    $ibu = t_ibu::insert([
                        'form_kematians_id' => DB::table('form_kematians')->orderBy('id', 'DESC')->limit(1)->value('id'),

                        'nik' => request('nik_ibu'),
                        'nama' => request('nama_ibu'),
                        'tgl_lahir' => request('tanggal_lahir_ibu'),
                        't_pekerjaan_id' => request('pekerjaan_ibu'),
                        'dukuh'  => request('dukuh_ibu'),
                        'rt' => request('rt_ibu'),
                        'kelurahan' => request('kelurahan_ibu'),
                        'kecamatan' => request('kecamatan_ibu'),
                        'kabupaten' => request('kabupaten_ibu'),
                        'lamp_ktp' => $filenya2
                    ]);

                    // Create Ayah
                    $ayah = t_ayah::insert([
                        'form_kematians_id' => DB::table('form_kematians')->orderBy('id', 'DESC')->limit(1)->value('id'),

                        'nik' => request('nik_ayah'),
                        'nama' => request('nama_ayah'),
                        'tgl_lahir' => request('tanggal_lahir_ayah'),
                        't_pekerjaan_id' => request('pekerjaan_ayah'),
                        'dukuh'  => request('dukuh_ayah'),
                        'rt' => request('rt_ayah'),
                        'kelurahan' => request('kelurahan_ayah'),
                        'kecamatan' => request('kecamatan_ayah'),
                        'kabupaten' => request('kabupaten_ayah'),
                        'lamp_ktp' => $filenya3,
                        'lamp_kk' => $filenya4
                    ]);

                    // Create Saksi I
                    $saksiI = t_saksi1::insert([
                        'form_kematians_id' => DB::table('form_kematians')->orderBy('id', 'DESC')->limit(1)->value('id'),

                        'nik' => request('nik_saksi1'),
                        'nama' => request('nama_saksi1'),
                        'tgl_lahir' => request('tanggal_lahir_saksi1'),
                        'jeniskelamin' => request('jenis_kelamin_saksi1'),
                        't_pekerjaan_id' => request('pekerjaan_saksi1'),
                        't_dukuh_id' => request('dukuh_saksi1'),
                        'rt' => request('rt_saksi1'),
                        'lamp_ktp' => $filenya7
                    ]);

                    // Create Saksi II
                    $saksiII = t_saksi2::insert([
                        'form_kematians_id' => DB::table('form_kematians')->orderBy('id', 'DESC')->limit(1)->value('id'),

                        'nik' => request('nik_saksi2'),
                        'nama' => request('nama_saksi2'),
                        'tgl_lahir' => request('tanggal_lahir_saksi2'),
                        'jeniskelamin' => request('jenis_kelamin_saksi2'),
                        't_pekerjaan_id' => request('pekerjaan_saksi2'),
                        't_dukuh_id' => request('dukuh_saksi2'),
                        'rt' => request('rt_saksi2'),
                        'lamp_ktp' => $filenya8
                    ]);

                } else {
                    // Surat
                    $surat = Surat::insert([
                        't_users_id' => request('id_user'),
                        't_keperluan_id' => 2,
                        'id_pengajuan' => str_shuffle($pin),
                        'keterangan' => request('keterangan'),
                        'lamp_pengantarRT' => $filenya,
                        'ktp_pelapor' => $filenya9
                    ]);
                    
                    // Create Akta Kematian
                    $create = form_kematian::insert([
                        't_surat_id' => DB::table('t_surat')->orderBy('id', 'DESC')->limit(1)->value('id'),
                        
                        'nik' => request('nik_jenazah'),
                        'nama' => request('nama_jenazah'),
                        'jeniskelamin' => request('jenis_kelamin_jenazah'),
                        'tempat_lahir' => request('tempat_lahir_jenazah'),
                        'tgl_lahir' => request('tanggal_lahir_jenazah'),
                        'tgl_mati' => request('tanggal_kematian'),
                        'pukul' => request('pukul_kematian'),
                        'sebab' => request('sebab_kematian'),
                        'tempat_kematian' => request('tempat_kematian'),
                        'menerangkan' => request('menerangkan'),
                        'rt' => request('rt_jenazah'),
                        
                        't_agama_id'=> request('agama_jenazah'),
                        't_pekerjaan_id'=> request('pekerjaan_jenazah'),
                        't_dukuh_id'=> request('dukuh_jenazah'),

                        'lamp_ktp' => $filenya5
                    ]);

                    // Create Ibu
                    $ibu = t_ibu::insert([
                        'form_kematians_id' => DB::table('form_kematians')->orderBy('id', 'DESC')->limit(1)->value('id'),

                        'nik' => request('nik_ibu'),
                        'nama' => request('nama_ibu'),
                        'tgl_lahir' => request('tanggal_lahir_ibu'),
                        't_pekerjaan_id' => request('pekerjaan_ibu'),
                        'dukuh'  => request('dukuh_ibu'),
                        'rt' => request('rt_ibu'),
                        'kelurahan' => request('kelurahan_ibu'),
                        'kecamatan' => request('kecamatan_ibu'),
                        'kabupaten' => request('kabupaten_ibu'),
                        'lamp_ktp' => $filenya2
                    ]);

                    // Create Ayah
                    $ayah = t_ayah::insert([
                        'form_kematians_id' => DB::table('form_kematians')->orderBy('id', 'DESC')->limit(1)->value('id'),

                        'nik' => request('nik_ayah'),
                        'nama' => request('nama_ayah'),
                        'tgl_lahir' => request('tanggal_lahir_ayah'),
                        't_pekerjaan_id' => request('pekerjaan_ayah'),
                        'dukuh'  => request('dukuh_ayah'),
                        'rt' => request('rt_ayah'),
                        'kelurahan' => request('kelurahan_ayah'),
                        'kecamatan' => request('kecamatan_ayah'),
                        'kabupaten' => request('kabupaten_ayah'),
                        'lamp_ktp' => $filenya3,
                        'lamp_kk' => $filenya4
                    ]);

                    // Create Saksi I
                    $saksiI = t_saksi1::insert([
                        'form_kematians_id' => DB::table('form_kematians')->orderBy('id', 'DESC')->limit(1)->value('id'),

                        'nik' => request('nik_saksi1'),
                        'nama' => request('nama_saksi1'),
                        'tgl_lahir' => request('tanggal_lahir_saksi1'),
                        'jeniskelamin' => request('jenis_kelamin_saksi1'),
                        't_pekerjaan_id' => request('pekerjaan_saksi1'),
                        't_dukuh_id' => request('dukuh_saksi1'),
                        'rt' => request('rt_saksi1'),
                        'lamp_ktp' => $filenya7
                    ]);

                    // Create Saksi II
                    $saksiII = t_saksi2::insert([
                        'form_kematians_id' => DB::table('form_kematians')->orderBy('id', 'DESC')->limit(1)->value('id'),

                        'nik' => request('nik_saksi2'),
                        'nama' => request('nama_saksi2'),
                        'tgl_lahir' => request('tanggal_lahir_saksi2'),
                        'jeniskelamin' => request('jenis_kelamin_saksi2'),
                        't_pekerjaan_id' => request('pekerjaan_saksi2'),
                        't_dukuh_id' => request('dukuh_saksi2'),
                        'rt' => request('rt_saksi2'),
                        'lamp_ktp' => $filenya8
                    ]);
                }

                if ($create) {
                    return redirect()->back()->with('success', 'Pengajuan berhasil terkirim, tunggu verifikasi dari petugas.');
                } else {
                    return redirect()->back()->with('errors', 'Pengajuan gagal, harap ulangi kembali.');
                }
            }

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Surat::where('id',$id)->get();
        $kematian = form_kematian::where('t_surat_id',$id)->get();
        $ibu = t_ibu::all();
        $ayah = t_ayah::all();
        $saksi1 = t_saksi1::all();
        $saksi2 = t_saksi2::all();

        $pekerjaan = Pekerjaan::all();
        $dukuh = Dukuh::all();
        $rt = Rt::all();
        $agama = Agama::all();
        $keperluan = Keperluan::all();
        $pengajuansurat = Surat::where('status', 'proses')->orderBy('created_at', 'desc')->get();
        $pengajuanumkm = Umkm::where('status', 'proses')->orderBy('created_at', 'desc')->get();
        $unverified =  User::where('role', 'unverified')->orderBy('created_at', 'desc')->get();
        return view('petugas.layanan_surat.edit.kematian', compact('data', 'kematian', 'ibu', 'ayah', 'saksi1', 'saksi2', 'pekerjaan', 'dukuh', 'rt', 'agama', 'keperluan', 'pengajuansurat', 'pengajuanumkm', 'unverified'));
    }

    public function lihat($id)
    {
        $data = Surat::where('id',$id)->get();
        $kematian = form_kematian::where('t_surat_id',$id)->get();
        $ibu = t_ibu::all();
        $ayah = t_ayah::all();
        $saksi1 = t_saksi1::all();
        $saksi2 = t_saksi2::all();

        $pekerjaan = Pekerjaan::all();
        $dukuh = Dukuh::all();
        $rt = Rt::all();
        $agama = Agama::all();
        $keperluan = Keperluan::all();
        $pengajuansurat = Surat::where('status', 'proses')->orderBy('created_at', 'desc')->get();
        $pengajuanumkm = Umkm::where('status', 'proses')->orderBy('created_at', 'desc')->get();
        $unverified =  User::where('role', 'unverified')->orderBy('created_at', 'desc')->get();
        return view('petugas.layanan_surat.lihat.kematian', compact('data', 'kematian', 'ibu', 'ayah', 'saksi1', 'saksi2', 'pekerjaan', 'dukuh', 'rt', 'agama', 'keperluan', 'pengajuansurat', 'pengajuanumkm', 'unverified'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array(
            'id'=>$id,
        );

        $ubah = array(
            'id_pengajuan'=>$id,
        );

        $data = array_merge($data, $request->all());

        $validator = Validator::make($data, [
            'id'=>'required|numeric|exists:t_surat,id',
            // 'status_pengajuan'=>'required|max:100',
            'no_surat'=>'required|max:255|string|unique:t_surat',
            'keterangan'=>'required|string',
            // 'tanda_tangan_kepala_desa' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048']
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {              
            $surat = Surat::where('id',$id)->update([
                'status'=> "terverifikasi",
                'no_surat'=>$request->no_surat,
                'keterangan' => $request->keterangan,

            ]);
            
            //   Create qr
            $qrstring = 'Pengajuan #' . $request->id_pengajuan.'/'.$request->id_name.'/'.$request->nik.'/'.$request->keperluannya;
            // Passing data
            view()->share(['surat' => $surat, 'qrstring' => $qrstring]);

            $ubah = Surat::find($ubah);
            $kematian = form_kematian::where('t_surat_id',$id)->get();
            $ibu = t_ibu::all();
            $ayah = t_ayah::all();
            $saksi1 = t_saksi1::all();
            $saksi2 = t_saksi2::all();
            view()->share([
                'suratnya' => $ubah,
                'kematian'=> $kematian,
                'ibu' => $ibu,
                'ayah' => $ayah,
                'saksi1' => $saksi1,
                'saksi2' => $saksi2
            ]);
            
            $pdf = PDF::loadView('mail.suket', $ubah)->setPaper('legal', 'landscape');
            $path = 'public/assets/1197268';
            $fileName =  ($request->id_pengajuan) . '-Suket.' . 'pdf' ;
            $pdf->save($path . '/' . $fileName);

            $pdf2 = PDF::loadView('mail.form_ketmati', $ubah)->setPaper('legal', 'potrait');
            $path2 = 'public/assets/1197824';
            $fileName2 =  ($request->id_pengajuan) . '-Lampiran.' . 'pdf' ;
            $pdf2->save($path2 . '/' . $fileName2);
            
            // return $pdf->download($fileName);
            // return $pdf2->stream('test-kematian_pdf.pdf');

            Mail::send('mail.content', ['surat' => $surat], function($message) use ($pdf, $pdf2, $request){
                $message->from('alvicky98@gmail.com');
                $message->to($request->email);
                $message->subject('Terverifikasi ID #' .$request->id_pengajuan );
                $message->attachData($pdf->output(), $request->id_pengajuan . '-Suket.' . 'pdf');
                $message->attachData($pdf2->output(), $request->id_pengajuan . '-Lampiran.' . 'pdf');
            });

            if ($surat) {
                return redirect('dashboard-petugas/layanan/masuk')->with('success', 'Verifikasi surat pengajuan berhasil.');
            } else {
                return redirect('dashboard-petugas/layanan/masuk')->with('errors', 'Verifikasi surat pengajuan gagal, harap ulangi kembali..');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = array(
            'id'=>$id
        );

        $data2 = array(
            't_surat_id'=>$id,
        );

        $validator = Validator::make($data, [
            'id'=>'required|numeric|exists:t_surat,id',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $pengantarRt = Surat::where('id',$id)->first();
        Mail::send('mail.refuse', ['pengantarRt' => $pengantarRt], function($message) use ($pengantarRt) {
            $message->from('alvicky98@gmail.com');
            $message->to($pengantarRt->User['email']);
            $message->subject('Pengajuan Tertolak ID #' .$pengantarRt->id_pengajuan );
        });
        
        $kematian = DB::table('form_kematians')->where('t_surat_id', $data2)->limit(1)->value('id');

        $ibu = t_ibu::where('form_kematians_id', $kematian)->first();
        $ayah = t_ayah::where('form_kematians_id', $kematian)->first();
        $nikah = t_ayah::where('form_kematians_id', $kematian)->first();
        $kk = t_ayah::where('form_kematians_id', $kematian)->first();
        $saksi1 = t_saksi1::where('form_kematians_id', $kematian)->first();
        $saksi2 = t_saksi2::where('form_kematians_id', $kematian)->first();

        $keterangan = form_kematian::where('t_surat_id',$id)->first();
        $keterangan2 = form_kematian::where('t_surat_id',$id)->where('lamp_keterangankematian', null)->first();
        $jenazah = form_kematian::where('t_surat_id',$id)->first();

        if ($keterangan2 != null) {
            // check file exist
            if (file_exists('public/assets/lampiran/pengantar_RT/'.$pengantarRt['lamp_pengantarRT'])) {
                unlink('public/assets/lampiran/pengantar_RT/'.$pengantarRt['lamp_pengantarRT']);
            }

            if (file_exists('public/assets/lampiran/ktp_ibu/'.$ibu['lamp_ktp'])) {
                unlink('public/assets/lampiran/ktp_ibu/'.$ibu['lamp_ktp']);
            }

            if (file_exists('public/assets/lampiran/ktp_ayah/'.$ayah['lamp_ktp'])) {
                unlink('public/assets/lampiran/ktp_ayah/'.$ayah['lamp_ktp']);
            }

            if (file_exists('public/assets/lampiran/saksi_I/'.$saksi1['lamp_ktp'])) {
                unlink('public/assets/lampiran/saksi_I/'.$saksi1['lamp_ktp']);
            }

            if (file_exists('public/assets/lampiran/saksi_II/'.$saksi2['lamp_ktp'])) {
                unlink('public/assets/lampiran/saksi_II/'.$saksi2['lamp_ktp']);
            }

            if (file_exists('public/assets/lampiran/kartu_keluarga/'.$kk['lamp_kk'])) {
                unlink('public/assets/lampiran/kartu_keluarga/'.$kk['lamp_kk']);
            }

            if (file_exists('public/assets/lampiran/ktp_jenazah/'.$jenazah['lamp_ktp'])) {
                unlink('public/assets/lampiran/ktp_jenazah/'.$jenazah['lamp_ktp']);
            }
        } else {
            // check file exist
            if (file_exists('public/assets/lampiran/pengantar_RT/'.$pengantarRt['lamp_pengantarRT'])) {
                unlink('public/assets/lampiran/pengantar_RT/'.$pengantarRt['lamp_pengantarRT']);
            }

            if (file_exists('public/assets/lampiran/ktp_ibu/'.$ibu['lamp_ktp'])) {
                unlink('public/assets/lampiran/ktp_ibu/'.$ibu['lamp_ktp']);
            }

            if (file_exists('public/assets/lampiran/ktp_ayah/'.$ayah['lamp_ktp'])) {
                unlink('public/assets/lampiran/ktp_ayah/'.$ayah['lamp_ktp']);
            }

            if (file_exists('public/assets/lampiran/saksi_I/'.$saksi1['lamp_ktp'])) {
                unlink('public/assets/lampiran/saksi_I/'.$saksi1['lamp_ktp']);
            }

            if (file_exists('public/assets/lampiran/saksi_II/'.$saksi2['lamp_ktp'])) {
                unlink('public/assets/lampiran/saksi_II/'.$saksi2['lamp_ktp']);
            }

            if (file_exists('public/assets/lampiran/kartu_keluarga/'.$kk['lamp_kk'])) {
                unlink('public/assets/lampiran/kartu_keluarga/'.$kk['lamp_kk']);
            }

            if (file_exists('public/assets/lampiran/keterangan_kematian_RS/'.$keterangan['lamp_keterangankematian'])) {
                unlink('public/assets/lampiran/keterangan_kematian_RS/'.$keterangan['lamp_keterangankematian']);
            }

            if (file_exists('public/assets/lampiran/ktp_jenazah/'.$jenazah['lamp_ktp'])) {
                unlink('public/assets/lampiran/ktp_jenazah/'.$jenazah['lamp_ktp']);
            }
        }

        $delete=Surat::where('id', $id)->delete();
        
        if ($delete) {
            return redirect()->back()->with('success','Pengajuan berhasil dihapus.');
        } else {
            return redirect()->back()->with('errors','Pengajuan gagal dihapus, harap ulangi kembali.');
        }
    }
}
