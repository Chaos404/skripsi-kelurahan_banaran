<?php

namespace App\Http\Controllers\Landingpage;

use App\Http\Controllers\Controller;
use App\Model\Profil\t_jabatan;
use App\Model\Profil\t_kelurahan;
use App\Model\Profil\t_perangkatdesa;
use App\Model\Profil\t_perangkatdesastruktur;

class StrukturpemerintahanController extends Controller
{
    public function index(){
        return view('landingpage.strukturpemerintahan', [
            'susunan' => t_perangkatdesastruktur::all(),

            'perangkatdesa' => t_perangkatdesa::all(),
            'jabatan' => t_jabatan::all(),

            'kelurahan' => t_kelurahan::all()
        ]);
    }
}
