<?php

namespace App\Http\Controllers\DashboardPetugas\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use PDF;
use App\Model\Surat;
use App\Model\Umkm;
use App\User;

class UmkmController extends Controller
{
    public function index(){
        $umkm = Umkm::where('status', 'terverifikasi')->get();

        $unverified =  User::where('role', 'unverified')->orderBy('created_at', 'desc')->limit(4)->get();
        $pengajuansurat = Surat::where('status', 'proses')->orderBy('created_at', 'desc')->limit(4)->get();
        $pengajuanumkm = Umkm::where('status', 'proses')->orderBy('created_at', 'desc')->limit(4)->get();

        return view('petugas.laporan.umkm_pdf',compact('umkm', 'unverified', 'pengajuansurat', 'pengajuanumkm'));
    }

    public function report()
    {
        //INISIASI 30 HARI RANGE SAAT INI JIKA HALAMAN PERTAMA KALI DI-LOAD
        //KITA GUNAKAN STARTOFMONTH UNTUK MENGAMBIL TANGGAL 1
        $start = Carbon::now()->startOfMonth()->format('Y-m-d H:i:s');
        //DAN ENDOFMONTH UNTUK MENGAMBIL TANGGAL TERAKHIR DIBULAN YANG BERLAKU SAAT INI
        $end = Carbon::now()->endOfMonth()->format('Y-m-d H:i:s');

        //JIKA USER MELAKUKAN FILTER MANUAL, MAKA PARAMETER DATE AKAN TERISI
        if (request()->date != '') {
            //MAKA FORMATTING TANGGALNYA BERDASARKAN FILTER USER
            $date = explode(' - ' ,request()->date);
            $start = Carbon::parse($date[0])->format('Y-m-d') . ' 00:00:01';
            $end = Carbon::parse($date[1])->format('Y-m-d') . ' 23:59:59';
        }

        //BUAT QUERY KE DB MENGGUNAKAN WHEREBETWEEN DARI TANGGAL FILTER
        $umkm = Umkm::where('status', 'terverifikasi')->whereBetween('updated_at', ['ASC', $end])->get();

        $unverified =  User::where('role', 'unverified')->orderBy('created_at', 'desc')->limit(4)->get();
        $pengajuansurat = Surat::where('status', 'proses')->orderBy('created_at', 'desc')->limit(4)->get();
        $pengajuanumkm = Umkm::where('status', 'proses')->orderBy('created_at', 'desc')->limit(4)->get();
        //KEMUDIAN LOAD VIEW
        return view('petugas.laporan.umkm', compact('umkm', 'unverified', 'pengajuansurat', 'pengajuanumkm'));
    }

    public function reportPdf($daterange)
    {
        $date = explode('+', $daterange); //EXPLODE TANGGALNYA UNTUK MEMISAHKAN START & END
        //DEFINISIKAN VARIABLENYA DENGAN FORMAT TIMESTAMPS
        $start = Carbon::parse($date[0])->format('Y-m-d') . ' 00:00:01';
        $end = Carbon::parse($date[1])->format('Y-m-d') . ' 23:59:59';

        //KEMUDIAN BUAT QUERY BERDASARKAN RANGE CREATED_AT YANG TELAH DITETAPKAN RANGENYA DARI $START KE $END
        $umkm = Umkm::where('status', 'terverifikasi')->whereBetween('updated_at', [$start, $end])->get();

        //LOAD VIEW UNTUK PDFNYA DENGAN MENGIRIMKAN DATA DARI HASIL QUERY
        $pdf = PDF::loadView('petugas.laporan.umkm_pdf', compact('umkm', 'date'))->setPaper('legal', 'potrait');
        //GENERATE PDF-NYA
        // return $pdf->stream();
        return $pdf->download('Laporan UMKM Banaran.pdf');
    }

    public function reportAll()
    {
        //BUAT QUERY KE DB MENGGUNAKAN WHEREBETWEEN DARI TANGGAL FILTER
        $umkm = Umkm::where('status', 'terverifikasi')->get();

        $unverified =  User::where('role', 'unverified')->orderBy('created_at', 'desc')->limit(4)->get();
        $pengajuansurat = Surat::where('status', 'proses')->orderBy('created_at', 'desc')->limit(4)->get();
        $pengajuanumkm = Umkm::where('status', 'proses')->orderBy('created_at', 'desc')->limit(4)->get();
        //KEMUDIAN LOAD VIEW
        return view('petugas.laporan.umkm', compact('umkm', 'unverified', 'pengajuansurat', 'pengajuanumkm'));
    }

    public function reportAllPdf()
    {
        $umkm = Umkm::where('status', 'terverifikasi')->get();
        $awal = Umkm::where('status', 'terverifikasi')->orderBy('updated_at','ASC')->oldest()->first();
        $akhir = Umkm::where('status', 'terverifikasi')->orderBy('updated_at', 'DESC')->latest()->first();


        //LOAD VIEW UNTUK PDFNYA DENGAN MENGIRIMKAN DATA DARI HASIL QUERY
        $pdf = PDF::loadView('petugas.laporan.umkm_allpdf', compact('umkm', 'awal', 'akhir'))->setPaper('legal', 'potrait');
        //GENERATE PDF-NYA
        // return $pdf->stream();
        return $pdf->download('Laporan UMKM Banaran.pdf');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
