<?php

namespace App\Http\Controllers\DashboardPetugas\Manageuser;

use App\Http\Controllers\Controller;
use App\Model\Agama;
use App\Model\Dukuh;
use App\Model\Pekerjaan;
use App\Model\Rt;
use App\Model\Surat;
use App\Model\Umkm;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;

class RegistrasiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('petugas.manageuser.register', [
            'user'=> User::where('role', 'unverified')->get(),
            'unverified'=> User::where('role', 'unverified')->orderBy('created_at', 'desc')->limit(4)->get(),
            'pengajuansurat'=>Surat::where('status', 'proses')->orderBy('created_at', 'desc')->limit(4)->get(),
            'pengajuanumkm'=>Umkm::where('status', 'proses')->orderBy('created_at', 'desc')->limit(4)->get(),
            'pekerjaan' => Pekerjaan::all(),
            'dukuh' => Dukuh::all(),
            'rt' => Rt::all(),
            'agama' => Agama::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array(
            'id'=>$id,
        );

        $data = array_merge($data, $request->all());

        $validator = Validator::make($data, [
            'id'=>'required|numeric|exists:t_users,id',
            'role'=>'required|max:100',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {          
            $user = User::where('id',$id)->update([
                'role' => request('role')
            ]);

            $user = User::where('id',$id)->first();
            Mail::send('mail.akun', ['user' => $user], function($message) use ($user) {
                $message->from('alvicky98@gmail.com');
                $message->to($user->email);
                $message->subject('Akun anda berhasil terverifikasi' );
            });

            if ($user) {
                return redirect()->back()->with('success', 'Verifikasi user baru berhasil.');
            } else {
                return redirect()->back()->with('errors', 'Verifikasi user baru gagal, harap coba kembali.');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = array(
            'id'=>$id
        );

        $validator = Validator::make($data, [
            'id'=>'required|numeric|exists:t_users,id',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $user = User::where('id',$id)->first();
        Mail::send('mail.refuse', ['user' => $user], function($message) use ($user) {
            $message->from('alvicky98@gmail.com');
            $message->to($user->email);
            $message->subject('Akun anda gagal terverifikasi' );
        });

        // check file exist
        if (file_exists('assets/1117688/'.$user['lamp_ktp'])) {
            unlink('assets/1117688/'.$user['lamp_ktp']);
        }

        // check file exist
        if (file_exists('assets/1117688/'.$user['lamp_kk'])) {
            unlink('assets/1117688/'.$user['lamp_kk']);
        }

        $delete=User::where('id', $id)->delete();
        if ($delete) {
            return redirect()->back()->with('success','Verifikasi user baru berhasil ditolak.');
        } else {
            return redirect()->back()->with('errors','UVerifikasi user baru gagal ditolak, harap coba kembali.');
        }

    }
}
