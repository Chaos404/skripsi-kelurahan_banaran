<?php

namespace App\Http\Controllers\DashboardPetugas\PendaftaranUmkm;

use App\Http\Controllers\Controller;
use App\Model\Dukuh;
use App\Model\Rt;
use App\Model\Surat;
use App\Model\Umkm;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;

class PengajuanmasukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('petugas.pendaftaran_umkm.masuk',[
            'unverified'=> User::where('role', 'unverified')->orderBy('created_at', 'desc')->limit(4)->get(),
            'pengajuansurat'=>Surat::where('status', 'proses')->orderBy('created_at', 'desc')->limit(4)->get(),
            'pengajuanumkm'=>Umkm::where('status', 'proses')->orderBy('created_at', 'desc')->limit(4)->get(),
            'umkm' => Umkm::where('status', 'proses')->get(),
            'dukuh' => Dukuh::all(),
            'rt' => Rt::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array(
            'id'=>$id,
        );

        $data = array_merge($data, $request->all());

        $validator = Validator::make($data, [
            'id'=>'required|numeric|exists:t_umkm,id',
            'status'=>'required|max:100',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            $umkm = Umkm::where('id',$id)->update([
                'status'=> $request->status,
                'keterangan' => $request->keterangan
            ]);

            $umkmsend = Umkm::where('id',$id)->first();
            Mail::send('mail.umkm', ['umkmsend' => $umkmsend], function($message) use ($umkmsend) {
                $message->from('alvicky98@gmail.com');
                $message->to($umkmsend->User['email']);
                $message->subject('Pengajuan UMKM Terverifikasi Merk ' .$umkmsend->merk_usaha );
            });

            if ($umkm) {
                return redirect()->back()->with('success', 'Verifikasi pendaftaran UMKM berhasil.');
            } else {
                return redirect()->back()->with('errors', 'Verifikasi pendaftaran UMKM gagal, harap ulangi kembali.');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = array(
            'id'=>$id
        );

        $validator = Validator::make($data, [
            'id'=>'required|numeric|exists:t_umkm,id',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $umkm = Umkm::where('id',$id)->first();
        Mail::send('mail.refuse', ['umkm' => $umkm], function($message) use ($umkm) {
            $message->from('alvicky98@gmail.com');
            $message->to($umkm->User['email']);
            $message->subject('Pengajuan UMKM Tertolak Merk ' .$umkm->merk_usaha );
        });


        $image = Umkm::where('id',$id)->first();
        $lamp_siup = Umkm::where('id',$id)->first();
        $lamp_npwp = Umkm::where('id',$id)->first();

        // check file exist
        if (file_exists('public/assets/1119126/'.$image['image'])) {
            unlink('public/assets/1119126/'.$image['image']);
        }

        if (file_exists('public/assets/1119126/'.$lamp_siup['lamp_siup'])) {
            unlink('public/assets/1119126/'.$lamp_siup['lamp_siup']);
        }

        if (file_exists('public/assets/1119126/'.$lamp_npwp['lamp_npwp'])) {
            unlink('public/assets/1119126/'.$lamp_npwp['lamp_npwp']);
        }

        $delete= Umkm::where('id', $id)->delete();
        
        if ($delete) {
            return redirect()->back()->with('success','Pengajuan Umkm berhasil dihapus.');
        } else {
            return redirect()->back()->with('errors','Pengajuan Umkm gagal dihapus, harap ulangi kembali.');
        }
    }
}
