<?php

namespace App\Http\Controllers\DashboardPetugas\PendaftaranUmkm;

use App\Http\Controllers\Controller;
use App\Model\Dukuh;
use App\Model\Rt;
use App\Model\Surat;
use App\Model\Umkm;
use App\User;
use Illuminate\Http\Request;

class PengajuandiverifikasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('petugas.pendaftaran_umkm.verify',[
            'unverified'=> User::where('role', 'unverified')->orderBy('created_at', 'desc')->limit(4)->get(),
            'pengajuansurat'=>Surat::where('status', 'proses')->orderBy('created_at', 'desc')->limit(4)->get(),
            'pengajuanumkm'=>Umkm::where('status', 'proses')->orderBy('created_at', 'desc')->limit(4)->get(),
            'umkm' => Umkm::where('status', 'terverifikasi')->orderBy('created_at', 'desc')->get(),
            'dukuh' => Dukuh::all(),
            'rt' => Rt::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
