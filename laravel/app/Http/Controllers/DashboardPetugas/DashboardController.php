<?php

namespace App\Http\Controllers\DashboardPetugas;

use App\Http\Controllers\Controller;
use App\Model\Form\form_kelahiran;
use App\Model\Form\form_kematian;
use App\Model\Keperluan;
use App\Model\Surat;
use App\Model\Umkm;
use App\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // return view('petugas.index',[
            $masyarakat =  User::where('role', 'masyarakat')->orderBy('created_at', 'desc')->get();
            $pengajuan_surat =  Surat::where('status', 'terverifikasi')->orderBy('created_at', 'desc')->get();
            $pengajuan_umkm =  Umkm::where('status', 'terverifikasi')->orderBy('created_at', 'desc')->get();


            $unverified =  User::where('role', 'unverified')->orderBy('created_at', 'desc')->limit(4)->get();
            $pengajuansurat = Surat::where('status', 'proses')->orderBy('created_at', 'desc')->limit(4)->get();
            $pengajuanumkm = Umkm::where('status', 'proses')->orderBy('created_at', 'desc')->limit(4)->get();
            
            $month = ['01','02','03','04','05','06','07','08','09','10','11','12'];

            $masyarakatChart = [];
            $surat1 = [];
            $surat2 = [];
            $surat3 = [];
            $surat4 = [];
            $surat5 = [];
            $surat6 = [];
            $surat7 = [];

            foreach ($month as $key => $value) {

                $masyarakatChart[] = User::where('role', 'masyarakat')->whereYear('updated_at', date('Y'))->where(\DB::raw("DATE_FORMAT(updated_at, '%m')"),$value)->count();
                $umkm[] = Umkm::where('status', 'terverifikasi')->whereYear('updated_at', date('Y'))->where(\DB::raw("DATE_FORMAT(updated_at, '%m')"),$value)->count();

                $surat1[] = Surat::where('t_keperluan_id', 1)->whereYear('updated_at', date('Y'))->where('status', 'terverifikasi')->where(\DB::raw("DATE_FORMAT(updated_at, '%m')"),$value)->count();
                $surat2[] = Surat::where('t_keperluan_id', 2)->whereYear('updated_at', date('Y'))->where('status', 'terverifikasi')->where(\DB::raw("DATE_FORMAT(updated_at, '%m')"),$value)->count();
                $surat3[] = Surat::where('t_keperluan_id', 3)->whereYear('updated_at', date('Y'))->where('status', 'terverifikasi')->where(\DB::raw("DATE_FORMAT(updated_at, '%m')"),$value)->count();
                $surat4[] = Surat::where('t_keperluan_id', 4)->whereYear('updated_at', date('Y'))->where('status', 'terverifikasi')->where(\DB::raw("DATE_FORMAT(updated_at, '%m')"),$value)->count();
                $surat5[] = Surat::where('t_keperluan_id', 5)->whereYear('updated_at', date('Y'))->where('status', 'terverifikasi')->where(\DB::raw("DATE_FORMAT(updated_at, '%m')"),$value)->count();
                $surat6[] = Surat::where('t_keperluan_id', 6)->whereYear('updated_at', date('Y'))->where('status', 'terverifikasi')->where(\DB::raw("DATE_FORMAT(updated_at, '%m')"),$value)->count();
                $surat7[] = Surat::where('t_keperluan_id', 7)->whereYear('updated_at', date('Y'))->where('status', 'terverifikasi')->where(\DB::raw("DATE_FORMAT(updated_at, '%m')"),$value)->count();
            }
            
            $all1 = Surat::where('status', 'terverifikasi')->where('t_keperluan_id', 1)->count();
            $all2 = Surat::where('status', 'terverifikasi')->where('t_keperluan_id', 2)->count();
            $all3 = Surat::where('status', 'terverifikasi')->where('t_keperluan_id', 3)->count();
            $all4 = Surat::where('status', 'terverifikasi')->where('t_keperluan_id', 4)->count();
            $all5 = Surat::where('status', 'terverifikasi')->where('t_keperluan_id', 5)->count();
            $all6 = Surat::where('status', 'terverifikasi')->where('t_keperluan_id', 6)->count();
            $all7 = Surat::where('status', 'terverifikasi')->where('t_keperluan_id', 7)->count();

        return view('petugas.index', compact('masyarakat', 'pengajuan_surat', 'pengajuan_umkm', 'unverified', 'pengajuansurat', 'pengajuanumkm', 'masyarakatChart', 'all1', 'all2', 'all3', 'all4', 'all5', 'all6', 'all7'))
            ->with('month',json_encode($month,JSON_NUMERIC_CHECK))
            ->with('masyarakatChart',json_encode($masyarakatChart,JSON_NUMERIC_CHECK))
            ->with('umkm',json_encode($umkm,JSON_NUMERIC_CHECK))

            ->with('surat1',json_encode($surat1,JSON_NUMERIC_CHECK))
            ->with('surat2',json_encode($surat2,JSON_NUMERIC_CHECK))
            ->with('surat3',json_encode($surat3,JSON_NUMERIC_CHECK))
            ->with('surat4',json_encode($surat4,JSON_NUMERIC_CHECK))
            ->with('surat5',json_encode($surat5,JSON_NUMERIC_CHECK))
            ->with('surat6',json_encode($surat6,JSON_NUMERIC_CHECK))
            ->with('surat7',json_encode($surat7,JSON_NUMERIC_CHECK));
    }
}
