<?php

namespace App\Http\Controllers\DashboardPetugas\LayananSurat;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Agama;
use App\Model\Dukuh;
use App\Model\Keperluan;
use App\Model\Pekerjaan;
use App\Model\Rt;
use App\Model\Surat;
use App\Model\Umkm;
use App\User;

class PengajuandiverifikasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('petugas.layanan_surat.verify', [
            'user'=> User::where('role', 'masyarakat')->get(),
            'unverified'=> User::where('role', 'unverified')->orderBy('created_at', 'desc')->limit(4)->get(),
            'pengajuansurat'=>Surat::where('status', 'proses')->orderBy('created_at', 'desc')->limit(4)->get(),
            'pengajuanumkm'=>Umkm::where('status', 'proses')->orderBy('created_at', 'desc')->limit(4)->get(),
            'surat' => Surat::where('status', 'terverifikasi')->orderBy('updated_at', 'desc')->get(),
            'dukuh' => Dukuh::all(),
            'pekerjaan' => Pekerjaan::all(),
            'rt' => Rt::all(),
            'agama' => Agama::all(),
            'keperluan' => Keperluan::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
