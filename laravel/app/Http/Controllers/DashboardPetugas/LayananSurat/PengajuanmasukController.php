<?php

namespace App\Http\Controllers\DashboardPetugas\LayananSurat;

use App\Http\Controllers\Controller;
use App\Model\Agama;
use App\Model\Dukuh;
use App\Model\Form\form_kelahiran;
use App\Model\Form\form_kematian;
use App\Model\Form\form_perpindahan;
use App\Model\Keperluan;
use App\Model\Pekerjaan;
use App\Model\Rt;
use App\Model\Surat;
use App\Model\Umkm;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;


class PengajuanmasukController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('petugas.layanan_surat.masuk', [
            'user'=> User::where('role', 'masyarakat')->get(),
            'unverified'=> User::where('role', 'unverified')->orderBy('created_at', 'desc')->limit(4)->get(),
            'pengajuansurat'=>Surat::where('status', 'proses')->orderBy('created_at', 'desc')->limit(4)->get(),
            'pengajuanumkm'=>Umkm::where('status', 'proses')->orderBy('created_at', 'desc')->limit(4)->get(),
            'surat' => Surat::where('status', 'proses')->get(),
            'dukuh' => Dukuh::all(),
            'pekerjaan' => Pekerjaan::all(),
            'rt' => Rt::all(),
            'agama' => Agama::all(),
            'keperluan' => Keperluan::all(),

            'kelahiran' => Surat::where('t_keperluan_id', 1)->get(),
            'kematian' => Surat::where('t_keperluan_id', 2)->get(),
            'pindahkeluar' => Surat::where('t_keperluan_id', 3)->get(),
            'pindahdatang' => Surat::where('t_keperluan_id', 4)->get()

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array(
            'id'=>$id,
        );

        $ubah = array(
            'id_pengajuan'=>$id,
        );

        $data = array_merge($data, $request->all());

        $validator = Validator::make($data, [
            'id'=>'required|numeric|exists:t_surat,id',
            // 'status_pengajuan'=>'required|max:100',
            'no_surat'=>'required|max:255|string|unique:t_surat',
            'keterangan'=>'required|string',
            // 'tanda_tangan_kepala_desa' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048']
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            // $surat = Surat::where('id',$id)->first();
            // // check file exist
            // if (file_exists('assets/1134798/'.$surat['image']) AND $surat['image'] != "") {
            //     unlink('assets/1134798/'.$surat['image']);
            // }

            // // Upload and change file
            // $file = $request->file('tanda_tangan_kepala_desa');

            // if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $file->getClientOriginalName())) {
            //     return redirect()->back()->with('errors','invalid file name.');
            // }

            // $folder_destination = 'assets/1134798';

            // $filetowrite = $folder_destination . '/' . $file->getClientOriginalName();
            // $file->move($folder_destination,$file->getClientOriginalName());                
            
            $surat = Surat::where('id',$id)->update([
                // 'ttd_kepaladesa'=>$file->getClientOriginalName(),
                'status_pengajuan'=>$request->status_pengajuan,
                'no_surat'=>$request->no_surat,
                'id_pengajuan' => $request->id_pengajuan,
                't_keperluan_id' => $request->keperluan,
                'keterangan' => $request->keterangan,

            ]);
            
            //   Create qr
            $qrstring = 'Pengajuan #' . $request->id_pengajuan.'/'.$request->id_name.'/'.$request->nik.'/'.$request->keperluannya;
            // Passing data
            view()->share(['surat' => $surat, 'qrstring' => $qrstring]);

            // $ubah = Surat::find($ubah);
            // view()->share('suratnya',$ubah);

            // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Php Word +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            // $domPdfPath = base_path( 'vendor/dompdf/dompdf');
            // \PhpOffice\PhpWord\Settings::setPdfRendererPath($domPdfPath);
            // \PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');

            // $suket = new \PhpOffice\PhpWord\TemplateProcessor(storage_path('Suket.docx'));
            // $suket->setValue('nomor', $request->no_surat);

            
            // $path = 'public/assets/1197268';
            // $fileName =  ($request->id_pengajuan) . '-Suket.' . 'docx' ;
            
            // // //Save to path
            // $saveSuket = $path . '/' . $fileName;
            // // $path = storage_path('suket.docx');
            // $suket->saveAs($saveSuket);

            // $temp = \PhpOffice\PhpWord\IOFactory::load($saveSuket);
            // $xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($temp, 'PDF');

            // $fileName2 =  ($request->id_pengajuan) . '-Suket.' . 'pdf' ;
            // $xmlWriter->save(($path . '/' . $fileName2), TRUE);
            // return response()->download($path . '/' . $fileName2);

            // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //Php Word +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            
            $pdf = PDF::loadView('mail.suket', $ubah)->setPaper('legal', 'landscape');
            $path = 'public/assets/1197268';
            $fileName =  ($request->id_pengajuan) . '-Suket.' . 'pdf' ;
            $pdf->save($path . '/' . $fileName);

            $pdf2 = PDF::loadView('mail.form_ketmati', $ubah)->setPaper('legal', 'potrait');
            $path2 = 'public/assets/1197824';
            $fileName2 =  ($request->id_pengajuan) . '-Lampiran.' . 'pdf' ;
            $pdf2->save($path2 . '/' . $fileName2);
            
            // return $pdf->download($fileName);
            return $pdf2->stream('test_pdf.pdf');

            Mail::send('mail.content', ['surat' => $surat], function($message) use ($xmlWriter, $request){
                $message->from('alvicky98@gmail.com');
                $message->to($request->email);
                $message->subject('Terverifikasi' . ' ID #' .$request->id_pengajuan );
                $message->attachData($pdf->output(), $request->id_pengajuan . '-Suket.' . 'pdf');
                $message->attachData($pdf2->output(), $request->id_pengajuan . '-Lampiran.' . 'pdf');
            });

            if ($surat) {
                return redirect()->back()->with('success', 'Verifikasi pengajuan surat berhasil.');
            } else {
                return redirect()->back()->with('errors', 'Verifikasi pengajuan surat gagal, harap ulangi kembali..');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = array(
            'id'=>$id
        );

        $validator = Validator::make($data, [
            'id'=>'required|numeric|exists:t_surat,id',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $surat = Surat::where('id',$id)->first();
        Mail::send('mail.refuse', ['surat' => $surat], function($message) use ($surat) {
            $message->from('alvicky98@gmail.com');
            $message->to($surat->email);
            $message->subject('Pengajuan Anda Tidak Terverifikasi' );
        });

        // check file exist
        if (file_exists('public/assets/1134798/'.$surat['lamp_pengantar'])) {
            unlink('public/assets/1134798/'.$surat['lamp_pengantar']);
        }

        $delete=Surat::where('id', $id)->delete();
        if ($delete) {
            return redirect()->back()->with('success','Verifikasi pengajuan surat berhasil ditolak.');
        } else {
            return redirect()->back()->with('errors','Verifikasi pengajuan surat gagal ditolak, harap coba kembali.');
        }
    }
}
