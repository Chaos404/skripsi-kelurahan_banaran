<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Model\Rt;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRt($id)
    {
        $rt = Rt::select('rt','id')->where('t_dukuh_id', $id)->get();
        return response()->json($rt);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nama_lengkap' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:t_users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'password_confirmation' => ['required'],
            'pekerjaan' => ['required', 'string', 'max:255'],
            'dukuh' => ['required', 'string', 'max:255'],
            'rt' => ['required', 'string', 'max:255'],
            'agama' => ['required', 'string', 'max:255'],
            'tempat_lahir' => ['required', 'string', 'max:255'],
            'tanggal_lahir' => ['required', 'string', 'max:255'],
            'jenis_kelamin' => ['required', 'string', 'max:255'],
            'nik' => ['required', 'numeric', 'digits:16', 'unique:t_users'],
            'no_kk' => ['required','numeric', 'digits:16'],
            'status_perkawinan' => ['required', 'string', 'max:255'],
            // 'lampiran_ktp' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
            'lampiran_kk' => ['required', 'file','image','mimes:jpeg,png,jpg','max:2048'],
            'g-recaptcha-response' => ['required','captcha'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // Available alpha caracters
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        // generate a pin based on 2 * 7 digits + a random character
        $pin = mt_rand(1000000, 9999999)
            . mt_rand(1000000, 9999999)
            . $characters[rand(0, strlen($characters) - 1)];

        $request = request();
        // //------Image1-----
        // $file1 = $request->file('lampiran_ktp');

        // if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file1->getClientOriginalName())) {
        //     return redirect('dashboard/catalog/product')->with('errors','invalid file name.');
        // }

        // $folder_destination = 'public/assets/1117688';

        // $filenya1 = str_shuffle($pin) . '-' . $file1->getClientOriginalName();
        // $filetowrite = $folder_destination . '/' . $filenya1;
        // $file1->move($folder_destination, $filetowrite);

        //------Image2-----
        $file2 = $request->file('lampiran_kk');

        if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", str_shuffle($pin) . '-' . $file2->getClientOriginalName())) {
            return redirect()->back()->with('errors','invalid file name.');
        }

        $folder_destination = 'public/assets/1117688';

        $filenya2 = str_shuffle($pin) . '-' . $file2->getClientOriginalName();
        $filetowrite = $folder_destination . '/' . $filenya2;
        $file2->move($folder_destination, $filetowrite);
        
        return User::create([
            'name' => $data['nama_lengkap'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            't_pekerjaan_id' => $data['pekerjaan'],
            't_dukuh_id' => $data['dukuh'],
            'rt' => $data['rt'],
            't_agama_id' => $data['agama'],
            'tempat_lahir' => $data['tempat_lahir'],
            'tgl_lahir' => $data['tanggal_lahir'],
            'jeniskelamin' => $data['jenis_kelamin'],
            'nik' => $data['nik'],
            'no_kk' => $data['no_kk'],
            'statusperkawinan' => $data['status_perkawinan'],
            // 'lamp_ktp'=> $filenya1,
            'lamp_kk'=> $filenya2,
        ]);
    }
}
