<?php

namespace App\Http\Controllers;
use App\User;
use Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function login(Request $request){
        // dd($request->all());
        $akun = array(
            'email' => $request->email,
            'password' => $request->password
        );
        
        if (Auth::attempt($akun)) {
            $user = User::where('email', $request->email)->first();

            if($user->isAdmin())
            {
                $request->session()->put('admin',$request->email);
                return redirect()->route('dashboardAdmin')->with('success', 'Login Success.');
            }
            elseif ($user->isPetugas())
            {
                $request->session()->put('petugas',$request->email);
                return redirect()->route('dashboardPetugas')->with('success', 'Login Success.');
            }
            $request->session()->put('user',$request->email);
            return redirect()->route('home')->with('success', 'Login Success.');
        }

        // elseif (Auth::attempt($akun)) {
        //     $user = User::where('email', $request->email)->first();

        //     if($user->isPetugas())
        //     // {
        //     //     // $request->session()->put('admin',$request->email);
        //     //     return redirect()->route('dashboardAdmin')->with('success', 'Login Success.');
        //     // }
        //     // elseif ($user->isPetugas())
        //     {
        //         $request->session()->put('petugas',$request->email);
        //         return redirect()->route('dashboardPetugas')->with('success', 'Login Success.');
        //     }
        //     $request->session()->put('user',$request->email);
        //     return redirect()->route('home')->with('success', 'Login Success.');
        // }
        return redirect()->back();
    }
}
