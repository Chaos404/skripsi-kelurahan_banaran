<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = 't_users';
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'email', 'password','t_pekerjaan_id', 't_dukuh_id', 't_agama_id', 'tempat_lahir', 'tgl_lahir', 'jeniskelamin', 'rt', 'alamat',
        'nik','no_kk', 'statusperkawinan','lamp_ktp', 'lamp_kk'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // // Admin
    // public function isAdmin()
    // {
    //     if($this->admin)
    //     {
    //         return true;
    //     }
    //     return false;
    // }

    // // Petugas
    // public function isPetugas()
    // {
    //     if($this->petugas)
    //     {
    //         return true;
    //     }
    //     return false;
    // }

    public function Surat(){
    	return $this->hasOne('App\Model\Surat');
    }

    public function Umkm(){
    	return $this->hasOne('App\Model\Umkm');
    }

    public function Dukuh() {
    	return $this->belongsTo('App\Model\Dukuh', 't_dukuh_id','id');
    }

    public function Agama() {
    	return $this->belongsTo('App\Model\Agama', 't_agama_id','id');
    }

    public function Pekerjaan() {
    	return $this->belongsTo('App\Model\Pekerjaan', 't_pekerjaan_id','id');
    }
    
}
