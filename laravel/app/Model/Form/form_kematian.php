<?php

namespace App\Model\Form;

use Illuminate\Database\Eloquent\Model;

class form_kematian extends Model
{
    public function Surat(){
    	return $this->hasOne('App\Model\Surat');
    }

    public function Agama() {
    	return $this->belongsTo('App\Model\Agama', 't_agama_id','id');
    }

    public function Ibu() {
    	return $this->belongsTo('App\Model\Form\t_ibu', 't_ibus_id','id');
    }

    public function Ayah() {
    	return $this->belongsTo('App\Model\Form\t_ayah', 't_ayahs_id','id');
    }

    public function Saksi1() {
    	return $this->belongsTo('App\Model\Form\t_saksi1', 't_saksi1s_id','id');
    }

    public function Saksi2() {
    	return $this->belongsTo('App\Model\Form\t_saksi2', 't_saksi2s_id','id');
    }

    public function Pekerjaan() {
    	return $this->belongsTo('App\Model\Pekerjaan', 't_pekerjaan_id','id');
    }

    public function Dukuh() {
    	return $this->belongsTo('App\Model\Dukuh', 't_dukuh_id','id');
    }
}
