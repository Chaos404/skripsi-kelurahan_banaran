<?php

namespace App\Model\Form;

use Illuminate\Database\Eloquent\Model;

class form_perpindahan extends Model
{
    public function Surat(){
    	return $this->hasOne('App\Model\Surat');
    }

    public function Jeniskepindahan() {
    	return $this->belongsTo('App\Model\Form\t_jeniskepindahan', 't_jeniskepindahans_id','id');
    }

    public function Dukuh() {
    	return $this->belongsTo('App\Model\Dukuh', 't_dukuh_id','id');
    }

    public function Tinggal() {
    	return $this->belongsTo('App\Model\Form\t_tinggal', 't_tinggals_id','id');
    }
}
