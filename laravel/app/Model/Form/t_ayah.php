<?php

namespace App\Model\Form;

use Illuminate\Database\Eloquent\Model;

class t_ayah extends Model
{
    public function Kelahiran(){
    	return $this->hasOne('App\Model\Form\form_kelahiran');
    }

    public function Kematian(){
    	return $this->hasOne('App\Model\Form\form_kematian');
    }

    public function Pekerjaan() {
    	return $this->belongsTo('App\Model\Pekerjaan', 't_pekerjaan_id','id');
    }
}
