<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Umkm extends Model
{
    protected $table = 't_umkm';

    protected $fillable = [
    	'id', 't_users_id', 'merk_usaha', 'slug', 'image', 'no_siup', 'no_npwp', 'no_whatsapp', 'lamp_siup', 'lamp_npwp', 'keterangan', 'status'
    ];

    public function User() {
    	return $this->belongsTo('App\User', 't_users_id','id');
    }
}
