<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Keperluan extends Model
{
    protected $table = 't_keperluan';

    protected $fillable = [
    	'id','keperluan'
    ];

    public function Surat(){
    	return $this->hasOne('App\Model\Surat');
    }
}
