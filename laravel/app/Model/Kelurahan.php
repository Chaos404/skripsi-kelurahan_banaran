<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Kelurahan extends Model
{
    protected $table = 't_kelurahan';

    protected $fillable = [
    	'id','kelurahan', 'image', 'alamat', 'nohp', 'email', 'npwp'
    ];
}
