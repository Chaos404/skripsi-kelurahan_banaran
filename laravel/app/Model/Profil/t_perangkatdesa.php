<?php

namespace App\Model\Profil;

use Illuminate\Database\Eloquent\Model;

class t_perangkatdesa extends Model
{
    public function Jabatan() {
    	return $this->belongsTo('App\Model\Profil\t_jabatan', 'jabatans_id','id');
    }
}
