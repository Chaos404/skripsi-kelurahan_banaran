<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Surat extends Model
{
    protected $table = 't_surat';

    protected $fillable = [
    	'id','t_keperluan_id', 't_users_id', 'id_pengajuan', 'no_surat', 'no_suratformulir', 'keterangan', 'lamp_pengantar', 'ttd_kepaladesa', 'status', 'ktp_pelapor', 'email', 'form_kelahirans_id', 'form_kematians_id'
    ];

    public function User() {
    	return $this->belongsTo('App\User', 't_users_id','id');
    }

    public function Keperluan() {
    	return $this->belongsTo('App\Model\Keperluan', 't_keperluan_id','id');
    }

    public function Kelahiran() {
    	return $this->belongsTo('App\Model\Form\form_kelahiran', 'form_kelahirans_id','id');
    }

    public function Kematian() {
    	return $this->belongsTo('App\Model\Form\form_kematian', 'form_kematians_id','id');
    }

    public function Perpindahan() {
    	return $this->belongsTo('App\Model\Form\form_perpindahan', 'form_perpindahans_id','id');
    }
}
