<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Rt extends Model
{
    protected $table = 't_rt';

    protected $fillable = [
    	'id','rt'
    ];
}
