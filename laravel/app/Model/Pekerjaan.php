<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pekerjaan extends Model
{
    protected $table = 't_pekerjaan';

    protected $fillable = [
    	'id','pekerjaan'
    ];

    public function User(){
    	return $this->hasOne('App\User');
    }

    public function Ibu(){
    	return $this->hasOne('App\Model\Form\t_ibu');
    }

    public function Ayah(){
    	return $this->hasOne('App\Model\Form\t_ayah');
    }

    public function Saksi1(){
    	return $this->hasOne('App\Model\Form\t_saksi1');
    }

    public function Saksi2(){
    	return $this->hasOne('App\Model\Form\t_saksi2');
    }

    public function Kematian(){
    	return $this->hasOne('App\Model\Form\form_kematian');
    }
}
