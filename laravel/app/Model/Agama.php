<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Agama extends Model
{
    protected $table = 't_agama';

    protected $fillable = [
    	'id','agama'
    ];

    public function User(){
    	return $this->hasOne('App\User');
    }

    public function Kematian(){
    	return $this->hasOne('App\Model\Form\form_kematian');
    }
}
