<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Dukuh extends Model
{
    protected $table = 't_dukuh';

    protected $fillable = [
    	'id','dukuh'
    ];

    public function User(){
    	return $this->hasOne('App\User');
    }

    public function Saksi1(){
    	return $this->hasOne('App\Model\Form\t_saksi1');
    }

    public function Saksi2(){
    	return $this->hasOne('App\Model\Form\t_saksi2');
    }

    public function Kematian(){
    	return $this->hasOne('App\Model\Form\form_kematian');
    }

    public function Perpindahan(){
    	return $this->hasOne('App\Model\Form\form_perpindahan');
    }
}
