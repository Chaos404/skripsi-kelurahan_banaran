<?php

namespace App\Model\Berita;

use Illuminate\Database\Eloquent\Model;

class t_berita extends Model
{
    public function Kategori() {
    	return $this->belongsTo('App\Model\Berita\t_kategoriberita', 'id_kategoriberitas','id');
    }
}
