<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('t_users')->truncate(); //for cleaning earlier data to avoid duplicate entries
        DB::table('t_users')->insert([
            'name' => 'the admin user',
            'email' => 'iamadmin@gmail.com',
            'role' => 'admin',
            'password' => Hash::make('password'),
        ]);
        DB::table('t_users')->insert([
            'name' => 'the petugas user',
            'email' => 'iamapetugas@gmail.com',
            'role' => 'petugas',
            'password' => Hash::make('password'),
        ]);
        DB::table('t_users')->insert([
            'name' => 'the masyarakat user',
            'email' => 'iamamasyarakat@gmail.com',
            'role' => 'masyarakat',
            'password' => Hash::make('password'),
        ]);
    }
}
