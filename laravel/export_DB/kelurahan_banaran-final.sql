-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 08, 2021 at 05:34 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kelurahan_banaran-final`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `form_kelahirans`
--

CREATE TABLE `form_kelahirans` (
  `id` int(11) NOT NULL,
  `t_surat_id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `jeniskelamin` enum('Laki - laki','Perempuan') DEFAULT NULL,
  `nomor_tempat_kelahiran` varchar(100) DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `pukul` time DEFAULT NULL,
  `jeniskelahiran` varchar(100) DEFAULT NULL,
  `kelahiran_ke` int(11) DEFAULT NULL,
  `penolong_kelahiran` varchar(100) DEFAULT NULL,
  `berat` float DEFAULT NULL,
  `tinggi` int(11) DEFAULT NULL,
  `lamp_KeteranganRS` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `form_kelahirans`
--

INSERT INTO `form_kelahirans` (`id`, `t_surat_id`, `nama`, `jeniskelamin`, `nomor_tempat_kelahiran`, `tempat_lahir`, `tgl_lahir`, `pukul`, `jeniskelahiran`, `kelahiran_ke`, `penolong_kelahiran`, `berat`, `tinggi`, `lamp_KeteranganRS`) VALUES
(38, 182, 'Bastian Stell', 'Laki - laki', '24234323', 'Rumah Sakit / Rumah Bersalin', '2021-03-01', '10:00:00', 'Tunggal', 3, 'Dokter', 3.5, 59, '67583318839688B-alvx-8.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `form_kematians`
--

CREATE TABLE `form_kematians` (
  `id` int(11) NOT NULL,
  `t_surat_id` int(11) NOT NULL,
  `t_agama_id` int(11) NOT NULL,
  `t_dukuh_id` int(11) NOT NULL,
  `t_pekerjaan_id` int(10) UNSIGNED NOT NULL,
  `nik` char(16) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `jeniskelamin` enum('Laki - laki','Perempuan') DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `tgl_mati` date DEFAULT NULL,
  `pukul` time DEFAULT NULL,
  `sebab` varchar(100) DEFAULT NULL,
  `tempat_kematian` varchar(100) DEFAULT NULL,
  `menerangkan` varchar(100) DEFAULT NULL,
  `rt` varchar(11) DEFAULT NULL,
  `lamp_ktp` varchar(255) DEFAULT NULL,
  `lamp_keterangankematian` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `form_kematians`
--

INSERT INTO `form_kematians` (`id`, `t_surat_id`, `t_agama_id`, `t_dukuh_id`, `t_pekerjaan_id`, `nik`, `nama`, `jeniskelamin`, `tempat_lahir`, `tgl_lahir`, `tgl_mati`, `pukul`, `sebab`, `tempat_kematian`, `menerangkan`, `rt`, `lamp_ktp`, `lamp_keterangankematian`) VALUES
(29, 183, 1, 10, 4, '3314012012730005', 'Syamin Syamsudin', 'Laki - laki', 'Sragen', '1973-12-20', '2021-03-03', '02:10:00', 'Sakit Biasa / Tua', 'Rumah', 'Lainnya', '13', '64H011622983639-alvx-5.PNG', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `form_perpindahans`
--

CREATE TABLE `form_perpindahans` (
  `id` int(11) NOT NULL,
  `t_surat_id` int(11) NOT NULL,
  `t_jeniskepindahans_id` int(11) NOT NULL,
  `t_dukuh_id` int(11) NOT NULL,
  `rt` varchar(11) NOT NULL,
  `kepala_keluarga` varchar(255) NOT NULL,
  `nomor_kk` char(16) DEFAULT NULL,
  `nik_kepalakeluarga` char(16) DEFAULT NULL,
  `alasan_pindah` varchar(100) DEFAULT NULL,
  `alasan_pindah2` varchar(100) DEFAULT NULL,
  `jenis_kepindahan` varchar(100) DEFAULT NULL,
  `statuskk_tidakpindah` varchar(100) DEFAULT NULL,
  `statuskk_pindah` varchar(100) NOT NULL DEFAULT 'NULL',
  `lamp_aktakelahiran` varchar(255) NOT NULL DEFAULT 'NULL',
  `lamp_ijazahterakhir` varchar(255) NOT NULL DEFAULT 'NULL',
  `lamp_aktanikah` varchar(255) DEFAULT NULL,
  `lamp_suratpindah` varchar(255) DEFAULT NULL,
  `tanggal_kedatangan` date DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `form_perpindahans`
--

INSERT INTO `form_perpindahans` (`id`, `t_surat_id`, `t_jeniskepindahans_id`, `t_dukuh_id`, `rt`, `kepala_keluarga`, `nomor_kk`, `nik_kepalakeluarga`, `alasan_pindah`, `alasan_pindah2`, `jenis_kepindahan`, `statuskk_tidakpindah`, `statuskk_pindah`, `lamp_aktakelahiran`, `lamp_ijazahterakhir`, `lamp_aktanikah`, `lamp_suratpindah`, `tanggal_kedatangan`, `updated_at`) VALUES
(135, 189, 3, 12, '10', 'Aminudin', NULL, NULL, 'Pekerjaan', NULL, 'Kepala Keluarga dan Seluruh Anggota Keluarga', 'Membuat KK Baru', 'Membuat KK Baru', '9358E6632866369-default.png', '83266989E665336-default.png', NULL, NULL, NULL, '2021-03-08 15:10:36'),
(136, 190, 2, 12, '10', 'Aminudin', NULL, NULL, 'Perumahan', NULL, 'Anggota Keluarga', 'Nomor KK Tetap', 'Membuat KK Baru', '34623J412608971-default.png', '487162J94261330-default.png', NULL, NULL, NULL, '2021-03-08 15:12:44'),
(137, 191, 1, 12, '10', 'Aminudin', NULL, NULL, 'Perumahan', NULL, 'Anggota Keluarga', 'Nomor KK Tetap', 'Membuat KK Baru', '82450747883T387-default.png', '034878T87824375-default.png', NULL, NULL, NULL, '2021-03-08 15:14:16'),
(138, 192, 1, 12, '10', 'Aminudin', '3314010712030878', '3314010703680001', NULL, NULL, NULL, NULL, 'Membuat KK Baru', 'Z72973624044141-default.png', '3404491412677Z2-default.png', NULL, '0424427164137Z9-default.png', '2021-03-05', '2021-03-08 15:34:42'),
(139, 193, 2, 12, '10', 'Aminudin', '3314010712030878', '3314010703680001', NULL, NULL, NULL, NULL, 'Membuat KK Baru', '516035S85605054-default.png', '051065S05453856-default.png', NULL, '60840551556S350-default.png', '2021-03-03', '2021-03-08 15:37:45'),
(140, 194, 3, 12, '10', 'Aminudin', '3314010712030878', '3314010703680001', NULL, NULL, NULL, NULL, 'Membuat KK Baru', '172249Q49275198-default.png', '942Q82194715927-default.png', NULL, '2791Q8949514272-default.png', '2021-03-01', '2021-03-08 15:38:51');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_10_18_165440_add_role_field_to_users_table', 1),
(5, '2020_11_30_011641_create_provinces_table', 2),
(6, '2020_11_30_011655_create_cities_table', 2),
(7, '2020_12_02_165145_create_provinces_table', 3),
(8, '2020_12_02_165156_create_cities_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('akusopo6969@gmail.com', '$2y$10$E9dqZmDqByzRP4PTxOw4Y.4jhFQCuQmVDqDc69Vw/YzNVXvUDCmAa', '2020-12-27 15:36:07'),
('alfidimas2103@gmail.com', '$2y$10$sddUvO2Mazr22lbQ6ryviOeMxHC0Owk62I2WH98LlXCD8Q0xALKjG', '2021-01-07 10:40:46'),
('muhammadvickyalhasri31@gmail.com', '$2y$10$tCwrvAB7/c1ZH/HBjyVCou6cBML1KtPYsmhLZ4Wt1JbGqbPsmHCzy', '2021-01-30 12:29:04');

-- --------------------------------------------------------

--
-- Table structure for table `t_agama`
--

CREATE TABLE `t_agama` (
  `id` int(11) NOT NULL,
  `agama` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_agama`
--

INSERT INTO `t_agama` (`id`, `agama`) VALUES
(1, 'Islam'),
(2, 'Kristen'),
(3, 'Katholik'),
(4, 'Hindu'),
(5, 'Budha');

-- --------------------------------------------------------

--
-- Table structure for table `t_ayahs`
--

CREATE TABLE `t_ayahs` (
  `id` int(11) NOT NULL,
  `form_kelahirans_id` int(11) DEFAULT NULL,
  `form_kematians_id` int(11) DEFAULT NULL,
  `t_pekerjaan_id` int(10) UNSIGNED NOT NULL,
  `nik` char(16) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `tgl_pencatatankawin` date DEFAULT NULL,
  `rt` varchar(11) DEFAULT NULL,
  `lamp_ktp` varchar(255) DEFAULT NULL,
  `lamp_kk` varchar(255) DEFAULT NULL,
  `lamp_bukunikah` varchar(255) DEFAULT NULL,
  `dukuh` varchar(100) DEFAULT NULL,
  `kelurahan` varchar(100) DEFAULT NULL,
  `kecamatan` varchar(100) DEFAULT NULL,
  `kabupaten` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_ayahs`
--

INSERT INTO `t_ayahs` (`id`, `form_kelahirans_id`, `form_kematians_id`, `t_pekerjaan_id`, `nik`, `nama`, `tgl_lahir`, `tgl_pencatatankawin`, `rt`, `lamp_ktp`, `lamp_kk`, `lamp_bukunikah`, `dukuh`, `kelurahan`, `kecamatan`, `kabupaten`) VALUES
(65, 38, NULL, 5, '3314011209860001', 'Adi Nur Cholis', '1986-09-12', '2010-03-15', '10', '87689138633588B-alvx-3.PNG', '888357B36839168-alvx-5.PNG', '398888B83136756-alvx-4.PNG', 'Dukuhrambat', 'Banaran', 'Kalijambe', 'Sragen'),
(66, NULL, 29, 1, '3314012908430001', 'Sutarno', '1943-08-29', NULL, '10', '6033981622H9614-alvx-7.PNG', '23311296H406698-alvx-4.PNG', NULL, 'Dukuhrambat', 'Banaran', 'Kalijambe', 'Sragen');

-- --------------------------------------------------------

--
-- Table structure for table `t_banners`
--

CREATE TABLE `t_banners` (
  `id` int(11) NOT NULL,
  `banner` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_banners`
--

INSERT INTO `t_banners` (`id`, `banner`, `created_at`, `updated_at`) VALUES
(1, '31F666126682514-banaran11.png', NULL, '2021-01-27 13:21:21'),
(2, '3Q2107006168616-1-min.jpg', NULL, '2021-02-01 19:02:21'),
(3, '47537767991490F-2-min.jpg', NULL, '2021-02-01 19:02:32');

-- --------------------------------------------------------

--
-- Table structure for table `t_beritas`
--

CREATE TABLE `t_beritas` (
  `id` int(11) NOT NULL,
  `id_kategoriberitas` int(11) NOT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `deskripsi` longtext DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_beritas`
--

INSERT INTO `t_beritas` (`id`, `id_kategoriberitas`, `judul`, `slug`, `image`, `deskripsi`, `created_at`, `updated_at`) VALUES
(4, 1, 'Penempelan Himbauan Pencegahan Covid-19', 'penempelan-himbauan-pencegahan-covid-19', '7780605982393V4-Petugas_sedang_menempelkan_sticker_pencegahan_Covid-19_01_37_27.jpg', '<div class=\"post-entry\">\r\n<p style=\"box-sizing: border-box; padding: 0px; margin: 0px; border: 0px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-weight: 400; font-stretch: inherit; font-size: 17px; line-height: inherit; font-family: \'IBM Sans\', -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Helvetica, Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', sans-serif; vertical-align: baseline; color: #333333; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;\"><strong>Banaran - </strong>Munculnya wabah Covid-19 yang sudah memasuki kecamatan Kalijambe sering membuat beberapa warga menjadi cemas. Tak jarang kecemasan yang dialami oleh warga akan membuat warga menjadi panik. Ketika seseorang sedang panik cenderung akan melakukan kegiatan yang sebenarnya tidak harusnya dilakukan. Kepanikan yang dialami oleh warga akan menurunkan sistem imun/kekebalan tubuh sehingga warga rentan untuk terkena beberapa virus.&nbsp;</p>\r\n<p style=\"box-sizing: border-box; padding: 0px; margin: 0px; border: 0px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-weight: 400; font-stretch: inherit; font-size: 17px; line-height: inherit; font-family: \'IBM Sans\', -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Helvetica, Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', sans-serif; vertical-align: baseline; color: #333333; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;\">Sebagai tindakan preventif dan sebagai pengingat kepada warga tentang pencegahan virus Covid-19 maka dilakukan penempelan sticker yang berisikan panduan pencegahan penyebaran virus Corona. Hal ini juga memiliki tujuan untuk menyadarkan masyarakat akan pentingnya mencegah penyebaran Cobid-19</p>\r\n</div>', NULL, '2021-02-01 19:06:01'),
(5, 1, 'Antisipasi Covid-19 dengan Penyemprotan Desinfektan', 'antisipasi-covid-19-dengan-penyemprotan-desinfektan', '72221715749784L-Petugas_sedang_melakukan_penyemprotan_desinfektan_di_area_masjid_Al-Amin_01_00_42.jpg', '<p style=\"box-sizing: border-box; padding: 0px; margin: 0px; border: 0px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-weight: 400; font-stretch: inherit; font-size: 17px; line-height: inherit; font-family: \'IBM Sans\', -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Helvetica, Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', sans-serif; vertical-align: baseline; color: #333333; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;\"><strong>Banaran - </strong>Masuknya Pandemi Covid-19 (Corona Virus Disease-2019)di Indonesia membuat beberapa wilayah harus melakukan himbauan Social Distancing dan menjaga kebersihan diri serta kebersihan lingkungan.&nbsp;</p>\r\n<p style=\"box-sizing: border-box; padding: 0px; margin: 0px; border: 0px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-weight: 400; font-stretch: inherit; font-size: 17px; line-height: inherit; font-family: \'IBM Sans\', -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Helvetica, Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', sans-serif; vertical-align: baseline; color: #333333; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;\">Salah satu cara Kampung KB Gayuh Lestari Dk. Rambat, Banara, Kalijambe untuk menjaga kebersihan dan higenitas lingkungan adalah dengan penyemprotan desinfektan. Penyemprotan Desinfektan ini dilakukan di rumah-rumah warga dan beberapa lokasi tempat berkumpulnya masyrakat seperti di masjid dan pos ronda di Rt.10 dan Rt.10A. Penyemprotan dilakukan oleh petugas yang berasal dari warga Dk. Rambat, Banaran, Kalijambe.</p>', NULL, '2021-02-01 19:06:43'),
(6, 1, 'Pembuatan Portal Akibat Maraknya Penyebaran Covid-19 dan Maling', 'pembuatan-portal-akibat-maraknya-penyebaran-covid-19-dan-maling', '6669K1542268788-Capture.PNG', '<div class=\"post-entry\">\r\n<div class=\"post-entry\">\r\n<div class=\"post-entry\">\r\n<p style=\"box-sizing: border-box; padding: 0px; margin: 0px; border: 0px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-weight: 400; font-stretch: inherit; font-size: 17px; line-height: inherit; font-family: \'IBM Sans\', -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Helvetica, Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', sans-serif; vertical-align: baseline; color: #333333; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;\"><strong>Banaran </strong>- Maraknya kasus penyebaran virus Corona di Kabupaten Sragen khususnya di Kecamatan Kalijambe membuat masyarakat sekitar melakukan LockDown mandiri. Salah satu cara yang dilakukan adalah dengan membuat beberapa portal di beberapa gang masuk perkampungan. Hal ini dilakukan untuk melakukan pembatasan sosial dan mengurangi arus keluar masuknya orang asing dari luar kampung yang memungkin untuk menyebarkan Covid-19</p>\r\n<p style=\"box-sizing: border-box; padding: 0px; margin: 0px; border: 0px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-weight: 400; font-stretch: inherit; font-size: 17px; line-height: inherit; font-family: \'IBM Sans\', -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Helvetica, Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', sans-serif; vertical-align: baseline; color: #333333; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;\">Pembuatan portal ini juga dilakukan untuk upaya masyarakat dalam menyikapi berita-berita tentang berkeliarannya orang asing atau pencuri dibeberapa daerah di Kecamatan Kalijambe.</p>\r\n</div>\r\n<p><span style=\"color: #333333; font-family: \'IBM Sans\', -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Helvetica, Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;\">&nbsp;</span></p>\r\n</div>\r\n</div>', NULL, '2021-02-01 19:13:47'),
(7, 1, 'Pembagian Tablet Zat Besi (FE)', 'pembagian-tablet-zat-besi-fe', 'H23585804038171-Pembagian_Tablet_Zat_Besi_(FE)_kepada_remaja_putri_21_45_22.jpg', '<div class=\"post-entry\">\r\n<p><strong>Banaran </strong>- <span style=\"color: #333333; font-family: \'IBM Sans\', -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Helvetica, Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;\">Pengurus Kampung KB Gayuh Lestari Dk. Rambat, Banaran, Kalijambe, Sragen melakukan kegiatan pembagian tablet FE (Zat Besi). Tablet FE merupakan tablet yang berguna untuk menambah darah yang mengandung 60 mg Besi Elemental dan 400 ug Asam Folat. Sasaran pemberian tablet Fe ini adalah remaja putri. Diharapkan dengan adanya kegiatan pembagian tablet Fe atau tablet tambah darah ini dapat mencegah gejala timbulnya penyakit anemia pada remaja putri sehingga mereka memiliki kesehatan yang optimal menjelang masa kehamilan</span></p>\r\n</div>', NULL, '2021-02-01 19:14:18'),
(8, 1, 'Pertemuan PKK Kampung KB Gayuh Lestari dengan Menerapkan Prokes', 'pertemuan-pkk-kampung-kb-gayuh-lestari-dengan-menerapkan-prokes', '69292768718812E-Anggota_PKK_tetap_mematuhi_protokol_kesehatan_dalam_sebuah_pertemuan_PKK_22_29_48.jpg', '<div class=\"post-entry\">\r\n<p><span style=\"color: #333333; font-family: \'IBM Sans\', -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Helvetica, Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;\"><strong>Banaran </strong>- Pandemi Covid 19 telah mematikan beberapa sektor kehidupan dimasyarakat, termasuk sektor organisasi. Dalam hal ini beberapa kegiatan organisasi di masyarakat ikut terganggu karena adanya peraturan untuk melakukan </span><span style=\"box-sizing: border-box; padding: 0px; margin: 0px; border: 0px; font-style: italic; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-weight: 400; font-stretch: inherit; font-size: 17px; line-height: inherit; font-family: \'IBM Sans\', -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Helvetica, Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', sans-serif; vertical-align: baseline; color: #333333; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;\">Social Distancing.&nbsp;</span><span style=\"color: #333333; font-family: \'IBM Sans\', -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Helvetica, Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;\">Hal ini membuat masyarakat susah untuk melakukan kegiatan bersosialisasi apalagi melakukan beberapa program kerja yang telah direncakan dalam sebuah organisasi desa seperti halnya PKK.</span><br style=\"box-sizing: border-box; padding: 0px; margin: 0px; color: #333333; font-family: \'IBM Sans\', -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Helvetica, Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;\" /><span style=\"color: #333333; font-family: \'IBM Sans\', -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Helvetica, Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;\">Setelah diumumkannya Indonesia akan memasuki tatanan kehidupan baru atau&nbsp;</span><span style=\"box-sizing: border-box; padding: 0px; margin: 0px; border: 0px; font-style: italic; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-weight: 400; font-stretch: inherit; font-size: 17px; line-height: inherit; font-family: \'IBM Sans\', -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Helvetica, Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', sans-serif; vertical-align: baseline; color: #333333; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;\">New Normal,&nbsp;</span><span style=\"color: #333333; font-family: \'IBM Sans\', -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Helvetica, Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;\">masyarakat dan beberapa pengurus organisasi desa tidak ambil langkah diam. Seperti halnya yang dilakukan oleh pengurus PKK Kampung KB Gayuh Lestari yang langsung melaksanakan kegiatan pertemuan rutin PKK yang bertujuan untuk membahas kelanjutan beberapa program kerja yang tertunda karena pandemi Covid 19. Kegiatan ini pu dilakukan dengan tetap mematuhi standar protokol kesehatan dengan tetap berjaga jarak dan menggunakan masker serta menjaga kebersihan diri.</span></p>\r\n</div>', NULL, '2021-02-01 19:14:41'),
(9, 1, 'Pemberian Alat Kontrasepsi kepada Akseptor KB', 'pemberian-alat-kontrasepsi-kepada-akseptor-kb', '5113S6812057220-Pemberian_Alat_Kontasepsi_berupa_pil_KB_kepada_Akseptor_KB_21_56_43.jpg', '<div class=\"post-entry\">\r\n<p style=\"box-sizing: border-box; padding: 0px; margin: 0px; border: 0px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-weight: 400; font-stretch: inherit; font-size: 17px; line-height: inherit; font-family: \'IBM Sans\', -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Helvetica, Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', sans-serif; vertical-align: baseline; color: #333333; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;\"><strong>Banaran</strong> -Keluarga Berencana merupakan salah satu gerakan yang dibentuk oleh pemerintah yang berguna untuk membentuk keluarga yang sehat dan sejahtera dengan membatasi jumlah kelahiran. Hal ini bermaksud untuk merencanakan dan mengatur jumlah keluarga dengan pembatasan jumlah kelahiran 2 anak dalam satu keluarga. Dalam hal ini masyarakat perlu menggunakan beberapa metode untuk melakukan perencanaan dan pembatasan jumlah kelahiran anak, salah satunya dengan menggunakan alat kontrasepsi.&nbsp;</p>\r\n<p style=\"box-sizing: border-box; padding: 0px; margin: 0px; border: 0px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-weight: 400; font-stretch: inherit; font-size: 17px; line-height: inherit; font-family: \'IBM Sans\', -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Helvetica, Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', sans-serif; vertical-align: baseline; color: #333333; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;\">Kampung KB Gayuh Lestari pada tanggal 7 Juli 2020 melakukan kegiatan pembagian alat kontrasepsi kepada Akseptor KB (Masyarakat yang mengikuti gerakan KB). Hal ini bertujuan untuk menciptakan tujuan gerakan KB yang dibentuk oleh Pemerintah</p>\r\n</div>', NULL, '2021-02-01 19:15:10'),
(10, 1, 'Pertemuan Rutin Karangtaruna Karya Bhakti', 'pertemuan-rutin-karangtaruna-karya-bhakti', '5317367280450V1-Ketua_Karang_Taruna_sedang_memberikan_sambutan_01_10_41.jpg', '<div class=\"post-entry\">\r\n<p style=\"box-sizing: border-box; padding: 0px; margin: 0px; border: 0px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-weight: 400; font-stretch: inherit; font-size: 17px; line-height: inherit; font-family: \'IBM Sans\', -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Helvetica, Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', sans-serif; vertical-align: baseline; color: #333333; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;\"><strong>Banaran </strong>- Pertemuan Rutin Karang Taruna \"Karya Bhakti\" merupakan kegiatan rutin yang dilakukan selama sebulan sekali. Kegiatan ini bertujuan untuk mengumpulkan pendapat, aspirasi remaja dalam kegiatan bermasyrakat dan sebagai tempat belajar berorganisasi bagi remaja Dk. Rambat, Banaran.&nbsp;</p>\r\n<p style=\"box-sizing: border-box; padding: 0px; margin: 0px; border: 0px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-weight: 400; font-stretch: inherit; font-size: 17px; line-height: inherit; font-family: \'IBM Sans\', -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Helvetica, Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', sans-serif; vertical-align: baseline; color: #333333; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;\">Pada pertemuan tanggal 22/03/2020 membahas tentang pentingnya mengetahui apa itu covid-19 dan bagaimana cara melindungi diri dari pendemi covid-19. Selain itu pada pertemuan tersebut ditunjuklah Sdr. Bukhori sebagai sie keagamaan sebagai perwakilan dan narahubung antara pihak karangtaruna dan pengurus masjid Al-Amin Dk. Rambat, Banaran, Kalijambe</p>\r\n</div>', NULL, '2021-02-01 19:15:55'),
(13, 1, 'Tua - Muda, Sambut Kemerdekaan RI dengan Acara Sepak Bola Daster', 'tua-muda-sambut-kemerdekaan-ri-dengan-acara-sepak-bola-daster', '7Y9857170991698-Peserta_dan_wasit_lomba_sepak_bola_daster_sedang_melakukan_foto_bersama_23_38_26.jpg', '<p><span style=\"color: #333333; font-family: \'IBM Sans\', -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Helvetica, Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;\"><strong>Banaran </strong>- Setelah dilakukannya pertandingan bola volly mini, dalam rangka memperingai HUT RI ke 75 masyarakat dan karang taruna menyelenggarakan lomba sepak bola daster yang diselenggarakan di lapangan bolla volly mini. Kegiatan ini diikuti oleh beberapa bekas pemain bola volly dan anak-anak di lingkungan Kampung KB Gayuh Lestari.&nbsp;&nbsp;</span></p>', '2021-01-20 10:20:49', '2021-02-01 19:16:22'),
(14, 1, 'Memeriahkan HUT RI ke-75 Karang Taruna Mengadakan Lomba Mengaji untuk Anak-anak', 'memeriahkan-hut-ri-ke-75-karang-taruna-mengadakan-lomba-mengaji-untuk-anak-anak', '99G095297913154-ewe.PNG', '<p><span style=\"color: #333333; font-family: \'IBM Sans\', -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Helvetica, Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;\"><strong>Banaran </strong>- Dalam rangkan memperingati HUT RI yang ke 75, Karang Taruna Karya Bhakti tidak hanya melakukan kegiatan lomba untuk orang dewasa melainkan juga untuk anak-anak. Kegiatan lomba untuk anak-anak dilaksanakan tepat pada tanggal 17 Agustus 2020. Adapun kegiatan lomba anak yang diselenggarakan adalah lomba adzan, lomba hafalan surat pendek, lomba pecah air, lomba estafet karet dan lomba estafet air. Kegiatan lomba dilaksanakan dalam satu hari dan diikuti oleh kurang lebih 30an anak.</span></p>', '2021-02-01 19:16:52', '2021-02-01 19:16:52');

-- --------------------------------------------------------

--
-- Table structure for table `t_dukuh`
--

CREATE TABLE `t_dukuh` (
  `id` int(11) NOT NULL,
  `dukuh` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_dukuh`
--

INSERT INTO `t_dukuh` (`id`, `dukuh`) VALUES
(1, 'Banaran'),
(2, 'Bentak'),
(3, 'Dukuh'),
(4, 'Karangasem'),
(5, 'Karangduwet'),
(6, 'Karangmojo'),
(7, 'Karangpung'),
(8, 'Karangturi'),
(9, 'Kranggan'),
(10, 'Ngemplak'),
(11, 'Pilangsari'),
(12, 'Dukuhrambat'),
(13, 'Wonosari');

-- --------------------------------------------------------

--
-- Table structure for table `t_ibus`
--

CREATE TABLE `t_ibus` (
  `id` int(11) NOT NULL,
  `form_kelahirans_id` int(11) DEFAULT NULL,
  `form_kematians_id` int(11) DEFAULT NULL,
  `t_pekerjaan_id` int(10) UNSIGNED NOT NULL,
  `nik` char(16) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `dukuh` varchar(100) DEFAULT NULL,
  `rt` varchar(11) DEFAULT NULL,
  `kelurahan` varchar(100) DEFAULT NULL,
  `kecamatan` varchar(100) DEFAULT NULL,
  `kabupaten` varchar(100) DEFAULT NULL,
  `lamp_ktp` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_ibus`
--

INSERT INTO `t_ibus` (`id`, `form_kelahirans_id`, `form_kematians_id`, `t_pekerjaan_id`, `nik`, `nama`, `tgl_lahir`, `dukuh`, `rt`, `kelurahan`, `kecamatan`, `kabupaten`, `lamp_ktp`) VALUES
(65, 38, NULL, 2, '3314011104890001', 'Rina Setiyani', '1989-04-11', 'Dukuhrambat', '10 A', 'Banaran', 'Kalijambe', 'Sragen', '3665313888B9887-alvx-2.PNG'),
(66, NULL, 29, 1, '3314011803490003', 'Mariyem', '1949-03-18', 'Ngemplak', '13', 'Banaran', 'Kalijambe', 'Sragen', '2831639069412H6-alvx-8.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `t_jabatans`
--

CREATE TABLE `t_jabatans` (
  `id` int(11) NOT NULL,
  `jabatan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_jabatans`
--

INSERT INTO `t_jabatans` (`id`, `jabatan`) VALUES
(1, 'Kepala Desa'),
(2, 'Sekretaris Desa'),
(3, 'Kaur Keuangan'),
(4, 'Kaur TU Umum'),
(5, 'Kaur Perencanaan'),
(6, 'Kasi Pemerintahan'),
(7, 'Kasi Kesra'),
(8, 'Kasi Pelayanan'),
(9, 'Kebayanan I'),
(10, 'Kebayanan II'),
(11, 'Kebayanan III');

-- --------------------------------------------------------

--
-- Table structure for table `t_jeniskepindahans`
--

CREATE TABLE `t_jeniskepindahans` (
  `id` int(11) NOT NULL,
  `kepindahan` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_jeniskepindahans`
--

INSERT INTO `t_jeniskepindahans` (`id`, `kepindahan`) VALUES
(1, 'Antar Kelurahan dalam Satu Kecamatan'),
(2, 'Antar Kecamatan dalam Satu Kabupaten'),
(3, 'Antar Kabupaten / Kota atau Antar Provinsi');

-- --------------------------------------------------------

--
-- Table structure for table `t_kategoriberitas`
--

CREATE TABLE `t_kategoriberitas` (
  `id` int(11) NOT NULL,
  `kategori` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_kategoriberitas`
--

INSERT INTO `t_kategoriberitas` (`id`, `kategori`) VALUES
(1, 'Berita'),
(2, 'Informasi');

-- --------------------------------------------------------

--
-- Table structure for table `t_keluargas`
--

CREATE TABLE `t_keluargas` (
  `id` int(11) NOT NULL,
  `form_perpindahans_id` int(11) NOT NULL,
  `nik` char(16) NOT NULL DEFAULT 'NULL',
  `nama` varchar(255) DEFAULT NULL,
  `shdk` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_keluargas`
--

INSERT INTO `t_keluargas` (`id`, `form_perpindahans_id`, `nik`, `nama`, `shdk`) VALUES
(163, 135, '3314010703680001', 'Aminudin', NULL),
(164, 135, '3314010805740001', 'Sri Hastuti', NULL),
(165, 135, '3314013103980001', 'Muhammad Vicky Al Hasri', NULL),
(166, 135, '3314012103040001', 'Dimas Alfi Setyadi', NULL),
(167, 136, '3314013103980001', 'Muhammad Vicky Al Hasri', NULL),
(168, 137, '3314013103980001', 'Muhammad Vicky Al Hasri', NULL),
(169, 138, '3314010703680001', 'Aminudin', 'Kepala Keluarga'),
(170, 138, '3314010805740001', 'Sri Hastuti', 'Istri'),
(171, 138, '3314013103980001', 'Muhammad Vicky Al Hasri', 'Anak'),
(172, 138, '3314012103040001', 'Dimas Alfi Setyadi', 'Anak'),
(173, 139, '3314010703680001', 'Aminudin', 'Kepala Keluarga'),
(174, 139, '3314010805740001', 'Sri Hastuti', 'Istri'),
(175, 139, '3314013103980001', 'Muhammad Vicky Al Hasri', 'Anak'),
(176, 139, '3314012103040001', 'Dimas Alfi Setyadi', 'Anak'),
(177, 140, '3314010703680001', 'Aminudin', 'Kepala Keluarga'),
(178, 140, '3314010805740001', 'Sri Hastuti', 'Istri'),
(179, 140, '3314013103980001', 'Muhammad Vicky Al Hasri', 'Anak'),
(180, 140, '3314012103040001', 'Dimas Alfi Setyadi', 'Anak');

-- --------------------------------------------------------

--
-- Table structure for table `t_kelurahans`
--

CREATE TABLE `t_kelurahans` (
  `id` int(11) NOT NULL,
  `kelurahan` varchar(100) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `alamat` longtext DEFAULT NULL,
  `nohp` varchar(15) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `npwp` int(20) DEFAULT NULL,
  `jumlah_penduduk` int(100) NOT NULL,
  `penduduk_laki` int(100) NOT NULL,
  `penduduk_perempuan` int(11) NOT NULL,
  `link_kampungkb` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_kelurahans`
--

INSERT INTO `t_kelurahans` (`id`, `kelurahan`, `image`, `alamat`, `nohp`, `email`, `npwp`, `jumlah_penduduk`, `penduduk_laki`, `penduduk_perempuan`, `link_kampungkb`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 'Jl. Solo-Purwodadi Km. 15, Banaran, Kalijambe, Sragen', '(X271) 66227778', 'desa.banaran@gmail.com', NULL, 5605, 2830, 2775, 'https://kampungkb.bkkbn.go.id/intervensi/11565', '2020-12-23 13:52:38', '2021-02-01 19:04:07');

-- --------------------------------------------------------

--
-- Table structure for table `t_keperluan`
--

CREATE TABLE `t_keperluan` (
  `id` int(11) NOT NULL,
  `keperluan` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_keperluan`
--

INSERT INTO `t_keperluan` (`id`, `keperluan`) VALUES
(1, 'Akta Kelahiran'),
(2, 'Akta Kematian'),
(3, 'Pindah Keluar'),
(4, 'Pindah Masuk (Kedatangan)'),
(5, 'KTP Baru'),
(6, 'Mengurus Kehilangan'),
(7, 'Keterangan Domisili');

-- --------------------------------------------------------

--
-- Table structure for table `t_pekerjaan`
--

CREATE TABLE `t_pekerjaan` (
  `id` int(10) UNSIGNED NOT NULL,
  `pekerjaan` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_pekerjaan`
--

INSERT INTO `t_pekerjaan` (`id`, `pekerjaan`) VALUES
(1, 'Belum/Tidak Bekerja'),
(2, 'Mengurus Rumah Tangga'),
(3, 'Pelajar/Mahasiswa'),
(4, 'Pensiunan'),
(5, 'Pegawai Negeri Sipil'),
(6, 'Karyawan Swasta');

-- --------------------------------------------------------

--
-- Table structure for table `t_perangkatdesas`
--

CREATE TABLE `t_perangkatdesas` (
  `id` int(11) NOT NULL,
  `jabatans_id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `nik` char(16) DEFAULT NULL,
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `jeniskelamin` varchar(100) DEFAULT NULL,
  `pendidikan_terakhir` varchar(100) DEFAULT NULL,
  `sk_pengangkatan` varchar(255) DEFAULT NULL,
  `tanggal_pengangkatan` date DEFAULT NULL,
  `nohp` char(13) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_perangkatdesas`
--

INSERT INTO `t_perangkatdesas` (`id`, `jabatans_id`, `nama`, `image`, `nik`, `tempat_lahir`, `tgl_lahir`, `jeniskelamin`, `pendidikan_terakhir`, `sk_pengangkatan`, `tanggal_pengangkatan`, `nohp`, `created_at`, `updated_at`) VALUES
(32, 1, 'Joko Rahayu, SE', '7242N9208532405-96431e8ba655fe035ba6e0cc2aafcf97.png', NULL, NULL, NULL, 'Laki - laki', NULL, '6M0352014318845-Laporan Penjualan (3).pdf', NULL, NULL, '2020-12-17 13:09:27', '2021-01-23 09:19:30'),
(36, 2, 'Putri Iknesia Widya Pangestika, S. Pd', '633676216600K56-PngItem_1104775.png', NULL, NULL, NULL, 'Perempuan', NULL, '13219199998U242-13131.pdf', NULL, NULL, '2020-12-25 06:45:20', '2021-01-23 09:04:36'),
(39, 9, 'Eni Kusumastuti', 'T56393560024828-PngItem_1104775.png', NULL, NULL, NULL, 'Perempuan', NULL, '023150P38634581-Laporan Layanan Surat.pdf', NULL, NULL, '2021-01-20 10:18:45', '2021-01-23 09:07:23'),
(40, 10, 'Bakti Ida Hutami', '05179103942S938-PngItem_1104775.png', NULL, NULL, NULL, 'Perempuan', NULL, NULL, NULL, NULL, '2021-01-23 09:11:08', '2021-01-23 09:11:08'),
(41, 11, 'Suramto', '9141994975159C5-96431e8ba655fe035ba6e0cc2aafcf97.png', NULL, NULL, NULL, 'Laki - laki', NULL, NULL, NULL, NULL, '2021-01-23 09:11:29', '2021-01-23 09:11:29'),
(42, 6, 'Bayu Ridho Widiatmojo, SH', '2491761280838C1-96431e8ba655fe035ba6e0cc2aafcf97.png', NULL, NULL, NULL, 'Laki - laki', NULL, NULL, NULL, NULL, '2021-01-23 09:12:01', '2021-01-23 09:12:01'),
(43, 8, 'Pudiyanto', '21P882653869248-96431e8ba655fe035ba6e0cc2aafcf97.png', NULL, NULL, NULL, 'Laki - laki', NULL, NULL, NULL, NULL, '2021-01-23 09:12:23', '2021-01-23 09:12:23'),
(44, 7, 'Andri Supriyatno, S.I.Pust', '329291R86833751-96431e8ba655fe035ba6e0cc2aafcf97.png', NULL, NULL, NULL, 'Laki - laki', NULL, NULL, NULL, NULL, '2021-01-23 09:13:01', '2021-01-23 09:13:01'),
(45, 4, 'Utin Rahmawati, SH', '25H588982763189-PngItem_1104775.png', NULL, NULL, NULL, 'Perempuan', NULL, NULL, NULL, NULL, '2021-01-23 09:13:33', '2021-01-23 09:13:33'),
(46, 5, 'Drs. Sumadi', '80795W973383120-96431e8ba655fe035ba6e0cc2aafcf97.png', NULL, NULL, NULL, 'Laki - laki', NULL, NULL, NULL, NULL, '2021-01-23 09:13:53', '2021-01-23 09:13:53'),
(47, 3, 'Kentias Hartiningsih, A.Md', '58896414653F988-PngItem_1104775.png', NULL, NULL, NULL, 'Perempuan', NULL, NULL, NULL, NULL, '2021-01-23 09:14:12', '2021-01-23 09:14:12');

-- --------------------------------------------------------

--
-- Table structure for table `t_perangkatdesastrukturs`
--

CREATE TABLE `t_perangkatdesastrukturs` (
  `id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_perangkatdesastrukturs`
--

INSERT INTO `t_perangkatdesastrukturs` (`id`, `image`, `updated_at`) VALUES
(1, '860168E20516741-Struktur Organisasi - Banaran.png', '2021-01-30 12:23:42');

-- --------------------------------------------------------

--
-- Table structure for table `t_rt`
--

CREATE TABLE `t_rt` (
  `id` int(11) NOT NULL,
  `t_dukuh_id` int(11) NOT NULL,
  `rt` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_rt`
--

INSERT INTO `t_rt` (`id`, `t_dukuh_id`, `rt`) VALUES
(1, 1, '09'),
(2, 1, '09 A'),
(3, 2, '07 A'),
(4, 3, '12'),
(5, 4, '01'),
(6, 4, '01 A'),
(7, 4, '02'),
(8, 5, '06'),
(9, 7, '05'),
(10, 7, '05 A'),
(11, 8, '03'),
(12, 8, '03 A'),
(13, 8, '04'),
(14, 9, '14'),
(15, 9, '15'),
(16, 10, '13'),
(17, 11, '08 A'),
(18, 12, '10'),
(19, 12, '10 A'),
(20, 13, '07'),
(21, 13, '08'),
(22, 10, '11'),
(23, 6, '16');

-- --------------------------------------------------------

--
-- Table structure for table `t_saksi1s`
--

CREATE TABLE `t_saksi1s` (
  `id` int(11) NOT NULL,
  `form_kelahirans_id` int(11) DEFAULT NULL,
  `form_kematians_id` int(11) DEFAULT NULL,
  `t_pekerjaan_id` int(10) UNSIGNED NOT NULL,
  `t_dukuh_id` int(11) NOT NULL,
  `nik` char(16) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `jeniskelamin` enum('Laki - laki','Perempuan') DEFAULT NULL,
  `lamp_ktp` varchar(255) DEFAULT NULL,
  `rt` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_saksi1s`
--

INSERT INTO `t_saksi1s` (`id`, `form_kelahirans_id`, `form_kematians_id`, `t_pekerjaan_id`, `t_dukuh_id`, `nik`, `nama`, `tgl_lahir`, `jeniskelamin`, `lamp_ktp`, `rt`) VALUES
(65, 38, NULL, 6, 12, '3314010505950001', 'Dwi Pamungkas', '1995-05-05', 'Laki - laki', '36138888B659387-alvx-6.PNG', '10'),
(66, NULL, 29, 3, 10, '3314010505950001', 'Dwi Pamungkas', '1995-05-05', 'Laki - laki', '90H694168213326-alvx-3.PNG', '13');

-- --------------------------------------------------------

--
-- Table structure for table `t_saksi2s`
--

CREATE TABLE `t_saksi2s` (
  `id` int(11) NOT NULL,
  `form_kelahirans_id` int(11) DEFAULT NULL,
  `form_kematians_id` int(11) DEFAULT NULL,
  `t_pekerjaan_id` int(10) UNSIGNED NOT NULL,
  `t_dukuh_id` int(11) NOT NULL,
  `nik` char(16) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `jeniskelamin` enum('Laki - laki','Perempuan') DEFAULT NULL,
  `lamp_ktp` varchar(255) DEFAULT NULL,
  `rt` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_saksi2s`
--

INSERT INTO `t_saksi2s` (`id`, `form_kelahirans_id`, `form_kematians_id`, `t_pekerjaan_id`, `t_dukuh_id`, `nik`, `nama`, `tgl_lahir`, `jeniskelamin`, `lamp_ktp`, `rt`) VALUES
(65, 38, NULL, 3, 12, '3314011010970001', 'Rini Setyowati', '1997-10-10', 'Perempuan', 'B68883891375386-alvx-7.PNG', '10 A'),
(66, NULL, 29, 3, 10, '3314011010970001', 'Rini Setyowati', '1997-10-10', 'Perempuan', 'H63619240216839-alvx-1.PNG', '11');

-- --------------------------------------------------------

--
-- Table structure for table `t_sambutans`
--

CREATE TABLE `t_sambutans` (
  `id` int(11) NOT NULL,
  `sambutan` longtext DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_sambutans`
--

INSERT INTO `t_sambutans` (`id`, `sambutan`, `created_at`, `updated_at`) VALUES
(1, '<p>Assalamu&rsquo;alaikum Wr Wb.</p>\r\n<p>Warga Masyarakat Desa Banaran yang kami hormati, ijinkanlah saya selaku Kepala Desa Banaran mengucapkan terima kasih serta Syukur Alhamdulillah atas Izin dan Ridho-Nya serta dukungan dari segenap masyarakat Desa Banaran, kami dapat menyajikan informasi seputar Desa Banaran, melalui sebuah layanan Digital berupa Website Resmi Desa Banaran ini.</p>\r\n<p>Website ini kami buat dengan tujuan untuk menjalin silaturahmi dengan seluruh warga masyarakat Desa Banaran, dan juga sebagai sarana pelayanan dan menyampaikan informasi rencana, program, perkembangan serta capaian pembangunan di wilaya Desa Banaran kepada seluruh warga masyarakat Desa Banaran pada khususnya, dan semua stakeholder Pemerintahan yang terkait pada umumnya.</p>\r\n<p>Kami juga membangun Sistem Informasi Desa di sisi pelayanan Kantor Desa Lamuk menggunakan system Teknologi Informasi yang terpadu, terpusat dan terkontrol. Kami berharap dan berupaya melalui semua itu, semoga dapat memberikan pelayanan yang lebih baik kepada seluruh masyarakat Desa Banaran serta stakeholder terkait lainnya.</p>\r\n<p>Harapannya Sistem Informasi Desa yang sedang kami bangun ini bermafaat, dan kami juga mengajak kepada seluruh warga masyarakat Desa Banaran ikut serta untuk bersinergi dengan kami, dimana dukungan pemikiran, sumbangsih saran dan masukkan yang membangun dan menginspirasi sangat kami nantikan.</p>\r\n<p>Semoga makin maju desaku, salam untuk semuanya.</p>\r\n<p>Wassalamu\'alaikum Wr.Wb.</p>', NULL, '2021-01-23 15:13:06');

-- --------------------------------------------------------

--
-- Table structure for table `t_surat`
--

CREATE TABLE `t_surat` (
  `id` int(11) NOT NULL,
  `t_keperluan_id` int(11) NOT NULL,
  `t_users_id` int(11) NOT NULL,
  `id_pengajuan` varchar(255) DEFAULT NULL,
  `no_surat` varchar(100) DEFAULT NULL,
  `no_suratformulir` varchar(100) DEFAULT NULL,
  `permohonan_ktp` varchar(100) DEFAULT NULL,
  `keterangan` longtext DEFAULT NULL,
  `status` enum('proses','terverifikasi') DEFAULT 'proses',
  `ktp_pelapor` varchar(255) DEFAULT NULL,
  `lamp_pengantarRT` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_surat`
--

INSERT INTO `t_surat` (`id`, `t_keperluan_id`, `t_users_id`, `id_pengajuan`, `no_surat`, `no_suratformulir`, `permohonan_ktp`, `keterangan`, `status`, `ktp_pelapor`, `lamp_pengantarRT`, `created_at`, `updated_at`) VALUES
(182, 1, 18, '26679K916179985', '123/35/I/2021', NULL, NULL, 'Lahir Anak Ketiga Dengan Nama Bastian Stell', 'terverifikasi', '3356B8886891837-alvx-1.PNG', '851833B38786896-default.png', '2021-03-08 13:57:56', '2021-03-08 14:08:12'),
(183, 2, 18, '627R55761720683', '234/35/III/2021', NULL, NULL, 'Kematian atas nama Syamin Syamsudin', 'terverifikasi', '4H1601933298662-alvx-9.PNG', '619038H46236192-default.png', '2021-03-08 14:21:09', '2021-03-08 14:29:07'),
(184, 5, 18, '2157G7781338351', '102/34/III/2021', NULL, 'KTP Baru', 'Guna membuat KTP Baru', 'terverifikasi', NULL, '54927O278953536-default.png', '2021-03-08 14:43:43', '2021-03-08 14:44:12'),
(185, 6, 18, '35286744154273T', '99/37/III/2021', NULL, NULL, 'Guna sebagai pelengkap dokumen kehilangan barang berupa Laptop Asus di Kost', 'terverifikasi', '38D586579699739-default.png', '8869D6953377995-default.png', '2021-03-08 14:49:36', '2021-03-08 14:49:56'),
(186, 7, 18, '7563110783194Q7', '67/36/III/2021', NULL, NULL, 'Menerangkan bahwa yang bersangkutan berdomisili di Desa Banaran', 'terverifikasi', '637399E79572922-default.png', '7973692E2975329-default.png', '2021-03-08 15:00:54', '2021-03-08 15:01:20'),
(189, 3, 18, '680E81144651820', '06/23/III/2021', '06/23/PIN-KELUAR/III/2021', NULL, 'Perpindahan tempat tinggal karena pekerjaan Orang Tua', 'terverifikasi', '269869363E38566-default.png', '93E236656896836-default.png', '2021-03-08 15:10:31', '2021-03-08 15:15:59'),
(190, 3, 18, '0018D8121360200', '08/22/III/2021', '08/22/PIN-KELUAR/III/2021', NULL, 'Pindah karena untuk menempati rumah sendiri', 'terverifikasi', '84J049621713623-default.png', '248936124607J31-default.png', '2021-03-08 15:12:44', '2021-03-08 15:16:51'),
(191, 3, 18, '1968177792849N5', '09/21/III/2021', '09/21/PIN-KELUAR/III/2021', NULL, 'Pindah karena untuk menempati rumah sendiri', 'terverifikasi', '87428348T370587-default.png', '87T832488530477-default.png', '2021-03-08 15:14:16', '2021-03-08 15:17:43'),
(192, 4, 18, '13051230Q199901', '12/45/III/2021', '12/45/PIN-MASUK/III/2021', NULL, 'Pindah karena menempati tempat tinggal baru', 'terverifikasi', '47612Z423149704-default.png', NULL, '2021-03-08 15:27:52', '2021-03-08 15:34:42'),
(193, 4, 18, '461230T12256735', '13/46/III/2021', '13/46/PIN-MASUK/III/2021', NULL, 'Pindah karena menempati tempat tinggal baru', 'terverifikasi', '38S504051566505-default.png', NULL, '2021-03-08 15:29:27', '2021-03-08 15:37:45'),
(194, 4, 18, 'W63977602173675', '14/47/III/2021', '14/47/PIN-MASUK/III/2021', NULL, 'Pindah karena menempati tempat tinggal baru', 'terverifikasi', '1817795942294Q2-default.png', NULL, '2021-03-08 15:30:44', '2021-03-08 15:38:50');

-- --------------------------------------------------------

--
-- Table structure for table `t_taglines`
--

CREATE TABLE `t_taglines` (
  `id` int(11) NOT NULL,
  `tagline` varchar(50) DEFAULT NULL,
  `deskripsi` longtext DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_taglines`
--

INSERT INTO `t_taglines` (`id`, `tagline`, `deskripsi`, `created_at`, `updated_at`) VALUES
(1, 'Kelurahan', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Expedita atque ad obcaecati quo animi itaque. Max 8 Kata', NULL, '2021-01-20 10:15:31'),
(2, 'Banaran', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Expedita atque ad obcaecati quo animi itaque. Max 8 Kata', NULL, '2020-12-11 15:28:53'),
(3, 'Maju', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Expedita atque ad obcaecati quo animi itaque. Max 8 Kata', NULL, '2020-12-11 15:29:05'),
(4, 'Sejahtera', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Expedita atque ad obcaecati quo animi itaque. Max 8 Kata', NULL, '2020-12-11 15:30:32');

-- --------------------------------------------------------

--
-- Table structure for table `t_tinggals`
--

CREATE TABLE `t_tinggals` (
  `id` int(11) NOT NULL,
  `form_perpindahans_id` int(11) NOT NULL,
  `dukuh` varchar(100) DEFAULT NULL,
  `rt` varchar(11) DEFAULT NULL,
  `rw` varchar(11) DEFAULT NULL,
  `kelurahan` varchar(100) DEFAULT NULL,
  `kecamatan` varchar(100) DEFAULT NULL,
  `kabupaten` varchar(100) DEFAULT NULL,
  `provinsi` varchar(255) NOT NULL,
  `kodepos` char(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_tinggals`
--

INSERT INTO `t_tinggals` (`id`, `form_perpindahans_id`, `dukuh`, `rt`, `rw`, `kelurahan`, `kecamatan`, `kabupaten`, `provinsi`, `kodepos`) VALUES
(40, 135, 'Duren Jaya', '03', '07', 'Duren Jaya', 'Bekasi Timur', 'Bekasi', 'Jawa Barat', '32534'),
(41, 136, 'Kategan', '02', '09', 'Gemolong', 'Gemolong', 'Sragen', 'Jawa Tengah', '12133'),
(42, 137, 'Padas', '02', '05', 'Ngebung', 'Kalijambe', 'Sragen', 'Jawa Tengah', '32534'),
(43, 138, 'Padas', '02', '05', 'Ngebung', 'Kalijambe', 'Sragen', 'Jawa Tengah', '12133'),
(44, 139, 'Kategan', '03', '07', 'Gemolong', 'Kalijambe', 'Sragen', 'Jawa Tengah', '32534'),
(45, 140, 'Duren Jaya', '03', '09', 'Duren Jaya', 'Bekasi Timur', 'Bekasi', 'Jawa Barat', '23425');

-- --------------------------------------------------------

--
-- Table structure for table `t_tupoksis`
--

CREATE TABLE `t_tupoksis` (
  `id` int(11) NOT NULL,
  `tupoksi` longtext DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_tupoksis`
--

INSERT INTO `t_tupoksis` (`id`, `tupoksi`, `created_at`, `updated_at`) VALUES
(1, '<p><strong>KEPALA DESA</strong></p>\r\n<p>Kepala Desa selaku PKPKD, mempunyai kewenangan:</p>\r\n<ol>\r\n<li>menetapkan kebijakan tentang pelaksanaan APBDesa;</li>\r\n<li>menetapkan kebijakan tentang pengelolaan barang milik Desa;</li>\r\n<li>melakukan tindakan yang mengakibatkan pengeluaran atas beban APB Desa;</li>\r\n<li>menetapkan PPKD;</li>\r\n<li>menyetujui DPA, DPPA, dan DPAL;</li>\r\n<li>menyetujui RAK Desa; dan</li>\r\n<li>menyetujui SPP.</li>\r\n</ol>\r\n<p>&nbsp;</p>\r\n<p><strong>SEKRETARIS DESA</strong></p>\r\n<p><strong>Tugas :</strong></p>\r\n<p>Sekretaris Desa bertugas membantu Kepala Desa dalam bidang <a href=\"https://format-administrasi-desa.blogspot.com/2018/12/format-administrasi-pemerintahan-desa-terbaru-download.html\">administrasi pemerintahan desa</a>.</p>\r\n<p>Selain tugas tersebut, Sekretaris Desa juga bertugas :</p>\r\n<ol>\r\n<li>mengoordinasikan penyusunan dan pelaksanaan kebijakan <a href=\"https://format-administrasi-desa.blogspot.com/2018/11/apbdes-2019.html\">APBDes</a></li>\r\n<li>mengoordinasikan penyusunan rancangan APBDes dan rancangan <a href=\"https://format-administrasi-desa.blogspot.com/2018/11/apa-yang-dimaksud-dengan-apbdes-dan-perubahan-apbdes.html\">Perubahan APBDes</a></li>\r\n<li>mengoordinasikan penyusunan rancangan Peraturan Desa tentang APBDes, Perubahan APBDes, dan pertanggungjawaban pelaksanaan APBDes</li>\r\n<li>mengoordinasikan penyusunan rancangan <a href=\"https://format-administrasi-desa.blogspot.com/2018/11/perkades-peraturan-kepala-desa-tentang-penjabaran-apbdes-terbaru-2019-2020.html\">Peraturan Kepala Desa tentang penjabaran APBDes dan perubahan penjabaran APBDes</a></li>\r\n<li>mengoordinasikan <a href=\"https://format-administrasi-desa.blogspot.com/2019/03/kumpulan-tupoksi-perangkat-desa-terbaru.html\">tugas perangkat Desa</a> lain yang menjalankan <a href=\"https://format-administrasi-desa.blogspot.com/2019/02/contoh-sk-ppkd-2019-terbaru-format-word.html\">tugas PPKD Desa</a></li>\r\n<li>mengoordinasikan penyusunan laporan keuangan Desa dalam rangka pertanggungjawaban pelaksanaan APBDes</li>\r\n<li>melakukan verifikasi terhadap <a href=\"https://format-administrasi-desa.blogspot.com/search/label/DPA%20APBDesa\">Dokumen Pelaksanaan Anggaran (DPA), Dokumen Perubahan Pelaksanaan Anggaran (DPPA), dan Dokumen Pelaksanaan Anggaran Lanjutan (DPAL)</a></li>\r\n<li>melakukan verifikasi terhadap Rencana Anggaran Kas Desa (RAK Desa)</li>\r\n<li>melakukan verifikasi terhadap bukti penerimaan dan pengeluaran APBDes</li>\r\n</ol>\r\n<p><strong>Fungsi</strong></p>\r\n<p>Untuk melaksanakan tugasnya, Sekretaris Desa mempunyai fungsi:</p>\r\n<ol>\r\n<li>Melaksanakan urusan ketatausahaan seperti tata naskah, <a href=\"https://format-administrasi-desa.blogspot.com/2018/11/kode-surat-desa.html\">administrasi surat menyurat</a>, arsip, dan ekspedisi.</li>\r\n<li>Melaksanakan urusan umum seperti penataan administrasi perangkat desa, penyediaan prasarana perangkat desa dan kantor, penyiapan rapat, pengadministrasian aset, inventarisasi, <a href=\"https://format-administrasi-desa.blogspot.com/2018/11/download-contoh-format-sppd-desa-kepala-desa-perangkat-desa-bpd-pkk-dan-lembaga-kemasyarakatan-desa-lainnya.html\">perjalanan dinas</a>, dan pelayanan umum.</li>\r\n<li>Melaksanakan urusan keuangan seperti pengurusan administrasi keuangan, administrasi sumber-sumber pendapatan dan pengeluaran, verifikasi administrasi keuangan, dan administrasi penghasilan Kepala Desa, Perangkat Desa, BPD, dan lembaga pemerintahan desa lainnya.</li>\r\n<li>Melaksanakan urusan perencanaan seperti menyusun rencana anggaran pendapatan dan belanja desa, menginventarisir data-data dalam rangka pembangunan, melakukan monitoring dan evaluasi program, serta penyusunan laporan</li>\r\n</ol>\r\n<p>&nbsp;</p>\r\n<p><strong>KAUR KEUANGAN</strong></p>\r\n<p><strong>Tugas&nbsp;</strong></p>\r\n<p>Kepala urusan keuangan bertugas membantu Sekretaris Desa melaksanakan fungsi kebendaharaan dalam urusan pelayanan administrasi keuangan desa.</p>\r\n<p>Selain tugas tersebut, Kaur Keuangan Desa juga bertugas :</p>\r\n<ol>\r\n<li>Menyusun Rencana Anggaran Kas Desa (RAK Desa)</li>\r\n<li>Melakukan penatausahaan yang meliputi menerima/menyimpan, menyetorkan/membayar, menatausahakan dan mempertanggungjawabkan penerimaan pendapatan Desa dan pengeluaran dalam rangka pelaksanaan APBDes</li>\r\n</ol>\r\n<p><strong>Fungsi</strong></p>\r\n<p>Untuk melaksanakan tugasnya, Kaur Keuangan mempunyai fungsi melaksanakan urusan keuangan seperti :</p>\r\n<ol>\r\n<li>Pengurusan administrasi keuangan</li>\r\n<li>Administrasi sumber-sumber pendapatan dan pengeluaran,</li>\r\n<li>Verifikasi administrasi keuangan, dan</li>\r\n<li>Administrasi penghasilan Kepala Desa, Perangkat Desa, BPD, dan lembaga pemerintahan desa lainnya.</li>\r\n</ol>\r\n<p>&nbsp;</p>\r\n<p><strong>KAUR PERENCANAAN</strong></p>\r\n<p><strong>Tugas&nbsp;</strong></p>\r\n<p>Kepala urusan perencanaan bertugas membantu Sekretaris Desa dalam urusan pelayanan administrasi perencanaan desa.</p>\r\n<p>Selain tugas tersebut, Kaur Perencanaan Desa juga bertugas :</p>\r\n<ol>\r\n<li>melakukan tindakan yang mengakibatkan pengeluaran atas beban anggaran belanja sesuai bidang tugasnya</li>\r\n<li>melaksanakan anggaran kegiatan sesuai bidang tugasnya</li>\r\n<li>mengendalikan kegiatan sesuai bidang tugasnya</li>\r\n<li>menyusun DPA (Dokumen Pelaksanaan Anggaran), DPPA (Dokumen Perubahan Pelaksanaan Anggaran), dan DPAL (Dokumen Pelaksanaan Anggaran Lanjutan) sesuai bidang tugasnya</li>\r\n<li>menandatangani perjanjian kerja sama dengan penyedia atas pengadaan barang/jasa untuk kegiatan yang berada dalam bidang tugasnya; dan</li>\r\n<li>menyusun laporan pelaksanaan kegiatan sesuai bidang tugasnya untuk pertanggungjawaban pelaksanaan Anggaran Pendapatan dan Belanja Desa (APBDes)</li>\r\n</ol>\r\n<p><strong>Fungsi</strong></p>\r\n<p>Untuk melaksanakan tugasnya, Kaur Perencanaan memiliki fungsi mengoordinasikan urusan perencanaan seperti :</p>\r\n<ol>\r\n<li>menyusun rencana anggaran pendapatan dan belanja desa;</li>\r\n<li>menginventarisir data-data dalam rangka pembangunan;</li>\r\n<li>melakukan monitoring;</li>\r\n<li>evaluasi program;</li>\r\n<li>penyusunan laporan.</li>\r\n</ol>\r\n<p>&nbsp;</p>\r\n<p><strong>KAUR TATA USAHA DAN UMUM</strong></p>\r\n<p><strong>Tugas Kaur Tata Usaha dan Umum</strong></p>\r\n<p>Kepala urusan tata usaha dan umum bertugas membantu Sekretaris Desa dalam urusan pelayanan administrasi ketatausahaan.</p>\r\n<p>Selain tugas tersebut, Kaur Tata Usaha Dan Umum juga bertugas :</p>\r\n<ol>\r\n<li>melakukan tindakan yang mengakibatkan pengeluaran atas beban anggaran belanja sesuai bidang tugasnya</li>\r\n<li>melaksanakan anggaran kegiatan sesuai bidang tugasnya</li>\r\n<li>mengendalikan kegiatan sesuai bidang tugasnya</li>\r\n<li>menyusun <a href=\"https://format-administrasi-desa.blogspot.com/2018/11/dpa-apbdesa-rka-desa-rkkd-dan-rab.html\">DPA</a> (Dokumen Pelaksanaan Anggaran), DPPA (Dokumen Perubahan Pelaksanaan Anggaran), dan DPAL (Dokumen Pelaksanaan Anggaran Lanjutan) sesuai bidang tugasnya</li>\r\n<li>menandatangani perjanjian kerja sama dengan penyedia atas pengadaan barang/jasa (b/j) untuk kegiatan yang berada dalam bidang tugasnya; dan</li>\r\n<li>menyusun laporan pelaksanaan kegiatan sesuai bidang tugasnya untuk pertanggungjawaban pelaksanaan Anggaran Pendapatan dan Belanja Desa (APBDes)</li>\r\n</ol>\r\n<p><strong>Fungsi Kaur Tata Usaha dan Umum</strong></p>\r\n<p>Untuk melaksanakan tugasnya, Kaur Tata Usaha dan Umum memiliki fungsi melaksanakan urusan ke-tatausaha-an seperti :</p>\r\n<ol>\r\n<li>tata naskah, administrasi surat menyurat, arsip, dan ekspedisi</li>\r\n<li>penataan administrasi perangkat desa, penyediaan prasarana perangkat desa dan kantor,</li>\r\n<li>penyiapan rapat, pengadministrasian aset, inventarisasi, <a href=\"https://format-administrasi-desa.blogspot.com/2018/11/download-contoh-format-sppd-desa-kepala-desa-perangkat-desa-bpd-pkk-dan-lembaga-kemasyarakatan-desa-lainnya.html\">perjalanan dinas</a>, dan pelayanan umum</li>\r\n</ol>\r\n<p>&nbsp;</p>\r\n<p><strong>KASI PEMERINTAHAN</strong></p>\r\n<p><strong>Tugas :</strong></p>\r\n<p>Apa saja tugas-tugas Kasi Pemerintahan Desa? Kepala seksi (Kasi) pemerintahan ini bertugas membantu Kepala Desa dalam melaksanakan tugas bidang pemerintahan desa.</p>\r\n<p>Selain tugas tersebut, Kasi Pemdes juga bertugas :</p>\r\n<ol>\r\n<li>melakukan tindakan yang mengakibatkan pengeluaran atas beban anggaran belanja sesuai bidang tugasnya</li>\r\n<li>melaksanakan anggaran kegiatan sesuai bidang tugasnya</li>\r\n<li>mengendalikan kegiatan sesuai bidang tugasnya</li>\r\n<li>menyusun <a href=\"https://format-administrasi-desa.blogspot.com/2018/11/dpa-apbdesa-rka-desa-rkkd-dan-rab.html\">DPA (Dokumen Pelaksanaan Anggaran), DPPA (Dokumen Perubahan Pelaksanaan Anggaran), dan DPAL (Dokumen Pelaksanaan Anggaran Lanjutan)</a> sesuai bidang tugasnya</li>\r\n<li>menandatangani perjanjian kerja sama dengan penyedia atas pengadaan barang/jasa untuk kegiatan yang berada dalam bidang tugasnya; dan</li>\r\n<li>menyusun laporan pelaksanaan kegiatan sesuai bidang tugasnya untuk pertanggungjawaban pelaksanaan Anggaran Pendapatan dan Belanja Desa (<a href=\"https://format-administrasi-desa.blogspot.com/2018/11/apbdes-2019.html\">APBDes</a>)</li>\r\n</ol>\r\n<p><strong>Fungsi :</strong></p>\r\n<p>Apa saja fungsi-fungsi Kasi Pemerintahan Desa? Untuk melaksanakan tugasnya, maka Kasi Pemerintahan memiliki fungsi:</p>\r\n<ol>\r\n<li>melaksanakan manajemen tata praja Pemerintahan</li>\r\n<li>menyusun rancangan regulasi desa</li>\r\n<li>pembinaan masalah pertanahan</li>\r\n<li>pembinaan ketenteraman dan ketertiban</li>\r\n<li>pelaksanaan upaya perlindungan masyarakat</li>\r\n<li>kependudukan</li>\r\n<li>penataan dan pengelolaan wilayah</li>\r\n<li>pendataan dan pengelolaan Profil Desa.</li>\r\n</ol>\r\n<p>&nbsp;</p>\r\n<p><strong>KASI KESEJAHTERAAN</strong></p>\r\n<p><strong>Tugas</strong></p>\r\n<p>Apa saja <a href=\"https://format-administrasi-desa.blogspot.com/2019/03/tupoksi-kasi-kesra-desa-terbaru.html\">tugas-tugas Kasi Kesra Desa</a>? Kepala seksi kesejahteraan (Kasi Kesra) ini bertugas membantu Kepala Desa dalam melaksanakan tugas bidang pembangunan dan pemberdayaan masyarakat desa.</p>\r\n<p>Selain tugas tersebut, Kasi Kesra juga bertugas :</p>\r\n<ol>\r\n<li>melakukan tindakan yang mengakibatkan pengeluaran atas beban anggaran belanja sesuai bidang tugasnya</li>\r\n<li>melaksanakan anggaran kegiatan sesuai bidang tugasnya</li>\r\n<li>mengendalikan kegiatan sesuai bidang tugasnya</li>\r\n<li>menyusun DPA (Dokumen Pelaksanaan Anggaran), DPPA (Dokumen Perubahan Pelaksanaan Anggaran), dan DPAL (Dokumen Pelaksanaan Anggaran Lanjutan) sesuai bidang tugasnya</li>\r\n<li>menandatangani perjanjian kerja sama dengan penyedia atas pengadaan barang/jasa untuk kegiatan yang berada dalam bidang tugasnya; dan</li>\r\n<li>menyusun laporan pelaksanaan kegiatan sesuai bidang tugasnya untuk pertanggungjawaban pelaksanaan Anggaran Pendapatan dan Belanja Desa (APBDes)</li>\r\n</ol>\r\n<p><strong>Fungsi</strong></p>\r\n<p>Apa saja fungsi-fungsi Kasi Kesra Desa? Untuk melaksanakan tugasnya, maka Kasi Kesra memiliki fungsi:</p>\r\n<ol>\r\n<li>melaksanakan pembangunan sarana prasarana perdesaan</li>\r\n<li>pembangunan bidang pendidikan dan kesehatan</li>\r\n<li>tugas sosialisasi serta motivasi masyarakat di bidang budaya, ekonomi, politik, lingkungan hidup, pemberdayaan keluarga, pemuda, olahraga, dan <a href=\"https://format-administrasi-desa.blogspot.com/2019/10/karang-taruna.html\">karang taruna</a>.</li>\r\n</ol>\r\n<p>&nbsp;</p>\r\n<p><strong>KASI PELAYANAN</strong></p>\r\n<p><strong>Tugas</strong></p>\r\n<p>Apa saja Tugas Kasi Pelayanan? Kepala Seksi (Kasi) pelayanan bertugas membantu Kepala Desa dalam melaksanakan tugas pelayanan sosial kemasyarakatan dan peningkatan kapasitas.</p>\r\n<p>Selain tugas tersebut, Kasi Pelayanan juga bertugas :</p>\r\n<ol>\r\n<li>melakukan tindakan yang mengakibatkan pengeluaran atas beban anggaran belanja sesuai bidang tugasnya</li>\r\n<li>melaksanakan anggaran kegiatan sesuai bidang tugasnya</li>\r\n<li>mengendalikan kegiatan sesuai bidang tugasnya</li>\r\n<li>menyusun DPA (Dokumen Pelaksanaan Anggaran), DPPA (Dokumen Perubahan Pelaksanaan Anggaran), dan DPAL (Dokumen Pelaksanaan Anggaran Lanjutan) sesuai bidang tugasnya</li>\r\n<li>menandatangani perjanjian kerja sama dengan penyedia atas pengadaan barang/jasa untuk kegiatan yang berada dalam bidang tugasnya; dan</li>\r\n<li>menyusun laporan pelaksanaan kegiatan sesuai bidang tugasnya untuk pertanggungjawaban pelaksanaan Anggaran Pendapatan dan Belanja Desa (<a href=\"https://format-administrasi-desa.blogspot.com/2018/11/apbdes-2019.html\">APBDes</a>)</li>\r\n</ol>\r\n<p><strong>Fungsi</strong></p>\r\n<p>Apa saja fungsi Kasi Pelayanan? Untuk melaksanakan tugasnya, maka Kasi Pelayanan memiliki fungsi:</p>\r\n<ol>\r\n<li>melaksanakan penyuluhan dan motivasi terhadap pelaksanaan hak dan kewajiban masyarakat</li>\r\n<li>meningkatkan upaya partisipasi masyarakat, pelestarian nilai sosial budaya masyarakat, keagamaan, dan ketenagakerjaan.</li>\r\n</ol>\r\n<p>&nbsp;</p>\r\n<p><strong>KEPALA DUSUN</strong></p>\r\n<p><strong>Tugas</strong>&nbsp;</p>\r\n<p>Kepala Dusun bertugas membantu Kepala Desa dalam pelaksanaan tugasnya di wilayahnya.</p>\r\n<p><strong>Fungsi</strong></p>\r\n<ol>\r\n<li>Untuk melaksanakan tugasnya, maka Kepala Dusun memiliki fungsi:</li>\r\n<li>Pembinaan ketenteraman dan ketertiban, pelaksanaan upaya perlindungan masyarakat, mobilitas kependudukan, dan penataan dan pengelolaan wilayah.</li>\r\n<li>Membantu Kasi dan Kaur Pelaksana Kegiatan Anggaran (PKA) dalam melaksanakan pengadaan barang/jasa dalam hal sifat dan jenis kegiatannya tidak dapat dilakukan sendiri</li>\r\n<li>Mengawasi pelaksanaan pembangunan di wilayahnya.</li>\r\n<li>Melaksanakan pembinaan kemasyarakatan dalam meningkatkan kemampuan dan kesadaran masyarakat dalam menjaga lingkungannya.</li>\r\n<li>Melakukan upaya-upaya pemberdayaan masyarakat dalam menunjang kelancaran penyelenggaraan pemerintahan dan pembangunan.</li>\r\n</ol>', '2020-12-15 14:54:42', '2021-01-23 08:31:14');

-- --------------------------------------------------------

--
-- Table structure for table `t_umkm`
--

CREATE TABLE `t_umkm` (
  `id` int(11) NOT NULL,
  `t_users_id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `nama_pemilik` varchar(255) DEFAULT NULL,
  `merk_usaha` varchar(255) DEFAULT NULL,
  `no_whatsapp` varchar(15) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `no_siup` varchar(100) DEFAULT NULL,
  `no_npwp` varchar(100) DEFAULT NULL,
  `lamp_siup` varchar(255) DEFAULT NULL,
  `lamp_npwp` varchar(255) DEFAULT NULL,
  `keterangan` longtext NOT NULL,
  `status` enum('proses','terverifikasi') NOT NULL DEFAULT 'proses',
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_umkm`
--

INSERT INTO `t_umkm` (`id`, `t_users_id`, `slug`, `nama_pemilik`, `merk_usaha`, `no_whatsapp`, `image`, `no_siup`, `no_npwp`, `lamp_siup`, `lamp_npwp`, `keterangan`, `status`, `created_at`, `updated_at`) VALUES
(17, 18, 'preppstudio', NULL, 'Preppstudio', '+6282266335773', '3Q5740857811348-C9E0BCD5-DABF-4357-8E62-078891C03AC5.jpeg', '123412.3243.43534.3445', '32423.23423.353234.35', '8750784433511Q8-default.png', '54518848037137Q-default.png', '<p><span class=\"ILfuVd\"><span class=\"hgKElc\"><strong>Preppstudio</strong> sendiri merupakan brand pakaian lokal khusus pria yang sempat vakum pada Bulan Juli lalu setelah enam bulan menyapa konsumen dengan produk pakaiannya yang memberikan look casual namun tetap stylish.</span></span> <span class=\"ILfuVd\"><span class=\"hgKElc\"><strong>Preppstudio</strong> sendiri merupakan brand pakaian lokal khusus pria yang sempat vakum pada Bulan Juli lalu setelah enam bulan menyapa konsumen dengan produk pakaiannya yang memberikan look casual namun tetap stylish. <strong>Preppstudio</strong> sendiri merupakan brand pakaian lokal khusus pria yang sempat vakum pada Bulan Juli lalu setelah enam bulan menyapa konsumen dengan produk pakaiannya yang memberikan look casual namun tetap stylish. <strong>Preppstudio</strong> sendiri merupakan brand pakaian lokal khusus pria yang sempat vakum pada Bulan Juli lalu setelah enam bulan menyapa konsumen dengan produk pakaiannya yang memberikan look casual namun tetap stylish. <strong>Preppstudio</strong> sendiri merupakan brand pakaian lokal khusus pria yang sempat vakum pada Bulan Juli lalu setelah enam bulan menyapa konsumen dengan produk pakaiannya yang memberikan look casual namun tetap stylish.<strong>Preppstudio</strong> sendiri merupakan brand pakaian lokal khusus pria yang sempat vakum pada Bulan Juli lalu setelah enam bulan menyapa konsumen dengan produk pakaiannya yang memberikan look casual namun tetap stylish. <strong>Preppstudio</strong> sendiri merupakan brand pakaian lokal khusus pria yang sempat vakum pada Bulan Juli lalu setelah enam bulan menyapa konsumen dengan produk pakaiannya yang memberikan look casual namun tetap stylish. <strong>Preppstudio</strong> sendiri merupakan brand pakaian lokal khusus pria yang sempat vakum pada Bulan Juli lalu setelah enam bulan menyapa konsumen dengan produk pakaiannya yang memberikan look casual namun tetap stylish.</span></span></p>', 'terverifikasi', '2021-03-06 12:55:19', '2021-03-06 12:56:28');

-- --------------------------------------------------------

--
-- Table structure for table `t_users`
--

CREATE TABLE `t_users` (
  `id` int(11) NOT NULL,
  `t_dukuh_id` int(11) DEFAULT NULL,
  `t_agama_id` int(11) DEFAULT NULL,
  `t_pekerjaan_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) NOT NULL DEFAULT 'NULL',
  `rt` varchar(5) DEFAULT 'NULL',
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `email` varchar(100) NOT NULL DEFAULT 'NULL',
  `jeniskelamin` enum('Laki - laki','Perempuan') NOT NULL,
  `nik` char(16) DEFAULT NULL,
  `no_kk` char(16) DEFAULT NULL,
  `statusperkawinan` varchar(100) DEFAULT NULL,
  `lamp_kk` varchar(255) DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `role` enum('admin','petugas','masyarakat','unverified') DEFAULT 'unverified',
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_users`
--

INSERT INTO `t_users` (`id`, `t_dukuh_id`, `t_agama_id`, `t_pekerjaan_id`, `name`, `rt`, `tempat_lahir`, `tgl_lahir`, `email`, `jeniskelamin`, `nik`, `no_kk`, `statusperkawinan`, `lamp_kk`, `email_verified_at`, `password`, `remember_token`, `role`, `created_at`, `updated_at`) VALUES
(2, 1, 1, 1, 'iamadmin', '09', 'iamadmin', '2020-11-30', 'iamadmin@gmail.com', 'Laki - laki', '9901653782386814', '9901653782386813', 'Belum Kawin', 'user-group-icon.png', NULL, '$2y$10$JYwvm3i.0e5gvCsjhB.oEuxvnqDlNA5rD4NA235ephI6OBRWwQWki', 'yzk5vveAfZe1RuPgQVNpQrZgl5EsGEcmtETBwgmeibrszXoGbmTGUw9aNRJ9', 'admin', '2021-10-30 14:03:27', '2020-11-30 14:03:27'),
(3, NULL, NULL, NULL, '', 'NULL', NULL, NULL, 'iamapetugas@gmail.com', '', NULL, NULL, NULL, NULL, NULL, '$2y$10$54TceYK4/bCmOAiwgVdd.ea6ktoUmF2DcdGCRp.QyyR76NpigxM.2', 'e1wX9dwNf0dvCDfo5qNjwGSNFGLhurmxO2XYF5Sg9ZZQG2WqnnEbD6idZ3lH', 'petugas', '2020-12-31 17:00:00', NULL),
(18, 12, 1, 3, 'Muhammad Vicky Al Hasri', '10', 'Sragen', '1998-03-31', 'muhammadvickyalhasri31@gmail.com', 'Laki - laki', '3314013103980003', '3314010712030878', 'Belum Kawin', '6281726302Y8447-KTP 2.jpg', NULL, '$2y$10$1amIYXI/AJkYaHleLCX76OdYJ7wD8/1fqYZT.XRGYFBI2aMmN0kFO', '6PeM86F1QUhah9Bsdp334qIypUQYoE6KXeMfspJVcqLw3cvufwsXG5uz7b26', 'masyarakat', '2021-02-01 19:28:35', '2021-02-02 18:23:54');

-- --------------------------------------------------------

--
-- Table structure for table `t_visimisis`
--

CREATE TABLE `t_visimisis` (
  `id` int(11) NOT NULL,
  `deskripsi` longtext DEFAULT NULL,
  `jenis` enum('visi','misi') NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_visimisis`
--

INSERT INTO `t_visimisis` (`id`, `deskripsi`, `jenis`, `created_at`, `updated_at`) VALUES
(5, 'Mewujudkan masyarakat Desa Banaran yang unggul, mandiri dan sejahtera.', 'visi', '2020-12-15 12:20:15', '2021-01-14 07:27:28'),
(6, 'Menciptakan tata kelola pemerintahan desa yang baik (good governance).', 'misi', '2020-12-15 12:20:41', '2020-12-15 14:09:57'),
(7, 'Meningkatkan pembangunan sarana dan prasarana desa diberbagai aspek.', 'misi', '2020-12-15 12:21:01', '2020-12-15 12:21:01'),
(8, 'Mewujudkan lingkungan masyarakat yang aman, nyaman dan kesejahteraan masyarakat.', 'misi', '2020-12-15 12:21:20', '2020-12-15 12:21:20');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role` enum('user','admin') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `form_kelahirans`
--
ALTER TABLE `form_kelahirans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `t_form_kelahirans_FKIndex5` (`t_surat_id`);

--
-- Indexes for table `form_kematians`
--
ALTER TABLE `form_kematians`
  ADD PRIMARY KEY (`id`),
  ADD KEY `form_kematian_FKIndex1` (`t_pekerjaan_id`),
  ADD KEY `form_kematian_FKIndex2` (`t_dukuh_id`),
  ADD KEY `form_kematians_FKIndex7` (`t_agama_id`),
  ADD KEY `form_kematians_ibfk_4` (`t_surat_id`);

--
-- Indexes for table `form_perpindahans`
--
ALTER TABLE `form_perpindahans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `form_perpindahans_FKIndex1` (`t_jeniskepindahans_id`),
  ADD KEY `form_perpindahans_FKIndex4` (`t_dukuh_id`),
  ADD KEY `form_perpindahans_ibfk_3` (`t_surat_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `t_agama`
--
ALTER TABLE `t_agama`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_ayahs`
--
ALTER TABLE `t_ayahs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `t_ayah_FKIndex2` (`t_pekerjaan_id`),
  ADD KEY `t_ayahs_ibfk_2` (`form_kelahirans_id`),
  ADD KEY `t_ayahs_ibfk_3` (`form_kematians_id`);

--
-- Indexes for table `t_banners`
--
ALTER TABLE `t_banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_beritas`
--
ALTER TABLE `t_beritas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `t_beritas_FKIndex1` (`id_kategoriberitas`);

--
-- Indexes for table `t_dukuh`
--
ALTER TABLE `t_dukuh`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_ibus`
--
ALTER TABLE `t_ibus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `t_ibu_FKIndex1` (`t_pekerjaan_id`),
  ADD KEY `t_ibus_ibfk_2` (`form_kelahirans_id`),
  ADD KEY `t_ibus_ibfk_3` (`form_kematians_id`);

--
-- Indexes for table `t_jabatans`
--
ALTER TABLE `t_jabatans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_jeniskepindahans`
--
ALTER TABLE `t_jeniskepindahans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kategoriberitas`
--
ALTER TABLE `t_kategoriberitas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_keluargas`
--
ALTER TABLE `t_keluargas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `t_keluargas_FKIndex1` (`form_perpindahans_id`);

--
-- Indexes for table `t_kelurahans`
--
ALTER TABLE `t_kelurahans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_keperluan`
--
ALTER TABLE `t_keperluan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_pekerjaan`
--
ALTER TABLE `t_pekerjaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_perangkatdesas`
--
ALTER TABLE `t_perangkatdesas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `t_perangkatdesas__FKIndex1` (`jabatans_id`);

--
-- Indexes for table `t_perangkatdesastrukturs`
--
ALTER TABLE `t_perangkatdesastrukturs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_rt`
--
ALTER TABLE `t_rt`
  ADD PRIMARY KEY (`id`),
  ADD KEY `t_rt_FKIndex1` (`t_dukuh_id`);

--
-- Indexes for table `t_saksi1s`
--
ALTER TABLE `t_saksi1s`
  ADD PRIMARY KEY (`id`),
  ADD KEY `t_saksi1_FKIndex1` (`t_dukuh_id`),
  ADD KEY `t_saksi1_FKIndex2` (`t_pekerjaan_id`),
  ADD KEY `t_saksi1s_ibfk_3` (`form_kelahirans_id`),
  ADD KEY `t_saksi1s_ibfk_4` (`form_kematians_id`);

--
-- Indexes for table `t_saksi2s`
--
ALTER TABLE `t_saksi2s`
  ADD PRIMARY KEY (`id`),
  ADD KEY `t_saksi2_FKIndex1` (`t_dukuh_id`),
  ADD KEY `t_saksi2_FKIndex2` (`t_pekerjaan_id`),
  ADD KEY `t_saksi2s_ibfk_3` (`form_kelahirans_id`),
  ADD KEY `t_saksi2s_ibfk_4` (`form_kematians_id`);

--
-- Indexes for table `t_sambutans`
--
ALTER TABLE `t_sambutans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_surat`
--
ALTER TABLE `t_surat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `t_surat_FKIndex1` (`t_users_id`),
  ADD KEY `t_surat_FKIndex2` (`t_keperluan_id`);

--
-- Indexes for table `t_taglines`
--
ALTER TABLE `t_taglines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_tinggals`
--
ALTER TABLE `t_tinggals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `t_tinggals_FKIndex1` (`form_perpindahans_id`);

--
-- Indexes for table `t_tupoksis`
--
ALTER TABLE `t_tupoksis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_umkm`
--
ALTER TABLE `t_umkm`
  ADD PRIMARY KEY (`id`),
  ADD KEY `t_umkm_FKIndex1` (`t_users_id`);

--
-- Indexes for table `t_users`
--
ALTER TABLE `t_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `t_users_FKIndex1` (`t_pekerjaan_id`),
  ADD KEY `t_users_FKIndex2` (`t_agama_id`),
  ADD KEY `t_users_FKIndex3` (`t_dukuh_id`);

--
-- Indexes for table `t_visimisis`
--
ALTER TABLE `t_visimisis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `form_kelahirans`
--
ALTER TABLE `form_kelahirans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `form_kematians`
--
ALTER TABLE `form_kematians`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `form_perpindahans`
--
ALTER TABLE `form_perpindahans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `t_agama`
--
ALTER TABLE `t_agama`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `t_ayahs`
--
ALTER TABLE `t_ayahs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `t_banners`
--
ALTER TABLE `t_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_beritas`
--
ALTER TABLE `t_beritas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `t_dukuh`
--
ALTER TABLE `t_dukuh`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `t_ibus`
--
ALTER TABLE `t_ibus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `t_jabatans`
--
ALTER TABLE `t_jabatans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `t_jeniskepindahans`
--
ALTER TABLE `t_jeniskepindahans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_kategoriberitas`
--
ALTER TABLE `t_kategoriberitas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `t_keluargas`
--
ALTER TABLE `t_keluargas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=181;

--
-- AUTO_INCREMENT for table `t_kelurahans`
--
ALTER TABLE `t_kelurahans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_keperluan`
--
ALTER TABLE `t_keperluan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `t_pekerjaan`
--
ALTER TABLE `t_pekerjaan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `t_perangkatdesas`
--
ALTER TABLE `t_perangkatdesas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `t_perangkatdesastrukturs`
--
ALTER TABLE `t_perangkatdesastrukturs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_rt`
--
ALTER TABLE `t_rt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `t_saksi1s`
--
ALTER TABLE `t_saksi1s`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `t_saksi2s`
--
ALTER TABLE `t_saksi2s`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `t_sambutans`
--
ALTER TABLE `t_sambutans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_surat`
--
ALTER TABLE `t_surat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=195;

--
-- AUTO_INCREMENT for table `t_taglines`
--
ALTER TABLE `t_taglines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `t_tinggals`
--
ALTER TABLE `t_tinggals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `t_tupoksis`
--
ALTER TABLE `t_tupoksis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `t_umkm`
--
ALTER TABLE `t_umkm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `t_users`
--
ALTER TABLE `t_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `t_visimisis`
--
ALTER TABLE `t_visimisis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `form_kelahirans`
--
ALTER TABLE `form_kelahirans`
  ADD CONSTRAINT `t_form_kelahirans_FKIndex5` FOREIGN KEY (`t_surat_id`) REFERENCES `t_surat` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `form_kematians`
--
ALTER TABLE `form_kematians`
  ADD CONSTRAINT `form_kematians_ibfk_1` FOREIGN KEY (`t_pekerjaan_id`) REFERENCES `t_pekerjaan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `form_kematians_ibfk_2` FOREIGN KEY (`t_dukuh_id`) REFERENCES `t_dukuh` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `form_kematians_ibfk_3` FOREIGN KEY (`t_agama_id`) REFERENCES `t_agama` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `form_kematians_ibfk_4` FOREIGN KEY (`t_surat_id`) REFERENCES `t_surat` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `form_perpindahans`
--
ALTER TABLE `form_perpindahans`
  ADD CONSTRAINT `form_perpindahans_ibfk_1` FOREIGN KEY (`t_jeniskepindahans_id`) REFERENCES `t_jeniskepindahans` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `form_perpindahans_ibfk_3` FOREIGN KEY (`t_surat_id`) REFERENCES `t_surat` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `form_perpindahans_ibfk_4` FOREIGN KEY (`t_dukuh_id`) REFERENCES `t_dukuh` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `t_ayahs`
--
ALTER TABLE `t_ayahs`
  ADD CONSTRAINT `t_ayahs_ibfk_1` FOREIGN KEY (`t_pekerjaan_id`) REFERENCES `t_pekerjaan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_ayahs_ibfk_2` FOREIGN KEY (`form_kelahirans_id`) REFERENCES `form_kelahirans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_ayahs_ibfk_3` FOREIGN KEY (`form_kematians_id`) REFERENCES `form_kematians` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_beritas`
--
ALTER TABLE `t_beritas`
  ADD CONSTRAINT `t_beritas_FKIndex1` FOREIGN KEY (`id_kategoriberitas`) REFERENCES `t_kategoriberitas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_ibus`
--
ALTER TABLE `t_ibus`
  ADD CONSTRAINT `t_ibus_ibfk_1` FOREIGN KEY (`t_pekerjaan_id`) REFERENCES `t_pekerjaan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `t_ibus_ibfk_2` FOREIGN KEY (`form_kelahirans_id`) REFERENCES `form_kelahirans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_ibus_ibfk_3` FOREIGN KEY (`form_kematians_id`) REFERENCES `form_kematians` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_keluargas`
--
ALTER TABLE `t_keluargas`
  ADD CONSTRAINT `t_keluargas_FKIndex1` FOREIGN KEY (`form_perpindahans_id`) REFERENCES `form_perpindahans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_perangkatdesas`
--
ALTER TABLE `t_perangkatdesas`
  ADD CONSTRAINT `t_perangkatdesas__FKIndex1` FOREIGN KEY (`jabatans_id`) REFERENCES `t_jabatans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_rt`
--
ALTER TABLE `t_rt`
  ADD CONSTRAINT `t_rt_ibfk_1` FOREIGN KEY (`t_dukuh_id`) REFERENCES `t_dukuh` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `t_saksi1s`
--
ALTER TABLE `t_saksi1s`
  ADD CONSTRAINT `t_saksi1s_ibfk_1` FOREIGN KEY (`t_dukuh_id`) REFERENCES `t_dukuh` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `t_saksi1s_ibfk_2` FOREIGN KEY (`t_pekerjaan_id`) REFERENCES `t_pekerjaan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `t_saksi1s_ibfk_3` FOREIGN KEY (`form_kelahirans_id`) REFERENCES `form_kelahirans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_saksi1s_ibfk_4` FOREIGN KEY (`form_kematians_id`) REFERENCES `form_kematians` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_saksi2s`
--
ALTER TABLE `t_saksi2s`
  ADD CONSTRAINT `t_saksi2s_ibfk_1` FOREIGN KEY (`t_dukuh_id`) REFERENCES `t_dukuh` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_saksi2s_ibfk_2` FOREIGN KEY (`t_pekerjaan_id`) REFERENCES `t_pekerjaan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_saksi2s_ibfk_3` FOREIGN KEY (`form_kelahirans_id`) REFERENCES `form_kelahirans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_saksi2s_ibfk_4` FOREIGN KEY (`form_kematians_id`) REFERENCES `form_kematians` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_surat`
--
ALTER TABLE `t_surat`
  ADD CONSTRAINT `t_surat_ibfk_1` FOREIGN KEY (`t_users_id`) REFERENCES `t_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_surat_ibfk_2` FOREIGN KEY (`t_keperluan_id`) REFERENCES `t_keperluan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_tinggals`
--
ALTER TABLE `t_tinggals`
  ADD CONSTRAINT `t_tinggals_FKIndex1` FOREIGN KEY (`form_perpindahans_id`) REFERENCES `form_perpindahans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_umkm`
--
ALTER TABLE `t_umkm`
  ADD CONSTRAINT `t_umkm_ibfk_1` FOREIGN KEY (`t_users_id`) REFERENCES `t_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_users`
--
ALTER TABLE `t_users`
  ADD CONSTRAINT `t_users_ibfk_1` FOREIGN KEY (`t_pekerjaan_id`) REFERENCES `t_pekerjaan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `t_users_ibfk_2` FOREIGN KEY (`t_agama_id`) REFERENCES `t_agama` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `t_users_ibfk_3` FOREIGN KEY (`t_dukuh_id`) REFERENCES `t_dukuh` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
